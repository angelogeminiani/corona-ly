#corona-ly#
***Application library and utilities for Corona SDK and Lua***

* Introduction
* How to Use in Your Projects

##Introduction##
"corona-ly" is little library with some language extensions mutuated from some famous javascript libraries.
The idea is to build better Lua applications using some language extensions and Object Oriented programming features (inheritance, polymorphism and encapsulation).

##How to Use in Your Projects##
Just copy "ly" folder in your project folder at the same level of *main.lua* file.
Once copied "ly" folder you can start using corona-ly.

**Example**:

```
#!lua

locale ly = require('ly.ly') --get ly library
locale trimmed_string = ly.str.trim(' this is a string to trim ') --sample string utility function
```
--

----------------------------------------------------------------------------------
-- export
--------------------------------------------------------------------------------
local composer = require( "composer" )
local widget = require("widget")

local ly = require('ly.ly')
local app = require("ly.lyApp")

local NavClass = require("ly.ui.elements.navbar.NavBar")
--local NavClass = require("ly.widgets.lyNavBar")

local EditClass = require("ly.ui.inputs.edit.Edit")
local SwitchClass = require("ly.ui.inputs.switch.Switch")
local IconSwitchClass = require("ly.ui.inputs.iconswitch.IconSwitch")

local ButtonClass = require("ly.ui.elements.button.Button")
local IconButtonClass = require("ly.ui.elements.button.IconButton")
local TextButtonClass = require("ly.ui.elements.button.TextButton")
local ObjectId = require('ly.lang.ObjectId')

local I18N_TEST_EDIT = app.i18n:get('TEST_EDIT')

--------------------------------------------------------------------------------
-- private
--------------------------------------------------------------------------------

local scene = composer.newScene()

local _ui_navbar
local _ui_edit, _ui_edit2, _ui_switch, _ui_switch2, _ui_button1, _ui_button2, _ui_button3
local myText1, myText2

local _count = 0

-- widget.setTheme(app.gui.theme.name)
-- physics.setDrawMode( "hybrid" )

local function mainLoop()
    
end

local function gotoBack(event)
    if(event.phase=='ended') then
        app.scene.back()  
    end
end

local function onTextBeginChange(event)
    print( "Begin editing", event.target.text )
end

local function onTextChanging(event)
    print(
    'oldText: '..tostring(event.oldValue), 
    'newCharacters: '..event.newCharacters, 
    'startPosition: '..tostring(event.startPosition), 
    'text: '..event.text
    )
end

local function onTextChanged(event)
    -- do something with defaulField's text
    print( "Final Text: ", event.value)
    _ui_navbar:setLabel( event.value )
    --myMap:requestLocation( event.target.text, mapLocationHandler )
    native.setKeyboardFocus( nil )
end

local function initNavBar(sceneGroup)
    
    local leftButton = {
        onEvent = gotoBack,
        width = 59,
        height = 32,
        defaultFile = "test/images/backbutton7_white.png",
        overFile = "test/images/backbutton7_white.png"
    }
    
    _ui_navbar = NavClass:new({
        title = I18N_TEST_EDIT,
        backgroundColor = { 0.96, 0.62, 0.34 },
        titleColor = {1, 1, 1},
        font = "HelveticaNeue",
        leftButton = leftButton
    })
    sceneGroup:insert(_ui_navbar:getView())
    print(_ui_navbar.width,_ui_navbar.height)
    
    --print(navBar:toString())
    --sceneGroup:remove(navBar:getView())
end

local function initScene(sceneGroup)
    local id1 = ObjectId:new()
    local id2 = ObjectId:new()
    
    --Variable for top alignment axis
    local topAlignAxis = 80
    
    -- Create first multi-line text object
    local options1 = 
    {
        text = id1:getValue(),
        x = 90,
        width = 120,     --required for multi-line and alignment
        font = native.systemFont,
        fontSize = 18
    }
    myText1 = display.newText( options1 )
    myText1:setFillColor( 1, 1, 1 )
    
    -- Set anchor Y on object to 0 (top)
    myText1.anchorY = 0
    -- Align object to top alignment axis
    myText1.y = topAlignAxis
    
    
    -- Create second multi-line text object
    local options2 = 
    {
        text = id2:getValue(),--.." The quick brown fox jumped over the lazy dog, then jumped back again.",
        x = 230,
        width = 120,     --required for multi-line and alignment
        font = native.systemFont,
        fontSize = 18
    }
    myText2 = display.newText( options2 )
    myText2:setFillColor( 0.6, 0.4, 0.8 )
    
    -- Set anchor Y on object to 0 (top)
    myText2.anchorY = 0
    -- Align object to top alignment axis
    myText2.y = topAlignAxis
    
    sceneGroup:insert(myText1)
    sceneGroup:insert(myText2)
    
    _ui_edit = EditClass:new({
        width = display.contentWidth-10,
        height = 30,
        --cornerRadius = 6,
        strokeWidth = 0,
        value = 'sample text',
        fontSize = 14,
        placeholder = _ui_navbar:getLabel(),
        font = "HelveticaNeue-Light",
        labelFont = "HelveticaNeue",
        labelFontSize = 14,
        labelWidth = 40,
        --listener = _textFieldHandler,
        label = 'Label',
        isSecure = false
    })
    _ui_edit:addEventListener(_ui_edit.onBeginChange, onTextBeginChange)
    _ui_edit:addEventListener(_ui_edit.onChanging, onTextChanging)
    _ui_edit:addEventListener(_ui_edit.onChanged, onTextChanged)
    _ui_edit:setPos( 5,200)
    sceneGroup:insert(_ui_edit.view)
    
    _ui_edit2 = EditClass:new({
        width = display.contentWidth-10,
        strokeWidth = 0,
        height = 30,
        value = 'sample password',
        fontSize = 14,
        placeholder = 'password here',
        font = "HelveticaNeue-Light",
        labelFont = "HelveticaNeue",
        labelFontSize = 14,
        labelWidth = 70,
        label = 'Password',
        isSecure = true
    })
    _ui_edit2:setPos( 5, _ui_edit.view.y + 50)
    sceneGroup:insert(_ui_edit2.view)
    
    --SWITCH
    _ui_switch = SwitchClass:new({
        id="enable_audio", model=app.settings,
        label='audio - transate this',
        x=_ui_edit2.view.x,
        y=_ui_edit2.view.y + 30,
        height =20
    })
    sceneGroup:insert(_ui_switch.view)
    
    --SWITCH
    _ui_switch2 = IconSwitchClass:new({
        id="star", model=app.settings,
        label='STAR',
        --labelFontSize = 12,
        x=_ui_edit2.view.x + 180,
        y=_ui_edit2.view.y + 30
    })
    sceneGroup:insert(_ui_switch2.view)
    
    --simple image
    local img = display.newImage('assets/app-icon/output/Icon.png')
    sceneGroup:insert(img)
    
    --BUTTON
    _ui_button1 = ButtonClass:new({
        width = display.contentWidth-30,
        height = 60,
        x = display.contentCenterX,
        y = _ui_switch.view.y + 100,
        cornerRadius = 6,
        backgroundColor = {1,0,0},
        strokeWidth = 3,
        strokeColor = {0.5, 0.5, 0.5},
        caption = "Click Me Baby!",
        fontSize = 18,
        font = "HelveticaNeue",
        fontColor = {1,1,1},
        iconFile = 'assets/app-icon/output/Icon.png'
    })
    -- Hide the native part of this until we need to show it on the screen.
    
    --_ui_button1:setPos( display.contentCenterX, _ui_switch.view.y + 100)
    sceneGroup:insert(_ui_button1.view)
    
    _ui_button1:addEventListener('pressed', function(event) 
        print(event.name, event.target._id)
        --disable/enable switch
        _ui_switch:setEnabled(not _ui_switch:isEnabled())
        
        _count = _count+1
        _ui_button1:setNotification(_count)
        _ui_button2:setNotification(_count)
        _ui_button3:setNotification(_count)
        
        ly.animate.pulse(_ui_button2, nil, {scale=2})
        ly.animate.pulse(img, nil, {})
        ly.animate.pulse(_ui_button1, nil, {scale=0.2})
        
        --enable activity indicator
        ly.dialog.waiting(true)
        ly.delay(function() ly.dialog.waiting(false) end, 2000)
    end)
    
    --ICON BUTTON
    _ui_button2 = IconButtonClass:new({
        width = 48,
        x = display.contentCenterX,
        y = _ui_button1.view.y + 100,
        text = 'ICON',
    })
    _ui_button2:setPos( display.contentCenterX, _ui_button1.view.y + 100)
    sceneGroup:insert(_ui_button2.view)
    
    _ui_button3 = TextButtonClass:new({
        height=48, width = 100,
        x = display.contentCenterX,
        y = _ui_button1.view.y + 100,
        text = 'TEXT',
        iconFile = 'ly/ui/elements/button/iconbutton.png'
    })
    _ui_button3:setPos( display.contentCenterX, _ui_button2.view.y + 100)
    sceneGroup:insert(_ui_button3.view)
    
    img.x = display.contentCenterX
    img.y = _ui_button3.view.y + 50
end

--------------------------------------------------------------------------------
-- handlers impl
--------------------------------------------------------------------------------

function scene:create( event )
    local sceneGroup = self.view
    
    local background = display.newRect(0, 0, display.contentWidth, display.contentHeight)
    background:setFillColor( 0.90, 0.90, 0.90 )
    background.x = display.contentWidth / 2
    background.y = display.contentHeight / 2
    
    sceneGroup:insert(background)
    
    initNavBar(sceneGroup)
    
    -- scrollview
    if (not sceneGroup._scroll) then
        sceneGroup._scroll = widget.newScrollView( {
            horizontalScrollDisabled = true,
            left=0, top = _ui_navbar.height,
            width = ly.screen.width,
            height = ly.screen.height - 0,
            hideBackground = true
        })
        sceneGroup:insert(sceneGroup._scroll) 
    end
end

function scene:show( event )
    local sceneGroup = self.view
    
    local phase = event.phase
    
    if phase == "will" then
        
        
        
        initScene(sceneGroup._scroll) 
        
        sceneGroup._scroll:setScrollHeight(900)
        
        -- Called when the scene is still off screen and is about to move on screen
    elseif phase == "did" then
        
        local sceneGroup = scene.view
        
        -- Called when the scene is now on screen
        -- 
        -- INSERT code here to make the scene come alive
        -- e.g. start timers, begin animation, play audio, etc.
        --physics.start()
        
        
        Runtime:addEventListener("enterFrame", mainLoop)
    end
end

function scene:hide( event )
    local sceneGroup = self.view
    
    local phase = event.phase
    
    if event.phase == "will" then
        -- Called when the scene is on screen and is about to move off screen
        
        -- remove the addressField since it contains a native object.
        
        --sceneGroup:remove(_ui_edit.view) --addressField:removeSelf()
        
        
        
        sceneGroup:remove(myText1)
        sceneGroup:remove(myText2)
        myText1 = nil
        myText2 = nil
        
        _ui_edit:free()
        _ui_edit = nil
        
        _ui_button1:free()
        _ui_button1=nil
        
        _ui_switch:free()
        _ui_switch = nil
        
        -- INSERT code here to pause the scene
        -- e.g. stop timers, stop animation, unload sounds, etc.)
        
        Runtime:removeEventListener("enterFrame", mainLoop)
        
    elseif phase == "did" then
        -- Called when the scene is now off screen
    end	
end

function scene:destroy( event )
    
    -- Called prior to the removal of scene's "view" (sceneGroup)
    -- 
    -- INSERT code here to cleanup the scene
    -- e.g. remove display objects, remove touch listeners, save state, etc.
    local sceneGroup = self.view
    
    if (_ui_navbar) then
        _ui_navbar:free()
        _ui_navbar = nil
    end
end

--------------------------------------------------------------------------------
-- handlers
--------------------------------------------------------------------------------

scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

--------------------------------------------------------------------------------
-- export
--------------------------------------------------------------------------------

return scene


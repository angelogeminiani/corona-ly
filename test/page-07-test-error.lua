--------------------------------------------------------------------------------
-- import
--------------------------------------------------------------------------------

local composer = require( 'composer' )

local ly = require('ly.ly')
local app = require('ly.lyApp')
local NavClass = require('ly.ui.elements.navbar.NavBar')

local ButtonClass = require('ly.ui.elements.button.Button')
local ActivityIndicator = require('ly.ui.screens.activityindicator.ActivityIndicator')

--------------------------------------------------------------------------------
-- fields
--------------------------------------------------------------------------------

local scene = composer.newScene()

--------------------------------------------------------------------------------
-- private
--------------------------------------------------------------------------------

local function _mainLoop()
    
end

local function _gotoBack(event)
    if(event.phase=='ended') then
        app.scene.back()  
    end
end

local function _initNavBar(sceneGroup)
    
    local leftButton = {
        onEvent = _gotoBack,
        width = 59,
        height = 32,
        defaultFile = "test/images/backbutton7_white.png",
        overFile = "test/images/backbutton7_white.png"
    }
    
    sceneGroup._ui_navbar = NavClass:new({
        title = "Test Edit",
        backgroundColor = { 0.96, 0.62, 0.34 },
        titleColor = {1, 1, 1},
        font = "HelveticaNeue",
        leftButton = leftButton
    })
    sceneGroup:insert(sceneGroup._ui_navbar.view)
    print(sceneGroup._ui_navbar.width, sceneGroup._ui_navbar.height)
    
    --print(navBar:toString())
    --sceneGroup:remove(navBar:getView())
end

--------------------------------------------------------------------------------
-- init
--------------------------------------------------------------------------------


local function _initScene(sceneGroup)
    
    sceneGroup._btn_1 = ButtonClass:new({
        width = display.contentWidth-30,
        height = 60,
        cornerRadius = 6,
        backgroundColor = {1,0,0},
        strokeWidth = 3,
        strokeColor = {0.5, 0.5, 0.5},
        caption = "Click Me for Error",
        fontSize = 18,
        font = "HelveticaNeue",
        fontColor = {1,1,1}
    })
    sceneGroup._btn_1:setPos( display.contentCenterX, sceneGroup._btn_1.view.y + 100)
    sceneGroup:insert(sceneGroup._btn_1.view)
    
    sceneGroup._btn_1:addEventListener('tap', function(event) 
        app.error.overlay({})
        ly.delay(function() 
            app.error.overlay({message='Error 2'})
        end, 100)
        ly.delay(function() 
            app.error.overlay({error='Error 3'})
        end, 2000)
    end)
    
    sceneGroup._btn_2 = ButtonClass:new({
        width = display.contentWidth-30,
        height = 60,
        cornerRadius = 6,
        backgroundColor = {1,0,0},
        strokeWidth = 3,
        strokeColor = {0.5, 0.5, 0.5},
        caption = "Click Me for Loading...",
        fontSize = 18,
        font = "HelveticaNeue",
        fontColor = {1,1,1}
    })
    sceneGroup._btn_2:setPos( display.contentCenterX, sceneGroup._btn_1.view.y + 100)
    sceneGroup:insert(sceneGroup._btn_2.view)
    
    sceneGroup._btn_2:addEventListener('tap', function(event) 
        native.setActivityIndicator(true)
        ly.delay(function() 
            native.setActivityIndicator(false)
        end, 1000)
    end)
    
    sceneGroup._btn_3 = ButtonClass:new({
        width = display.contentWidth-30,
        height = 60,
        cornerRadius = 6,
        backgroundColor = {1,0,0},
        strokeWidth = 3,
        strokeColor = {0.5, 0.5, 0.5},
        caption = "Loading... with timeout",
        fontSize = 18,
        font = "HelveticaNeue",
        fontColor = {1,1,1}
    })
    sceneGroup._btn_3:setPos( display.contentCenterX, sceneGroup._btn_2.view.y + 100)
    sceneGroup:insert(sceneGroup._btn_3.view)
    
    sceneGroup._btn_3:addEventListener('tap', function(event) 
        ActivityIndicator.setOptions({timeout=1000})
        native.setActivityIndicator(true)
        ly.delay(function() 
            native.setActivityIndicator(false)
        end, 5000)
    end)
end

--------------------------------------------------------------------------------
-- handlers impl
--------------------------------------------------------------------------------

function scene:create( event )
    local sceneGroup = self.view
    
    local background = display.newRect(0, 0, display.contentWidth, display.contentHeight)
    background:setFillColor( 0.90, 0.90, 0.90 )
    background.x = display.contentWidth / 2
    background.y = display.contentHeight / 2
    
    sceneGroup:insert(background)
    
end

function scene:show( event )
    local sceneGroup = self.view
    
    local phase = event.phase
    
    if phase == "will" then
        
        _initNavBar(sceneGroup)
        _initScene(sceneGroup) 
        
        -- Called when the scene is still off screen and is about to move on screen
    elseif phase == "did" then
        
        local sceneGroup = scene.view
        
        -- Called when the scene is now on screen
        -- 
        -- INSERT code here to make the scene come alive
        -- e.g. start timers, begin animation, play audio, etc.
        --physics.start()
        
        
        Runtime:addEventListener("enterFrame", _mainLoop)
    end
end

function scene:hide( event )
    local sceneGroup = self.view
    
    local phase = event.phase
    
    if event.phase == "will" then
        -- Called when the scene is on screen and is about to move off screen
        
        -- remove the addressField since it contains a native object.
        
        --sceneGroup:remove(_ui_edit.view) --addressField:removeSelf()
        
        if (sceneGroup._ui_progress) then
            sceneGroup._ui_progress:removeSelf()
            sceneGroup._ui_progress = nil
        end
        
        if (sceneGroup._ui_navbar) then
            sceneGroup._ui_navbar:removeSelf()
            sceneGroup._ui_navbar = nil
        end
        
        
        -- INSERT code here to pause the scene
        -- e.g. stop timers, stop animation, unload sounds, etc.)
        
        Runtime:removeEventListener("enterFrame", _mainLoop)
        
    elseif phase == "did" then
        -- Called when the scene is now off screen
    end	
end

function scene:destroy( event )
    
    -- Called prior to the removal of scene's "view" (sceneGroup)
    -- 
    -- INSERT code here to cleanup the scene
    -- e.g. remove display objects, remove touch listeners, save state, etc.
    local sceneGroup = self.view
    
end

--------------------------------------------------------------------------------
-- handlers
--------------------------------------------------------------------------------

scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

--------------------------------------------------------------------------------
-- export
--------------------------------------------------------------------------------

return scene


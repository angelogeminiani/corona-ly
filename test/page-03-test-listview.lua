
----------------------------------------------------------------------------------
-- export
--------------------------------------------------------------------------------
local composer = require( "composer" )
local widget = require("widget")

local app = require("ly.lyApp")
local ly = require("ly.ly")
local lySys = require("ly.utils.lySys")
local NavClass = require("ly.ui.elements.navbar.NavBar")
--local NavClass = require("ly.widgets.lyNavBar")

local ListViewClass = require("ly.ui.elements.listview.ListView")

--------------------------------------------------------------------------------
-- private
--------------------------------------------------------------------------------

local scene = composer.newScene()
local navBar

local listView
local items = {}

local feedURL = 'http://gdata.youtube.com/feeds/mobile/users/CoronaLabs/uploads?max-results=20&alt=rss&orderby=published&format=1'

-- widget.setTheme(app.gui.theme.name)
-- physics.setDrawMode( "hybrid" )

local function mainLoop()
    
end

local function gotoBack()
    app.scene.back()
end

local function initNavBar(sceneGroup)
    
    if(navBar)then
        return 
    end
    
    local leftButton = {
        onEvent = gotoBack,
        width = 59,
        height = 32,
        defaultFile = "test/images/backbutton7_white.png",
        overFile = "test/images/backbutton7_white.png"
    }
    
    navBar = NavClass:new({
        title = "Test ListView",
        backgroundColor = { 0.96, 0.62, 0.34 },
        titleColor = {1, 1, 1},
        font = "HelveticaNeue",
        leftButton = leftButton
    })
    sceneGroup:insert(navBar:getView())
    
end

--
-- this function gets called when we tap on a row.
--
local onRowTouch = function( event )
    if event.phase == "release" then
        
        local index = event.row.index
        print(index)
    end
    return true
end

local onNeedReload = function( event )
    print('NEED RELOAD')
    return true
end

local onTopReached = function(event)
    print('REACHED TOP LIMIT')
    return true 
end

local onBottomReached = function(event)
    print('REACHED BOTTOM LIMIT')
    if(ly.isNotNull(listView)) then
        listView:toggleBottomSpinner(true)
        ly.delay(function() 
            listView:toggleBottomSpinner(false)
        end, 2000)
    else
        print('RAISED EVENT AFTED COMPONENT DESTRUCTION!!')
    end
    
    return true 
end

-- 
-- This function is used to draw each row of the tableView
--

local function onRowRender(event)
    
    --
    -- set up the variables we need that are passed via the event table
    --
    local row = event.row
    local item = event.row.params.item
    local rowHeight = event.row.params.rowHeight
    local id = row.index
    
    --
    -- boundry check to make sure we are tnot trying to access a story that
    -- doesnt exist.
    --
    if id > #items then return true end
    
    print(id, "row render "..item.name)
    
    row.height = rowHeight
    
    row.bg = display.newRect(0, 0, display.contentWidth, rowHeight)
    row.bg.anchorX = 0
    row.bg.anchorY = 0
    row.bg:setFillColor( 1, 1, 1 )
    
    row:insert(row.bg)
    
    --
    -- If you want an icon on the left side of the table view, define it here
    -- it uses the table above to get all the information we need
    --
    row.icon = display.newImageRect(app.theme.icons, 12 , 40, 40 )
    row.icon.x = 20
    row.icon.y = row.height / 2
    row:insert(row.icon)
    
    --
    -- Now create the first line of text in the table view with the headline
    -- of the story item.
    --
    local name = item.name
    if string.len(name) > 26 then
        name = string.sub(item.title, 1, 26) .. "..."
    end
    row.title = display.newText( name, 12, 0, app.theme.fontBold, 18 )
    row.title.anchorX = 0
    row.title.anchorY = 0.5
    row.title:setFillColor( 0 )
    
    row.title.y = 22
    row.title.x = 42
    
    --
    -- show the publish time in grey below the headline
    --
    local description = item.description
    row.subtitle = display.newText( description, 12, 0, app.theme.font, 14)
    row.subtitle.anchorX = 0
    row.subtitle:setFillColor( 0.375, 0.375, 0.375 )
    row.subtitle.y = row.height - 18
    row.subtitle.x = 42
    
    --
    -- Add a graphical right arrow to the right side to indicate the reader
    -- should touch the row for more information
    --
    row.rightArrow = display.newImageRect(app.theme.icons, 15 , 40, 40)
    row.rightArrow.x = display.contentWidth - 20
    row.rightArrow.y = row.height / 2
    -- must insert everything into event.view:
    row:insert(row.title )
    row:insert(row.subtitle)
    row:insert(row.rightArrow)
    return true
end


local function initScene(sceneGroup)
    
    listView = ListViewClass:new({
        width = display.contentWidth,
        height = display.contentHeight - navBar.height,
        top = navBar.height,
        rowHeight = 100
    })
    
    -- add to scene
    sceneGroup:insert(listView.view)
    
    --add items
    items = {
        {name='test 1', description='description 1'},
        {name='test 2', description='description 2'},
        {name='test 3', description='description 3'},
        {name='test 4', description='description 4'},
        {name='test 5', description='description 5'},
        {name='test 6', description='description 6'},
        {name='test 7', description='description 7'},
        {name='test 8', description='description 8'},
        {name='test 9', description='description 9'},
        {name='test 1', description='description 1'},
        {name='test 2', description='description 2'},
        {name='test 3', description='description 3'},
        {name='test 4', description='description 4'},
        {name='test 5', description='description 5'},
        {name='test 6', description='description 6'},
        {name='test 7', description='description 7'},
        {name='test 8', description='description 8'},
        {name='test 9', description='description 9'},
    }
    listView:addItems(items)
    listView:addEventListener(listView.EventRowRender, onRowRender)
    listView:addEventListener(listView.EventRowTouch, onRowTouch)
    listView:addEventListener(listView.EventNeedReload, onNeedReload)
    listView:addEventListener(listView.EventTopLimitReached, onTopReached)
    listView:addEventListener(listView.EventBottomLimitReached, onBottomReached)
    listView:render()
end

local function removePanel(sceneGroup) 
    
    --sceneGroup:remove(panel:getView())
    listView = nil
end

--------------------------------------------------------------------------------
-- handlers impl
--------------------------------------------------------------------------------

function scene:create( event )
    local sceneGroup = self.view
    
    local background = display.newRect(0, 0, display.contentWidth, display.contentHeight)
    background:setFillColor( 0.90, 0.90, 0.90 )
    background.x = display.contentWidth / 2
    background.y = display.contentHeight / 2
    
    sceneGroup:insert(background)
    
    initNavBar(sceneGroup)
    
    initScene(sceneGroup) 
end

function scene:show( event )
    local sceneGroup = self.view
    
    local phase = event.phase
    
    if phase == "will" then
        
        
        
        -- Called when the scene is still off screen and is about to move on screen
    elseif phase == "did" then
        
        local sceneGroup = scene.view
        
        -- Called when the scene is now on screen
        -- 
        -- INSERT code here to make the scene come alive
        -- e.g. start timers, begin animation, play audio, etc.
        --physics.start()
        
        
        Runtime:addEventListener("enterFrame", mainLoop)
    end
end

function scene:hide( event )
    local sceneGroup = self.view
    
    local phase = event.phase
    
    if event.phase == "will" then
        -- Called when the scene is on screen and is about to move off screen
        
        -- remove panel
        removePanel(sceneGroup)
        
        sceneGroup:remove(navBar:getView())
        
        -- INSERT code here to pause the scene
        -- e.g. stop timers, stop animation, unload sounds, etc.)
        
        Runtime:removeEventListener("enterFrame", mainLoop)
        
    elseif phase == "did" then
        -- Called when the scene is now off screen
    end	
end

function scene:destroy( event )
    
    -- Called prior to the removal of scene's "view" (sceneGroup)
    -- 
    -- INSERT code here to cleanup the scene
    -- e.g. remove display objects, remove touch listeners, save state, etc.
    local sceneGroup = self.view
    
end

--------------------------------------------------------------------------------
-- handlers
--------------------------------------------------------------------------------

scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

--------------------------------------------------------------------------------
-- export
--------------------------------------------------------------------------------

return scene



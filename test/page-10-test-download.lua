--

----------------------------------------------------------------------------------
-- export
--------------------------------------------------------------------------------
local composer = require( "composer" )
local widget = require("widget")

local ly = require('ly.ly')
local app = require("ly.lyApp")

local NavClass = require("ly.ui.elements.navbar.NavBar")
--local NavClass = require("ly.widgets.lyNavBar")

local EditClass = require("ly.ui.inputs.edit.Edit")
local ButtonClass = require("ly.ui.elements.button.Button")

local FB_TEST_IMG = 'https://fbcdn-profile-a.akamaihd.net/hprofile-ak-xfa1/v/t1.0-1/c34.34.431.431/531138_4935070182558_1639141782_n.jpg?oh=82c23eadfde7341efa8580903ca2c9ae&oe=56F9DE63&__gda__=1454558697_ee38c2d6f09612db30bd425ec4f25bd0'


--------------------------------------------------------------------------------
-- private
--------------------------------------------------------------------------------

local scene = composer.newScene()

local _ui_navbar
local _ui_edit, _ui_button1

-- widget.setTheme(app.gui.theme.name)
-- physics.setDrawMode( "hybrid" )

local function mainLoop()
    
end

local function gotoBack(event)
    if(event.phase=='ended') then
        app.scene.back()  
    end
end


local function initNavBar(sceneGroup)
    
    local leftButton = {
        onEvent = gotoBack,
        width = 59,
        height = 32,
        defaultFile = "test/images/backbutton7_white.png",
        overFile = "test/images/backbutton7_white.png"
    }
    
    _ui_navbar = NavClass:new({
        title = 'Download',
        backgroundColor = { 0.96, 0.62, 0.34 },
        titleColor = {1, 1, 1},
        font = "HelveticaNeue",
        leftButton = leftButton
    })
    sceneGroup:insert(_ui_navbar:getView())
    print(_ui_navbar.width,_ui_navbar.height)
    
    --print(navBar:toString())
    --sceneGroup:remove(navBar:getView())
end

local function networkListener( sceneGroup, event )
    if ( event.isError ) then
        print( "Network error - download failed" )
    elseif ( event.phase == "began" ) then
        print( "Progress Phase: began" )
    elseif ( event.phase == "ended" ) then
        print( "Displaying response image file" )
        
        local myImage = display.newImage( event.response.filename, event.response.baseDirectory, 60, 40 )
        myImage.alpha = 0
        myImage.x = ly.screen.centerX
        myImage.y = ly.screen.centerY
        ly.graphics.scaleByHeight(myImage, 300)
        transition.to( myImage, { alpha=1.0 } )
        
        sceneGroup:insert(myImage)
    end
end



local function _download(sceneGroup, url)
    local params = {}
    params.progress = true
    
    network.download(
    url,
    "GET",
    function (event) 
        networkListener(sceneGroup, event)
    end,
    params,
    "image-downloaded.png",
    system.TemporaryDirectory
    )
    
end

local function _download2(sceneGroup, url)
    local options = {}
    options.method = 'GET'
    options.baseDir = system.DocumentsDirectory 
    options.fileName = ly.path.getName(url)
    options.params = {progress=true}
    options.flag_timestamp = ly.toBoolean(options.flag_timestamp, true)
    
    network.download(url, options.method, 
    function(event) 
        networkListener(sceneGroup, event)
    end, 
    options.params, options.fileName, options.baseDir)
end

local function initScene(sceneGroup)
    
    _ui_edit = EditClass:new({
        width = display.contentWidth-10,
        height = 30,
        --cornerRadius = 6,
        strokeWidth = 0,
        value = FB_TEST_IMG,
        fontSize = 14,
        placeholder = _ui_navbar:getLabel(),
        font = "HelveticaNeue-Light",
        labelFont = "HelveticaNeue",
        labelFontSize = 14,
        labelWidth = 40,
        label = 'Url',
        isSecure = false
    })
    _ui_edit:addEventListener(_ui_edit.onChanged, function (event) 
        
    end)
    _ui_edit:setPos( 5, 5)
    sceneGroup:insert(_ui_edit.view)
    
    --BUTTON
    _ui_button1 = ButtonClass:new({
        width = display.contentWidth-30,
        height = 25,
        cornerRadius = 3,
        backgroundColor = {1,0,0},
        strokeWidth = 1,
        strokeColor = {0.5, 0.5, 0.5},
        caption = "Download",
        fontSize = 14,
        font = "HelveticaNeue",
        fontColor = {1,1,1},
        --iconFile = 'assets/app-icon/output/Icon.png'
    })
    -- Hide the native part of this until we need to show it on the screen.
    
    _ui_button1:setPos( display.contentCenterX, _ui_edit.view.y + 60)
    sceneGroup:insert(_ui_button1.view)
    
    _ui_button1:addEventListener('tap', function(event) 
        local url = _ui_edit:getValue()
        _download2 (sceneGroup, url)
    end)
end

--------------------------------------------------------------------------------
-- handlers impl
--------------------------------------------------------------------------------

function scene:create( event )
    local sceneGroup = self.view
    
    local background = display.newRect(0, 0, display.contentWidth, display.contentHeight)
    background:setFillColor( 0.90, 0.90, 0.90 )
    background.x = display.contentWidth / 2
    background.y = display.contentHeight / 2
    
    sceneGroup:insert(background)
    
    initNavBar(sceneGroup)
    
    -- scrollview
    if (not sceneGroup._scroll) then
        sceneGroup._scroll = widget.newScrollView( {
            horizontalScrollDisabled = true,
            left=0, top = _ui_navbar.height,
            width = ly.screen.width,
            height = ly.screen.height - 0,
            hideBackground = true
        })
        sceneGroup:insert(sceneGroup._scroll) 
    end
end

function scene:show( event )
    local sceneGroup = self.view
    
    local phase = event.phase
    
    if phase == "will" then
        
        
        
        initScene(sceneGroup._scroll) 
        
        sceneGroup._scroll:setScrollHeight(900)
        
        -- Called when the scene is still off screen and is about to move on screen
    elseif phase == "did" then
        
        local sceneGroup = scene.view
        
        -- Called when the scene is now on screen
        -- 
        -- INSERT code here to make the scene come alive
        -- e.g. start timers, begin animation, play audio, etc.
        --physics.start()
        
        
        Runtime:addEventListener("enterFrame", mainLoop)
    end
end

function scene:hide( event )
    local sceneGroup = self.view
    
    local phase = event.phase
    
    if event.phase == "will" then
        -- Called when the scene is on screen and is about to move off screen
        
        -- remove the addressField since it contains a native object.
        
        --sceneGroup:remove(_ui_edit.view) --addressField:removeSelf()
        
        _ui_edit:free()
        _ui_edit = nil
        
        -- INSERT code here to pause the scene
        -- e.g. stop timers, stop animation, unload sounds, etc.)
        
        Runtime:removeEventListener("enterFrame", mainLoop)
        
    elseif phase == "did" then
        -- Called when the scene is now off screen
    end	
end

function scene:destroy( event )
    
    -- Called prior to the removal of scene's "view" (sceneGroup)
    -- 
    -- INSERT code here to cleanup the scene
    -- e.g. remove display objects, remove touch listeners, save state, etc.
    local sceneGroup = self.view
    
    if (_ui_navbar) then
        _ui_navbar:free()
        _ui_navbar = nil
    end
end

--------------------------------------------------------------------------------
-- handlers
--------------------------------------------------------------------------------

scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

--------------------------------------------------------------------------------
-- export
--------------------------------------------------------------------------------

return scene


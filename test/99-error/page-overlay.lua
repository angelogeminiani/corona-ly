--

--------------------------------------------------------------------------------
-- import
--------------------------------------------------------------------------------
local composer = require( "composer" )

local ly = require('ly.ly')
local app = require("ly.lyApp")

local ButtonClass = require('ly.ui.elements.button.Button')

--------------------------------------------------------------------------------
-- constants
--------------------------------------------------------------------------------
local scene = composer.newScene()

local I18N_RETRY = app.i18n:get('RETRY')

--------------------------------------------------------------------------------
-- HANDLERS
--------------------------------------------------------------------------------

local function _on_button_tap(self)
    -- close overlay
    app.scene.overlayOff('99.1')
    
    local params = self._params
    if(params and params.service) then
        local service = params.service
        if(ly.isFunction(service)) then
            ly.invoke(service)
        elseif(ly.isObject(service)) then
            ly.delay(function() 
                app.service.invoke(service.name, service.method, service.options, service.callback)
            end, 800)
        end
    end
end

--------------------------------------------------------------------------------
-- VIEW BUILD
--------------------------------------------------------------------------------

local function _init(self)
    local params = self._params or {}
    local err = params.err
    local sceneGroup = self.view
    
    local pos = 80
    
    --back ground round rect
    sceneGroup._background = display.newRoundedRect(0, 0, display.contentWidth-50, display.contentHeight-pos, 10)
    sceneGroup._background:setFillColor( 1, 1, 1, 0.9 )
    sceneGroup._background.x = display.contentWidth / 2
    sceneGroup._background.y = display.contentHeight / 2
    sceneGroup:insert(sceneGroup._background)
    
    pos = pos+20
    
    local lbl_height = sceneGroup._background.height-15
    local lbl_width = sceneGroup._background.width-30
    
    -- MESSAGE
    local message = I18N_RETRY or 'Error'
    if(ly.isObject(err) and err.code) then
        local code = err.code:upper()
        code = ly.str.replace(code, ' ', '_')
        message = ly.toString(app.i18n:get(code), message)
    end
    sceneGroup._lbl_message = display.newText{
        text = message,     
        x = 160,
        y = pos,
        width = lbl_width,
        height= lbl_height,
        font = app.theme.fontText, --native.systemFontBold,   
        fontSize = 25,
        align = "center"  --new alignment parameter
    }
    sceneGroup._lbl_message.anchorY = 0
    sceneGroup._lbl_message:setFillColor( 0.55, 0.55, 0.55 )
    sceneGroup:insert(sceneGroup._lbl_message)
    
    -- BUTTON
    local caption = params.caption or I18N_RETRY
    sceneGroup._button = ButtonClass:new({
        width = lbl_width,
        height = 60,
        cornerRadius = 6,
        backgroundColor = {1,1,1},
        strokeWidth = 3,
        strokeColor = {1, 1, 1},
        caption = caption,
        fontSize = 18,
        font = app.theme.fontAction,
        fontColor = {0,0,0}
    })
    sceneGroup._button:setPos( display.contentCenterX, lbl_height)
    sceneGroup:insert(sceneGroup._button.view)
    
    sceneGroup._button:addEventListener('tap', function() 
        _on_button_tap(self)
    end)
end

local function _free(self)
    local sceneGroup = self.view
    if(ly.isDisplayObject(sceneGroup)) then
        if(ly.isDisplayObject(sceneGroup._background)) then
            sceneGroup._background:removeSelf()
            sceneGroup._background = nil
        end
    end
end


--------------------------------------------------------------------------------
-- handlers impl
--------------------------------------------------------------------------------

function scene:create( event )
    local sceneGroup = self.view
    
    local background = display.newRect(0, 0, display.contentWidth, display.contentHeight)
    background:setFillColor( 0, 0, 0, 0.5 )
    background.x = display.contentWidth / 2
    background.y = display.contentHeight / 2
    
    sceneGroup:insert(background)
end

function scene:show( event )
    local sceneGroup = self.view
    
    local phase = event.phase
    
    if phase == "will" then
        
        self._parent = event.parent
        self._params = event.params
        
        _init(self) 
        
	-- Called when the scene is still off screen and is about to move on screen
    elseif phase == "did" then
        
        local sceneGroup = scene.view
        
        -- Called when the scene is now on screen
        -- 
        -- INSERT code here to make the scene come alive
        -- e.g. start timers, begin animation, play audio, etc.
        --physics.start()
        
        
        --Runtime:addEventListener("enterFrame", mainLoop)
    end
end

function scene:hide( event )
    local sceneGroup = self.view
    
    local phase = event.phase
    
    if event.phase == "will" then
        -- Called when the scene is on screen and is about to move off screen
        
        -- INSERT code here to pause the scene
        -- e.g. stop timers, stop animation, unload sounds, etc.)
        
        --Runtime:removeEventListener("enterFrame", mainLoop)
        
    elseif phase == "did" then
        -- Called when the scene is now off screen
        
    end	
end

function scene:destroy( event )
    
    -- Called prior to the removal of scene's "view" (sceneGroup)
    -- 
    -- INSERT code here to cleanup the scene
    -- e.g. remove display objects, remove touch listeners, save state, etc.
    local sceneGroup = self.view
    _free(self)
end

--------------------------------------------------------------------------------
-- handlers
--------------------------------------------------------------------------------

scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

--------------------------------------------------------------------------------
-- export
--------------------------------------------------------------------------------

return scene


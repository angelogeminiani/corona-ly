--

----------------------------------------------------------------------------------
-- export
--------------------------------------------------------------------------------
local composer = require( "composer" )
local widget = require("widget")

local ly = require("ly.ly")
local app = require("ly.lyApp")

--------------------------------------------------------------------------------
-- private
--------------------------------------------------------------------------------

local scene = composer.newScene()


-- widget.setTheme(app.gui.theme.name)
-- physics.setDrawMode( "hybrid" )

local function mainLoop()
    
end

local function changePage(event)
    local targetId = event.target.id
    local params = { effect=event.target.effect}
    app.scene.goto(targetId, params or {})
end

local function initMenu(sceneGroup)
    -- TITLE
    local menuText = display.newText( {		    
        text = ly.str.quote("Menu"),     
        x = display.contentWidth/2,
        y = 40,
        width = 150,
        height= 40,
        font = native.systemFontBold,   
        fontSize = 20,
        align = "center"  --new alignment parameter
    });
    menuText:setFillColor( 0, 0, 0 )
    sceneGroup:insert(menuText)
    
    -- scroll
    local scroll = widget.newScrollView({
        top = 70,
        left = 10,
        width = 300,
        height = display.contentHeight - 80,
        --scrollWidth = 600,
        --scrollHeight = 800,
        horizontalScrollDisabled=true
    })
    sceneGroup:insert(scroll)
    
    -- buttons
    local btn01 = widget.newButton({
        id="01",
        top=20,
        left = 20,
        labelAlign="left",
        onRelease=changePage,
        label = app.i18n:get('TEST_EDIT')
    })
    scroll:insert(btn01)
    
    local btn02 = widget.newButton({
        id="02",
        top=80,
        left=20,
        labelAlign="left",
        onRelease=changePage,
        label="Test Panel"
    })
    scroll:insert(btn02)
    
    local btn03 = widget.newButton({
        id="03",
        top=120,
        left=20,
        labelAlign="left",
        onRelease=changePage,
        label="Test ListView"
    })
    scroll:insert(btn03)
    
    local btn04 = widget.newButton({
        id="04",
        top=160,
        left=20,
        labelAlign="left",
        onRelease=changePage,
        label="Test Net"
    })
    scroll:insert(btn04)
    
    local btn05 = widget.newButton({
        id="05",
        top=200,
        left=20,
        labelAlign="left",
        onRelease=changePage,
        label="Test Progress Bar"
    })
    scroll:insert(btn05)
    
    local btn06 = widget.newButton({
        id="06",
        top=240,
        left=20,
        labelAlign="left",
        onRelease=changePage,
        label="Test Crop"
    })
    scroll:insert(btn06)
    
    local btn07 = widget.newButton({
        id="07",
        top=280,
        left=20,
        labelAlign="left",
        onRelease=changePage,
        label="Test Error"
    })
    scroll:insert(btn07)
    
    local btn08 = widget.newButton({
        id="08",
        top=320,
        left=20,
        labelAlign="left",
        onRelease=changePage,
        label="Test Web"
    })
    scroll:insert(btn08)
    
    local btn09 = widget.newButton({
        id="09",
        top=360,
        left=20,
        labelAlign="left",
        onRelease=changePage,
        label="Test Tabs"
    })
    scroll:insert(btn09)
    
    local btn10 = widget.newButton({
        id="10",
        top=400,
        left=20,
        labelAlign="left",
        onRelease=changePage,
        label="Test Download"
    })
    scroll:insert(btn10)
end



--------------------------------------------------------------------------------
-- handlers impl
--------------------------------------------------------------------------------

function scene:create( event )
    local sceneGroup = self.view
    
    local background = display.newRect(0, 0, display.contentWidth, display.contentHeight)
    background:setFillColor( 0.90, 0.90, 0.90 )
    background.x = display.contentWidth / 2
    background.y = display.contentHeight / 2
    
    sceneGroup:insert(background)
end

function scene:show( event )
    local sceneGroup = self.view
    
    local phase = event.phase
    
    if phase == "will" then
        
        
        initMenu(sceneGroup) 
        --loadstring('initMenu(sceneGroup)')()
        
	-- Called when the scene is still off screen and is about to move on screen
    elseif phase == "did" then
        
        local sceneGroup = scene.view
        
        -- Called when the scene is now on screen
        -- 
        -- INSERT code here to make the scene come alive
        -- e.g. start timers, begin animation, play audio, etc.
        --physics.start()
        
        
        Runtime:addEventListener("enterFrame", mainLoop)
    end
end

function scene:hide( event )
    local sceneGroup = self.view
    
    local phase = event.phase
    
    if event.phase == "will" then
        -- Called when the scene is on screen and is about to move off screen
        
        -- INSERT code here to pause the scene
        -- e.g. stop timers, stop animation, unload sounds, etc.)
        
        Runtime:removeEventListener("enterFrame", mainLoop)
        
    elseif phase == "did" then
        -- Called when the scene is now off screen
    end	
end

function scene:destroy( event )
    
    -- Called prior to the removal of scene's "view" (sceneGroup)
    -- 
    -- INSERT code here to cleanup the scene
    -- e.g. remove display objects, remove touch listeners, save state, etc.
    local sceneGroup = self.view
    
end

--------------------------------------------------------------------------------
-- handlers
--------------------------------------------------------------------------------

scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

--------------------------------------------------------------------------------
-- export
--------------------------------------------------------------------------------

return scene


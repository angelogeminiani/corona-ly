
----------------------------------------------------------------------------------
-- export
--------------------------------------------------------------------------------
local composer = require( "composer" )
local widget = require("widget")

local ly = require('ly.ly')
local app = require("ly.lyApp")
local lySys = require("ly.utils.lySys")
local NavClass = require("ly.ui.elements.navbar.NavBar")
--local NavClass = require("ly.widgets.lyNavBar")

local PanelClass = require("ly.ui.elements.panel.Panel")
--local PanelClass = require("ly.widgets.lyPanel")

--------------------------------------------------------------------------------
-- private
--------------------------------------------------------------------------------

local scene = composer.newScene()

-- widget.setTheme(app.gui.theme.name)
-- physics.setDrawMode( "hybrid" )

local function mainLoop()
    
end

local function gotoBack(event)
    if(event.phase=='ended') then
        app.scene.back()  
    end
    return true
end

local function buttonOnClick(sceneGroup, event)
    if (sceneGroup._panel) then
        sceneGroup._panel:toggle()
    end
end

local function _initNavBar(sceneGroup)
    
    if(sceneGroup._navBar)then
        return 
    end
    
    local leftButton = {
        onEvent = gotoBack,
        width = 59,
        height = 32,
        defaultFile = "test/images/backbutton7_white.png",
        overFile = "test/images/backbutton7_white.png"
    }
    
    sceneGroup._navBar = NavClass:new({
        title = "Test Panel",
        backgroundColor = { 0.96, 0.62, 0.34 },
        titleColor = {1, 1, 1},
        font = "HelveticaNeue",
        leftButton = leftButton
    })
    sceneGroup:insert(sceneGroup._navBar.view)
    
end

local function _initButtons(sceneGroup)
    
    if (sceneGroup._scroll) then
        return
    end
    
    -- scroll
    sceneGroup._scroll = widget.newScrollView({
        top = 70,
        left = 10,
        width = 300,
        height = display.contentHeight - 80,
        --scrollWidth = 600,
        --scrollHeight = 800,
        horizontalScrollDisabled=true
    })
    sceneGroup:insert(sceneGroup._scroll)
    
    -- buttons
    local btn01 = widget.newButton({
        id="01",
        top=20,
        left = 20,
        labelAlign="left",
        onRelease=function(event) buttonOnClick(sceneGroup, event) end,
        label="Toggle Panel"
    })
    sceneGroup._scroll:insert(btn01)
    
end

local function _initScene(sceneGroup)
    
    local function onRowTouch( event )
        local popupName = event.row.params.popupName
        local service = event.row.params.service
        
        if popupName == nil then -- the cancel button
            local myPanel = event.row.params.parent
            myPanel:hide()
            return true
        end
        
        if popupName == "mail" then
            native.showPopup( popupName, 
            {
                body = body,
                attachment = attachment,
            })
        elseif popupName == "sms" then
            native.showPopup( popupName, 
            {
                body = body,
            })
        elseif popupName == "social" then
            local isAvailable = native.canShowPopup( popupName, service )
            if isAvailable then
                native.showPopup( popupName, 
                {
                    service = service,
                    message = message,
                    image = image,
                    url = url
                })
            else
                if lySys.isSimulator then
                    native.showAlert( "Build for device", "This plugin is not supported on the Corona Simulator, please build for an iOS/Android device or the Xcode simulator", { "OK" } )
                else
                    -- Popup isn't available.. Show error message
                    native.showAlert( "Cannot send " .. service .. " message.", "Please setup your " .. service .. " account or check your network connection (on android this means that the package/app (ie Twitter) is not installed on the device)", { "OK" } )
                end
            end
        end
        
        return true
    end
    
    local function onRowRender( event )
        local row = event.row
        local id = row.index
        local params = event.row.params
        
        if row.isCategory then
            row.text = display.newText("Share", 0, 0, app.theme.font, 14)
            row.text:setFillColor( 0.67 )
            row.text.x = display.contentCenterX
            row.text.y = row.contentHeight * 0.5
            row:insert(row.text)
        else
            row.text = display.newText(params.label, 0, 0, app.theme.font, 18)
            row.text:setFillColor( 0.33, 0.5, 1.0 )
            row.text.x = display.contentCenterX
            row.text.y = row.contentHeight * 0.5
            row:insert(row.text)
        end
        
    end
    
    sceneGroup._panel = PanelClass:new({
        location = "bottom",
        width = display.contentWidth,
        height = 240,
        speed = 500,
    })
    
    sceneGroup._tableView = widget.newTableView({
        top = 0, 
        left = 0,
        width = display.contentWidth - 16, 
        height = 240, 
        hideBackground = false, 
        backgroundColor = { 0.5 },
        noLines = true,
        onRowRender = onRowRender,
        onRowTouch = onRowTouch 
    })
    sceneGroup._tableView.x = 0
    sceneGroup._tableView.y = 0
    
    sceneGroup._tableView:insertRow{
        rowHeight = 40,
        isCategory = true,
        rowColor = { 1, 1, 1 },
    }
    sceneGroup._tableView:insertRow{
        rowHeight = 40,
        isCategory = false,
        rowColor = { 1, 1, 1 },
        params = {
            popupName = "social",
            service = "facebook",
            label = "Facebook",
            message = "Some text to post to facebook",
            image = nil,  -- See the native.showPopup("social") plugin for image paramters.
            url = nil,    -- See the native.showPopup("social") plugin for url paramters.
        }
    }
    sceneGroup._tableView:insertRow{
        rowHeight = 40,
        isCategory = false,
        rowColor = { 1, 1, 1 },
        params = {
            popupName = "social",
            service = "twitter",
            label = "Twitter",
            message = "Some text to post to twitter",
            image = nil,  -- See the native.showPopup("social") plugin for image paramters.
            url = nil,    -- See the native.showPopup("social") plugin for url paramters.
        }
    }
    sceneGroup._tableView:insertRow{
        rowHeight = 40,
        isCategory = false,
        rowColor = { 1, 1, 1 },
        params = {
            popupName = "mail",
            label = "Email",
            body = "Some text to email",
            attachment = nil, -- See the native.showPopup("mail") API for attachment paramters.
        }
    }
    sceneGroup._tableView:insertRow{
        rowHeight = 40,
        isCategory = false,
        rowColor = { 1, 1, 1 },
        params = {
            popupName = "sms",
            label = "Message",
            body = "Some text to text",
        }
    }
    sceneGroup._tableView:insertRow{
        rowHeight = 40,
        isCategory = false,
        rowColor = { 1, 1, 1 },
        params = {
            label = "Cancel",
            parent = sceneGroup._panel
        }
    }
    
    -- creates panel and add tableView
    
    sceneGroup._panel:insert( sceneGroup._tableView )
    
    sceneGroup:insert(sceneGroup._panel.view)
end

local function removePanel(sceneGroup) 
    --panel:getView():remove( tableView )
    sceneGroup._tableView = nil
    
    --sceneGroup:remove(panel:getView())
    if(sceneGroup._panel)then
        sceneGroup._panel:free()
        sceneGroup._panel = nil
    end
    
end

--------------------------------------------------------------------------------
-- handlers impl
--------------------------------------------------------------------------------

function scene:create( event )
    local sceneGroup = self.view
    
    local background = display.newRect(0, 0, display.contentWidth, display.contentHeight)
    background:setFillColor( 0.90, 0.90, 0.90 )
    background.x = display.contentWidth / 2
    background.y = display.contentHeight / 2
    sceneGroup:insert(background)

end

function scene:show( event )
    local sceneGroup = self.view
    
    local phase = event.phase
    
    if phase == "will" then
        
        _initNavBar(sceneGroup)
        
        _initButtons(sceneGroup)
        
        _initScene(sceneGroup) 
        
        -- Called when the scene is still off screen and is about to move on screen
    elseif phase == "did" then
        
        local sceneGroup = scene.view
        
        -- Called when the scene is now on screen
        -- 
        -- INSERT code here to make the scene come alive
        -- e.g. start timers, begin animation, play audio, etc.
        --physics.start()
        
        
        Runtime:addEventListener("enterFrame", mainLoop)
    end
end

function scene:hide( event )
    local sceneGroup = self.view
    
    local phase = event.phase
    
    if event.phase == "will" then
        -- Called when the scene is on screen and is about to move off screen
        
        -- remove panel
        removePanel(sceneGroup)
        
        if(sceneGroup._navBar) then
            sceneGroup._navBar:free()
            sceneGroup._navBar = nil
        end
        
        if(sceneGroup._scroll) then
            sceneGroup._scroll:removeSelf()
            sceneGroup._scroll = nil
        end
        
        -- INSERT code here to pause the scene
        -- e.g. stop timers, stop animation, unload sounds, etc.)
        
        Runtime:removeEventListener("enterFrame", mainLoop)
        
    elseif phase == "did" then
        -- Called when the scene is now off screen
    end	
end

function scene:destroy( event )
    
    -- Called prior to the removal of scene's "view" (sceneGroup)
    -- 
    -- INSERT code here to cleanup the scene
    -- e.g. remove display objects, remove touch listeners, save state, etc.
    local sceneGroup = self.view
    
end

--------------------------------------------------------------------------------
-- handlers
--------------------------------------------------------------------------------

scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

--------------------------------------------------------------------------------
-- export
--------------------------------------------------------------------------------

return scene



--------------------------------------------------------------------------------
-- import
--------------------------------------------------------------------------------

local composer = require( 'composer' )
local widget = require('widget')

local ly = require('ly.ly')
local app = require('ly.lyApp')

local NavClass = require('ly.ui.elements.navbar.NavBar')

local PanelClass = require('ly.ui.elements.panel.BarPanelPicture')

--------------------------------------------------------------------------------
-- fields
--------------------------------------------------------------------------------

local scene = composer.newScene()

local IMAGE_TO_CROP = 'image_to_crop.jpg'

--------------------------------------------------------------------------------
-- private
--------------------------------------------------------------------------------

local function _mainLoop()
    
end

local function _gotoBack(event)
    if(event.phase=='ended') then
        app.scene.back()  
    end
end

local function _add_picture(self, filename, baseDir)
    
    --create image
    self.view._tmp_image = display.newImage(filename, baseDir)
    self.view._tmp_image.anchorX = 0.5
    self.view._tmp_image.anchorY=0.5
    self.view._tmp_image.x = ly.screen.width*0.5
    self.view._tmp_image.y = ly.screen.height*0.5
    self.view:insert(self.view._tmp_image)
    
    -- SCALE IMAGE TO ADJUST TO SCREEN
    local width = ly.screen.width - 6
    ly.graphics.scaleByWidth(self.view._tmp_image, width)
    
    self.view._panel:toFront()
end

local function _triggered_on_picture(self, event)
    local filename = event.filename
    local baseDir = event.baseDir
    
    if (ly.isDisplayObject(self.view._tmp_image)) then
        self.view._tmp_image:removeSelf()
        self.view._tmp_image = nil
    end
    
    ly.delay(function() 
        -- add and scale original image
        _add_picture(self, filename, baseDir)
        
        -- crop
        local display_image = self.view._tmp_image
        if(ly.isDisplayObject(display_image)) then
            
            local width = display_image.contentWidth
            ly.graphics.cropCenterImage(display_image, filename, baseDir, width, width)
            
            ly.delay(function() 
                if (ly.isDisplayObject(self.view._tmp_image)) then
                    self.view._tmp_image:removeSelf()
                    self.view._tmp_image = nil
                end
                ly.delay(function() 
                    -- replace with cropped image
                    _add_picture(self, filename, baseDir)
                end, 300)
            end, 300)
            
        end
    end, 300)
    
end

--------------------------------------------------------------------------------
-- init
--------------------------------------------------------------------------------

local function _initNavBar(sceneGroup)
    
    local leftButton = {
        onEvent = _gotoBack,
        width = 59,
        height = 32,
        defaultFile = "test/images/backbutton7_white.png",
        overFile = "test/images/backbutton7_white.png"
    }
    
    sceneGroup._ui_navbar = NavClass:new({
        title = "Test Edit",
        backgroundColor = { 0.96, 0.62, 0.34 },
        titleColor = {1, 1, 1},
        font = "HelveticaNeue",
        leftButton = leftButton
    })
    sceneGroup:insert(sceneGroup._ui_navbar.view)
    print(sceneGroup._ui_navbar.width, sceneGroup._ui_navbar.height)
    
    --print(navBar:toString())
    --sceneGroup:remove(navBar:getView())
end

local function _initPanel(self)
    local sceneGroup = self.view
    
    sceneGroup._panel = PanelClass:new({
        location = "bottom",
        width = display.contentWidth,
        height = 240,
        speed = 500,
    })
    sceneGroup:insert(sceneGroup._panel.view)
end

local function _initScene(self)
    
    local sceneGroup = self.view
    
    _initPanel(self)
    
    local function on_picture(event)
        --_gotPicture(self, event)
        _triggered_on_picture(self, event)
        return true
    end
    if(self.view._panel) then
        self.view._panel:addEventListener(PanelClass.onPicture, on_picture)
    end
end

--------------------------------------------------------------------------------
-- handlers impl
--------------------------------------------------------------------------------

function scene:create( event )
    local sceneGroup = self.view
    
    local background = display.newRect(0, 0, display.contentWidth, display.contentHeight)
    background:setFillColor( 0.90, 0.90, 0.90 )
    background.x = display.contentWidth / 2
    background.y = display.contentHeight / 2
    
    sceneGroup:insert(background)
    
end

function scene:show( event )
    local sceneGroup = self.view
    
    local phase = event.phase
    
    if phase == "will" then
        
        _initNavBar(sceneGroup)
        
        _initScene(self) 
        
        -- Called when the scene is still off screen and is about to move on screen
    elseif phase == "did" then
        
        local sceneGroup = scene.view
        
        -- Called when the scene is now on screen
        -- 
        -- INSERT code here to make the scene come alive
        -- e.g. start timers, begin animation, play audio, etc.
        --physics.start()
        
        
        Runtime:addEventListener("enterFrame", _mainLoop)
    end
end

function scene:hide( event )
    local sceneGroup = self.view
    
    local phase = event.phase
    
    if event.phase == "will" then
        -- Called when the scene is on screen and is about to move off screen
        
        -- remove the addressField since it contains a native object.
        
        --sceneGroup:remove(_ui_edit.view) --addressField:removeSelf()
        
        if (sceneGroup._panel) then
            sceneGroup._panel:removeSelf()
            sceneGroup._panel = nil
        end
        
        if (sceneGroup._ui_navbar) then
            sceneGroup._ui_navbar:removeSelf()
            sceneGroup._ui_navbar = nil
        end
        
        
        -- INSERT code here to pause the scene
        -- e.g. stop timers, stop animation, unload sounds, etc.)
        
        Runtime:removeEventListener("enterFrame", _mainLoop)
        
    elseif phase == "did" then
        -- Called when the scene is now off screen
    end	
end

function scene:destroy( event )
    
    -- Called prior to the removal of scene's "view" (sceneGroup)
    -- 
    -- INSERT code here to cleanup the scene
    -- e.g. remove display objects, remove touch listeners, save state, etc.
    local sceneGroup = self.view
    
end

--------------------------------------------------------------------------------
-- handlers
--------------------------------------------------------------------------------

scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

--------------------------------------------------------------------------------
-- export
--------------------------------------------------------------------------------

return scene


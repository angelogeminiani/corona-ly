--------------------------------------------------------------------------------
-- import
--------------------------------------------------------------------------------

local composer = require( 'composer' )

local ly = require('ly.ly')
local app = require('ly.lyApp')
local NavClass = require('ly.ui.elements.navbar.NavBar')

local ButtonClass = require('ly.ui.elements.button.Button')
local WebScreen = require('ly.ui.screens.web.WebScreen')

--------------------------------------------------------------------------------
-- fields
--------------------------------------------------------------------------------

local scene = composer.newScene()

--------------------------------------------------------------------------------
-- private
--------------------------------------------------------------------------------

local function _mainLoop()
    
end

local function _gotoBack(event)
    if(event.phase=='ended') then
        app.scene.back()  
    end
end

local function _initNavBar(sceneGroup)
    
    local leftButton = {
        onEvent = _gotoBack,
        width = 59,
        height = 32,
        defaultFile = "test/images/backbutton7_white.png",
        overFile = "test/images/backbutton7_white.png"
    }
    
    sceneGroup._ui_navbar = NavClass:new({
        title = "Web",
        backgroundColor = { 0.96, 0.62, 0.34 },
        titleColor = {1, 1, 1},
        font = "HelveticaNeue",
        leftButton = leftButton
    })
    sceneGroup:insert(sceneGroup._ui_navbar.view)
    print(sceneGroup._ui_navbar.width, sceneGroup._ui_navbar.height)
    
    --print(navBar:toString())
    --sceneGroup:remove(navBar:getView())
end

--------------------------------------------------------------------------------
-- init
--------------------------------------------------------------------------------


local function _initScene(sceneGroup)
    
    sceneGroup._web = WebScreen:new({
        --positionate
        width = display.contentWidth,
        height = display.contentHeight - sceneGroup._ui_navbar.height,
        y = display.contentHeight*0.5 + sceneGroup._ui_navbar.height*0.5,
        x = ly.screen.centerX,
        --navigate
        url = 'http://www.funnygain.com'
    })
    sceneGroup:insert(sceneGroup._web.view)
    
end

--------------------------------------------------------------------------------
-- handlers impl
--------------------------------------------------------------------------------

function scene:create( event )
    local sceneGroup = self.view
    
    local background = display.newRect(0, 0, display.contentWidth, display.contentHeight)
    background:setFillColor( 0.90, 0.90, 0.90 )
    background.x = display.contentWidth / 2
    background.y = display.contentHeight / 2
    
    sceneGroup:insert(background)
    
end

function scene:show( event )
    local sceneGroup = self.view
    
    local phase = event.phase
    
    if phase == "will" then
        
        _initNavBar(sceneGroup)
        _initScene(sceneGroup) 
        
        -- Called when the scene is still off screen and is about to move on screen
    elseif phase == "did" then
        
        local sceneGroup = scene.view
        
        -- Called when the scene is now on screen
        -- 
        -- INSERT code here to make the scene come alive
        -- e.g. start timers, begin animation, play audio, etc.
        --physics.start()
        
        
        Runtime:addEventListener("enterFrame", _mainLoop)
    end
end

function scene:hide( event )
    local sceneGroup = self.view
    
    local phase = event.phase
    
    if event.phase == "will" then
        -- Called when the scene is on screen and is about to move off screen
        
        -- remove the addressField since it contains a native object.
        
        --sceneGroup:remove(_ui_edit.view) --addressField:removeSelf()
        
        if (sceneGroup._ui_progress) then
            sceneGroup._ui_progress:removeSelf()
            sceneGroup._ui_progress = nil
        end
        
        if (sceneGroup._ui_navbar) then
            sceneGroup._ui_navbar:removeSelf()
            sceneGroup._ui_navbar = nil
        end
        
        
        -- INSERT code here to pause the scene
        -- e.g. stop timers, stop animation, unload sounds, etc.)
        
        Runtime:removeEventListener("enterFrame", _mainLoop)
        
    elseif phase == "did" then
        -- Called when the scene is now off screen
    end	
end

function scene:destroy( event )
    
    -- Called prior to the removal of scene's "view" (sceneGroup)
    -- 
    -- INSERT code here to cleanup the scene
    -- e.g. remove display objects, remove touch listeners, save state, etc.
    local sceneGroup = self.view
    
end

--------------------------------------------------------------------------------
-- handlers
--------------------------------------------------------------------------------

scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

--------------------------------------------------------------------------------
-- export
--------------------------------------------------------------------------------

return scene


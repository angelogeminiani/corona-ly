--------------------------------------------------------------------------------
-- import
--------------------------------------------------------------------------------
local widget = require( "widget" )

local composer = require( 'composer' )

local ly = require('ly.ly')
local app = require('ly.lyApp')
local NavClass = require('ly.ui.elements.navbar.NavBar')

local ScreenClass = require('ly.ui.elements.tabview.TabView')

--------------------------------------------------------------------------------
-- fields
--------------------------------------------------------------------------------

local scene = composer.newScene()

--------------------------------------------------------------------------------
-- private
--------------------------------------------------------------------------------

local function _mainLoop()
    
end

local function handleTabBarEvent( event )
    print( event.target._id )  --reference to button's 'id' parameter
end



local function _gotoBack(event)
    if(event.phase=='ended') then
        app.scene.back()  
    end
end

local function _initNavBar(sceneGroup)
    
    local leftButton = {
        onEvent = _gotoBack,
        width = 59,
        height = 32,
        defaultFile = "test/images/backbutton7_white.png",
        overFile = "test/images/backbutton7_white.png"
    }
    
    sceneGroup._ui_navbar = NavClass:new({
        title = "Web",
        backgroundColor = { 0.96, 0.62, 0.34 },
        titleColor = {1, 1, 1},
        font = "HelveticaNeue",
        leftButton = leftButton
    })
    sceneGroup:insert(sceneGroup._ui_navbar.view)
    print('NAVBAR', sceneGroup._ui_navbar.width, sceneGroup._ui_navbar.height)
    
    --print(navBar:toString())
    --sceneGroup:remove(navBar:getView())
end

--------------------------------------------------------------------------------
-- init
--------------------------------------------------------------------------------


local function _initScene(sceneGroup)
    
    -- Configure the tab buttons to appear within the bar
    local buttons = {
        {
            label = "Tab1",
            selected = true,
        },
        {
            label = "Tab2",
            selected = false,
        },
        {
            label = "Tab3",
            selected = false,
        },
    }
    
    -- Create the widget
    local params = {}
    params.height = ly.screen.height - sceneGroup._ui_navbar.height
    params.width = ly.screen.width
    params.x = ly.screen.centerX
    params.y = ly.screen.centerY + sceneGroup._ui_navbar.height*0.5
    params.buttonMargin = 0
    params.separatorWidth = 1
    
    sceneGroup._tabBar = ScreenClass:new(params)
    sceneGroup:insert(sceneGroup._tabBar.view)
    
    --add tabs
    sceneGroup._tabBar:setButtons(buttons)
    
    --VIEW 1
    local rect_1 = display.newRect(ly.screen.centerX-50,10, 100, 100)
    rect_1:setFillColor( .4, .4, .8 )
    sceneGroup._tabBar:insert(1, rect_1)
    
    --VIEW 2
    local rect_2 = display.newRect(ly.screen.centerX-50,10, 100, 100)
    rect_2:setFillColor( .4, .8, .4 )
    sceneGroup._tabBar:insert(2, rect_2)
    
    --VIEW 1
    local rect_3 = display.newRect(ly.screen.centerX-50,10, 100, 100)
    rect_3:setFillColor( .8, .4, .4 )
    sceneGroup._tabBar:insert(3, rect_3)
end

--------------------------------------------------------------------------------
-- handlers impl
--------------------------------------------------------------------------------

function scene:create( event )
    local sceneGroup = self.view
    
    local background = display.newRect(0, 0, display.contentWidth, display.contentHeight)
    background:setFillColor( 0.90, 0.90, 0.90 )
    background.x = display.contentWidth / 2
    background.y = display.contentHeight / 2
    
    sceneGroup:insert(background)
    
end

function scene:show( event )
    local sceneGroup = self.view
    
    local phase = event.phase
    
    if phase == "will" then
        
        _initNavBar(sceneGroup)
        _initScene(sceneGroup) 
        
        -- Called when the scene is still off screen and is about to move on screen
    elseif phase == "did" then
        
        local sceneGroup = scene.view
        
        -- Called when the scene is now on screen
        -- 
        -- INSERT code here to make the scene come alive
        -- e.g. start timers, begin animation, play audio, etc.
        --physics.start()
        
        
        Runtime:addEventListener("enterFrame", _mainLoop)
    end
end

function scene:hide( event )
    local sceneGroup = self.view
    
    local phase = event.phase
    
    if event.phase == "will" then
        -- Called when the scene is on screen and is about to move off screen
        
        -- remove the addressField since it contains a native object.
        
        --sceneGroup:remove(_ui_edit.view) --addressField:removeSelf()
        
        if (sceneGroup._tabBar) then
            sceneGroup._tabBar:removeSelf()
            sceneGroup._tabBar = nil
        end
        
        if (sceneGroup._ui_navbar) then
            sceneGroup._ui_navbar:removeSelf()
            sceneGroup._ui_navbar = nil
        end
        
        
        -- INSERT code here to pause the scene
        -- e.g. stop timers, stop animation, unload sounds, etc.)
        
        Runtime:removeEventListener("enterFrame", _mainLoop)
        
    elseif phase == "did" then
        -- Called when the scene is now off screen
    end	
end

function scene:destroy( event )
    
    -- Called prior to the removal of scene's "view" (sceneGroup)
    -- 
    -- INSERT code here to cleanup the scene
    -- e.g. remove display objects, remove touch listeners, save state, etc.
    local sceneGroup = self.view
    
end

--------------------------------------------------------------------------------
-- handlers
--------------------------------------------------------------------------------

scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

--------------------------------------------------------------------------------
-- export
--------------------------------------------------------------------------------

return scene



----------------------------------------------------------------------------------
-- export
--------------------------------------------------------------------------------
local composer = require( "composer" )
local widget = require("widget")

local app = require("ly.lyApp")
local ly = require("ly.ly")

local NavClass = require("ly.ui.elements.navbar.NavBar")

local EditClass = require("ly.ui.inputs.edit.Edit")
local ButtonClass = require("ly.ui.elements.button.Button")

local ServiceLogin = require('test.services.ServiceLogin')

--------------------------------------------------------------------------------
-- private
--------------------------------------------------------------------------------

local scene = composer.newScene()
local navBar
local lbl_title, fld_username, fld_password, tm, button1
local feedURL = 'http://gdata.youtube.com/feeds/mobile/users/CoronaLabs/uploads?max-results=20&alt=rss&orderby=published&format=1'

-- widget.setTheme(app.gui.theme.name)
-- physics.setDrawMode( "hybrid" )

local function mainLoop()
    
end

local function gotoBack()
    app.scene.back()
end

local function initNavBar(sceneGroup)
    
    if(navBar)then
        return 
    end
    
    local leftButton = {
        onEvent = gotoBack,
        width = 59,
        height = 32,
        defaultFile = "test/images/backbutton7_white.png",
        overFile = "test/images/backbutton7_white.png"
    }
    
    navBar = NavClass:new({
        title = "Test Network",
        backgroundColor = { 0.96, 0.62, 0.34 },
        titleColor = {1, 1, 1},
        font = "HelveticaNeue",
        leftButton = leftButton
    })
    sceneGroup:insert(navBar:getView())
    
end

local function initScene(sceneGroup)
    local connected_color = {1, 0.3, 0.3}
    
    tm = ly.cicle(function() 
        if (ly.net.isConnected())then
            connected_color = {0.3, 1, 0.3}
        else
            connected_color = {1, 0.3, 0.3}
        end 
        lbl_title:setFillColor( unpack(connected_color) )
        return false --infinite loop
    end, 500)
    
    --title
    local opt_title = 
    {
        text = "Login",     
        x = 160,
        y = navBar.height + 50,
        width = display.contentWidth - 20,
        height= 25,
        font = "Conquest", --native.systemFontBold,   
        fontSize = 25,
        align = "center"  --new alignment parameter
    }
    lbl_title = display.newText( opt_title )
    lbl_title:setFillColor( unpack(connected_color) )
    sceneGroup:insert(lbl_title)
    
    fld_username = EditClass:new({
        width = display.contentWidth-10,
        height = 30,
        --cornerRadius = 6,
        strokeWidth = 0,
        text = "",
        fontSize = 14,
        placeholder = "Write Username",
        font = "HelveticaNeue-Light",
        labelFont = "HelveticaNeue",
        labelFontSize = 14,
        labelWidth = 70,
        -- listener = textFieldHandler,
        label = "Username"
    })
    fld_username:setPos( display.contentCenterX, lbl_title.y + lbl_title.height + 50)
    sceneGroup:insert(fld_username.view)
    
    fld_password = EditClass:new({
        width = display.contentWidth-10,
        height = 30,
        --cornerRadius = 6,
        strokeWidth = 0,
        text = "",
        fontSize = 14,
        placeholder = "Write Password",
        font = "HelveticaNeue-Light",
        labelFont = "HelveticaNeue",
        labelFontSize = 14,
        labelWidth = 70,
        -- listener = textFieldHandler,
        label = "Password",
        isSecure = true
    })
    fld_password:setPos( display.contentCenterX, lbl_title.y + lbl_title.height + 100)
    sceneGroup:insert(fld_password.view)
    
    button1 = ButtonClass:new({
        width = display.contentWidth-30,
        height = 60,
        cornerRadius = 6,
        backgroundColor = {1,1,1},
        strokeWidth = 3,
        strokeColor = {1, 1, 1},
        caption = "Login",
        fontSize = 18,
        font = "HelveticaNeue",
        fontColor = {0,0,0}
    })
    -- Hide the native part of this until we need to show it on the screen.
    
    button1:setPos( display.contentCenterX, navBar.height + 250)
    sceneGroup:insert(button1.view)
    
    button1:addEventListener('tap', function(event) 
        -- do login
        if (ly.net.isConnected())then
            ServiceLogin.login(fld_username:getValue(), fld_password:getValue(),
            function(err, data)
               print(ly.json.encode(err), data) 
            end)
        end
    end)
end

local function removePanel(sceneGroup) 
    
    --sceneGroup:remove(panel:getView())
    
end

--------------------------------------------------------------------------------
-- handlers impl
--------------------------------------------------------------------------------

function scene:create( event )
    local sceneGroup = self.view
    
    local background = display.newRect(0, 0, display.contentWidth, display.contentHeight)
    background:setFillColor( 0.90, 0.90, 0.90 )
    background.x = display.contentWidth / 2
    background.y = display.contentHeight / 2
    
    sceneGroup:insert(background)
    
    initNavBar(sceneGroup)
    
    initScene(sceneGroup) 
end

function scene:show( event )
    local sceneGroup = self.view
    
    local phase = event.phase
    
    if phase == "will" then
        
        
        
        -- Called when the scene is still off screen and is about to move on screen
    elseif phase == "did" then
        
        local sceneGroup = scene.view
        
        -- Called when the scene is now on screen
        -- 
        -- INSERT code here to make the scene come alive
        -- e.g. start timers, begin animation, play audio, etc.
        --physics.start()
        
        
        Runtime:addEventListener("enterFrame", mainLoop)
    end
end

function scene:hide( event )
    local sceneGroup = self.view
    
    local phase = event.phase
    
    if event.phase == "will" then
        -- Called when the scene is on screen and is about to move off screen
        
        -- remove panel
        removePanel(sceneGroup)
        
        sceneGroup:remove(navBar.view)
        
        --tm.cancel()
        timer.cancel(tm)
        tm = nil
        
        sceneGroup:remove(fld_username.view)
        sceneGroup:remove(fld_password.view)
        fld_username = nil
        fld_password = nil
        
        sceneGroup:remove(button1.view)
        button1=nil
        
        -- INSERT code here to pause the scene
        -- e.g. stop timers, stop animation, unload sounds, etc.)
        
        Runtime:removeEventListener("enterFrame", mainLoop)
        
    elseif phase == "did" then
        -- Called when the scene is now off screen
    end	
end

function scene:destroy( event )
    
    -- Called prior to the removal of scene's "view" (sceneGroup)
    -- 
    -- INSERT code here to cleanup the scene
    -- e.g. remove display objects, remove touch listeners, save state, etc.
    local sceneGroup = self.view
    
end

--------------------------------------------------------------------------------
-- handlers
--------------------------------------------------------------------------------

scene:addEventListener( "create", scene )
scene:addEventListener( "show", scene )
scene:addEventListener( "hide", scene )
scene:addEventListener( "destroy", scene )

--------------------------------------------------------------------------------
-- export
--------------------------------------------------------------------------------

return scene



--ly.fmt
--------------------------------------------------------------------------------
-- injected
--------------------------------------------------------------------------------

local ly

--------------------------------------------------------------------------------
-- SINGLETON
--------------------------------------------------------------------------------

-- Creates Module
local M = {}

--------------------------------------------------------------------------------
-- private
--------------------------------------------------------------------------------

---============================================================
-- add comma to separate thousands
-- 
local function comma_value(n)
    local left,num,right = string.match(n,'^([^%d]*%d)(%d*)(.-)$')
    return left..(num:reverse():gsub('(%d%d%d)','%1,'):reverse())..right
end

---============================================================
-- rounds a number to the nearest decimal places
--
local function _round(val, decimal)
    if (decimal) then
        return math.floor( (val * 10^decimal) + 0.5) / (10^decimal)
    else
        return math.floor(val+0.5)
    end
end

--===================================================================
-- given a numeric value formats output with comma to separate thousands
-- and rounded to given decimal places
--
--
local function _format_num(n, decimal, prefix, neg_prefix)
    local str_amount,  formatted, famount, remain
    
    decimal = decimal or 2  -- default 2 decimal places
    neg_prefix = neg_prefix or '-' -- default negative sign
    prefix = prefix or ''
    
    famount = math.abs(_round(n,decimal))
    famount = math.floor(famount)
    
    remain = _round(math.abs(n) - famount, decimal)
    
    -- comma to separate the thousands
    formatted = comma_value(famount)
    
    -- attach the decimal portion
    if (decimal > 0) then
        remain = string.sub(tostring(remain),3)
        formatted = formatted .. '.' .. remain ..
        string.rep('0', decimal - string.len(remain))
    end
    
    -- attach prefix string e.g '$' 
    formatted = prefix .. formatted 
    
    -- if value is negative then format accordingly
    if (n<0) then
        if (neg_prefix=='()') then
            formatted = '('..formatted ..')'
        else
            formatted = neg_prefix .. formatted 
        end
    end
    
    return formatted
end

local function _number(n, options)
    options = options or {}
    local decimals = ly.toNumber(options.decimals, 0)
    local prefix = ly.toString(options.prefix,'') 
    local neg_prefix = ly.toString(options.neg_prefix,'-') 
    local dec_sep = ly.toString(options.decimalSeparator,',') 
    local tho_sep = ly.toString(options.thousandSeparator, '.')
    
    local result = _format_num(n, decimals, prefix, neg_prefix)
    
    --format decimal and thousand separator
    if (dec_sep~='.') then
        result = ly.str.replace(result, dec_sep, 't')
        result = ly.str.replace(result, tho_sep, 'd')
        result = ly.str.replace(result, 'd', dec_sep)
        result = ly.str.replace(result, 't', tho_sep)
    end
    
    return result
end

local function _number_reduced(n, options)
    local opts = options or {decimals=3}
    local decimals = opts.decimals;
    local suffix = '';
    n = ly.toNumber(n)
    if(n>1000) then
        n = n/1000
        suffix = 'K'
        if(decimals==-1) then
            decimals = 3
        end
    elseif(n>1000*1000) then
        n = n/(1000*1000)
        suffix = 'M'
        if(decimals==-1) then
            decimals = 3
        end
    elseif(n>1000*1000*1000) then
        n = n/(1000*1000*1000)
        suffix = 'T'
        if(decimals==-1) then
            decimals = 3
        end
    else
        --999
        if(decimals==-1) then
            decimals = 0
        end
    end
    opts.decimals = decimals
    local result = _number(n, opts)
    return result..suffix
end
    

--------------------------------------------------------------------------------
-- injection
--------------------------------------------------------------------------------

M.ly = function(instance)
    ly = instance
end

--------------------------------------------------------------------------------
-- public
--------------------------------------------------------------------------------

M.number = _number
M.numberReduced = _number_reduced

--------------------------------------------------------------------------------
-- export
--------------------------------------------------------------------------------

return M


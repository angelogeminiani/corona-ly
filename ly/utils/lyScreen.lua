--------------------------------------------------------------------------------
-- import
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
-- injected
--------------------------------------------------------------------------------

local ly

--------------------------------------------------------------------------------
-- SINGLETON
--------------------------------------------------------------------------------

-- Creates Module
local M = {}

--------------------------------------------------------------------------------
-- private
--------------------------------------------------------------------------------

local aspectRatio = display.pixelHeight / display.pixelWidth
local device_width = aspectRatio > 1.5 and 320 or math.ceil( 480 / aspectRatio )
local device_height = aspectRatio < 1.5 and 480 or math.ceil( 320 * aspectRatio )
--local device_width = aspectRatio > 1.5 and 800 or math.floor( 1200 / aspectRatio )
--local device_height = aspectRatio < 1.5 and 1200 or math.floor( 800 * aspectRatio )
local device_height_threshold = device_height/480

local function _statusbar_hidden(value)
    if(ly.isBoolean(value))then
        M.statusBar.is_hidden = value
        if(value) then
            display.setStatusBar(display.HiddenStatusBar)
            M.statusBar.height = 0
        else
            display.setStatusBar(display.DefaultStatusBar)
            M.statusBar.height = display.topStatusBarContentHeight
        end
    end
    return M.statusBar.is_hidden
end

--------------------------------------------------------------------------------
-- injection
--------------------------------------------------------------------------------

M.ly = function(instance)
    ly = instance
end

--------------------------------------------------------------------------------
-- public
--------------------------------------------------------------------------------

-- fields
M.ratio = aspectRatio
M.isTall = aspectRatio > 1.5
M.isTablet  = device_width > 320
M.width = device_width
M.height = device_height
M.height_threshold = device_height_threshold
M.centerX = display.contentCenterX
M.centerY = display.contentCenterY

M.statusBar = {}
M.statusBar.is_hidden = false
M.statusBar.height = display.topStatusBarContentHeight
M.statusBar.hidden = _statusbar_hidden

--------------------------------------------------------------------------------
-- export
--------------------------------------------------------------------------------

return M
-- ly.graphics
--------------------------------------------------------------------------------
-- import
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
-- injected
--------------------------------------------------------------------------------

local ly

--------------------------------------------------------------------------------
-- SINGLETON
--------------------------------------------------------------------------------

-- Creates Module
local M = {}

--------------------------------------------------------------------------------
-- CONSTANTS
--------------------------------------------------------------------------------

local ANCHOR_MIDDLE = 'middle' --0.5 0.5
local ANCHOR_TOPLEFT = 'topleft' --0 0
local ANCHOR_BOTTOMRIGHT = 'bottomright' --1 1
local ANCHOR_TOPRIGHT = 'topright' --1 0
local ANCHOR_BOTTOMLEFT = 'bottomleft' --0 1

local ALIGN_TOP = 'altop'
local ALIGN_RIGHT = 'alright'
local ALIGN_BOTTOM = 'albottom'
local ALIGN_LEFT = 'alleft'
local ALIGN_TOPRIGHT = 'altopright'
local ALIGN_TOPLEFT = 'altopleft'
local ALIGN_BOTTOMRIGHT = 'albottomright'
local ALIGN_BOTTOMLEFT = 'albottomleft'

--------------------------------------------------------------------------------
-- private
--------------------------------------------------------------------------------


local function _isScalableObject(item)
    return ly.isNotNull(item) and ly.isFunction(item.scale)  
end

local function _getAnchorMode(item)
    local result = ''
    if(item and item.anchorX) then
        if(item.anchorX==0.5 and item.anchorY==0.5) then
            result = ANCHOR_MIDDLE
        elseif(item.anchorX==0 and item.anchorY==0) then
            result = ANCHOR_TOPLEFT
        elseif(item.anchorX==1 and item.anchorY==1) then
            result = ANCHOR_BOTTOMRIGHT
        elseif(item.anchorX==1 and item.anchorY==0) then
            result = ANCHOR_TOPRIGHT
        elseif(item.anchorX==0 and item.anchorY==1) then
            result = ANCHOR_BOTTOMLEFT
        end   
    end
    return result
end

local function _setAnchorMode(item, mode)
    local result = ''
    if(item and item.anchorX) then
        if(mode==ANCHOR_MIDDLE) then
            item.anchorX = 0.5
            item.anchorY = 0.5
        elseif(mode==ANCHOR_TOPLEFT) then
            item.anchorX = 0
            item.anchorY = 0
        elseif(mode == ANCHOR_BOTTOMRIGHT) then
            item.anchorX = 1
            item.anchorY = 1
        elseif(mode == ANCHOR_TOPRIGHT) then
            item.anchorX=1  
            item.anchorY=0
        elseif(mode == ANCHOR_BOTTOMLEFT) then
            result = ANCHOR_BOTTOMLEFT
            item.anchorX=0  
            item.anchorY=1
        end  
        return item.anchorX, item.anchorY
    end
    return -1, -1 --error
end

---Change anchor to a display object mantainig same position on screen
-- @param displayObject
-- @param anchors {anchorX=0, anchorY=0}
-- @return

local function _setAnchor(displayObject, anchors)
    if(ly.isDisplayObject(displayObject) and ly.isObject(anchors)) then
        local anchorX = anchors.anchorX or displayObject.anchorX
        local anchorY = anchors.anchorY or displayObject.anchorY
        
        local x, y = 0, 0
        local object_height = displayObject.contentHeight;
        local object_width = displayObject.contentWidth;
        -- X
        if (anchorX~=displayObject.anchorX) then
            if (displayObject.anchorX==0) then
                --from left
                if(anchorX==0.5) then
                    --to center
                    x = displayObject.x + (object_width*0.5)
                elseif(anchorX==1) then
                    -- to right
                    x = displayObject.x + (object_width*1)
                end
            elseif (displayObject.anchorX==0.5) then
                -- from center
                if(anchorX==0) then
                    -- to left
                    x = displayObject.x - (object_width*0.5)
                elseif(anchorX==1) then
                    -- to right 
                    x = displayObject.x + (object_width*0.5)
                end
            elseif (displayObject.anchorX==1) then
                --from right
                if(anchorX==0.5) then
                    -- to center
                    x = displayObject.x - (object_width*0.5)
                elseif(anchorX==0) then
                    -- to left
                    x = displayObject.x - (object_width*1)
                end
            end
        else
            x = displayObject.x
        end
        
        -- X
        if (anchorY~=displayObject.anchorY) then
            if (displayObject.anchorY==0) then
                --from top
                if(anchorY==0.5) then
                    --to center
                    y = displayObject.y + (object_height*0.5)
                elseif(anchorY==1) then
                    -- to bottom
                    y = displayObject.x + (object_height*1)
                end
            elseif (displayObject.anchorY==0.5) then
                -- from center
                if(anchorY==0) then
                    -- to top
                    y = displayObject.y - (object_height*0.5)
                elseif(anchorY==1) then
                    -- to bottom 
                    y = displayObject.y + (object_height*0.5)
                end
            elseif (displayObject.anchorY==1) then
                --from bottom
                if(anchorY==0.5) then
                    -- to center
                    y = displayObject.y - (object_height*0.5)
                elseif(anchorY==0) then
                    -- to top
                    y = displayObject.y - (object_height*1)
                end
            end
        else
            y = displayObject.y
        end
        
        -- set new anchors
        displayObject.anchorX = anchorX
        displayObject.anchorY = anchorY
        displayObject.x = x
        displayObject.y = y
    end
end


local function _getBounds(displayObject_or_file, baseDir)
    local result =nil
    if(ly.isNotNull(displayObject_or_file)) then
        if(_isScalableObject(displayObject_or_file)) then
            result = {
                width = displayObject_or_file.width,
                height = displayObject_or_file.height,
                contentWidth = displayObject_or_file.contentWidth,
                contentHeight = displayObject_or_file.contentHeight
            }
        elseif (ly.isString(displayObject_or_file)) then
            baseDir = baseDir or system.DocumentsDirectory
            local image = display.newImage(displayObject_or_file, baseDir, 0,0)
            if (ly.isNotNull(image)) then
                result = _getBounds(image)
                image:removeSelf()
                image = nil
            end
        end
    end
    return result    
end

local function _scaleByHeight(item, targetHeight)
    local result = 1
    targetHeight = targetHeight or ly.screen.height
    if(_isScalableObject(item)) then
        result = targetHeight/item.height
        --scale preserving original size
        item:scale(result, result)
    end  
    return result
end

local function _resizeByHeight(item, targetHeight)
    local result = 1
    targetHeight = targetHeight or ly.screen.height
    if(_isScalableObject(item)) then
        result = targetHeight/item.height
        --change size
        item.width = item.width * result
        item.height = item.height * result
    end  
    return result
end

local function _scaleByWidth(item, targetWidth)
    local result = 1
    targetWidth = targetWidth or ly.screen.width
    if(_isScalableObject(item)) then
        result = targetWidth/item.width
        item:scale(result, result)
    end  
    return result
end

local function _resizeByWidth(item, targetWidth)
    local result = 1
    targetWidth = targetWidth or ly.screen.width
    if(_isScalableObject(item)) then
        result = targetWidth/item.width
        --change size
        item.width = item.width * result
        item.height = item.height * result
    end  
    return result
end

local function _crop(sourceFile, targetFile, baseDir, x, y, width, height)
    local result = ''
    if(ly.isNotEmpty(sourceFile) and (ly.isNotEmpty(targetFile))) then
        baseDir = baseDir or system.DocumentsDirectory
        local img_x = x or 0
        local img_y = y or 0
        local img_width = width or 100
        local img_height = height or 100
        
        -- x, y, width, height are in scaled units - convert to image pixel values.
        --[[
        local img_x = (x - display.screenOriginX) / display.contentScaleX
        local img_y = (y - display.screenOriginY) / display.contentScaleY
        local img_width = width / display.contentScaleX
        local img_height = height / display.contentScaleY]]--
        
        -- check image bounds and adjust if needed
        local bounds = _getBounds(sourceFile, baseDir)
        img_width = ly.conditional (img_width>bounds.width, bounds.width, img_width)
        img_height = ly.conditional (img_height>bounds.height, bounds.height, img_height)
        local is_square = (width==height)
        if (is_square and (img_width~=img_height)) then
            img_width = ly.conditional(img_width<img_height, img_width, img_height)
            img_height = ly.conditional(img_height<img_width, img_height, img_width)
        end
        
        -- Make an image sheet from the screen capture file, with a single frame consisting
        -- of the crop target.
        local frame = {
            x = img_x, -- - (img_width/2),
            y = img_y, -- - (img_height/2),
            width = img_width ,
            height = img_height,
        }
        frame.x = ly.conditional(frame.x>0, frame.x, 0)
        frame.y = ly.conditional(frame.y>0, frame.y, 0)
        local options = {
            frames = {frame},
            sheetContentWidth = bounds.width,  -- width of original 1x size of entire sheet
            sheetContentHeight = bounds.height  -- height of original 1x size of entire sheet
        }
        local screenImageSheet = graphics.newImageSheet(sourceFile, baseDir, options)
        
        -- Create an image from the image sheet, then save it to a file.
        local captureImg = display.newImage(screenImageSheet, 1)--,  frame.width, frame.height)
        captureImg.x = 0
        captureImg.y = 0
        captureImg.width  = frame.width
        captureImg.height = frame.height
        display.save(captureImg, targetFile, baseDir)
        
        -- Clean up
        captureImg:removeSelf()
        captureImg = nil
        screenImageSheet = nil
        
        result = system.pathForFile(targetFile, baseDir)
    end
    return result
end

local function _cropCenter(sourceFile, targetFile, baseDir, width, height)
    local result = ''
    if(ly.isNotEmpty(sourceFile) and (ly.isNotEmpty(targetFile))) then
        baseDir = baseDir or system.DocumentsDirectory
        
        local image_bounds = _getBounds(sourceFile, baseDir)
        if(image_bounds) then
            width = width or 100
            height = height or 100
            
            local image_width = image_bounds.width;
            local image_height = image_bounds.height;
            
            width = ly.conditional(width>image_width, image_width, width)
            height = ly.conditional(height>image_height, image_height, height)
            local x = (image_width - width) *0.5
            local y = (image_height - height) *0.5
            
            result = _crop(sourceFile, targetFile, baseDir, x, y, width, height)
        end
    end
    return result
end

local function _toImagePixelBounds(x, y, width, height)
    if(ly.isArray(x)) then
        x, y, width, height = unpack(x)
    elseif (ly.isObject(x)) then
        local obj = x
        x = obj.x
        y = obj.y
        width = obj.width
        height = obj.height
    end
    -- x, y, width, height are in scaled units - convert to image pixel values.
    local img_x = (x - display.screenOriginX) / display.contentScaleX
    local img_y = (y - display.screenOriginY) / display.contentScaleY
    local img_width = width / display.contentScaleX
    local img_height = height / display.contentScaleY
    return { 
        x = img_x,
        y = img_y,
        width = img_width,
        height = img_height
    }
end

local function _cropImage(image, targetFile, baseDir, x, y, width, height)
    local result = ''
    
    if (ly.isDisplayObject(image)) then
        baseDir = baseDir or system.DocumentsDirectory
        
        -- save image
        display.save(image, "image_to_crop.jpg", baseDir)
        
        result = _crop("image_to_crop.jpg", targetFile, baseDir, x, y, width, height)
        
        --revove temp file
        os.remove(system.pathForFile("image_to_crop.jpg", baseDir))
    end
    
    return result
end

local function _cropCenterImage(image, targetFile, baseDir, width, height)
    local result = ''
    
    if (ly.isDisplayObject(image)) then
        baseDir = baseDir or system.DocumentsDirectory
        
        width = width or 100
        height = height or 100
        
        local image_width = image.contentWidth; -- screen width
        local image_height = image.contentHeight; -- screen height
        
        local is_square = (width==height)
        width = ly.conditional(width>image_width, image_width, width)
        height = ly.conditional(height>image_height, image_height, height)
        local x = (image_width - width) *0.5
        local y = (image_height - height) *0.5
        
        if (is_square and (width~=height)) then
            width = ly.conditional(width<height, width, height)
            height = ly.conditional(height<width, height, width)
        end
        
        -- save image
        display.save(image, "image_to_crop.jpg", baseDir)
        
        -- convert bounds to image pixel bounds
        local image_pixel_bounds = _toImagePixelBounds(x, y, width, height)
        x, y = image_pixel_bounds.x, image_pixel_bounds.y
        width, height = image_pixel_bounds.width, image_pixel_bounds.height
        
        -- crop
        result = _crop("image_to_crop.jpg", targetFile, baseDir, x, y, width, height)
        
        --revove temp file
        os.remove(system.pathForFile("image_to_crop.jpg", baseDir))
    end
    
    return result
end

local function _centerX(item)
    local result = 1
    if(_isScalableObject(item)) then
        item.x = ly.screen.width/2
    end  
    return result
end

local function _align(item, mode, margins, parent)
    local result = false
    if(_isScalableObject(item)) then
        result = true
        --parent's height and width
        local parent_width = ly.screen.width
        local parent_height = ly.screen.height
        if (_isScalableObject(parent)) then
            parent_width = parent.width
            parent_height = parent.height
        end
        --margins
        local margin_x, margin_y = 0, 0
        local function _get_margin(index)
            if(ly.isTable(margins))then
                return ly.toNumber(margins[index],0)
            else
                return ly.toNumber(margins, 0)
            end
        end
        --mode
        if (mode==ALIGN_BOTTOM) then
            item.anchorY = 0
            margin_y = _get_margin(1)
            item.y = parent_height - (margin_y + item.height)
        elseif(mode==ALIGN_BOTTOMLEFT) then
            item.anchorY = 0
            item.anchorX = 0
            margin_y = _get_margin(1)-- bottom 
            margin_x = _get_margin(2)-- left 
            item.y = parent_height - (margin_y + item.height)
            item.x = margin_x
        elseif(mode==ALIGN_BOTTOMRIGHT) then
            item.anchorY = 0
            item.anchorX = 0 
            margin_y = _get_margin(1)-- bottom 
            margin_x = _get_margin(2)-- right 
            item.y = parent_height - (margin_y + item.height)
            item.x = parent_width - (margin_x + item.width)
        elseif(mode==ALIGN_LEFT) then  
            item.anchorX = 0 
            margin_x = _get_margin(1)-- left 
            item.x = margin_x
        elseif(mode==ALIGN_RIGHT) then
            item.anchorX = 0 
            margin_x = _get_margin(1)-- right 
            item.x = parent_width - (margin_x + item.width)
        elseif(mode==ALIGN_TOP) then
            item.anchorY = 0
            margin_y = _get_margin(1)
            item.y = margin_y
        elseif(mode==ALIGN_TOPLEFT) then
            item.anchorY = 0
            item.anchorX = 0 
            margin_y = _get_margin(1)-- top 
            margin_x = _get_margin(2)-- left 
            item.y = margin_y
            item.x = margin_x
        elseif(mode==ALIGN_TOPRIGHT) then
            item.anchorY = 0
            item.anchorX = 0 
            margin_y = _get_margin(1)-- top 
            margin_x = _get_margin(2)-- right 
            item.y = margin_y
            item.x = parent_width - (margin_x + item.width)
        end
    end
    return result
end

local function _alignLeft(item, margin)
    local margins = {ly.toNumber(margin)}
    _align(item, ALIGN_LEFT, margins, nil) 
end

local function _alignRight(item, margin, parent)
    local margins = {ly.toNumber(margin)}
    _align(item, ALIGN_RIGHT, margins, parent) 
end

local function _alignTop(item, margin, parent)
    local margins = {ly.toNumber(margin)}
    _align(item, ALIGN_TOP, margins, parent) 
end

local function _alignBottom(item, margin, parent)
    local margins = {ly.toNumber(margin)}
    _align(item, ALIGN_BOTTOM, margins, parent) 
end

--------------------------------------------------------------------------------
-- injection
--------------------------------------------------------------------------------

M.ly = function(instance)
    ly = instance
end

--------------------------------------------------------------------------------
-- public
--------------------------------------------------------------------------------

-- constants
M.ANCHOR_MIDDLE = ANCHOR_MIDDLE
M.ANCHOR_BOTTOMLEFT = ANCHOR_BOTTOMLEFT
M.ANCHOR_BOTTOMRIGHT = ANCHOR_BOTTOMRIGHT
M.ANCHOR_TOPLEFT = ANCHOR_TOPLEFT
M.ANCHOR_TOPRIGHT = ANCHOR_TOPRIGHT
M.ALIGN_BOTTOM = ALIGN_BOTTOM
M.ALIGN_BOTTOMLEFT = ALIGN_BOTTOMLEFT
M.ALIGN_BOTTOMRIGHT = ALIGN_BOTTOMRIGHT
M.ALIGN_LEFT = ALIGN_LEFT
M.ALIGN_RIGHT = ALIGN_RIGHT
M.ALIGN_TOP = ALIGN_TOP
M.ALIGN_TOPLEFT = ALIGN_TOPLEFT
M.ALIGN_TOPRIGHT = ALIGN_TOPRIGHT

-- methods
M.getAnchorMode = _getAnchorMode
M.setAnchorMode = _setAnchorMode
M.setAnchor = _setAnchor

M.centerX = _centerX
M.align = _align
M.alignLeft = _alignLeft
M.alignRight = _alignRight
M.alignTop = _alignTop
M.alignBottom = _alignBottom

M.getBounds = _getBounds
M.scaleByHeight = _scaleByHeight
M.scaleByWidth = _scaleByWidth
M.resizeByHeight = _resizeByHeight
M.resizeByWidth = _resizeByWidth
M.cropImage = _cropImage
M.cropCenterImage = _cropCenterImage
M.crop = _crop
M.cropCenter = _cropCenter


--------------------------------------------------------------------------------
-- export
--------------------------------------------------------------------------------

return M


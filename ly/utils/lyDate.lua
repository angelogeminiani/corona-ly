---Date utility library
--Format patterns @see: http://php.net/manual/en/function.strftime.php
---

--------------------------------------------------------------------------------
-- import
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
-- injected
--------------------------------------------------------------------------------

local ly

--------------------------------------------------------------------------------
-- SINGLETON
--------------------------------------------------------------------------------

-- Creates Module
local M = {}

--------------------------------------------------------------------------------
-- private
--------------------------------------------------------------------------------

---Return a timestamp.
-- Use ly.sys.timestamp() if you need a timestamp working under Windows.
-- @param dateString
-- @return

local function _timestamp(dateString)
    dateString = dateString or os.date("!%Y-%m-%dT%T%z",os.time()) --"2012-01-12T12:04:35.03-0400" --

    local pattern = "(%d+)%-(%d+)%-(%d+)%a(%d+)%:(%d+)%:([%d%.]+)([Z%p])(%d%d)%:?(%d%d)"
    local year, month, day, hour, minute, seconds, tzoffset, offsethour, offsetmin =
    dateString:match(pattern)
    local timestamp = os.time(
    {year=year, month=month, day=day, hour=hour, min=minute, sec=seconds} )
    local offset = 0
    if ( tzoffset ) then
        if ( tzoffset == "+" or tzoffset == "-" ) then  -- we have a timezone!
            offset = offsethour * 60 + offsetmin
            if ( tzoffset == "-" ) then
                offset = offset * -1
            end
            timestamp = timestamp + offset
        end
    end
    return timestamp
end

local function _timeToMidnight()
    
    local function get_timezone_offset(ts)
        local utcdate   = os.date("!*t", ts)
        local localdate = os.date("*t", ts)
        localdate.isdst = false -- this is the trick
        return os.difftime(os.time(localdate), os.time(utcdate))
    end
    local offset = get_timezone_offset(os.time())
    
    local UTCNow = os.time()
    local UTCTable = os.date("*t", UTCNow)
    local dstValue
    if UTCTable.isdst == true then
        dstValue = -14400
    else
        dstValue = -18000
    end
    
    local midnight = {year=UTCTable.year, month=UTCTable.month, day=UTCTable.day, hour=23, min=59}  
    local timeDifference = (((os.time(midnight) - (UTCNow + dstValue)) + offset)/60)/60
    
    local hoursRemaining = math.floor(timeDifference)
    local minsRemaining = math.round((timeDifference%1)*60)
    
    print(hoursRemaining .. " hours and " .. minsRemaining .. " mins")
end

---returns a table with the following fields: 
-- year (four digits), 
-- month (1-12), 
-- day (1-31), 
-- hour (0-23), 
-- min (0-59),  
-- sec (0-61), 
-- wday (weekday; Sunday is 1), 
-- yday (day of the year), 
-- isdst (daylight saving flag, a boolean).
-- @param os_time (optionsl)
-- @return
local function _date(os_time)
    os_time = os_time or os.time()
    return os.date('*t') -- return table with all time fields
end

local function _now()
    return _date()
end

local function _inc(t, value, field)
    t = t or _now()
    t[field] = t[field] + value

    return _date(os.time(t))
end

local function _dec(t, value, field)
    t = t or _now()
    t[field] = t[field] - value

    return _date(os.time(t))
end

--------------------------------------------------------------------------------
-- public
--------------------------------------------------------------------------------

M.ly = function(instance)
    ly = instance
end

-- methods
M.timestamp = _timestamp

M.now = _now
M.date = _date
M.inc = _inc
M.dec = _dec

--------------------------------------------------------------------------------
-- export
--------------------------------------------------------------------------------

return M






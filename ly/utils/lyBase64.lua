--------------------------------------------------------------------------------
-- import
--------------------------------------------------------------------------------

local mime = require( 'mime' )

--------------------------------------------------------------------------------
-- injected
--------------------------------------------------------------------------------

local ly

--------------------------------------------------------------------------------
-- SINGLETON
--------------------------------------------------------------------------------

-- Creates Module
local M = {}

--------------------------------------------------------------------------------
-- private
--------------------------------------------------------------------------------


local function _encode(data)
    if (ly.isEmpty(data))then
        return nil
    end
    local len = data:len()
    local t = {}
    for i=1,len,384 do
        local n = math.min(384, len+1-i)
        if n > 0 then
            local s = data:sub(i, i+n-1)
            local enc, _ = mime.b64(s)
            t[#t+1] = enc
        end
    end
    return table.concat(t)
end

local function _decode(data)
    if (ly.isEmpty(data))then
        return nil
    end
    local len = data:len()
    local t = {}
    for i=1,len,384 do
        local n = math.min(384, len+1-i)
        if n > 0 then
            local s = data:sub(i, i+n-1)
            local dec, _ = mime.unb64(s)
            t[#t+1] = dec
        end
    end
    return table.concat(t)
end

local function _encodeBinary(filename, baseDir)        
    local path = filename
    if (baseDir) then
       path =  system.pathForFile( filename, baseDir)
    end		
    local imageData =  ly.fs.read(path)	
    --base64 encode it	
    return _encode(imageData)
end

local function _decodeBinary(data, filename, baseDir)     
    local path = filename
    if (baseDir) then
       path =  system.pathForFile( filename, baseDir)
    end	
    local imageData =  _decode(data)
    ly.fs.write(path, imageData)
    --base64 encode it	
    return path
end

--------------------------------------------------------------------------------
-- injection
--------------------------------------------------------------------------------

M.ly = function(instance)
    ly = instance
end

--------------------------------------------------------------------------------
-- public
--------------------------------------------------------------------------------

M.encode = _encode
M.decode = _decode
M.encodeBinary = _encodeBinary
M.decodeBinary = _decodeBinary

--------------------------------------------------------------------------------
-- export
--------------------------------------------------------------------------------

return M


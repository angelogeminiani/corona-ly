--ly.dialog
--------------------------------------------------------------------------------
-- injected
--------------------------------------------------------------------------------

local ly

--------------------------------------------------------------------------------
-- SINGLETON
--------------------------------------------------------------------------------

-- Creates Module
local M = {
    _waiting_count = 0
}

--------------------------------------------------------------------------------
-- private
--------------------------------------------------------------------------------
local function _waiting(visible, callback)
    ly.delay(function() 
        if(ly.isBoolean(visible))then
            if (visible) then
                M._waiting_count = M._waiting_count+1
                if (M._waiting_count==1) then
                    native.setActivityIndicator(true) 
                end
            else
                M._waiting_count = M._waiting_count-1
                if (M._waiting_count==0 or M._waiting_count<0) then
                    M._waiting_count=0
                    native.setActivityIndicator(false) 
                end
            end
        else
            --force close
            M._waiting_count=0
            native.setActivityIndicator(false) 
        end
        ly.invoke(callback)
    end, 100)
end

local function _show(title, message, buttons, callback, async)
    title = title or ''
    message = message or ''
    buttons = buttons or {'OK'}
    
    return native.showAlert( title, message, buttons, function(event)
        if event.action == "clicked" then
            local btn_index = event.index
            if (async) then
                ly.delay(callback, 100, btn_index, event) 
            else
                ly.invoke(callback, btn_index, event) 
            end
        end
    end)
end

local function _alert(title, message, callback, async)
    return _show(title, message, nil, callback, async)  
end

--------------------------------------------------------------------------------
-- public
--------------------------------------------------------------------------------

M.ly = function(instance)
    ly = instance
end

M.waiting = _waiting
M.alert = _alert
M.show = _show

--------------------------------------------------------------------------------
-- export
--------------------------------------------------------------------------------

return M



--------------------------------------------------------------------------------
-- import
--------------------------------------------------------------------------------

local socket = require( 'socket' )

--------------------------------------------------------------------------------
-- injected
--------------------------------------------------------------------------------

local ly

--------------------------------------------------------------------------------
-- CONSTANTS
--------------------------------------------------------------------------------

local NETWORK_ERROR = 'NETWORK_ERROR'
local NETWORK_SYS_ERROR = 'NETWORK_SYS_ERROR'

--------------------------------------------------------------------------------
-- SINGLETON
--------------------------------------------------------------------------------

-- Creates Module
local M = {
    params={
        errorMessage='error',
        errorCode='error'
    }
}

--------------------------------------------------------------------------------
-- private
--------------------------------------------------------------------------------

local function _toStringParams(params)
    local result = ''
    ly.forEach(params, function(value, key, index) 
        if(result:len()>0)then
            result = result..'&' 
        end
        value = ly.str.urlEncode(tostring(value))
        result = result..key..'='..value
    end)  
    return result
end

local function _addParamsToUrl(url, params)
    if(ly.isNotNull(params) and ly.hasPairs(params)) then
        if (ly.str.indexOf(url, '?')>0) then
            url = url..'&'.._toStringParams(params)
        else
            url = url..'?'.._toStringParams(params) 
        end
    end
    return url
end

local function _checkError(event)
    if(ly.isNotNull(event)) then
        if (event.isError) then
            event.error = event.error or {}
            event.error.message = 'Network Error'
            event.error.code = NETWORK_ERROR
        elseif (ly.json.isJSONObject(event.response)) then
            local tmp = ly.json.decode(event.response)
            if (ly.isObject(tmp)) then
                local param_err_msg = M.params.errorMessage
                local param_err_code = M.params.errorCode
                local val_err_msg = ly.getValue(tmp, param_err_msg, '', true)
                local val_err_code = ly.getValue(tmp, param_err_code, '', true)
                if (ly.isString(val_err_msg) and ly.isNotEmpty(val_err_msg)) then
                    event.isError = true
                    event.error = event.error or {}
                    event.error.message = val_err_msg
                    event.error.code = val_err_code
                end
            end
        end
    end
end

local function _isConnectedTo(address, port)
    if(socket) then
        port = port or 80
        local netConn = socket.connect(address, port)
        if netConn == nil then
            return false
        end
        netConn:close()
        return true
    else
        return false
    end
end

local function _isConnected()
    return _isConnectedTo('www.google.com', 80)
end

local function _getMimeType(fileName)
    local ext = tostring(ly.path.getExtension(fileName))
    if (ext:len()==0) then
        ext = fileName
    end
    ext = ly.str.replace(ext, '.', '')
    local response = M.MimeTypes[ext]
    if (ly.isEmpty(response)) then
        response = M.MimeTypes['bin'] 
    end
    return response
end

local function _post(url, callback, opt_params)
    if(network) then
        local body = ''
        if opt_params then
            body = _toStringParams(opt_params)
        end
        local headers = {}
        headers["Content-Type"] = "application/x-www-form-urlencoded"
        headers["Accept-Language"] = "en-US"
        
        local params = {}
        params.headers = headers
        params.body = body
        
        return network.request( url, "POST", function(event)
            _checkError(event)
            ly.invoke(callback, event)
        end, params )
    else
        --no network
        local event = {}
        event.error = event.error or {}
        event.error.message = 'Network not available'
        event.error.code = NETWORK_SYS_ERROR
        ly.invoke(callback, event)
    end
end

local function _get(url, callback, opt_params)
    if(network) then
        url = _addParamsToUrl(url, opt_params)
        return network.request( url, "GET", function(event)
            _checkError(event)
            ly.invoke(callback, event)
        end)
    else
        --no network
        local event = {}
        event.error = event.error or {}
        event.error.message = 'Network not available'
        event.error.code = NETWORK_SYS_ERROR
        ly.invoke(callback, event)
    end
end

--http://docs.coronalabs.com/api/library/network/download.html
local function _download(url, callback, options)
    options = options or {}
    options.method = options.method or 'GET'
    options.baseDir = options.baseDir or system.DocumentsDirectory 
    options.fileName = options.fileName or ly.path.getName(url)
    options.params = options.params or {}
    options.flag_timestamp = ly.toBoolean(options.flag_timestamp, true)
    
    --add timestamp to url
    if (options.flag_timestamp) then
        url = _addParamsToUrl(url, {timestamp=ly.sys.timestamp()})
    end
    
    network.download(url, options.method, 
    function(event) 
        ly.invoke(callback, event)
    end, 
    options.params, options.fileName, options.baseDir)
end

--https://docs.coronalabs.com/api/library/network/upload.html
local function _upload(url, callback, options)
    options = options or {}
    options.method = options.method or 'PUT'
    options.baseDir = options.baseDir or system.DocumentsDirectory 
    options.fileName = options.fileName or ly.path.getName(url)
    options.contentType = options.contentType or 'image/jpeg'
    options.params = options.params or {}
    options.fields = options.fields or {}
    
    options.params.bodyType = options.params.bodyType or 'binary' -- text, binary
    options.params.headers = options.params.headers or {}
    options.params.headers['Content-Type'] = options.params.headers['Content-Type'] or options.contentType
    options.params.progress = ly.toBoolean(options.params.progress, false) --if true enables the progress events
    
    -- contains fields
    if(not ly.isEmpty(options.fields)) then
        url = url..'?'.._toStringParams(options.fields)
    end
    
    network.upload( url, options.method, 
    function(event) 
        if (event.isError) then
            ly.invoke(callback, event, nil)
        elseif (event.phase=='ended') then
            ly.invoke(callback, nil, event)
        end
    end, 
    options.params, options.fileName, options.baseDir)
end

local function _uploadMultipart(url, callback, options)
    options = options or {}
    if(options.multipart) then
        local params = {}
        params.body = options.multipart:getBody() -- Must call getBody() first!
        params.headers = options.multipart:getHeaders() -- Headers not valid until getBody() is called.
        
        network.request( url, "POST", function(event) 
            _checkError(event)
            ly.invoke(callback, event)
        end, params)
    else
        --invalid request
        ly.invoke(callback, {error='Missing "MultipartFormData" passed as "multipart" options field'})
    end
    
end

local function _getClientIpAndPort()
    
    local someRandomIP = "192.168.1.122" --This address you make up
    local someRandomPort = "3102" --This port you make up  
    local mySocket = socket.udp() --Create a UDP socket like normal
    --This is the weird part, we need to set the peer for some reason
    mySocket:setpeername(someRandomIP,someRandomPort) 
    --I believe this binds the socket
    --Then we can obtain the correct ip address and port
    local myDevicesIpAddress, somePortChosenByTheOS = mySocket:getsockname()-- returns IP and Port
    
    return myDevicesIpAddress, somePortChosenByTheOS
end
--------------------------------------------------------------------------------
-- injection
--------------------------------------------------------------------------------

M.ly = function(instance)
    ly = instance
end

--------------------------------------------------------------------------------
-- public
--------------------------------------------------------------------------------

-- fields
M.MimeTypes = {
    bin = 'application/octet-stream',
    dat = 'application/octet-stream',
    dump = 'application/octet-stream',
    jpg = 'image/jpeg',
    png = 'image/png',
    gif = 'image/gif',
    bmp = 'image/bmp',
    bm = 'image/bmp',
    ico = 'image/x-icon',
    json = 'application/json',
    txt = 'text/plain',
    htm = 'text/html',
    html = 'text/html',
    xml = 'text/xml',
    mp3 = 'audio/mpeg3',
    zip = 'application/x-compressed'
}
M.NETWORK_ERROR = NETWORK_ERROR

-- config
M.config = function(config_obj)
    ly.merge(config_obj, M.params, true)
end

-- methods
M.getMimeType = _getMimeType
M.isConnectedTo = _isConnectedTo
M.isConnected = _isConnected
M.post = _post
M.get = _get
M.download = _download
M.upload = _upload
M.uploadMultipart = _uploadMultipart
M.getClientIpAndPort = _getClientIpAndPort

--------------------------------------------------------------------------------
-- export
--------------------------------------------------------------------------------

return M
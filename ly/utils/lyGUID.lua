--
-- GUID Module v1.0.0
-- Creates what looks to be a GUID.
--
--------------------------------------------------------------------------------
-- import
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
-- injected
--------------------------------------------------------------------------------

local ly

--------------------------------------------------------------------------------
-- SINGLETON
--------------------------------------------------------------------------------

local M = {};

--------------------------------------------------------------------------------
-- private
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
-- public
--------------------------------------------------------------------------------

M.char = {"A", "B", "C", "D", "E", "F","1","2","3","4","5","6","7","8","9"};
M.isHyphen = {[9]=1,[14]=1,[19]=1,[24]=1};
M.currentGuid = nil;
M.used = {};

math.randomseed(os.time());

M.generate = function()
    local z=0
    local loops = 0;
    
    while(true) do
        loops = loops +1;
        --If we can't get it in 20 tries than we have bigger problems.
        if(loops > 20) then return false; end
        
        local pass = {};
        local x, a, z
        for z = 1,36 do
            if M.isHyphen[z] then
                x='-';
            else
                a = math.random(1,#M.char); -- randomly choose a character from the "guid.char" array
                x = M.char[a]
            end
            table.insert(pass, x) -- add new index into array.
        end
        z = nil
        M.currentGuid = tostring(table.concat(pass)); -- concatenate all indicies of the array, then return concatenation.
        
        --[[
        if not guid.used[guid.currentGuid] then
            guid.used[guid.currentGuid] = guid.currentGuid;
            return(guid.currentGuid);
        else
            print('Duplicated a Previously Created GUID.');
        end
        ]]--
        
        return(M.currentGuid);
    end
end

M.ly = function(instance)
    ly = instance
end

--------------------------------------------------------------------------------
-- export
--------------------------------------------------------------------------------

return M;


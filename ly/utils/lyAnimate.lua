--ly.animate
--------------------------------------------------------------------------------
-- import
--------------------------------------------------------------------------------


--------------------------------------------------------------------------------
-- injected
--------------------------------------------------------------------------------

local ly

--------------------------------------------------------------------------------
-- SINGLETON
--------------------------------------------------------------------------------

-- Creates Module
local M = {}

--------------------------------------------------------------------------------
-- private
--------------------------------------------------------------------------------

local function _cancel(tagOrObject)
    --[[
    ALL: transition.cancel( )
    BY REF: transition.cancel( transitionReference )
    BY OBJECT: transition.cancel( displayObject )
    BY NAME: transition.cancel( tagName )
    ]]--
    if(ly.isNull(tagOrObject)) then
        transition.cancel()
    else
        if (ly.isString(tagOrObject)) then
            transition.cancel( tagOrObject )
        elseif (ly.isLyObject(tagOrObject)) then
            transition.cancel(tagOrObject.view)
        else
            transition.cancel(tagOrObject)
        end
    end
end

--------------------------------------------------------------------------------
-- animations
--------------------------------------------------------------------------------

local function _fade(displayObject, callback, options)
    if(ly.isDisplayObject(displayObject) or ly.isLyObject(displayObject)) then
        options = options or {}
        options.tag = options.tag 
        options.alpha = options.alpha or 0
        
        local target = ly.conditional(ly.isLyObject(displayObject), 
        displayObject.view, displayObject)
        
        if(ly.isDisplayObject(target)) then
            transition.to(target, {
                time=300,
                alpha = options.alpha,
                onComplete = function() 
                    ly.invoke(callback)
                end
            })
        end
    end 
end

local function _fade_in(displayObject, callback)
    _fade(displayObject, callback, {tag='fadeIn', alpha=1})
end

local function _fade_out(displayObject, callback)
    _fade(displayObject, callback, {tag='fadeOut', alpha=0})
end

local function _rotate(displayObject, callback, options)
    if(ly.isDisplayObject(displayObject) or ly.isLyObject(displayObject)) then
        options = options or {}
        options.tag = options.tag or 'rotate'
        options.degrees = options.degrees or 360
        
        local target = ly.conditional(ly.isLyObject(displayObject), 
        displayObject.view, displayObject)
        
        if(ly.isDisplayObject(target)) then
            local initial_rotation = target.rotation
            transition.to(target, {
                time=300,
                rotation = options.degrees + initial_rotation,
                onComplete = function() 
                    target.rotation = initial_rotation
                    
                    ly.invoke(callback)
                end
            })
        end
    end 
end

local function _expand(displayObject, callback, options)
    if(ly.isDisplayObject(displayObject) or ly.isLyObject(displayObject)) then
        
        options = options or {}
        options.tag = options.tag or 'expand'
        options.alpha = options.alpha or 0
        options.scale = options.scale or 2
        options.transition_in = options.transition_in or easing.linear
        options.time_in = options.time_in or 300
        
        local target = ly.conditional(ly.isLyObject(displayObject), 
        displayObject.view, displayObject)
        
        if(ly.isDisplayObject(target)) then
            transition.to(target, {
                tag = options.tag,
                time = options.time_in,
                alpha = options.alpha,
                xScale = options.scale,
                yScale = options.scale,
                transition=options.transition_in,
                onComplete = function() 
                    if(ly.isDisplayObject(target)) then
                        transition.to(target, {
                            tag = options.tag,
                            time = 0,
                            alpha = 1,
                            xScale = 1,
                            yScale = 1,
                            --transition=easing.linear,
                            onComplete = function()
                                ly.invoke(callback, true) -- animation completed
                            end
                        })
                    else
                        ly.invoke(callback, true) -- animation not completed
                    end
                end
            })
            return true --animated
        else
            ly.invoke(callback, false) -- animation not completed
            return false --not animated
        end
    end
    return false --not animated
end


local function _flash(displayObject, callback, options)
    if(ly.isDisplayObject(displayObject) or ly.isLyObject(displayObject)) then
        
        options = options or {}
        options.tag = options.tag or 'flash'
        options.alpha = options.alpha or 0
        options.transition_in = options.transition_in or easing.linear
        options.transition_out = options.transition_out or easing.outBounce
        options.time_in = options.time_in or 300
        options.time_out = options.time_out or 300
        
        local target = ly.conditional(ly.isLyObject(displayObject), displayObject.view, displayObject)
        
        if(ly.isDisplayObject(target)) then
            transition.to(target, {
                tag = options.tag,
                time = options.time_in,
                alpha = options.alpha,
                transition=options.transition_in,
                onComplete = function() 
                    if(ly.isDisplayObject(target)) then
                        transition.to(target, {
                            tag = options.tag,
                            time = options.time_out,
                            alpha = 1,
                            transition=options.transition_out,
                            onComplete = function()
                                ly.invoke(callback, true) -- animation completed
                            end
                        })
                    else
                        ly.invoke(callback, false) -- animation not completed
                    end
                end
            })
            return true --animated
        else
            ly.invoke(callback, false) -- animation not completed
            return false --not animated
        end
    end
    return false --not animated
end

local function _pulse(displayObject, callback, options)
    if(ly.isDisplayObject(displayObject) or ly.isLyObject(displayObject)) then
        
        options = options or {}
        options.tag = options.tag or 'pulse'
        options.scale = options.scale or 0.7
        options.transition_in = options.transition_in or easing.linear
        options.transition_out = options.transition_out or easing.outBounce
        options.time_in = options.time_in or 300
        options.time_out = options.time_out or 800
        
        local target = ly.conditional(ly.isLyObject(displayObject), displayObject.view, displayObject)
        
        if(ly.isDisplayObject(target)) then
            transition.to(target, {
                tag = options.tag,
                time = options.time_in,
                xScale = options.scale,
                yScale = options.scale,
                transition=options.transition_in,
                onComplete = function() 
                    if(ly.isDisplayObject(target)) then
                        transition.to(target, {
                            tag = options.tag,
                            time = options.time_out,
                            xScale = 1,
                            yScale = 1,
                            transition=options.transition_out,
                            onComplete = function()
                                ly.invoke(callback, true) -- animation completed
                            end
                        })
                    else
                        ly.invoke(callback, false) -- animation not completed
                    end
                end
            })
            return true --animated
        else
            ly.invoke(callback, false) -- animation not completed
            return false --not animated
        end
    end
    return false --not animated
end

--------------------------------------------------------------------------------
-- injection
--------------------------------------------------------------------------------

M.ly = function(instance)
    ly = instance
end

--------------------------------------------------------------------------------
-- public
--------------------------------------------------------------------------------

--global
M.cancel = _cancel

--animations
M.fadeIn = _fade_in
M.fadeOut = _fade_out
M.pulse = _pulse
M.flash = _flash
M.expand = _expand
M.rotate = _rotate

--------------------------------------------------------------------------------
-- export
--------------------------------------------------------------------------------

return M


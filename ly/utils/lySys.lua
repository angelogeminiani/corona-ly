-- ly.sys
--------------------------------------------------------------------------------
-- import
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
-- injected
--------------------------------------------------------------------------------

local ly

--------------------------------------------------------------------------------
-- SINGLETON
--------------------------------------------------------------------------------

-- Creates Module
local M = {
    deviceId = '',
    platformName='',
    isANDROID = false,
    isIOS = false,
    isWINPHONE=false,
    isMAC=false,
    isWIN=false,
    isSimulator=true,
    osANDROID = false,
    osIOS = false,
    osWINPHONE=false,
    osMAC=false,
    osWIN=false
}

--------------------------------------------------------------------------------
-- private
--------------------------------------------------------------------------------

local function _isANDROID()
    return (system.getInfo("platformName")=="Android") 
end

local function _isIOS()
    return (system.getInfo("platformName")=="iPhone OS") 
end

local function _isWINPHONE()
    return  (system.getInfo("platformName")=="WinPhone") 
end

local function _isMAC()
    return (system.getInfo("platformName")=="Mac OS X") 
end

local function _isWIN()
    return system.getInfo("platformName")=="Win"
end

local function _isSimulator()
    return ( system.getInfo("environment") == "simulator" ) 
end

local function _osANDROID()
    return ( system.getInfo("environment") ~= "simulator" ) and (system.getInfo("platformName")=="Android") 
end

local function _osIOS()
    return ( system.getInfo("environment") ~= "simulator" ) and (system.getInfo("platformName")=="iPhone OS") 
end

local function _osWINPHONE()
    return ( system.getInfo("environment") ~= "simulator" ) and (system.getInfo("platformName")=="WinPhone") 
end

local function _osMAC()
    return ( system.getInfo("environment") ~= "simulator" ) and (system.getInfo("platformName")=="Mac OS X") 
end

local function _osWIN()
    return ( system.getInfo("environment") ~= "simulator" ) and (system.getInfo("platformName")=="Win") 
end

--[[
Returns right path for a resource even if it's Android.
If Android, remember to rename resources due android files restrictions. 
i.e. from "myimage.png" to "myimage.png.txt".
Specifically, the following types can not be read from the resources directory: 
.html, .htm., .3gp, .m4v, .mp4, .png, .jpg, and .ttf.
@see: http://docs.coronalabs.com/api/library/system/pathForFile.html
--]]
local function pathForResource(fileName)
    if (_isANDROID()) then
        return system.pathForFile(fileName) 
    else
        return fileName
    end
end

local function _timestamp()
    local timestamp
    if (_isWIN()) then
        timestamp = ly.guid.generate()
    else
        timestamp = ly.date.timestamp() --err on windows
    end
    return timestamp
end

local function _hideKeyboard(callback, time)
    if(ly.isFunction(callback)) then
        time = time or 100
        ly.delay(function() 
            native.setKeyboardFocus( nil )
            ly.invoke(callback)
        end, time)
    else
        native.setKeyboardFocus( nil )
    end
end

local function _showKeyboard(textField, callback, time)
    if(ly.isNotNull(textField)) then
        if(ly.isFunction(callback)) then
            time = time or 100
            ly.delay(function() 
                native.setKeyboardFocus( textField )
                ly.invoke(callback)
            end, time)
        else
            native.setKeyboardFocus( textField )
        end
    else
        ly.invoke(callback)
    end
end

local function _exit()
    native.requestExit()
end

local function _openUrl(url)
    if(ly.isNotEmpty(url)) then
        system.openURL( url )
    end
end

--------------------------------------------------------------------------------
-- public
--------------------------------------------------------------------------------

M.ly = function(instance)
    ly = instance
end

-- fields
M.deviceId = system.getInfo("deviceID")
M.platformName = system.getInfo("platformName")
M.isANDROID = _isANDROID()
M.isIOS = _isIOS()
M.isWINPHONE = _isWINPHONE()
M.isMAC = _isMAC()
M.isWIN = _isWIN()
M.isSimulator = _isSimulator()
M.osANDROID = _osANDROID()
M.osIOS = _osIOS()
M.osWINPHONE = _osWINPHONE()
M.osMAC = _osMAC()
M.osWIN = _osWIN()

-- methods
M.pathForResource = pathForResource
M.timestamp = _timestamp

--native
M.hideKeyboard = _hideKeyboard
M.showKeyboard = _showKeyboard
M.exit = _exit

M.openUrl = _openUrl

--------------------------------------------------------------------------------
-- export
--------------------------------------------------------------------------------

return M


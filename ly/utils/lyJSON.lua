-- ly.json
--------------------------------------------------------------------------------
-- import
--------------------------------------------------------------------------------

local json = require('json')

--------------------------------------------------------------------------------
-- injected
--------------------------------------------------------------------------------

local ly

--------------------------------------------------------------------------------
-- SINGLETON
--------------------------------------------------------------------------------

local M = {};

--------------------------------------------------------------------------------
-- private
--------------------------------------------------------------------------------

local function _isJSONObject(value)
    if (ly.isString(value)) then
        value = value or ''
        value = ly.str.trim(value)
        return ly.str.starts(value, '{') and ly.str.ends(value, '}') 
    else
        return ly.isObject(value)
    end
end

local function _isJSONArray(value)
    if (ly.isString(value)) then
        value = value or ''
        value = ly.str.trim(value)
        return ly.str.starts(value, '[') and ly.str.ends(value, ']') 
    else
        return ly.isArray(value)
    end
end

local function _decode(item_or_text)
    if (ly.isString(item_or_text)) then
        return json.decode(item_or_text)
    elseif(ly.isTable(item_or_text)) then
        return item_or_text
    else
        return nil
    end
end

local function _encode(item_or_text)
    if (ly.isString(item_or_text)) then
        return item_or_text
    elseif(ly.isTable(item_or_text)) then
        return json.encode(item_or_text)
    else
        return '{}'
    end 
end

local function _getValue(t, keyOrPath, def_val, deep)
    local result = nil
    t = _decode(t)
    if (ly.isNotNull(t))then
        result = ly.getValue(t, keyOrPath, def_val, deep)
    end
    return result or def_val
end

local function _setValue(t, keyOrPath, value, deep)
    t = _decode(t)
    if (ly.isNotNull(t))then
        ly.setValue(t, keyOrPath, value, deep)
    end
    return M
end

local function _getNumber(t, keyOrPath, def_val, deep)
    return ly.toNumber( _getValue(t, keyOrPath, def_val, deep), def_val )
end

local function _getString(t, keyOrPath, def_val, deep)
    return ly.toString( _getValue(t, keyOrPath, def_val, deep), def_val )
end

local function _getBoolean(t, keyOrPath, def_val, deep)
    return ly.toBoolean( _getValue(t, keyOrPath, def_val, deep), def_val )
end

--------------------------------------------------------------------------------
-- injection
--------------------------------------------------------------------------------

M.ly = function(instance)
    ly = instance
end

--------------------------------------------------------------------------------
-- public
--------------------------------------------------------------------------------

M.decode = _decode
M.encode = _encode
M.isJSONObject = _isJSONObject
M.isJSONArray = _isJSONArray
M.getValue = _getValue
M.setValue = _setValue
M.getNumber = _getNumber
M.getString = _getString
M.getBoolean = _getBoolean

--------------------------------------------------------------------------------
-- export
--------------------------------------------------------------------------------

return M



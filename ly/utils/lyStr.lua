--------------------------------------------------------------------------------
-- import
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
-- injected
--------------------------------------------------------------------------------

local ly

--------------------------------------------------------------------------------
-- SINGLETON
--------------------------------------------------------------------------------

-- Creates Module
local M = {}

--------------------------------------------------------------------------------
-- private
--------------------------------------------------------------------------------

local function _fillRight(str, len, char)
    if char == nil then char = ' ' end
    return str .. string.rep(char, len - #str)
end

local function _fillLeft(str, len, char)
    if char == nil then char = ' ' end
    return string.rep(char, len - #str) .. str
end

local function _quote(text)
    if(ly.isString(text) and string.len(text)>0) then
        return string.format("%q", text)  
    else
        return string.format("%q", "")
    end
end

local function _starts(String,Start)
    if (ly.isNotEmpty(String) and ly.isNotEmpty(Start)) then
        return string.sub(String,1,string.len(Start))==Start
    else
        return false
    end
end

local function _ends(String,End)
    return End=='' or string.sub(String,-string.len(End))==End
end

local function _concat(sep, ...)
    local result = '';
    for i, path in ipairs(arg) do
        if ((not _ends(result, sep)) and (not _starts(path, sep))) then
            --nor result or path have separator
            result = result..sep..path 
        else 
            if ((_ends(result, sep)) and (_starts(path, sep))) then
                -- both have separator
                result = result..path
            else
                --result or path already have separator
                result = result..path  
            end 
        end
    end
    
    return result;
end

local function _concatPath(...)    
    return _concat('/', unpack(arg));
end

local function _split(text, delimiter)
    if(delimiter=='.') then
        delimiter = '%.'
    else
        delimiter = delimiter or '%.'
    end
    local result = { }
    if(ly.isString(text)) then
        local from  = 1
        local delim_from, delim_to = string.find( text, delimiter, from  )
        while delim_from do
            table.insert( result, string.sub( text, from , delim_from-1 ) )
            from  = delim_to + 1
            delim_from, delim_to = string.find( text, delimiter, from  )
        end
        table.insert( result, string.sub( text, from  ) )
    end
    return result
end

-- template replacemnt
-- replaces any %{field}% by variables.field
local function _template(text, variables, prefix, suffix)
    if(ly.isString(text)) then
        prefix = prefix or '%%{'
        suffix = suffix or '}%%'
        local pattern = prefix..'(.-)'..suffix
        --return (text:gsub('%%{(.-)}%%', function (key) return tostring(variables[key] or '') end))
        return (text:gsub(pattern, 
        function (key) 
            return tostring(variables[key] or '') 
        end))
    else
        return ''
    end
end

local function _replace(text, from, to)  --Replaces @from to @to in @str
    if not from then return nil end
    text = tostring(text)
    local pcs = _split(text, from)
    text = pcs[1]
    for n=2,#pcs do
        text = text..to..pcs[n]
    end
    return text
end

local function _find(text, match_text, startIndex)  --Finds @match in @str optionally after @startIndex
    if not match_text then return nil end
    text = tostring(text)
    local _ = startIndex or 1
    local _s = nil
    local _e = nil
    local _len = match_text:len()
    while true do
        local _t = text:sub( _ , _len + _ - 1)
        if _t == match_text then
            _s = _
            _e = _ + _len - 1
            break
        end
        _ = _ + 1
        if _ > text:len() then break end
    end
    if _s == nil then return nil else return _s, _e end
end

--
-- Find the last instance of a pattern in a string.
--
local function _findLastIndex(s, pattern, plain)
    local curr = 0 --not found
    repeat
        local next = s:find(pattern, curr + 1, plain)
        if (next) then curr = next end
    until (not next)
    if (curr > 0) then
        return curr
    end	
end

local function _indexOf(s, pattern)
    local result = 0
    result = s:find(pattern, 1, true)
    return result or 0
end

local function _chunk(text, size)  --Splits @str into chunks of length @size
    if not size then return nil end
    text = tostring(text)
    local num2App = size - (#text%size)
    text = text..(string.rep(string.char(0), num2App) or "")
    assert(#text%size==0)
    local chunks = {}
    local numChunks = #text / size
    local chunk = 0
    while chunk < numChunks do
        local start = chunk * size + 1
        chunk = chunk+1
        if start+size-1 > #text-num2App then
            if text:sub(start, #text-num2App) ~= (nil or "") then
                chunks[chunk] = text:sub(start, #text-num2App)
            end
        else
            chunks[chunk] = text:sub(start, start+size-1)
        end
    end
    return chunks
end

local function _toClassPath(path)
    local result = nil
    if (path) then
        result = _replace(path, '/', '.')
        if (_starts(result, '.')) then
            result = result:sub(2)
        end
        if (_ends(result, '.')) then
            result = result:sub(1,#result-1)
        end
    end
    return result
end

local function _toCharTable(text)  --Returns table of @str's chars
    if not text then return nil end
    text = tostring(text)
    local chars = {}
    for n=1,#text do
        chars[n] = text:sub(n,n)
    end
    return chars
end

local function _toByteTable(str)  --Returns table of @str's bytes
    if not str then return nil end
    str = tostring(str)
    local bytes = {}
    for n=1,#str do
        bytes[n] = str:byte(n)
    end
    return bytes
end

local function _fromCharTable(chars)  --Returns string made of chracters in @chars
    if not chars or type(chars)~="table" then return nil end
    return table.concat(chars)
end

local function _fromByteTable(bytes)  --Returns string made of bytes in @bytes
    if not bytes or type(bytes)~="table" then return nil end
    local str = ""
    for n=1,#bytes do
        str = str..string.char(bytes[n])
    end
    return str
end

local function _contains(text,lookup)  --Returns true if @text contains @lookup
    if not text then return nil end
    text = tostring(text)
    for n=1, #text-#lookup+1 do
        if text:sub(n,n+#lookup-1) == lookup then return true end
    end
    return false
end

local function _trunc(str, lenght, opt_addellipses)
    local result = str or ''
    if (string.len(result) > lenght) then
        result = string.sub(result, 1, lenght)
        if opt_addellipses then
            result = result..'...'
        end
    end
    return result
end

local function _trim(str)  --Trim @str of initial/trailing whitespace
    if not str then return nil end
    str = tostring(str)
    return (str:gsub("^%s*(.-)%s*$", "%1"))
end

local function _firstLetterUpper(str)  --Capitilizes first letter of @str
    if not str then return nil end
    str = tostring(str)
    str = str:gsub("%a", string.upper, 1)
    return str
end

local function _titleCase(str)  --Changes @str to title case
    if not str then return nil end
    str = tostring(str)
    local function tchelper(first, rest)
        return first:upper()..rest:lower()
    end
    str = str:gsub("(%a)([%w_']*)", tchelper)
    return str
end

local function _isRepetition(str, pat)  --Checks if @str is a repetition of @pat
    if not str then return nil end
    str = tostring(str)
    return "" == str:gsub(pat, "")
end

local function _isRepetitionWS(str, pat)  --Checks if @str is a repetition of @pat seperated by whitespaces
    if not str then return nil end
    str = tostring(str)
    return not str:gsub(pat, ""):find"%S"
end

local function _urlDecode(str)  --Url decodes @str
    if not str then return nil end
    str = tostring(str)
    str = string.gsub (str, "+", " ")
    str = string.gsub (str, "%%(%x%x)", function(h) return string.char(tonumber(h,16)) end)
    str = string.gsub (str, "\r\n", "\n")
    return str
end

local function _urlEncode(str)  --Url encodes @str
    if not str then return nil end
    str = tostring(str)
    if (str) then
        str = string.gsub (str, "\n", "\r\n")
        str = string.gsub (str, "([^%w ])", function (c) return string.format ("%%%02X", string.byte(c)) end)
        str = string.gsub (str, " ", "+")
    end
    return str
end

local function _isEmail(str)  --Checks if @str is a valid email address
    if not str then return nil end
    str = tostring(str)
    if (str:match("[A-Za-z0-9%.%%%+%-]+@[A-Za-z0-9%.%%%+%-]+%.%w%w%w?%w?")) then
        return true
    else
        return false
    end
end


--------------------------------------------------------------------------------
-- injection
--------------------------------------------------------------------------------

M.ly = function(instance)
    ly = instance
end

--------------------------------------------------------------------------------
-- public
--------------------------------------------------------------------------------

-- methods
M.find = _find
M.findLastIndex = _findLastIndex
M.indexOf = _indexOf
M.starts = _starts
M.ends = _ends
M.contains = _contains

M.quote = _quote
M.template = _template
M.replace = _replace
M.urlDecode = _urlDecode
M.urlEncode = _urlEncode
M.trunc = _trunc
M.trim = _trim
M.firstLetterUpper = _firstLetterUpper
M.titleCase = _titleCase
M.fillRight = _fillRight
M.fillLeft = _fillLeft
    
M.split = _split
M.chunk = _chunk

M.concatPath = _concatPath
M.toClassPath = _toClassPath
M.toCharTable = _toCharTable
M.toByteTable = _toByteTable
M.fromCharTable = _fromCharTable
M.fromByteTable = _fromByteTable

M.isRepetition = _isRepetition
M.isRepetitionWS = _isRepetitionWS
M.isEmail = _isEmail

--------------------------------------------------------------------------------
-- export
--------------------------------------------------------------------------------

return M




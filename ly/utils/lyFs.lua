--------------------------------------------------------------------------------
-- import
--------------------------------------------------------------------------------
local lfs = require( "lfs" )

--------------------------------------------------------------------------------
-- injected
--------------------------------------------------------------------------------

local ly

--------------------------------------------------------------------------------
-- SINGLETON
--------------------------------------------------------------------------------

-- Creates Module
local M = {}

--------------------------------------------------------------------------------
-- CONSTANTS
--------------------------------------------------------------------------------

local MODE_FILE = 'file'
local MODE_DIR = 'directory'

--------------------------------------------------------------------------------
-- private
--------------------------------------------------------------------------------

local function _attributes(path)
    local result = {}
    if (path) then
        result =  lfs.attributes(path) 
    end
    return result
end

local function _attribute(path, attr_name)
    local result = ''
    if (path) then
        result =  lfs.attributes(path, attr_name) 
    end
    return result
end

local function _dir(path, parent, result, filter)
    result = result or {}
    path = path or system.pathForFile( "", system.DocumentsDirectory )
    parent = parent or ''
    
    for item in lfs.dir(path) do
        -- item is the current file or directory name
        if (item~='.' and item~='..') then
            local partial_item_path = ly.path.join(parent, item)
            local full_item_path = ly.path.join(path, item)
            local mode = _attribute(full_item_path, 'mode')
            if(not ly.isEmpty(partial_item_path))then
                if (filter) then
                    if (filter==mode) then
                        result[partial_item_path]=mode  
                    elseif(ly.str.starts(filter, '.') and ly.path.getExtension(partial_item_path)==filter) then
                        result[partial_item_path]=mode  
                    end
                else
                    result[partial_item_path]=mode  
                end
                if(mode==MODE_DIR)then
                    _dir(full_item_path, item, result, filter)
                end 
            end
        end
    end
    return result
end

local function _list(path)
    return _dir(path) 
end

local function _listFiles(path, filter)
    filter = filter or MODE_FILE --all files or '.ext'
    return _dir(path, nil, nil, filter) 
end

local function _listDirs(path)
    return _dir(path, nil, nil, MODE_DIR) 
end

-- return true if file exists. 
--@fname: File Name i.e. "/images/image.png", 
--@path: root path i.e. "system.ResourceDirectory"
local function _fileExists( fname, path )
    
    local results = false
    
    local filePath = system.pathForFile( fname, path ) or fname 
    
    --filePath will be 'nil' if file doesn't exist and the path is 'system.ResourceDirectory'
    if ( filePath ) then
        filePath = io.open( filePath, 'r' )
    end
    
    if ( filePath ) then
        --print( "File found: " .. fname )
        --clean up file handles
        filePath:close()
        filePath = nil
        
        results = true
    else
        --print( "File does not exist: " .. fname )
    end
    
    return results
end

local function _read(fileName)
    if (not _fileExists(fileName)) then
        return nil
    end
    local result = nil
    local rfile = io.open(fileName, 'rb') --binary
    if (rfile) then
        result = rfile:read( "*a" ) 
        io.close(rfile)
        rfile = nil
    end
    return result
end

local function _write(fileName, data)
    local wfile = io.open( fileName, "wb" ) --binary
    wfile:write( data )
    
    io.close( wfile )
    wfile = nil
end

local function _readText(fileName)
    local result = ''
    if (not _fileExists(fileName)) then
        return ''
    end
    
    local rfile = io.open(fileName, 'r')
    if (rfile) then
        result = rfile:read( "*a" ) 
        io.close(rfile)
        rfile = nil
    end
    
    return result
end

local function _writeText(fileName, text)
    local wfile = io.open( fileName, "w" )
    wfile:write( text )
    
    io.close( wfile )
    wfile = nil
end

---Creates subfolders. Subfolders can be created in system.DocumentsDirectory
--and system.TemporaryDirectory
-- @param paths
-- @param base
-- @return

local function _mkdirs(path, baseDir)
    if(path and path:len()>0)then
        baseDir = baseDir or system.DocumentsDirectory
        
        --get raw path to documents directory
        local docs_path = system.pathForFile( '', baseDir )
        
        --change current working directory
        local success = lfs.chdir( docs_path ) --returns 'true' on success
        if ( success ) then
            local paths = ly.str.split(path, '/')
            ly.forEach(paths, function(value, key, index) 
                if (ly.isNotNull(value) and ly.isString(value) and (value:len()>0)) then
                    lfs.mkdir( value ) 
                    success = lfs.chdir( lfs.currentdir() .. '/' .. value )
                    if (not success) then
                        return false
                    end
                end
            end)
        end
    end
end

local function _remove(pathForFile, base)
    if (base) then
        pathForFile = system.pathForFile( pathForFile, base )
    end
    local result, reason = os.remove( pathForFile )
    return result, reason --boolean, string
end

local function _rename(pathForFileOld, pathForFileNew)
    local result, reason = os.rename( pathForFileOld, pathForFileNew ) 
    return result, reason --boolean, string
end

local function _copyFile( srcName, srcPath, dstName, dstPath, overwrite )
    
    local results = false
    
    local srcPath = _fileExists( srcName, srcPath )
    
    if ( srcPath == false ) then
        return nil  -- nil = source file not found
    end
    
    --check to see if destination file already exists
    if not ( overwrite ) then
        if ( _fileExists( dstName, dstPath ) ) then
            return 1  -- 1 = file already exists (don't overwrite)
        end
    end
    
    --get paths
    local rfilePath = system.pathForFile( srcName, srcPath )
    local wfilePath = system.pathForFile( dstName, dstPath )
    
    --creates subfolder if does not exists
    local wdirPath = ly.path.getDirectory(dstName)
    _mkdirs(wdirPath, dstPath)
    
    --copy the source file to the destination file
    local rfh = io.open( rfilePath, "rb" )
    local wfh = io.open( wfilePath, "wb" )
    
    if not ( wfh ) then
        print( "writeFileName open error!" )
        return false
    else
        --read the file from 'system.ResourceDirectory' and write to the destination directory
        local data = rfh:read( "*a" )
        if not ( data ) then
            print( "read error!" )
            return false
        else
            if not ( wfh:write( data ) ) then
                print( "write error!" )
                return false
            end
        end
    end
    
    results = 2  -- 2 = file copied successfully!
    
    --clean up file handles
    rfh:close()
    wfh:close()
    
    return results
end
--------------------------------------------------------------------------------
-- public
--------------------------------------------------------------------------------

M.ly = function(instance)
    ly = instance
end

-- methods
M.attributes = _attributes
M.attribute = _attribute
M.list = _list
M.listFiles = _listFiles
M.listDirs = _listDirs
M.fileExists = _fileExists
M.read = _read
M.write = _write
M.readText = _readText
M.writeText = _writeText
M.mkdirs = _mkdirs
M.remove = _remove
M.rename = _rename
M.copyFile = _copyFile


--------------------------------------------------------------------------------
-- export
--------------------------------------------------------------------------------

return M



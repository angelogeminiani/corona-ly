--ly.debug
--------------------------------------------------------------------------------
-- injected
--------------------------------------------------------------------------------

local ly

--------------------------------------------------------------------------------
-- SINGLETON
--------------------------------------------------------------------------------

local LEVEL_DEBUG = 'debug'
local LEVEL_INFO = 'info'
local LEVEL_WARN = 'warning'
local LEVEL_ERROR = 'error'

-- Creates Module
local M = {
    _enable_print = true,
    _level = LEVEL_DEBUG
}

local reallyPrint = print
function print(...)
    if M._enable_print then
        reallyPrint(unpack(arg))
    end
end

--------------------------------------------------------------------------------
-- private
--------------------------------------------------------------------------------

local function _enable_print(value)
    M._enable_print = ly.toBoolean(value)
end

local function _set_level(level)
    M._level = level or LEVEL_DEBUG
end

local function _is_level_enabled(level)
    local response = true
    if(ly.isNotEmpty(level)) then
        if(level~=M._level) then
            if(M._level == LEVEL_INFO) then
                response = (level==LEVEL_WARN or level==LEVEL_ERROR)
            elseif(M._level == LEVEL_WARN) then
                response = (level==LEVEL_ERROR)
            elseif(M._level == LEVEL_ERROR) then
                response = false
            end
        end
    end
    return response
end

local function _build_message(level, message)
    level = level or M.LEVEL_DEBUG
    local dt = os.date( "%c" )
    local response = {
        level = '['..level:upper()..']',
        date = dt,
        text = message
    }
    --'['..level:upper()..']'..' '..dt..' - '..message
    return response
end

local function _log(level, msg, callback)
    local message = _build_message(level, msg)
    if (_is_level_enabled(level)) then
        if(ly.sys.isSimulator)then
            print(message.level..' '..message.date..' - '..message.text)
            ly.invoke(callback)
        else
            ly.dialog.alert(message.level..' '..message.date, message.text, callback)
        end
    else
        ly.invoke(callback)
    end
end

local function _debug(message, callback)
    _log(M.LEVEL_DEBUG, message, callback)
end

local function _info(message, callback)
    _log(M.LEVEL_INFO, message, callback)
end

local function _warn(message, callback)
    _log(M.LEVEL_WARN, message, callback)
end

local function _error(message, callback)
    _log(M.LEVEL_ERROR, message, callback)
end

--------------------------------------------------------------------------------
-- public
--------------------------------------------------------------------------------

M.ly = function(instance)
    ly = instance
end

--const
M.LEVEL_DEBUG = LEVEL_DEBUG
M.LEVEL_INFO = LEVEL_INFO
M.LEVEL_WARN = LEVEL_WARN
M.LEVEL_ERROR = LEVEL_ERROR

-- init
M.enablePrint = _enable_print
M.setLevel = _set_level

-- methods
M.log = _log
M.debug = _debug
M.info = _info
M.warn = _warn
M.error = _error

--------------------------------------------------------------------------------
-- export
--------------------------------------------------------------------------------

return M



--------------------------------------------------------------------------------
-- import
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
-- injected
--------------------------------------------------------------------------------

local ly

--------------------------------------------------------------------------------
-- SINGLETON
--------------------------------------------------------------------------------

-- Creates Module
local M = {}

--------------------------------------------------------------------------------
-- private
--------------------------------------------------------------------------------

local function _isHttp(p)
    p = tostring(p):lower()
    return ly.str.starts(p, M.HTTP)
end

local function _isHttps(p)
    p = tostring(p):lower()
    return ly.str.starts(p, M.HTTPS)
end

local function _isabsolute(p)
    p = M.translate(p, "/")
    if (ly.sys.isWIN) then
        return M.getDrive(p):len()>0
    else
        return ly.str.starts(p, "/")
    end
end

--
-- Get the absolute file path from a relative path. The requested
-- file path doesn't actually need to exist.
--

local function _getabsolute(p)
    -- normalize the target path
    local result = M.translate(p, "/")
    if (result == "") then result = "." end
    
    -- if the directory is already absolute I don't need to do anything
    if  (M.isAbsolute(p)) then
        return p
    end
    
    -- split up the supplied relative path and tackle it bit by bit
    local tokens = ly.str.split(p, '/')
    for n, part in ipairs(tokens) do
        if (part == "" and n == 1) then
            result = "/"
        elseif (part == "..") then
            result = M.getDirectory(result)
        elseif (part ~= ".") then
            result = M.join(result, part)
        end
    end
    
    -- if I end up with a trailing slash remove it
    if(ly.str.ends(result, "/"))then
        result = result:sub(1, -2)
    end
    
    return result
end

--
-- Retrieve the filename portion of a path, without any extension.
--
local function _getbasename(p)
    local name = M.getName(p)
    local i = ly.str.findLastIndex(name, ".", true)
    if (i) then
        return name:sub(1, i - 1)
    else
        return name
    end
end

--
-- Retrieve the directory portion of a path, or an empty string if 
-- the path does not include a directory.
--
local function _getdirectory(p)
    local i = ly.str.findLastIndex(p, "/", true)
    if (i) then
        if i > 1 then i = i - 1 end
        return p:sub(1, i)
    else
        return "."
    end
end

--
-- Retrieve the drive letter, if a Windows path.
--
local function _getdrive(p)
    local ch1 = p:sub(1,1)
    local ch2 = p:sub(2,2)
    if ch2 == ':' then
        return ch1
    else
        return nil
    end
end

--
-- Retrieve the file extension. i.e. ".txt"
--
local function _getextension(p)
    local i = ly.str.findLastIndex(p, ".", true)
    if (i) then
        return p:sub(i)
    else
        return ""
    end
end

--
-- Retrieve the filename portion of a path.
--
local function _getname(p)
    local result = p
    local i = ly.str.findLastIndex(p, "[/\\]")
    if (i) then
        result = p:sub(i + 1)
    end
    
    i = ly.str.indexOf(result, "?")
    if(i>0) then
        result = result:sub(1, i-1)
    end
    
    return result
end

--
-- Returns the relative path from src to dest.
--
local function _getrelative(src, dst)
    -- normalize the two paths
    src = M.getAbsolute(src)
    dst = M.getAbsolute(dst)
    
    -- same directory?
    if (src == dst) then
        return "."
    end
    
    -- dollar macro? Can't tell what the real path is; use absolute
    -- This enables paths like $(SDK_ROOT)/include to work correctly.
    if ly.str.starts(dst, "$") then
        return dst
    end
    
    src = src .. "/"
    dst = dst .. "/"
    
    -- find the common leading directories
    local idx = 0
    while (true) do
        local tst = src:find("/", idx + 1, true)
        if tst then
            if src:sub(1,tst) == dst:sub(1,tst) then
                idx = tst
            else
                break
            end
        else
            break
        end
    end
    
    -- if they have nothing in common return absolute path
    local first = src:find("/", 0, true)
    if idx <= first then
        return dst:sub(1, -2)
    end
    
    -- trim off the common directories from the front 
    src = src:sub(idx + 1)
    dst = dst:sub(idx + 1)
    
    -- back up from dst to get to this common parent
    local result = ""		
    idx = src:find("/")
    while (idx) do
        result = result .. "../"
        idx = src:find("/", idx + 1)
    end
    
    -- tack on the path down to the dst from here
    result = result .. dst
    
    -- remove the trailing slash
    return result:sub(1, -2)
end


--
-- Returns true if the filename represents a C/C++ source code file. This check
-- is used to prevent passing non-code files to the compiler in makefiles. It is
-- not foolproof, but it has held up well. I'm open to better suggestions.
--
local function _iscfile(fname)
    local extensions = { ".c", ".s", ".m" }
    local ext = M.getExtension(fname):lower()
    return table.contains(extensions, ext)
end

local function _iscppfile(fname)
    local extensions = { ".cc", ".cpp", ".cxx", ".c", ".s", ".m", ".mm" }
    local ext = M.getExtension(fname):lower()
    return table.contains(extensions, ext)
end

--
-- Returns true if the filename represents a Windows resource file. This check
-- is used to prevent passing non-resources to the compiler in makefiles.
--
local function _isresourcefile(fname)
    local extensions = { ".rc" }
    local ext = M.getExtension(fname):lower()
    return table.contains(extensions, ext)
end

--
-- Join two pieces of a path together into a single path.
--
local function _join(...)
    local result = '';
    for i, path in ipairs(arg) do
        if ((not ly.str.ends(result, '/')) and (not ly.str.starts(path, '/'))) then
            --nor result or path have separator
            --result = result..'/'..path 
            if (_isHttp(path)) then
                result = result..path  
            else
                result = result..'/'..path 
            end
        else 
            if ((ly.str.ends(result, '/')) and (ly.str.starts(path, '/'))) then
                -- both have separator
                result = result..path
            else
                --result or path already have separator
                result = result..path  
            end 
        end
    end
    
    return result;
end


--
-- Takes a path which is relative to one location and makes it relative
-- to another location instead.
--

local function _rebase(p, oldbase, newbase)
    p = M.getAbsolute(M.join(oldbase, p))
    p = M.getRelative(newbase, p)
    return p
end


--
-- Convert the separators in a path from one form to another. If `sep`
-- is nil, then a platform-specific separator is used.
--

local function _translate(p, sep)
    if (type(p) == "table") then
        local result = { }
        for _, value in ipairs(p) do
            table.insert(result, M.translate(value))
        end
        return result
    else
        if (not sep) then
            if (ly.sys.isWIN) then
                sep = "\\"
            else
                sep = "/"
            end
        end
        local result = p:gsub("[/\\]", sep)
        return result
    end
end


--
-- Converts from a simple wildcard syntax, where * is "match any"
-- and ** is "match recursive", to the corresponding Lua pattern.
--
-- @param pattern
--    The wildcard pattern to convert.
-- @returns
--    The corresponding Lua pattern.
--

local function _wildcards(pattern)
    -- Escape characters that have special meanings in Lua patterns
    pattern = pattern:gsub("([%+%.%-%^%$%(%)%%])", "%%%1")
    
    -- Replace wildcard patterns with special placeholders so I don't
    -- have competing star replacements to worry about
    pattern = pattern:gsub("%*%*", "\001")
    pattern = pattern:gsub("%*", "\002")
    
    -- Replace the placeholders with their Lua patterns
    pattern = pattern:gsub("\001", ".*")
    pattern = pattern:gsub("\002", "[^/]*")
    
    return pattern
end

--------------------------------------------------------------------------------
-- public
--------------------------------------------------------------------------------

M.ly = function(instance)
    ly = instance
end

-- fields
M.HTTP = 'http'
M.HTTPS = 'https'

-- methods
M.isHttp = _isHttp
M.isHttps = _isHttps
M.isAbsolute = _isabsolute
M.getAbsolute = _getabsolute
M.getBasename = _getbasename
M.getDirectory = _getdirectory
M.getDrive = _getdrive
M.getExtension = _getextension
M.getName = _getname
M.getRelative = _getrelative
M.isCFile = _iscfile
M.isCppFile = _iscppfile
M.isResourceFile = _isresourcefile
M.join = _join
M.rebase = _rebase
M.translate = _translate
M.wildcards = _wildcards

--------------------------------------------------------------------------------
-- export
--------------------------------------------------------------------------------

return M
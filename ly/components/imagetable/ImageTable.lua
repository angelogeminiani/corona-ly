---
-- Helper class that hold spritesheets and images
-- 

-- 

--ly.components.imagetable.ImageTable
--------------------------------------------------------------------------------
-- import
--------------------------------------------------------------------------------
local ly = require('ly.ly')

local Super = require('ly.components.BaseComponent') -- Superclass reference


--------------------------------------------------------------------------------
-- const
--------------------------------------------------------------------------------

local BASE_DIR = 'baseDir'
local HEIGHT = 'height'
local WIDTH = 'width'

--------------------------------------------------------------------------------
-- private
--------------------------------------------------------------------------------

local function _add(self, key, fileName, options)
    local baseDir = options[BASE_DIR]
    self._data[key] = {
        filename = fileName,
        baseDir = baseDir,
        sheet= graphics.newImageSheet( fileName, baseDir, options ),
        options = options
    }
end

local function _remove(self, key)
    self._data[key] = nil
end

local function _get_image(self, key, index)
    local item = self._data[key]
    if(ly.isNotNull(item))then
        if(item.sheet) then
            --indexed sheet
            local sheet = item.sheet
            local options = item.options
            index = ly.conditional(ly.isNotNull(index), index, 1)
            return display.newImageRect(sheet, index, options[WIDTH], options[HEIGHT])
        elseif(ly.isNotEmpty(item.filename)) then
            --single image
            return display.newImage(item.filename, item.baseDir)
        end
    end
    return nil
end

--------------------------------------------------------------------------------
-- init
--------------------------------------------------------------------------------

local function _init(self)
    local options = self._options
    
    self._data = {}
    
end

local function _free(self)
    self._data = nil
end

--------------------------------------------------------------------------------
-- constructor
--------------------------------------------------------------------------------

local ComponentClass = Class(Super) --inherits Object Class

function ComponentClass:initialize(options)
    if (Super.initialize(self, options)) then
        self._className = 'ly.components.imagetable.ImageTable'
        _init(self)
        return true
    else
        return false
    end
end

function ComponentClass:finalize()
    if(Super.finalize(self)) then
        -- clear internal fields
        _free(self)
        
        return true
    else
        return false
    end
end

--------------------------------------------------------------------------------
-- public
-------------------------------------------------------------------------------- 

--SINGLETON INSTANCE
ComponentClass.singleton = ComponentClass.singleton or ComponentClass:new({})

function ComponentClass:add(key, fileName, options)
    options = options or {}
    options.baseDir = options.baseDir or system.ResourceDirectory
    if(key and fileName)then
        _add(self, key, fileName, options)
    end
    return self
end

function ComponentClass:remove(key)
    _remove(self, key)
    return self
end

function ComponentClass:getImage(key, index)
    return _get_image(self, key, index)
end
--------------------------------------------------------------------------------
-- export
-------------------------------------------------------------------------------- 

return ComponentClass




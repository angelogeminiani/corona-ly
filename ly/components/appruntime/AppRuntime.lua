local CLASS_NAME = 'ly.components.appruntime.AppRuntime'

--------------------------------------------------------------------------------
-- import
--------------------------------------------------------------------------------
local ly = require('ly.ly')

local Super = require('ly.components.BaseComponent') -- Superclass reference

--------------------------------------------------------------------------------
-- CONST
--------------------------------------------------------------------------------

local TYPE_LOCAL = 'local'
local TYPE_REMOTE = 'remote'
local STATE_ACTIVE = 'active'
local STATE_INACTIVE = 'inactive'

--inbound
local IN_EVENT_SYSTEM = 'system'
local IN_EVENT_KEY = 'key'
local IN_EVENT_NOTIFICATION = 'notification'
local IN_EVENT_ACCELEROMETER = 'accelerometer'
local IN_EVENT_LOCATION = 'location'

--outbound
local EVENT_SYS_ON_START = 'applicationStart'
local EVENT_SYS_ON_SUSPEND = 'applicationSuspend'
local EVENT_SYS_ON_EXIT = 'applicationExit'
local EVENT_SYS_ON_OPEN_URL = 'applicationOpen' --open URL

local EVENT_KEY_ON_KEY = IN_EVENT_KEY
local EVENT_KEY_ON_BACK = 'back'
local EVENT_KEY_ON_SUBMIT = 'submit' --tab or enter keys

local EVENT_NOTIFICATION_ON_REGISTER = 'notification_register'
local EVENT_NOTIFICATION_ON_LOCALE = 'notification_locale'
local EVENT_NOTIFICATION_ON_REMOTE = 'notification_remote'

local EVENT_ON_ACCELERATE = 'accelerate'
local EVENT_ON_SHAKE = 'shake'
local EVENT_ON_LONGSHAKE = 'longshake'

local EVENT_ON_LOCATION = 'location'

local LAUNCH_ARGS = ...

--------------------------------------------------------------------------------
-- trigger
--------------------------------------------------------------------------------

local function _do_key(self, event)
    self:dispatchEvent(event)
end

local function _do_key_back(self, event)
    self:dispatchEvent({
        target = self,
        name = EVENT_KEY_ON_BACK,
        keyName = event.keyName
    })
end

local function _do_key_submit(self, event)
    self:dispatchEvent({
        target = self,
        name = EVENT_KEY_ON_SUBMIT,
        keyName = event.keyName
    })
end

local function _do_notification(self, event)
    local notification_event = {
        name = ly.conditional(TYPE_LOCAL==event.type, EVENT_NOTIFICATION_ON_LOCALE, EVENT_NOTIFICATION_ON_REMOTE),
        target = self,
        event = event,
        notification = {
            alert = event.alert, --string value consisting of the message to display,
            applicationState = event.applicationState, --string value of either "active" or "inactive"
            badge = event.badge, --number representing the badge count (iOS only)
            custom = event.custom, --if this table exists, there may be data from the push service that helps your app react to the data. This is known as "deep linking"
            sound = event.sound, --string value representing a sound file, i.e. "alarm.caf"
        }
    }
    self:dispatchEvent(notification_event)
end

local function _do_notification_registration(self, data)
    local registration_event = ly.defaults({
        name = EVENT_NOTIFICATION_ON_REGISTER,
        target = self
    }, data)
    self:dispatchEvent(registration_event)
end

local function _do_shake(self, event)
    local accelerometer_event = {
        name = EVENT_ON_SHAKE,
        target = self,
        event = event
    }
    ly.defaults(accelerometer_event, event)
    self:dispatchEvent(accelerometer_event)
end

local function _do_longshake(self, event)
    local accelerometer_event = {
        name = EVENT_ON_LONGSHAKE,
        target = self,
        event = event,
        isLongShake = true
    }
    ly.defaults(accelerometer_event, event)
    self:dispatchEvent(accelerometer_event)
    --shake event
    accelerometer_event.name = EVENT_ON_SHAKE
    self:dispatchEvent(accelerometer_event)
end

local function _do_accelerate(self, event)
    local accelerometer_event = {
        name = EVENT_ON_ACCELERATE,
        target = self,
        event = event
    }
    ly.defaults(accelerometer_event, event)
    self:dispatchEvent(accelerometer_event)
end

local function _do_location(self, event)
    local location_event = {
        name = EVENT_ON_LOCATION,
        target = self,
        event = event --embedded native event
    }
    ly.defaults(location_event, event)
    self:dispatchEvent(location_event)
end

--------------------------------------------------------------------------------
-- handlers
--------------------------------------------------------------------------------

local function _handle_sys_event(self, name)
    self:dispatchEvent({
        name = name,
        target = self
    })
end

local function _handle_key_event(self, event)
    local options = self._options
    local phase = event.phase
    local name = event.keyName
    local handle_back = options.back and (ly.sys.isSimulator or ly.sys.isANDROID or ly.sys.isWINPHONE)
    
    if('back'==name and handle_back and phase=='down') then
        ly.logger.debug('Triggering "back" event')
        _do_key_back(self, event)
        return true
    elseif (('tab'==name or 'enter'==name)  and phase=='down') then
        _do_key_submit(self, event)
    else
        --unhadled
        _do_key(self, event)
        return false
    end
    return false
end

local function _handle_notification_event(self, event)
    if ( event.type == TYPE_REMOTE ) then
        
        -- Handle the push notification
        if ( event.badge and event.badge > 0 ) then 
            native.setProperty( "applicationIconBadgeNumber", event.badge - 1 )
        end
        _do_notification(self, event)
        
    elseif ( event.type == TYPE_LOCAL ) then
        
        -- Handle the local notification
        local badge_num = native.getProperty( "applicationIconBadgeNumber" )
        badge_num = badge_num - 1
        native.setProperty( "applicationIconBadgeNumber", badge_num )
        _do_notification(self, event)
        
    elseif ( event.type == "remoteRegistration" ) then 
        
        --code to register your device with the service
        local deviceToken = event.token
        local deviceType = 1  --default to iOS
        if ( ly.sys.isANDROID ) then
            deviceType = 3
        end
        
        _do_notification_registration(self, {
            deviceToken = deviceToken,
            deviceType = deviceType, --1=ios, 3=android
            deviceId = ly.sys.deviceId,
            devicePlatrom = ly.sys.platformName
        })
    end
end

local function _handle_accelerometer_event(self, event)
    local options = self._options
    if(options) then
        if (event.isShake) then
            
            --long shake
            local limit = options.longshake_count
            local timeout = options.longshake_timeout
            ly.logger.debug('shake timeout: '..tostring(timeout) )
            self._longshake_counter = ly.inc(self._longshake_counter)
            
            --longshake timeout
            if(self._longshake_timer) then
                timer.cancel(self._longshake_timer)
                self._longshake_timer = nil
            end
            self._longshake_timer = ly.delay(function() 
                --reset counter for timeout
                self._longshake_timer = nil
                self._longshake_counter = 0
            end, timeout)
            
            if(self._longshake_counter>=limit) then
                self._longshake_counter = 0
                
                _do_longshake(self, event)
                
            else
                _do_shake(self, event)
            end
        else
            _do_accelerate(self, event)
        end
    end
end

local function _handle_location_event(self, event)
    local options = self._options
    if(options and options.location) then
        _do_location(self, event)
    end
end
--------------------------------------------------------------------------------
-- private
--------------------------------------------------------------------------------

local function _set_options(self, options)
    if(ly.isNotEmpty(self._options) and ly.isNotEmpty(options)) then
        ly.extend(self._options, options, true)
    end
end

--------------------------------------------------------------------------------
-- init
--------------------------------------------------------------------------------

local function _init(self)
    local options = self._options
    
    options.back = ly.toBoolean(options.back, true) --override back button
    options.location = ly.toBoolean(options.location, false) --enable GPS location events
    options.longshake_count = ly.toNumber(options.longshake_count, 10) --trigger an event after a number of shake
    options.longshake_timeout = ly.toNumber(options.longshake_timeout, 1000) --1 seconds timeout
    
    self._longshake_counter = 0
    self._longshake_timer = nil
    
    --handlers
    local function handle_sys_events(event)
        _handle_sys_event(self, event.type)
        return true
    end
    local function handle_key_events(event)
        return _handle_key_event(self, event)
    end
    local function handle_notification_events(event)
        _handle_notification_event(self, event)
    end
    local function handle_accelerometer_events(event)
        _handle_accelerometer_event(self, event)
        return true
    end
    local function handle_location_events(event)
        _handle_location_event(self, event)
        return true
    end
    
    --system events
    Runtime:addEventListener( IN_EVENT_SYSTEM, handle_sys_events)
    --key events
    Runtime:addEventListener( IN_EVENT_KEY, handle_key_events)
    --notification events
    Runtime:addEventListener( IN_EVENT_NOTIFICATION, handle_notification_events)
    --accelerometer events
    Runtime:addEventListener( IN_EVENT_ACCELEROMETER, handle_accelerometer_events)
    
    --location events
    if(options.location) then
        Runtime:addEventListener( IN_EVENT_LOCATION, handle_location_events)
    end
    
    self.onFree = function() 
        Runtime:removeEventListener( IN_EVENT_SYSTEM, handle_sys_events)
        Runtime:removeEventListener( IN_EVENT_KEY, handle_key_events)
        Runtime:removeEventListener( IN_EVENT_NOTIFICATION, handle_notification_events)
        Runtime:removeEventListener( IN_EVENT_ACCELEROMETER, handle_accelerometer_events)
        Runtime:removeEventListener( IN_EVENT_LOCATION, handle_location_events)
    end
end

local function _free(self)
    
end

--------------------------------------------------------------------------------
-- constructor
--------------------------------------------------------------------------------

local ComponentClass = Class(Super) --inherits Object Class

function ComponentClass:initialize(options)
    --calls parent Class's initialize()
    --with "self" as this instance
    if (Super.initialize(self, options)) then
        self._className = CLASS_NAME
        _init(self)
        return true
    else
        return false
    end
end

function ComponentClass:finalize()
    if(Super.finalize(self)) then
        -- clear internal fields
        _free(self)
        
        return true
    else
        return false
    end
end

--------------------------------------------------------------------------------
-- public
-------------------------------------------------------------------------------- 

ComponentClass.onSysStart = EVENT_SYS_ON_START
ComponentClass.onSysSuspend = EVENT_SYS_ON_SUSPEND
ComponentClass.onSysExit = EVENT_SYS_ON_EXIT
ComponentClass.onSysOpenUrl = EVENT_SYS_ON_OPEN_URL

ComponentClass.onKey = EVENT_KEY_ON_KEY
ComponentClass.onKeyBack = EVENT_KEY_ON_BACK
ComponentClass.onKeySubmit = EVENT_KEY_ON_SUBMIT

ComponentClass.onNotificationRegister = EVENT_NOTIFICATION_ON_REGISTER
ComponentClass.onNotificationLocale = EVENT_NOTIFICATION_ON_LOCALE
ComponentClass.onNotificationRemote = EVENT_NOTIFICATION_ON_REMOTE

ComponentClass.onShake = EVENT_ON_SHAKE
ComponentClass.onLongShake = EVENT_ON_LONGSHAKE
ComponentClass.onAccelerate = EVENT_ON_ACCELERATE

ComponentClass.onLocation = EVENT_ON_LOCATION

function ComponentClass:lauchArgs(launchArgs)
    --debug info
    ly.logger.debug(ly.json.encode(LAUNCH_ARGS))
    
    if ( launchArgs and launchArgs.notification ) then
        local event = ly.defaults({
            type = TYPE_REMOTE,
            applicationState = STATE_INACTIVE,
        }, launchArgs.notification)
        _handle_notification_event(self,  event )
    end
end

--- Set options and overwrite defaults.
-- @param options Options, 
--   {back=true, location=false, longshake_count=10, longshake_timeout=1000}
-- "back": (Boolean) indicate if overwrite back button behaviour
-- "longshake_count": (Number) trigger an event after a number of shake
-- "longshake_timeout": (Number) 1 seconds timeout
-- "location": (Boolean) enable location (GPS) request
-- @return
function ComponentClass:setOptions(options)
    --set options
    _set_options(self, options)
    
    -- reset handlers and initialize again
    ly.invoke(self.onFree)
    _init(self)
end

--------------------------------------------------------------------------------
-- export
-------------------------------------------------------------------------------- 

return ComponentClass




--ly.components.picturepicker.PicturePicker
--------------------------------------------------------------------------------
-- import
--------------------------------------------------------------------------------
local ly = require('ly.ly')
local app = require('ly.lyApp')

local Super = require('ly.components.BaseComponent') -- Superclass reference

--------------------------------------------------------------------------------
-- CONST
--------------------------------------------------------------------------------

local CLASS_NAME = 'ly.components.picturepicker.PicturePicker'

--------------------------------------------------------------------------------
-- events
--------------------------------------------------------------------------------

local EVENT_ON_PICTURE = 'gotpicture'

--------------------------------------------------------------------------------
-- private
--------------------------------------------------------------------------------


local function _on_photo_complete(self, event)
    --print('_on_photo_complete')
    
    if ( ly.isEventListener(self) and event.completed ) then
        
        --reset flag
        app.storage:setValue('take_picture', false)
        
        --event
        --print('dispatchEvent: '..EVENT_ON_PICTURE)
        ly.delay(function() 
            self:dispatchEvent({
                target=self,
                name = EVENT_ON_PICTURE,
                filename= 'tmp_picture.jpg',
                baseDir = system.TemporaryDirectory
            })
        end, 100)
    end
end

local function _do_take(self)
    app.storage:setValue('take_picture', true) --add flag for main.lua
    
    if ( media.hasSource( media.Camera ) ) then
        media.capturePhoto( { 
            listener = function(event) 
                _on_photo_complete(self, event) 
            end ,
            destination = {
                baseDir = system.TemporaryDirectory,
                filename = "tmp_picture.jpg",
                type = "image"
            }
        } )
    else
        native.showAlert( "Camera not Found", "This device does not have a camera.", { "OK" } )
    end
end

local function _do_select(self)
    app.storage:setValue('take_picture', true) --add flag for main.lua
    
    if ( media.hasSource( media.Camera ) ) then
        media.selectPhoto( { 
            mediaSource = media.PhotoLibrary, 
            listener = function(event) 
                _on_photo_complete(self, event) 
            end  ,
            destination = {
                baseDir = system.TemporaryDirectory,
                filename = "tmp_picture.jpg",
                type = "image"
            }
        } )
    else
        native.showAlert( "Camera not Found", "This device does not have a camera.", { "OK" } )
    end 
end

--------------------------------------------------------------------------------
-- init
--------------------------------------------------------------------------------

local function _init(self)
    local options = self._options
    
end

local function _free(self)
    
end

--------------------------------------------------------------------------------
-- constructor
--------------------------------------------------------------------------------

local ComponentClass = Class(Super) --inherits Object Class

function ComponentClass:initialize(options)
    if (Super.initialize(self, options)) then
        self._className = CLASS_NAME
        _init(self)
        return true
    else
        return false
    end
end

function ComponentClass:finalize()
    if(Super.finalize(self)) then
        -- clear internal fields
        _free(self)
        
        return true
    else
        return false
    end
end

--------------------------------------------------------------------------------
-- public
-------------------------------------------------------------------------------- 

ComponentClass.onPicture = EVENT_ON_PICTURE

function ComponentClass:takePicture()
    return _do_take(self)
end

function ComponentClass:selectPicture()
    return _do_select(self)
end

--------------------------------------------------------------------------------
-- export
-------------------------------------------------------------------------------- 

return ComponentClass




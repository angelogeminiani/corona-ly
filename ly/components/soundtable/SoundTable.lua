--------------------------------------------------------------------------------
-- import
--------------------------------------------------------------------------------
local ly = require('ly.ly')
local app = require('ly.lyApp')

local Super = require('ly.components.BaseComponent') -- Superclass reference

--------------------------------------------------------------------------------
-- CONST
--------------------------------------------------------------------------------

local EVENT_COMPLETE = 'on_complete'

--------------------------------------------------------------------------------
-- private
--------------------------------------------------------------------------------

local function _isSoundEnabled()
    if(app and app.settings)then
        return ly.toBoolean(app.settings['enable_audio'])
    end
    return false
end

local function _init(self)
    local options = self._options
    options.baseDir = options.baseDir or system.ResourceDirectory
    
    --internal list
    self._table = {}
end

local function _get(self, key)
    if(key and self._table and self._table[key])then
        return self._table[key]
    end
    return nil
end

local function _remove(self, key)
    if(key and self._table and self._table[key])then
        local item = self._table[key]
        audio.dispose(item.handle)
        self._table[key] = nil
    end
    return nil
end

local function _add(self, key, fileName, isStream)
    self._table = self._table or {} --create if any
    
    --dispose existing
    _remove(self, key)
    
    --add item onto table
    self._table[key] = {
        name = key,
        file_name = fileName,
        is_stream = isStream,
        handle = nil
    }
    
    -- load audio
    if (isStream) then
        self._table[key].handle = audio.loadStream(fileName, self._options.baseDir)
    else
        self._table[key].handle = audio.loadSound(fileName, self._options.baseDir)
    end
end



--------------------------------------------------------------------------------
-- constructor
--------------------------------------------------------------------------------

local ComponentClass = Class(Super) --inherits Object Class

function ComponentClass:initialize(options)
    if (Super.initialize(self, options)) then
        self._className = 'ly.components.soundtable.SoundTable'
        _init(self)
        return true
    else
        return false
    end
end

function ComponentClass:finalize()
    --calls parent Class's finalize()
    --with "self" as this instance
    if(Super.finalize(self)) then
        -- clear internal fields
        if(self._table) then
            ly.forEach(function (item, key, index)
                if(item and item.handle) then
                    --free audio memory
                    audio.dispose(item.handle)
                    item.handle = nil
                end
            end)
            self._table = nil
        end
        return true
    else
        return false
    end
end

--------------------------------------------------------------------------------
-- public
-------------------------------------------------------------------------------- 

ComponentClass.EVENT_COMPLETE = EVENT_COMPLETE

function ComponentClass:getSound(key)
    return _get(self, key)
end

function ComponentClass:add(key, fileName, isStream)
    if(key and fileName)then
        _add(self, key, fileName, isStream)
    end
    return self
end

function ComponentClass:remove(key)
    _remove(self, key)
    return self
end

---Play a sound
-- @param key Sound to play
-- @param options {channel = 1, loops = -1, duration = 30000, fadein = 5000, onComplete=func}
-- @return self

function ComponentClass:play(key, options)
    if(_isSoundEnabled()) then
        local item = self:getSound(key)
        if(item)then
            options = options or {}
            local callback = options.onComplete
            options.onComplete = function (event) 
                --invoke callback if any
                ly.invoke(callback, event)
                --dispatch event
                self:dispatchEvent({name=EVENT_COMPLETE, sound=item, 
                channel=event.channel, completed=event.completed, handle=event.handle})
            end
            audio.play(item.handle, options)
        end
    end
    return self
end

--------------------------------------------------------------------------------
-- export
-------------------------------------------------------------------------------- 

return ComponentClass




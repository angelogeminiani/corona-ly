---
-- Helper class that load images from local and from remote sources with a 
-- unique asynchronous method.
-- USES CACHE to store images.
-- 

-- 

local CLASS_NAME = 'ly.components.imageloader.ImageLoader'
--------------------------------------------------------------------------------
-- import
--------------------------------------------------------------------------------
local ly = require('ly.ly')

local Super = require('ly.components.BaseComponent') -- Superclass reference

local StorageClass = require('ly.classes.storage.LocalStorage')

--------------------------------------------------------------------------------
-- const
--------------------------------------------------------------------------------

local ROOT = '/imageloader'
local BASE_DIR = system.TemporaryDirectory
local FILE_NAME = 'imageloader.json'

local ITEM = {
    _id='', --MD5 key
    filename=''
}
local RESPONSE = {
    filename='',
    baseDirectory = system.ResourceDirectory
}
--------------------------------------------------------------------------------
-- private
--------------------------------------------------------------------------------

local function _remove_item(self, _id)
    local options = self._options
    local result = false
    
    local item = self._storage:getValue(_id)
    if(item) then
        self._storage:setValue(_id, nil)
        ly.fs.remove(item.filename, options.baseDir)
        result = true
    end
    return result
end

local function _remove_item_from_url(self, url)
    local _id = ly.crypto.MD5(url)
    return _remove_item(self, _id)
end

local function _clear_data(self)
    if(ly.isNotNull(self._storage)) then
        
        -- loop on all items to remove
        self._storage:forEach(function(value, key) 
            if(value and value._id) then
                _remove_item(value._id)
            end
        end)
        
        -- empty storage
        self._storage:clear()
    end
end

local function _download(self, id, url, baseDir, callback)
    local options = self._options
    local ext = ly.path.getExtension(ly.path.getName(url)) or '.jpg'
    local fileName = id..ext
    fileName = ly.path.join(options.root, fileName)
    
    ly.net.download(url, function(event) 
        if ( event.isError ) then
            --error
            ly.invoke(callback, 'NETWORK_ERROR')
        elseif ( event.phase == "began" ) then
            --print( "Progress Phase: began" )
        elseif ( event.phase == "ended" ) then
            local response = event.response
            if(ly.isObject(response)) then
                local filename = response.filename
                local baseDir = response.baseDirectory
                
                --convert .base64 files into binary
                if(ly.path.getExtension(filename)=='.base64') then
                    local fileimage = ly.str.replace(filename, '.base64', '.jpg')
                    local data = ly.fs.read(system.pathForFile(filename, baseDir))
                    local fullpath = ly.base64.decodeBinary(data, fileimage, baseDir)
                    --replace filename
                    response.filename = fileimage
                    --remove base64
                    ly.fs.remove(filename, baseDir)
                end
                
                ly.invoke(callback, nil, response)
            else
                ly.invoke(callback, {code='FILE_NOT_FOUND', data=filename})
            end
            
        end
    end, {baseDir=baseDir, fileName=fileName, flag_timestamp=false}) 
end

local function _is_in_storage(self, _id)
    local options = self._options
    local baseDir = options.baseDir
    if(self._storage:hasKey(_id)) then
        local item = self._storage:getValue(_id)
        if (ly.isNotEmpty(item) and ly.isNotEmpty(item.filename)) then
            --verify
            local bounds = ly.graphics.getBounds(item.filename, baseDir)
            
            return ly.isNotEmpty(bounds) and bounds.width>0
        end
    end
    return false
end

local function _load_item(self, params, callback)
    --parameters
    local filename, baseDir
    if (ly.isArray(params)) then
        filename, baseDir = unpack(params)
    elseif (ly.isObject(params)) then
        filename = params.filename
        baseDir = params.baseDir
    elseif(ly.isString(params)) then
        filename = params
    end
    
    if(ly.path.isHttp(filename)) then
        local options = self._options
        baseDir = options.baseDir -- cache directory
        --external image
        local _id = ly.crypto.MD5(filename)
        if(_is_in_storage(self, _id)) then
            -- FROM CACHE
            local item = self._storage:getValue(_id)
            ly.invoke(callback, nil, {filename=item.filename, baseDirectory=baseDir})
        else
            --DOWNLOAD
            _download(self, _id, filename, baseDir, function(err, response) 
                if(not err and ly.isNotEmpty(response) and ly.isNotEmpty(response.filename)) then
                    --add to cache
                    local item = {
                        _id = _id,
                        filename = response.filename
                    }
                    self._storage:setValue(_id, item)
                    
                    ly.invoke(callback, err, {filename=response.filename, baseDirectory=response.baseDirectory})
                else
                    ly.invoke(callback, err, {filename='', baseDirectory=nil})
                end
            end)
        end
    else
        -- nothig todo, this is just a local image
        baseDir = baseDir or system.ResourceDirectory
        ly.invoke(callback, nil, {filename=filename, baseDirectory=baseDir})
    end
end

local function _init(self)
    local options = self._options
    
    options.root = options.root or ROOT
    options.baseDir = options.baseDir or BASE_DIR
    
    --creates storage
    self._storage = StorageClass:new({
        root=options.root, name=FILE_NAME, baseDir = options.baseDir
    })
    
    
end

local function _free(self)
    
end

--------------------------------------------------------------------------------
-- constructor
--------------------------------------------------------------------------------

local ComponentClass = Class(Super) --inherits Object Class

function ComponentClass:initialize(options)
    if (Super.initialize(self, options)) then
        self._className = CLASS_NAME
        _init(self)
        return true
    else
        return false
    end
end

function ComponentClass:finalize()
    if(Super.finalize(self)) then
        -- clear internal fields
        _free(self)
        
        return true
    else
        return false
    end
end

--------------------------------------------------------------------------------
-- public
-------------------------------------------------------------------------------- 

--SINGLETON INSTANCE
ComponentClass.singleton = ComponentClass.singleton or ComponentClass:new({})

function ComponentClass:clearCache()
    -- remove files
    
    -- remove data from storage
    _clear_data(self)
end

function ComponentClass:loadImage(params, callback)
    _load_item(self, params, callback)
end

function ComponentClass:removeItem(url)
    return _remove_item_from_url(self, url)
end

--------------------------------------------------------------------------------
-- export
-------------------------------------------------------------------------------- 

return ComponentClass




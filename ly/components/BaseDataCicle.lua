
local CLASS_NAME = 'ly.components.BaseDataCicle'

--------------------------------------------------------------------------------
-- import
--------------------------------------------------------------------------------
local ly = require('ly.ly')

local Super = require('ly.components.BaseComponent') -- Superclass reference

--------------------------------------------------------------------------------
-- events
--------------------------------------------------------------------------------

local EVENT_NEW_DATA = 'new_data'
local EVENT_DATA = 'data'
local EVENT_BEFORE_CICLE = 'before_cicle'

--------------------------------------------------------------------------------
-- const
--------------------------------------------------------------------------------

local DEFAULT_INTERVAL = 10000 --10 seconds

--------------------------------------------------------------------------------
-- triggers
--------------------------------------------------------------------------------

local function _trigger_new_data(self, data)
    if (ly.isNotNull(data)) then
        self:dispatchEvent({
            name = EVENT_NEW_DATA,
            target = self,
            data = data
        })
    end
end

local function _trigger_data(self, data)
    if (ly.isNotNull(data)) then
        self:dispatchEvent({
            name = EVENT_DATA,
            target = self,
            data = data
        })
    end
end

local function _trigger_before_cicle(self)
    self:dispatchEvent({
        name = EVENT_BEFORE_CICLE,
        target = self,
        data = data
    })
end

--------------------------------------------------------------------------------
-- private
--------------------------------------------------------------------------------

local function _invoke_on_cicle(self)
    -- run hook
    if(ly.isFunction(self.onCicle))then
        --pause cicle
        self._stopped = true
        _trigger_before_cicle(self) --before cicle event
        ly.invoke(self.onCicle, function(err, data) 
            --resume cicle
            self._stopped = false
            if(not err and ly.isNotNull(data)) then
                --print(ly.json.encode(data))
                if(ly.hasSize(data))then
                    _trigger_new_data(self, data) --new data event
                end
                _trigger_data(self, data) --data event
            end
        end)
    end
end

local function _cicle(self)
    local options = self._options
    if(not self._running) then
        
        --first run
        if(not self._stopped) then
            self._runnig = true
            -- run hook
            _invoke_on_cicle(self)
        else
            self._runnig = false
        end
        
        --cicle
        ly.cicle(function()
            if(not self._stopped) then
                self._runnig = true
                -- run hook
                _invoke_on_cicle(self)
            else
                self._runnig = false
                return true --exit loop
            end
        end, options.interval)
    end
end

--------------------------------------------------------------------------------
-- init
--------------------------------------------------------------------------------

local function _init(self)
    local options = self._options
    options.interval = options.interval or DEFAULT_INTERVAL
    
    self._stopped = false --exposed (start, stop)
    self._runnig = false --only internal use
    
    --autostart
    _cicle(self)
end

local function _free(self)
    self._stopped = true
end

--------------------------------------------------------------------------------
-- constructor
--------------------------------------------------------------------------------

local ComponentClass = Class(Super) --inherits Object Class

function ComponentClass:initialize(options)
    if (Super.initialize(self, options)) then
        self._className = CLASS_NAME
        _init(self)
        return true
    else
        return false
    end
end

function ComponentClass:finalize()
    if(Super.finalize(self)) then
        -- clear internal fields
        _free(self)
        
        return true
    else
        return false
    end
end

--------------------------------------------------------------------------------
-- public
-------------------------------------------------------------------------------- 

ComponentClass.onBeforeCicle = EVENT_BEFORE_CICLE
ComponentClass.onNewData = EVENT_NEW_DATA
ComponentClass.onData = EVENT_DATA

function ComponentClass:setOptions(options)
    if(ly.isNotNull(options)) then
        self._options = ly.merge(options, self._options, true)
    end
end

function ComponentClass:stop()
    self._stopped = true
end

function ComponentClass:start()
    self._stopped = false
    if(not self._running) then
        _cicle(self)
    end
end

function ComponentClass:isRunnung()
    return self._running
end

--------------------------------------------------------------------------------
-- export
-------------------------------------------------------------------------------- 

return ComponentClass




local CLASS_NAME = 'ly.components.BaseDataEntity'

--------------------------------------------------------------------------------
-- import
--------------------------------------------------------------------------------
local ly = require('ly.ly')
local app = require('ly.lyApp')

local Super = require('ly.components.BaseComponent') -- Superclass reference

--------------------------------------------------------------------------------
-- CONST
--------------------------------------------------------------------------------

local FLD_ID = '_id'

--------------------------------------------------------------------------------
-- private
--------------------------------------------------------------------------------

local function _getId(param)
    local user = ly.conditional(ly.isNotNull(param._data), param._data, param)
    if(ly.isObject(user)) then
        return user[FLD_ID] or ''
    end
    return ''
end

--------------------------------------------------------------------------------
-- init
--------------------------------------------------------------------------------

local function _init(self)
    local options = self._options
    local data = options.data or options
    if(data._data) then
        --User class
        self._data = data._data
    else
        --string or object
        self._data =  ly.json.decode(data)
    end
end

local function _free(self)
    
end

--------------------------------------------------------------------------------
-- constructor
--------------------------------------------------------------------------------

local ComponentClass = Class(Super) --inherits Object Class

function ComponentClass:initialize(options)
    if (Super.initialize(self, options)) then
        self._className = CLASS_NAME
        _init(self)
        return true
    else
        return false
    end
end

function ComponentClass:finalize()
    if(Super.finalize(self)) then
        -- clear internal fields
        _free(self)
        
        return true
    else
        return false
    end
end

--------------------------------------------------------------------------------
-- public
-------------------------------------------------------------------------------- 

ComponentClass.FLD_ID = FLD_ID

function ComponentClass:toString()
    return ly.conditional(ly.isNotNull(self._data), ly.json.encode(self._data), '{}')
end

function ComponentClass:hasRawData()
    return ly.isNotNull(self._data)
end

function ComponentClass:rawData()
    return self._data
end

function ComponentClass:getId()
    return _getId(self)
end

function ComponentClass:equals(obj)
    obj = ComponentClass:new(obj)
    return self:getId() == obj:getId()
end

--------------------------------------------------------------------------------
-- export
-------------------------------------------------------------------------------- 

return ComponentClass




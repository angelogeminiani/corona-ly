local CLASS_NAME = 'ly.components.BaseComponent'

--------------------------------------------------------------------------------
-- import
--------------------------------------------------------------------------------

local ly = require('ly.ly')

local Super = require('ly.classes.emitter.EventEmitter') -- Superclass reference

--------------------------------------------------------------------------------
-- fields
--------------------------------------------------------------------------------


--------------------------------------------------------------------------------
-- private
--------------------------------------------------------------------------------

local function _init(self, options)
    options = options or {}
    
    --store options
    self._options = options
end

local function _free(self)
    
end

--------------------------------------------------------------------------------
-- constructor
--------------------------------------------------------------------------------

local ComponentClass = Class(Super) --inherits Object Class

function ComponentClass:initialize(options)
    if (Super.initialize(self, options)) then
        self._className = CLASS_NAME
        _init(self, options)
        return true
    else
        return false
    end
end

function ComponentClass:finalize()
    _free(self)
    if(Super.finalize(self)) then
        -- clear internal fields
        self._options = nil
        return true
    else
        return false
    end
end


--------------------------------------------------------------------------------
-- public
-------------------------------------------------------------------------------- 




--------------------------------------------------------------------------------
-- export
-------------------------------------------------------------------------------- 

return ComponentClass




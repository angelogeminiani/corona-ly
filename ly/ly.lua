--------------------------------------------------------------------------------
-- import
--------------------------------------------------------------------------------


--------------------------------------------------------------------------------
-- SINGLETON
--------------------------------------------------------------------------------

-- Creates Module
local ly = {}

--------------------------------------------------------------------------------
-- private
--------------------------------------------------------------------------------

--- is

local function _isNull(value)
    return (nil==value or 'nil'==value) 
end

local function _isNotNull(value)
    return (nil~=value and 'nil'~=value)
end

local function _isEmpty(value)
    if(ly.isTable(value)) then
        return not ly.hasSize(value)
    else
        return (nil==value) or (tostring(value):len()==0) 
    end
end

local function _isNotEmpty(value)
    return not _isEmpty(value)  
end

local function _isZero(value)
    return _isNull(value) or _isEmpty(value) or value=='0' or value==0  
end

local function _isNotZero(value)
    return not _isZero(value)  
end

local function _isString(value)
    return _isNotNull(value) and (type(value) == "string")
end

local function _isNumber(value)
    return _isNotNull(value) and (type(value) == "number")
end

local function _isTable(value)
    return _isNotNull(value) and (type(value) == "table")
end

local function _isArray(value)
    return _isNotNull(value) and (type(value) == "table") and (#value>0)
end

local function _isObject(value)
    return _isNotNull(value) and (type(value) == "table") and (#value==0)
end

local function _isFunction(value)
    return _isNotNull(value) and (type(value) == "function")
end

local function _isBoolean(value)
    return _isNotNull(value) and (type(value) == "boolean")
end

local function _isTrue(value)
    return _isNotNull(value) and (type(value) == "boolean") and value==true
end

local function _isFalse(value)
    return _isNotNull(value) and (type(value) == "boolean") and value==false
end

local function _isDisplayObject(...)
    local response = false
    local items = arg
    if(#items>0) then
        for i = 1, #items do
            local value = items[i]
            response = _isObject(value) and _isFunction(value['removeSelf'])
            if(not response)then
                break
            end
        end
    else
        response = false
    end
    return response
end

local function _isEventListener(...)
    --return _isObject(value) and _isFunction(value['removeEventListener'])
    local response = false
    local items = arg
    if(#items>0) then
        for i = 1, #items do
            local value = items[i]
            response = _isObject(value) and _isFunction(value['removeEventListener'])
            if(not response)then
                break
            end
        end
    else
        response = false
    end
    return response
end

local function _isLyObject(...)
    --return _isObject(value) and _isFunction(value['free'])
    local response = false
    local items = arg
    if(#items>0) then
        for i = 1, #items do
            local value = items[i]
            response = _isObject(value) and _isFunction(value['free'])
            if(not response)then
                break
            end
        end
    else
        response = false
    end
    return response
end

--- to

local function _toBoolean(value, def_val)
    if(ly.isNotNull(value)) then
        if (ly.isBoolean(value)) then
            return value
        else
            value = tostring(value):lower()
            return value=='true'
        end
    else
        if (nil==def_val) then
            return false 
        else
            return def_val 
        end
    end 
end

local function _toString(value, def_val)
    if (_isNotEmpty(value)) then
        return tostring(value) 
    else
        return def_val or ''
    end
end

local function _toNumber(value, def_val)
    if (value) then
        if(ly.isNumber(value)) then
            return value
        else
            return tonumber(value) or def_val or 0 
        end
    else
        return def_val or 0
    end
end

local function _toValue(value, def_val)
    if(_isNotNull(value)) then
        return value
    else
        return def_val
    end
end

--- number

local function _inc(number, value)
    value = value or 1
    return _toNumber(number, 0) + value
end

local function _dec(number, value)
    value = value or 1
    return _toNumber(number, 0) - value
end

--- object

local function _hasPairs(t)
    local count=0
    if(t) then
        for key,value in pairs(t) do
            count = count+1
            break
        end
    end
    return count>0
end

local function _countPairs(t)
    local count=0
    if(t) then
        for key,value in pairs(t) do
            count = count+1
        end
    end
    return count
end

local function _hasSize(t)
    if(_isArray(t)) then
        return #t>0
    elseif(_isObject(t)) then
        return _hasPairs(t)
    elseif(_isString(t)) then
        return t:len()>0
    else
        return false
    end
end

local function _sizeOf(t)
    if(_isArray(t)) then
        return #t
    elseif(_isObject(t)) then
        return _countPairs(t)
    elseif(_isString(t)) then
        return t:len()
    else
        return 0
    end
end

local function _indexOf(t, value)
    if _isArray(t) then
        for i = 1, #t do
            if value == t[i] then
                return i
            end
        end
    end
    return -1
end

local function _forEach(t, callback, count_flag)
    local index=0
    if(ly.isNotNull(callback)) then
        if (_isTable(t)) then
            --table
            local count = 0
            if (count_flag) then 
                count = _countPairs(t)
            end
            for key,value in pairs(t) do
                index = index+1
                if (nil~=callback(value, key, index, count)) then
                    break
                end
            end
        else
            --simple value or nil
            callback(t, 1, 1, 1)
        end
    end
end

--- Filter table 
-- @param t Table to filter
-- @param callback: Function (value, key, index):Booelan
-- @return New instance of a filtered Table

local function _filter(t, callback)
    local result = {}
    local index=0
    if(ly.isNotNull(callback)) then
        for key,value in pairs(t) do
            index = index+1
            if (callback(value, key, index)) then
                result[key]=value
            end
        end
    end
    return result
end

local function _merge(source, target, overwrite)
    target = target or {}
    if (source) then
        for key,value in pairs(source) do
            if (_isNull(target[key]) or overwrite) then
                target[key] = value
            end
        end 
    end
    return target
end

local function _extend(destination, source, overwrite)
    return _merge(source, destination, overwrite)
end

local function _defaults(destination, defaults)
    return _extend(destination, defaults, false)
end

local function _clone(source)
    local target = {}
    if (source) then
        for key,value in pairs(source) do
            if (ly.isTable(value)) then
                target[key] = _clone(value)
            else
                target[key] = value
            end
        end 
    end
    return target
end

local function _getValue(t, keyOrPath, def_val, deep)
    local result = nil
    if (_isNotNull(t)) then
        if (deep) then
            local curr = t
            local keys = ly.str.split(keyOrPath, '%.')
            ly.forEach(keys, function(value, key, index, count) 
                if (index==count) then
                    result = curr[value]
                else
                    curr = curr[value]
                    if (_isNull(curr)) then
                        return false -- exit loop
                    end
                end
            end, true)-- loop with count value
        else
            result = t[keyOrPath]  
        end
    end
    return _toValue(result, def_val)   
end

local function _setValue(t, keyOrPath, value, deep)
    if (_isNotNull(t)) then
        if (deep) then
            local curr = t
            local keys = ly.str.split(keyOrPath, '%.')
            ly.forEach(keys, function(v, key, index, count) 
                if (index==count) then
                    curr[v] = value
                else
                    if (_isNull(curr[v])) then
                        -- create
                        curr[v]={}
                    end
                    curr = curr[v]
                end
            end, true)-- loop with count value
        else
            t[keyOrPath] = value 
        end
    end
    return ly 
end

local function _getString(t, keyOrPath, def_val, deep)
    local result = _getValue(t, keyOrPath, def_val, deep)
    return _toString(result, def_val)
end

local function _getNumber(t, keyOrPath, def_val, deep)
    local result = _getValue(t, keyOrPath, def_val, deep)
    return _toNumber(result, def_val)
end

local function _getBoolean(t, keyOrPath, def_val, deep)
    local result = _getValue(t, keyOrPath, def_val, deep)
    return _toBoolean(result, def_val)
end

--- operators

---Ternaty Operator.
-- like: a = c=='23' ? 'true' : 'false'
-- usage: a = ly.conditional(c=='23', 'true', 'false') 
-- @param condition
-- @param if_true
-- @param if_false
-- @return

local function _conditional(condition, if_true, if_false)
    if (ly.isNull(if_true)) then
        if_true = true
    end
    if (ly.isNull(if_false)) then
        if_false = false
    end
    if (ly.toBoolean(condition, false)) then
        return if_true
    else
        return if_false 
    end
end


--- function

local function _delay(func, wait, ...) 
    
    local function listener(event)
        local delayed_func = event.source.func 
        local params = event.source.params or {}
        
        delayed_func(unpack(params))
    end
    
    local wait_t = wait or 0
    local tm = timer.performWithDelay(wait_t, listener, 1)
    tm.params = arg
    tm.func = func
    
    return tm --return timer to stop if needed: timer.cancel(tm)
end

local function _cicle(func, wait, ...) 
    if (_isNull(func)) then
        return nil
    end
    
    local function listener(event)
        local tm = event.source
        local delayed_func = tm.func 
        local params = tm.params or {}
        
        local response = delayed_func(unpack(params))
        -- check if hadled (return True = exit cicle)
        if ly.isTrue(response)  then
            timer.cancel(tm)
        end
    end
    
    local wait_t = wait or 0
    local tm = timer.performWithDelay(wait_t, listener, 0)
    tm.params = arg
    tm.func = func
    
    tm.cancel = function() 
        if(tm)then
            timer.cancel(tm)
            tm=nil
        end
    end
    
    return tm --return timer to stop if needed
end

local function _cicleRnd(func, minWait, maxWait, ...)
    if (_isNull(func)) then
        return nil
    end
    minWait = minWait or 1000
    maxWait = maxWait or 3000
    if (minWait > maxWait) then maxWait = (minWait + maxWait) end
    
    local time_passed = system.getTimer()
    local ratio = minWait * (minWait/maxWait)
    
    --start cicle
    return _cicle(function() 
        --calculate rnd wait time
        local wait = math.random( minWait, maxWait )
        if (time_passed+wait<system.getTimer()) then
            time_passed = system.getTimer()
            return func(unpack(arg))
        end
    end, ratio, unpack(arg))
end

local function _cicleCount(func, wait, num_cicle, ...)
    if(ly.isFunction(func)) then
        local count = 0
        return _cicle(function(...) 
            if(count>=num_cicle) then
                return true
            end
            count=count+1
            return func (unpack(arg))
        end, wait, unpack(arg))
    else
        return nil
    end
end

local function _invoke (func, ...)
    if (_isFunction(func)) then
        return func(unpack(arg))
    else
        return nil
    end
end

---Run function in protected mode
-- @param func function to invoke
-- @return boolean (success), ... (returned values from function)

local function _try (func, ...)
    if (_isFunction(func)) then
        return pcall(func, unpack(arg))
    else
        return nil
    end
end

--------------------------------------------------------------------------------
-- public
--------------------------------------------------------------------------------

-- injection --

ly.animate = ly.animate or require('ly.utils.lyAnimate')
ly.animate.ly(ly)

ly.base64 = ly.base64 or require('ly.utils.lyBase64')
ly.base64.ly(ly)

ly.color = ly.color or require('ly.utils.lyColor')
ly.color.ly(ly)

ly.crypto = ly.crypto or require('ly.utils.lyCrypto')
ly.crypto.ly(ly)

ly.date = ly.date or require('ly.utils.lyDate')
ly.date.ly(ly)

ly.dialog = ly.dialog or require('ly.utils.lyDialog')
ly.dialog.ly(ly)

ly.fmt = ly.fmt or require('ly.utils.lyFmt')
ly.fmt.ly(ly)

ly.fs = ly.fs or require('ly.utils.lyFs')
ly.fs.ly(ly)

ly.graphics = ly.graphics or require('ly.utils.lyGraphics')
ly.graphics.ly(ly)

ly.guid = ly.guid or require('ly.utils.lyGUID')
ly.guid.ly(ly)

ly.json = ly.json or require('ly.utils.lyJSON')
ly.json.ly(ly)

ly.logger = ly.logger or require('ly.utils.lyLogger')
ly.logger.ly(ly)

ly.math = ly.math or require('ly.utils.lyMath')
ly.math.ly(ly)

ly.net = ly.net or require('ly.utils.lyNet')
ly.net.ly(ly)

ly.path = ly.path or require('ly.utils.lyPath')
ly.path.ly(ly)

ly.screen = ly.screen or require('ly.utils.lyScreen')
ly.screen.ly(ly)

ly.sys = ly.sys or require('ly.utils.lySys')
ly.sys.ly(ly)

ly.str = ly.str or require('ly.utils.lyStr')
ly.str.ly(ly)

-- methods --

-- is
ly.isNull = _isNull
ly.isNotNull = _isNotNull
ly.isEmpty = _isEmpty
ly.isNotEmpty = _isNotEmpty
ly.isZero = _isZero
ly.isNotZero = _isNotZero
ly.isString = _isString
ly.isNumber = _isNumber
ly.isBoolean = _isBoolean
ly.isTrue = _isTrue
ly.isFalse = _isFalse
ly.isFunction = _isFunction
ly.isTable = _isTable
ly.isArray = _isArray
ly.isObject = _isObject
ly.isDisplayObject = _isDisplayObject
ly.isEventListener = _isEventListener
ly.isLyObject = _isLyObject

-- to
ly.toBoolean = _toBoolean
ly.toString = _toString
ly.toNumber = _toNumber
ly.toValue = _toValue

-- number
ly.inc = _inc
ly.dec = _dec

-- object
ly.hasPairs = _hasPairs
ly.countPairs = _countPairs
ly.hasSize = _hasSize
ly.sizeOf = _sizeOf
ly.forEach = _forEach
ly.filter = _filter
ly.merge = _merge
ly.extend = _extend
ly.defaults = _defaults
ly.clone = _clone
ly.getValue = _getValue
ly.setValue = _setValue
ly.getString = _getString
ly.getNumber = _getNumber
ly.getBoolean = _getBoolean

ly.indexOf = _indexOf

--operators
ly.conditional = _conditional

-- function
ly.delay = _delay
ly.cicle = _cicle
ly.cicleRnd = _cicleRnd
ly.cicleCount = _cicleCount
ly.invoke = _invoke
ly.try = _try

--------------------------------------------------------------------------------
-- export
--------------------------------------------------------------------------------

return ly




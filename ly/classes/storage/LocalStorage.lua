--------------------------------------------------------------------------------
-- import
--------------------------------------------------------------------------------
local json = require ('json')

local ly = require('ly.ly')

local Super = require('ly.lang.Object') -- Superclass reference

--------------------------------------------------------------------------------
-- import
--------------------------------------------------------------------------------

local TIMESTAMP = '_timestamp'

--------------------------------------------------------------------------------
-- private
--------------------------------------------------------------------------------

local function _isValidFieldName(fieldName)
    return fieldName~=TIMESTAMP
end

local function _saveData(self)
    --validate _data
    if (not self._data)then
        local timestamp
        if (ly.sys.isWIN) then
            timestamp = ly.guid.generate()
        else
            timestamp = ly.date.timestamp() --err on windows
        end
        self._data = {}
        self._data[TIMESTAMP] = timestamp
    end
    
    local sdata = json.encode(self._data)
    ly.fs.writeText(self._path, sdata)
end

local function _loadData(self)
    self._data = {}
    local sdata = ly.fs.readText(self._path)
    if (ly.json.isJSONObject(sdata)) then
        self._data = json.decode(sdata)
    end
end

local function _clear(self)
    self._data = nil
    _saveData(self)
end

local function _countData(self)
    local result = 0
    if(self._data) then
        ly.forEach(self._data, function(value, key, index) 
            if (value and _isValidFieldName(key)) then
                result = result+1  
            end
        end)
    end
    return result
end

local function _hasData(self)
    local result = false
    if(self._data) then
        ly.forEach(self._data, function(value, key, index) 
            if (value and _isValidFieldName(key)) then
                result = true
                return false
            end
        end)
    end
    return result
end

local function _forEach(self, callback)
    if(_hasData(self)) then
        ly.forEach(self._data, function(value, key, index) 
            if (value and _isValidFieldName(key)) then
                -- invoke callback
                ly.invoke(callback, value, key, index)
            end
        end)
    else
        ly.invoke(callback)
    end
end

local function init(self, options)
    local opts = options or {}
    
    self._root = opts['root'] or '/data'
    self._name = opts['name'] or 'local_storage.json'
    self._baseDir = opts['baseDir'] or system.DocumentsDirectory
    self._relativePath = ly.path.join(self._root, self._name)
    self._path = system.pathForFile( self._relativePath, self._baseDir )
    self._deep = ly.toBoolean(opts['deep'], true) 
    
    -- creates file and directories
    if (not ly.fs.fileExists(self._path)) then
        ly.fs.mkdirs(ly.path.getDirectory(self._relativePath), self._baseDir) 
        _saveData(self)
    end
    
    -- read content and create internal json object
    _loadData(self) 
end

--------------------------------------------------------------------------------
-- constructor
--------------------------------------------------------------------------------

local LocalStorageClass = Class(Super) --inherits Object Class

function LocalStorageClass:initialize(...)
    if(Super.initialize(self, unpack(arg))) then
        self._className = 'ly.classes.storage.LocalStorage'
        init(self, self._args[1])
        return true
    else
        return false
    end
end

function LocalStorageClass:finalize()
    -- clear local
    self._data = nil
    
    --calls parent Class's finalize()
    --with "self" as this instance
    Super.finalize(self)
end

--------------------------------------------------------------------------------
-- public
--------------------------------------------------------------------------------

function LocalStorageClass:toString()
    return Super.toString(self)
end

function LocalStorageClass:isEmpty()
    return not _hasData(self)
end

function LocalStorageClass:count()
    return _countData(self)
end 

function LocalStorageClass:forEach(callback)
    _forEach(self, callback)
end 

function LocalStorageClass:hasKey(key)
    return not not self:getValue(key)
end

function LocalStorageClass:getValue(key, def_val, deep)
    if(ly.isNull(deep)) then
        deep = self._deep
    end
    if(ly.isNotNull(self) and ly.isNotNull(self._data)) then
        return ly.getValue(self._data, key, def_val, deep)
    else
        return def_val 
    end
end

function LocalStorageClass:setValue(key, value, deep)
    if(ly.isNull(deep)) then
        deep = self._deep
    end
    if(ly.isNotNull(self) and ly.isNotNull(self._data)) then
        ly.setValue(self._data, key, value, deep)
        _saveData(self)
    end
    return self
end

function LocalStorageClass:clear()
    _clear(self)
end

function LocalStorageClass:getNumber(key, def_val, deep)
    return ly.toNumber( self:getValue(key, def_val, deep), def_val )
end

function LocalStorageClass:getString(key, def_val, deep)
    return ly.toString( self:getValue(key, def_val, deep), def_val )
end

function LocalStorageClass:getBoolean(key, def_val, deep)
    return ly.toBoolean( self:getValue(key, def_val, deep), def_val )
end

--------------------------------------------------------------------------------
-- export
--------------------------------------------------------------------------------

return LocalStorageClass



--[[

This is a sample translation file.
Translation file names must be of type: <lang>.lua
i.e. "base.lua", "it.lua", "en.lua", "de.lua", etc..

"base.lua" is a special file. It contains default values if other files are not 
found.
This file contains defaults for an unsupported language.

WARNING:
Always include a "base.lua"" file.
"base.lua"" file is required to manage unsupported languages or bad lang code
requests.

]]--

--------------------------------------------------------------------------------
-- MODULE
--------------------------------------------------------------------------------

local M = {};

--------------------------------------------------------------------------------
-- public
--------------------------------------------------------------------------------

M.NETWORK_ERROR = 'Network Error!'

--calendar
M.CAL_WDAY_1 = 'Sunday'
M.CAL_WDAY_2 = 'Monday'
M.CAL_WDAY_3 = 'Thuesday'
M.CAL_WDAY_4 = 'Wednesday'
M.CAL_WDAY_5 = 'Thursday'
M.CAL_WDAY_6 = 'Friday'
M.CAL_WDAY_7 = 'Saturday'
M.CAL_MONTH_1 = 'January'
M.CAL_MONTH_2 = 'February'
M.CAL_MONTH_3 = 'March'
M.CAL_MONTH_4 = 'April'
M.CAL_MONTH_5 = 'May'
M.CAL_MONTH_6 = 'June'
M.CAL_MONTH_7 = 'July'
M.CAL_MONTH_8 = 'August'
M.CAL_MONTH_9 = 'September'
M.CAL_MONTH_10 = 'October'
M.CAL_MONTH_11 = 'November'
M.CAL_MONTH_12 = 'Dicember'

--write below all your translations

--------------------------------------------------------------------------------
-- export
--------------------------------------------------------------------------------

return M;




--------------------------------------------------------------------------------
-- import
--------------------------------------------------------------------------------

local ly = require('ly.ly')
local LocalStorage = require ('ly.classes.storage.LocalStorage')

local Super = require('ly.classes.emitter.EventEmitter') -- Superclass reference

--------------------------------------------------------------------------------
-- CONSTANTS
--------------------------------------------------------------------------------

local BASE = 'base' -- base dictionary
local ROOT = '/i18n' -- default documents directory
local DEF_LANG = 'en'

--------------------------------------------------------------------------------
-- EVENTS
--------------------------------------------------------------------------------

local EVENT_ON_LANG_CHANGE = 'onLangChange'

local LANG_CODES = {}
LANG_CODES['1040'] = {
    id = 1040,
    language = 'it',
    country = 'IT',
    identifier = 'it-IT',
    decimalSeparator = ',',
    thousandSeparator = '.',
}
LANG_CODES['2057'] = {
    id = 2057,
    language = 'en',
    country = 'UK',
    identifier = 'en-UK',
    decimalSeparator = '.',
    thousandSeparator = ',',
}
LANG_CODES['1033'] = {
    id = 1033,
    language = 'en',
    country = 'US',
    identifier = 'en-US',
    decimalSeparator = '.',
    thousandSeparator = ',',
}
LANG_CODES['1036'] = {
    id = 1036,
    language = 'fr',
    country = 'FR',
    identifier = 'fr-FR',
    decimalSeparator = ',',
    thousandSeparator = '.',
}
LANG_CODES['1031'] = {
    id = 1031,
    language = 'de',
    country = 'DE',
    identifier = 'de-DE',
    decimalSeparator = ',',
    thousandSeparator = '.',
}
LANG_CODES['1034'] = {
    id = 1034,
    language = 'es',
    country = 'ES',
    identifier = 'es-ES',
    decimalSeparator = ',',
    thousandSeparator = '.',
}
--------------------------------------------------------------------------------
-- private
--------------------------------------------------------------------------------

local function _getLangObject(language)
    local result = nil
    ly.forEach(LANG_CODES, function(value, key, index) 
        if (language==value.language) then
            result = value
            return true
        end
    end)
    return result
end

local function _getSysLang()
    local result
    if(ly.sys.isWIN) then
        local id = tostring(system.getPreference( "locale", "identifier" ))
        local lang = LANG_CODES[id]
        if (ly.isNotNull(lang)) then
            result = lang['language'] or 'en'
        else
            result = 'en'    
        end
    else
        result = system.getPreference( "locale", "language" ) 
    end
    
    result = result or 'en'
    
    return result
end

----Validate lang code or detect system language 
-- @param opt_lang Nil or lang code. i.e. "en-UK"
-- @return lang code. i.e. "en"
local function _lang(opt_lang)
    opt_lang = opt_lang or ''
    if (ly.isEmpty(opt_lang)) then
        opt_lang = _getSysLang()
    end
    local sep_idx = opt_lang:find('-', 1, true)
    if (sep_idx and sep_idx>0) then
        opt_lang = opt_lang:sub(1, sep_idx-1)
    end
    return opt_lang or DEF_LANG
end

----Returns name of language file for storage
-- @param opt_lang lang code
-- @return "en.json"
local function _fileName(opt_lang)
    opt_lang = _lang(opt_lang)
    return opt_lang..'.json'
end

local function _addItem(dic, key, value)
    if (dic and ly.isNotNull(value)) then
        dic:setValue(key, value) --set value to storage
    end
end

local function _addItems(dic, items)
    if (dic and items) then
        ly.forEach(items, function(value, key, index) 
            _addItem(dic, key, value)
        end)
    end
end

local function _exists(self, opt_lang)
    opt_lang = _lang(opt_lang)
    local options = self._arg[1]
    local path = ly.path.join(options.root, _fileName(opt_lang))
    return ly.fs.exists(path, system.DocumentsDirectory)
end

local function _getDictionary(self, lang)
    lang = _lang(lang)
    local result = self._dictionaries[lang]
    if (not result) then
        result = self._dictionaries[BASE]
    end
    return result
end

local function _initDictionary(self, lang, reset)
    local options = self._args[1]
    local dic = LocalStorage:new({root=options.root, name=_fileName(lang)}) 
    
    --check if dictionary could load data fron disc
    if (dic:isEmpty() or reset) then
        local success, tmp = ly.try(function() return require(options.resources..'.'..lang) end)
        if (success) then
            --VALID DICTIONARY
            _addItems(dic, tmp) 
        else
            --NO DICTIONARY: resource does not exist in archive
            dic = nil
        end
    end
    
    return dic
end

local function _finalize(self)
    if self._dictionaries then
        ly.forEach(self._dictionaries, function(value,key, index) 
            if(value)then
                value:free()
                self._dictionaries[key]=nil
            end 
        end)
    end
    self._dictionaries = nil
end

local function _init(self)
    local options = self._options
    
    --validate options
    options = options or {}
    options.lang = _lang(options.lang) or DEF_LANG  --current lang 
    options.root = options.root or ROOT --documents sub-folder where store translation files
    options.resources = ly.str.toClassPath(options.resources) or '' --lua tables to generate translations files
    options.absolutePath = ly.path.join(system.pathForFile( "", system.DocumentsDirectory ), options.root)
    --options.reset (not set by default)
    local reset = options.reset
    options.reset = false
    
    self._args[1] = options --update options
    
    self._old_lang = self.lang
    self.lang = options.lang --lang property
    self.locale = _getLangObject(self.lang)
    
    --clear existing data
    _finalize(self)
    
    --init dictionary
    self._dictionaries = {}
    self._dictionaries[BASE] = _initDictionary(self, BASE, reset)
    self._dictionaries[options.lang] = _initDictionary(self, options.lang, reset)
    
    --events
    if (self._old_lang and self._old_lang~=self.lang) then
        self:dispatchEvent({
            name = EVENT_ON_LANG_CHANGE, 
            old_lang = self._old_lang, 
            new_lang = self.lang
        })
    end
end

--------------------------------------------------------------------------------
-- constructor
--------------------------------------------------------------------------------

local I18nManagerClass = Class(Super) --inherits Object Class

function I18nManagerClass:initialize(options)
    if(Super.initialize(self, options)) then
        self._className = 'ly.classes.i18n.I18nManager'
        --store options
        self._options = options
        --initialize
        _init(self)
        
        return true
    else
        if (options.reset) then
            _init(self)
            return true
        else
            return false
        end
    end
end

function I18nManagerClass:finalize()
    -- clear local fields
    _finalize(self)
    
    --calls parent Class's finalize()
    --with "self" as this instance
    Super.finalize(self)
end

--------------------------------------------------------------------------------
-- public
--------------------------------------------------------------------------------

function I18nManagerClass:setLang(lang)
    if (self and self._args and self._args[1]) then
        local options = self._args[1]
        lang = _lang(lang)
        options.reset = lang ~= options.lang
        options.lang = lang --update options
        self:initialize(options) --init again 
    end
end

function I18nManagerClass:get(key)
    local result = ''
    if(not ly.isEmpty(key))then
        local dic = _getDictionary(self, self.lang)
        if (dic) then
            result = dic:getValue(key)
        end
    end
    return result
end

function I18nManagerClass:set(key, value)
    local dic = _getDictionary(self, self.lang)
    if (dic) then
        dic:setValue(key, value)
    end
    return self
end

function I18nManagerClass:clear()
    _finalize(self)
end

function I18nManagerClass:regenerate()
    if (self and self._args and self._args[1]) then
        local options = self._options or self._args[1]
        options.reset = true --update options
        _init(self)
    end
end
--------------------------------------------------------------------------------
-- properties
--------------------------------------------------------------------------------

I18nManagerClass.lang = nil --value not set by default

--------------------------------------------------------------------------------
-- events
--------------------------------------------------------------------------------

I18nManagerClass.EventLangChange = EVENT_ON_LANG_CHANGE

--------------------------------------------------------------------------------
-- export
--------------------------------------------------------------------------------

return I18nManagerClass


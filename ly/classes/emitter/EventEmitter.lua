--------------------------------------------------------------------------------
-- import
--------------------------------------------------------------------------------

local ly = require('ly.ly')

local Super = require('ly.lang.Object') -- Superclass reference

--------------------------------------------------------------------------------
-- private
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
-- constructor
--------------------------------------------------------------------------------
local BaseClass = Class(Super) --inherits 

function BaseClass:initialize(...)
    --calls parent Class's initialize()
    --with "self" as this instance
    if (Super.initialize(self, unpack(arg))) then
        self._emitter = display.newGroup() 
        self._className = 'ly.classes.emitter.EventEmitter'
        return true
    else
        --already initialized
        return false
    end
end

function BaseClass:finalize()
    --calls parent Class's initialize()
    --with "self" as this instance
    Super.finalize(self)
    
    self._emitter = nil
end

--------------------------------------------------------------------------------
-- public
--------------------------------------------------------------------------------

function BaseClass:addEventListener(eventName, listener)
    if(ly.isDisplayObject(self._emitter)) then
        if (ly.isFunction(listener)) then
            self._emitter:addEventListener(eventName, listener)
        else
            print('LISTENER cannot be nil')
        end
    end
end

function BaseClass:removeEventListener( eventName, listener )
    if(ly.isDisplayObject(self._emitter)) then
        if (ly.isFunction(listener)) then
            self._emitter:removeEventListener(eventName, listener)
        else
            print('LISTENER cannot be nil')
        end
    end
end

function BaseClass:dispatchEvent( event )
    if (self._emitter) then
        self._emitter:dispatchEvent( event )
    end
end

--------------------------------------------------------------------------------
-- export
--------------------------------------------------------------------------------

return BaseClass


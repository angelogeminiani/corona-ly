--[[
    DEPENDENCIES:
        - plug in ["CoronaProvider.gameNetwork.google"] 
        
        https://docs.coronalabs.com/daily/api/library/gameNetwork/index.html
        
        Apple: https://docs.coronalabs.com/daily/plugin/gameNetwork-apple/index.html
        Google: https://docs.coronalabs.com/daily/plugin/gameNetwork-google/index.html
--]]
local CLASS_NAME = 'ly.classes.gamenetwork.GameNetwork'

--------------------------------------------------------------------------------
-- import
--------------------------------------------------------------------------------
local json = require ('json')

local ly = require('ly.ly')

local Super = require('ly.lang.Object') -- Superclass reference

--------------------------------------------------------------------------------
-- plugins
--------------------------------------------------------------------------------

local gameNetwork = require( "gameNetwork" ) -- corona plugin

--------------------------------------------------------------------------------
-- CONSTANTS
--------------------------------------------------------------------------------

local ERR_NOT_ENABLED = {code='ERR_NOT_ENABLED', message='ERR_NOT_ENABLED'}
local ERR_PLATFORM_NOT_SUPPORTED = {code='ERR_PLATFORM_NOT_SUPPORTED', message='ERR_PLATFORM_NOT_SUPPORTED'}
local ERR_PLATFORM_ERROR = {code='ERR_PLATFORM_ERROR', message='ERR_PLATFORM_ERROR'}
local STR_ERR_LOGIN = 'ERR_LOGIN'

local PLATFORM_GOOGLE = 'google'
local PLATFORM_APPLE = 'gamecenter'

--------------------------------------------------------------------------------
-- private
--------------------------------------------------------------------------------

local function _gn_init(self, callback)
    if(not self._options.status_init) then
        local platform = self._options.platform
        if(ly.isNotEmpty(platform)) then
            gameNetwork.init( platform, function(event)
                
                self._options.status_init = true -- STATUS INIT
                
                if(platform==PLATFORM_GOOGLE)then
                    --google
                    if(event.isError) then
                        ly.invoke(callback, {code=event.errorCode, message=event.errorMessage}, false)  
                    else
                        ly.invoke(callback, nil, true) 
                    end 
                elseif(platform==PLATFORM_APPLE) then
                    --apple
                    if(event.data) then
                        ly.invoke(callback, nil, true)   
                    else
                        ly.invoke(callback, {code=event.errorCode, message=event.errorMessage}, false)    
                    end
                else
                    ly.invoke(callback, ERR_PLATFORM_NOT_SUPPORTED, false)   
                end
            end )
        else
            ly.invoke(callback, ERR_NOT_ENABLED) 
        end 
    else
        ly.invoke(callback, nil, true)    
    end
end

local function _gn_request(self, command, params)
    if(ly.isNotNull(params)) then
        gameNetwork.request(command, params) 
    else
        gameNetwork.request(command) 
    end
end

local function _gn_show(self, command, params)
    if(ly.isNotNull(params)) then
        gameNetwork.show(command, params) 
    else
        gameNetwork.show(command) 
    end
end

--------------------------------------------------------------------------------
-- warpper
--------------------------------------------------------------------------------

local function _login(self, callback, silent)
    _gn_init(self, function(err, response) 
        if(ly.isNotNull(err)) then
            ly.invoke(callback, err, false)
        else
            silent = ly.toBoolean(silent, false)
            local platform = self._options.platform
            if(platform==PLATFORM_GOOGLE) then
                self._options.status_login = _gn_request(self, 'isConnected')
                if(not self._options.status_login) then
                    _gn_request(self, 'login', { 
                        userInitiated = silent, --If it's set to false, this call will simply attempt to connect without trying to resolve any errors
                        listener=function(event) 
                            if(ly.isNotNull(event.data))then
                                if(event.data.isError)then
                                    ly.invoke(callback, {code=event.data.errorCode or STR_ERR_LOGIN, message=event.data.errorMessage or STR_ERR_LOGIN}, false) 
                                else
                                    self._options.status_login = true -- SET STATUS LOGIN
                                    ly.invoke(callback, nil, true)  
                                end
                            else
                                self._options.status_login = true -- SET STATUS LOGIN
                                ly.invoke(callback, nil, true)
                            end
                        end 
                    })
                else
                    --already loggedin
                    ly.invoke(callback, nil, true) 
                end
            elseif(platform==PLATFORM_APPLE) then
                self._options.status_login = true
                ly.invoke(callback, nil, true)
            else
                ly.invoke(callback, ERR_PLATFORM_NOT_SUPPORTED, false)
            end  
        end
    end)
end

local function _logout(self, callback)
    if(self._options.status_login) then
        
        self._options.status_login = false -- SET STATUS LOGOUT
        
        local platform = self._options.platform
        if(platform==PLATFORM_GOOGLE) then
            _gn_request(self, 'logout')
        end
        ly.invoke(callback, nil, true) 
    else
        ly.invoke(callback, nil, true)
    end
end

local function _load_local_player(self, callback)
    _login(self, function(err, response) 
        if(ly.isNotNull(err)) then
            ly.invoke(callback, err, nil)
        else
            local platform = self._options.platform
            print('PLATFORM='..platform)
            if(platform==PLATFORM_GOOGLE) then
                _gn_request(self, 'loadLocalPlayer',  { 
                    listener=function(event) 
                        if(ly.isNotNull(event.data)) then
                            ly.invoke(callback, nil, {id=event.data.playerID, alias=event.data.alias})   
                        else
                            ly.invoke(callback, ERR_PLATFORM_ERROR, nil)  
                        end
                    end 
                }) 
            elseif(platform==PLATFORM_APPLE) then
                _gn_request(self, 'loadLocalPlayer',  { 
                    listener=function(event) 
                        if(ly.isNotNull(event.data)) then
                            ly.invoke(callback, nil, {
                                id=event.data.playerID, 
                                alias=event.data.alias,
                                friends = event.data.friends, -- Array of player IDs (strings) representing the friends of the local player.
                            })   
                        else
                            ly.invoke(callback, ERR_PLATFORM_ERROR, nil)  
                        end
                    end 
                })  
            else
                ly.invoke(callback, ERR_PLATFORM_NOT_SUPPORTED, nil)  
            end
        end
    end)
end

--------------------------------------------------------------------------------
-- initialization
--------------------------------------------------------------------------------

local function _init(self, options)
    options = options or {}
    --store options
    self._options = options
    
    --status
    self._options.enabled = ly.isNotNull(gameNetwork)
    
    --platform
    if ly.isEmpty(self._options.platform) then
        -- autodetect from OS if not setted
        if(ly.sys.isANDROID) then
            self._options.platform = PLATFORM_GOOGLE
        elseif(ly.sys.isIOS) then
            self._options.platform = PLATFORM_APPLE
        else
            self._options.platform =''
            self._options.enabled = false
        end
    end
    
    --internal ststus
    self._options.status_init = false -- true when init is called
    self._options.status_login = false -- true when user il logged (ignored for Apple)
    
    
end

--------------------------------------------------------------------------------
-- constructor
--------------------------------------------------------------------------------

local GameNetwork = Class(Super) --inherits Object Class

function GameNetwork:initialize(...)
    if(Super.initialize(self, unpack(arg))) then
        self._className = CLASS_NAME
        _init(self, self._args[1])
        return true
    else
        return false
    end
end

function GameNetwork:finalize()
    -- clear local
    self._data = nil
    
    --calls parent Class's finalize()
    --with "self" as this instance
    Super.finalize(self)
end

--------------------------------------------------------------------------------
-- public
--------------------------------------------------------------------------------

function GameNetwork:enabled()
    return ly.toBoolean(self._options.enabled)
end

-- NATIVE

function GameNetwork:init(callback)
    if(self:enabled()) then
        _gn_init(self, callback)
    else
        ly.invoke(callback, ERR_NOT_ENABLED, false)  
    end
end

function GameNetwork:request(command, params)
    if(self:enabled()) then
        _gn_request(self, command, params)
    else
        print('GAME_NETWORK not enabled')
    end
end

function GameNetwork:show(command, params)
    if(self:enabled()) then
        _gn_show(self, command, params)
    end
end

-- PROPERTIES

function GameNetwork:initialized()
    return self._options.status_init
end

-- WRAPPERS

--- Login. This affect only GOOGLE and is ignored for APPLE.
-- Auto-Initialize the connection, no need to call 'init'
-- @param callback Callback
-- @param silent (ONLY GOOGLE) If it's set to true, this call will simply attempt to connect 
--              without trying to resolve any errors.
--              Set to true only if you want 

function GameNetwork:login(callback, silent)
    self:init(function(err, response) 
        if(ly.isNotNull(err)) then
            ly.invoke(callback, err)
        else
            _login(self, callback, silent)
        end
    end)
end

function GameNetwork:logout(callback)
    self:init(function(err, response) 
        if(ly.isNotNull(err)) then
            ly.invoke(callback, err)
        else
            _logout(self, callback)
        end
    end)
end

--- Get current player and login if required
--  Auto-Initialize the connection, no need to call 'init' 
-- @param callback
function GameNetwork:loadLocalPlayer(callback)
    self:init(function(err, response) 
        if(ly.isNotNull(err)) then
            ly.invoke(callback, err)
        else
            _load_local_player(self, callback)
        end
    end)
end



--------------------------------------------------------------------------------
-- export
--------------------------------------------------------------------------------

GameNetwork.GOOGLE = PLATFORM_GOOGLE
GameNetwork.APPLE = PLATFORM_APPLE

return GameNetwork



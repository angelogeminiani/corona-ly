---
-- Helper class to parse URL
-- 

-- 

local CLASS_NAME = 'ly.classes.url.URL'
--------------------------------------------------------------------------------
-- import
--------------------------------------------------------------------------------
local ly = require('ly.ly')

local Super = require('ly.components.BaseComponent') -- Superclass reference

--------------------------------------------------------------------------------
-- const
--------------------------------------------------------------------------------

local SEP_PROTOCOL = '://'
local SEP_QUERY = '?'
local SEP_PARAM = '&'
local SEP_VALUE = '='

--------------------------------------------------------------------------------
-- private
--------------------------------------------------------------------------------

local function _parse(self, url)
    --get protocol
    local prot_tokens = ly.str.split(url, SEP_PROTOCOL)
    if(#prot_tokens==2) then
        -- set protocol
        self.protocol = prot_tokens[1]    
        
        -- parse path
        local path_tokens = ly.str.split(prot_tokens[2], SEP_QUERY)
        if(#path_tokens>0) then
            -- set path
            self.path = path_tokens[1]
            
            -- parse query
            if(#path_tokens==2) then
                -- set query
                self.query = path_tokens[2]
                
                if(ly.isNotEmpty(self.query)) then
                    --parse params
                    local param_tokens = ly.str.split(self.query, SEP_PARAM)
                    self.params = {}
                    ly.forEach(param_tokens, function (item) 
                        local item_tokens = ly.str.split(item, SEP_VALUE)
                        if(#item_tokens==2)then
                            --add value removing spaces
                            self.params[ly.str.trim(item_tokens[1])]=ly.str.trim(item_tokens[2])
                        end
                    end)
                end
            end
        end
    end
end

local function _set_url(self, options)
    if(ly.isObject(options)) then
        self.url = options.url or 'http://'
    elseif(ly.isString(options)) then
        self.url = options
    else
        self.url = ''
    end
    
    _parse(self, self.url)
end

--------------------------------------------------------------------------------
-- init
--------------------------------------------------------------------------------

local function _init(self)
    
    --init properties
    self.url = ''
    self.protocol = ''
    self.path = ''
    self.query = ''
    self.params = {}
    
    -- begin parse
    _set_url(self, self._options)
end

local function _free(self)
    
end

--------------------------------------------------------------------------------
-- constructor
--------------------------------------------------------------------------------

local ComponentClass = Class(Super) --inherits Object Class

function ComponentClass:initialize(options)
    if (Super.initialize(self, options)) then
        self._className = CLASS_NAME
        _init(self)
        return true
    else
        return false
    end
end

function ComponentClass:finalize()
    if(Super.finalize(self)) then
        -- clear internal fields
        _free(self)
        
        return true
    else
        return false
    end
end

--------------------------------------------------------------------------------
-- public
-------------------------------------------------------------------------------- 

function ComponentClass:setUrl(value)
    _set_url(self, value)
end

--------------------------------------------------------------------------------
-- export
-------------------------------------------------------------------------------- 

return ComponentClass




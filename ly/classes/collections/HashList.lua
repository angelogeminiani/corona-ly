--ly.classes.collections.HashList
--------------------------------------------------------------------------------
-- import
--------------------------------------------------------------------------------
local ly = require('ly.ly')
local Super = require('ly.lang.ObjectId') -- Superclass reference

--------------------------------------------------------------------------------
-- private
--------------------------------------------------------------------------------

local function _init(self)
    
    self._data = {
        indexes = {},   --array
        hashmap = {}    --table
    }
    self.count = #self._data.indexes
    
end

local function _initialized(self)
    return (ly.isNotNull(self._data)) and (ly.isNotNull(self._data.indexes)) and (ly.isNotNull(self._data.hashmap))
end

local function _indexToKey(self, idx)
    local key = nil
    local index = ly.toNumber(idx, 0)
    if (index>0) then
        key = self._data.indexes[index]  
    end 
    return key, index
end

local function _keyToIndex(self, key)
    local index = 0
    if(key) then
        key = tostring(key)
        ly.forEach(self._data.indexes, function(value, idx) 
            if (value==key) then
                index=idx
                return true
            end
        end)
    end
    return index, key
end

local function _get(self, key_or_index) 
    local result = nil
    if (_initialized(self)) then
        if (ly.isString(key_or_index)) then
            result = self._data.hashmap[key_or_index] 
        else    
            local key = _indexToKey(self, key_or_index)  
            if(key)then
                result = self._data.hashmap[key]
            end
        end
    end
    return result
end

local function _add(self, item, key)
    if (_initialized(self)) then
        local index = (#self._data.indexes) + 1 
        key = key or tostring(index)
        if (nil==self._data.hashmap[key]) then
            self._data.indexes[index] = key
            self._data.hashmap[key] = item
            self.count = #self._data.indexes
        else
            --exists
            error('Item with key "'..key..'" already exists!')
        end
    end
end

local function _remove(self, key_or_index)
    local result = nil
    local index, key
    if (_initialized(self)) then
        if (ly.isString(key_or_index)) then
            index, key = _keyToIndex(self, key_or_index)
        else    
            key, index = _indexToKey(self, key_or_index)  
        end 
        if(key and index>0) then
            result = self._data.hashmap[key]
            self._data.hashmap[key]=nil
            table.remove(self._data.indexes, index)
            self.count = table.maxn(self._data.indexes)  
        end
    end
    return result, key, index
end
--------------------------------------------------------------------------------
-- constructor
--------------------------------------------------------------------------------
local BaseClass = Class(Super) --inherits 

function BaseClass:initialize(...)
    --calls parent Class's initialize()
    --with "self" as this instance
    if (Super.initialize(self, unpack(arg))) then
        self._className = 'ly.classes.collections.HashList'
        _init(self) 
        return true
    else
        --already initialized
        return false
    end
end

function BaseClass:finalize()
    if (self._data) then
        self._data = nil
    end
    --calls parent Class's initialize()
    --with "self" as this instance
    return Super.finalize(self)
end

--------------------------------------------------------------------------------
-- public
--------------------------------------------------------------------------------

function BaseClass:clear()
    _init(self) 
end

function BaseClass:forEach(callback)
    if(ly.isFunction(callback) and _initialized(self)) then
        ly.forEach(self._data.hashmap, callback) 
    end
end

function BaseClass:filter(callback)
    local result = BaseClass:new()
    if(ly.isFunction(callback) and _initialized(self)) then
        ly.forEach(self._data.hashmap, function(value, key, index) 
            if(callback(value. key, index)) then
                result:add(value, key)
            end
        end)
    end
    return result
end

function BaseClass:get(key_or_index)
    return _get(self, key_or_index) 
end

function BaseClass:add(item, key)
    _add(self, item, key) 
end

function BaseClass:tryAdd(item, key)
    return ly.try(function() _add(self, item, key) end) 
end

function BaseClass:remove(key_or_index)
    return _remove(self, key_or_index) 
end

function BaseClass:pop()
    return _remove(self, self.count) 
end


--------------------------------------------------------------------------------
-- export
--------------------------------------------------------------------------------

return BaseClass


--------------------------------------------------------------------------------
-- import
--------------------------------------------------------------------------------

require ("ly.lang.Class")
local json = require "json"

local ly = require('ly.ly')

--------------------------------------------------------------------------------
-- fields
--------------------------------------------------------------------------------

local Object = Class() --inherits Base Class

--------------------------------------------------------------------------------
-- instance
--------------------------------------------------------------------------------

--INSTANCE FUNCTIONS

--- Constructor handler
-- @return Boolean: False if object was already initialized, otherwise True.
function Object:initialize(...)
    if (self._initialized) then
        return false
    end
    
    self._args = arg --table of variant arguments
    self._className = 'ly.lang.Object'
    --self._initialized = true
    
    return true
end

--- Destructor handler
-- @return Boolean: False if object was already finalized, otherwise True.
function Object:finalize()
    if (self._finalized) then
        return false
    end
    --self._finalized = true
    self._args = nil
    return true
end

function Object:toString()
    --return string instance of this object
    if (nil~=self) then
        return json.encode(self, {indent=true})
    else
        return 'Null instance of "self".'
    end
end

return Object
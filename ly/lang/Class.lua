--Class.lua
--[[
    Global class for inheritance and polymorphism.
    Implementation from this post:
    http://www.omidahourai.com/from-zero-to-oo-ardentkids-guide-to-object-oriented-lua-with-corona-sdk
    
    How to use:
        Require this file in your main.lua (require("ly.lang.Class"))
    
    Sample Object ('Classes/Object.lua'):
    ------------------------------------------  
        -- Classes/Object.lua
 
        local Object = Class() --inherits Base Class

        --INSTANCE FUNCTIONS
        function Object:initialize(img)
          --creates our instance's display image
            local image = display.newImage(img)
            self.image = image
        end

        function Object:show(config)
            --show this object somewhere on-screen
            local image = self.image
            image.x, image.y = config.x, config.y
            image.isVisible = true
        end

        function Object:hide()
            --hide this object
            local image = self.image
            image.isVisible = false 
        end

        return Object
    ------------------------------------------  

    Extends Classes/Object.lua:
    ------------------------------------------  
        -- Classes/Objects/Animal.lua

        local Super = require('Classes.Object') -- Superclass reference
        local Animal = Class(Super) --inherits Object Class

        --INSTANCE FUNCTIONS
        function Animal:initialize(img)
            --calls parent Class's initialize()
            --with "self" as this instance
            Super.initialize(self, img)
            self.health = 100
            self.lives = 1
        end

        function Animal:show(config)
            --calls parent Class's show()
            --with "self" as this instance
            Super.show(self, config)
        end

        function Animal:hide()
            --calls parent Class's hide()
            --with "self" as this instance
            Super.hide(self)
        end

        function Animal:move()
            --code to make animal move goes here.
        end

        function Animal:jump()
            --code to make animal jump goes here.
        end

        return Animal
    ------------------------------------------  
    
]]--
--------------------------------------------------------------------------------
-- import
--------------------------------------------------------------------------------

local ly = require('ly.ly')

--------------------------------------------------------------------------------
-- implementation
--------------------------------------------------------------------------------

local Base -- base of the class hierarchy (forward reference)

function Class(Super)
    Super = Super or Base
    local prototype = setmetatable({}, Super)
    prototype.class = prototype
    prototype.super = Super
    prototype.__index = prototype
    return prototype
end

Base = Class()

function Base:new(...)
    local instance = setmetatable({}, self)
    
    instance._initialized = false
    instance._finalized = false
    instance.onFree = nil --hook to handle finalization
    
    if (nil ~= instance.initialize) then
        instance:initialize(unpack(arg))
        instance._initialized = true
    end
    
    return instance
end

function Base:free()
    if (nil ~= self.finalize) and (not self._finalized) then
        --onFree hook
        if (ly.isFunction(self.onFree)) then
            ly.invoke(self.onFree)
        end
        --finalization
        self:finalize()
        self._finalized = true
    end
    return self._finalized
end

function Base:removeSelf()
    self:free()
end

function Base:initialize() return true end
function Base:finalize() return true end

function Base:get()
    local Instances = self.Instances
    if (not Instances[1]) then local obj = self:new() end
    return table.remove(Instances, 1)
end

function Base:dispose()
    table.insert(self.Instances, self)
end


--------------------------------------------------------------------------------
-- import
--------------------------------------------------------------------------------

local Super = require('ly.lang.Object') -- Superclass reference
local ly = require('ly.ly')

--------------------------------------------------------------------------------
-- private
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
-- constructor
--------------------------------------------------------------------------------

local BaseClass = Class(Super) --inherits Object Class

--INSTANCE FUNCTIONS
function BaseClass:initialize(...)
    --calls parent Class's initialize()
    --with "self" as this instance
    if (Super.initialize(self, unpack(arg))) then
        self._id = ly.guid.generate()
        self._className = 'ly.lang.ObjectId'
        return true
    else
        self._id = 'init error'
        return false
    end
end

function BaseClass:finalize()
    --calls parent Class's initialize()
    --with "self" as this instance
    if(Super.finalize(self)) then
        self._id = nil
        return true
    else
        return false
    end
end

--------------------------------------------------------------------------------
-- public
--------------------------------------------------------------------------------

function BaseClass:toString()
    return Super.toString(self)
end

function BaseClass:getValue()
    if(ly.isNotNull(self)) then
        return self._id
    else
        return '' 
    end
end

--------------------------------------------------------------------------------
-- export
--------------------------------------------------------------------------------

return BaseClass


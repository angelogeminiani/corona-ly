
--------------------------------------------------------------------------------
-- import
--------------------------------------------------------------------------------
local widget = require('widget')

local ly = require("ly.ly")

local Super = require('ly.ui.BaseInput') -- Superclass reference

--------------------------------------------------------------------------------
-- events
--------------------------------------------------------------------------------

local ON_CHANGED = Super.onChanged

--------------------------------------------------------------------------------
-- private
--------------------------------------------------------------------------------

local function _init_background(self)
    local options = self._options
    
    --POSITIONATE VIEW
    local bgWidth = options.width
    --if widget.isSeven() then
    --    bgWidth = bgWidth + opt.labelWidth -- make room in the box for the label
    --end
    local background = display.newRect( 0, 0, bgWidth, options.height)
    background:setFillColor(unpack(options.backgroundColor))
    background.strokeWidth = options.strokeWidth
    background.stroke = options.strokeColor
    background.anchorX = 0.5
    background.anchorY = 0.5
    
    return background
end

local function _init_switch(self)
    local options = self._options
    
    -- switch
    local switch = widget.newSwitch
    {
        left = 0,
        top = 0,
        style = options.style,
        id = options.id,
        initialSwitchState = self:getValue(),
        onRelease = function(event) 
            self:setValue(event.target.isOn)
            return true
        end
    }
    switch.anchorX = 0
    switch.anchorY = 0.5
    switch.x = 0
    switch.enabled = true --added custom property
    
    return switch
end

local function _init_text(self)
    local options = self._options
    local switch = self.view._switch
    
    --label
    local labelParameters = {
        x = 0,
        y = 0, 
        text = options.label,
        width = 0,
        height = 0,
        font = options.labelFont,
        fontSize = options.labelFontSize, 
        align = "left"
    }
    local text = display.newText(labelParameters)
    text:setFillColor(unpack(options.labelFontColor))
    text.x = (switch.width) + switch.x + 5
    text.y = options.height/2
    text.anchorX = 0
    text.anchorY = 0
    
    return text
end

local function _init(self)
    local options = self._options
    --position and size options
    options.x = options.x or 0
    options.y = options.y or 0
    options.width = options.width or display.contentWidth
    options.height = options.height or 20
    --style options
    options.style = options.style or 'onOff'
    options.backgroundColor = options.backgroundColor or nil
    --label options
    options.label = options.label or ""
    options.labelFont = options.labelFont or native.systemFont
    options.labelFontSize = options.labelFontSize or options.height * 0.67
    options.labelFontColor = options.labelFontColor or { 0, 0, 0 }
    
    local sceneGroup = self.view
    sceneGroup.x = options.x
    sceneGroup.y = options.y
    
    -- background
    if(options.backgroundColor)then
        sceneGroup._background = _init_background(self)
        sceneGroup:insert(sceneGroup._background)
    end
    
    -- switch
    sceneGroup._switch = _init_switch(self)
    sceneGroup:insert(sceneGroup._switch)
    
    -- text
    sceneGroup._text = _init_text(self)
    sceneGroup:insert(sceneGroup._text)
    
end

local function _set_enabled(self, value)
    if(self and self.view and self.view._switch and ly.isBoolean(value)) then
        self.view._switch.enabled = value
        if(value) then
            -- enabled
            transition.to(self.view._switch, {alpha=1, time=200})
        else
            -- not enabled
            transition.to(self.view._switch, {alpha=0.3, time=200})
        end
    end
end

--------------------------------------------------------------------------------
-- constructor
--------------------------------------------------------------------------------

local ElementClass = Class(Super) --inherits Object Class

function ElementClass:initialize(options)
    --calls parent Class's initialize()
    --with "self" as this instance
    if(Super.initialize(self, options)) then
        _init(self)
        
        self.height = self.view.height
        self.width = self.view.width
        
        return true
    else
        return false
    end
end

function ElementClass:finalize()
    --calls parent Class's initialize()
    --with "self" as this instance
    if(Super.finalize(self)) then
        if (nil~=self.view) then
            print('WARNING: Probable Memory Leak issue.') 
        end
        return true
    else
        return false
    end
end

--------------------------------------------------------------------------------
-- public
--------------------------------------------------------------------------------

function ElementClass:setLabel(value)
    self.view._text.text = value
end

function ElementClass:getLabel()
    return self.view._text.text
end

function ElementClass:setEnabled(value)
    _set_enabled(self, value)
end

function ElementClass:isEnabled()
    if(self and self.view and self.view._switch) then
        return self.view._switch.enabled
    end
    return false
end

function ElementClass:setValue(value, isAnimated)
    if(not ly.isNull(value)) then
        local switch = self.view._switch
        local isOn = ly.toBoolean(value, false)
        isAnimated=ly.toBoolean(isAnimated, false)
        
        if ( switch.enabled == false ) then
            switch:setState( { isOn = not isOn, isAnimated = isAnimated } )
        else
            self.view._switch:setState({
                isOn = isOn,
                isAnimated = isAnimated,
                onComplete=function() 
                    Super.setValue(self, value)
                end
            }) 
        end
    end
end

--------------------------------------------------------------------------------
-- export
--------------------------------------------------------------------------------

return ElementClass

--------------------------------------------------------------------------------
-- import
--------------------------------------------------------------------------------

local widget = require("widget")

local ly = require('ly.ly')
local Super = require('ly.ui.BaseInput') -- Superclass reference
local ElementClass = Class(Super) --inherits Object Class


--------------------------------------------------------------------------------
-- events
--------------------------------------------------------------------------------

local ON_BEGIN_CHANGE = 'begin_change'
local ON_CHANGING = 'changing'
local ON_CHANGED = Super.onChanged
local ON_END_CHANGE = 'end_change'

--------------------------------------------------------------------------------
-- CONSTANTS
--------------------------------------------------------------------------------

local COLOR_PLACEHOLDER = {0.5, 0.5, 0.5}
local COLOR_EDIT = {1,1,1}
local BULLET = '•'

local IS_ANDROID = false --ly.sys.osANDROID

--------------------------------------------------------------------------------
-- triggers
--------------------------------------------------------------------------------

local function _do_begin_changing(self, event)
    if (ly.isEventListener(self)) then
        event.target = self
        event.name = ON_BEGIN_CHANGE
        event.value = self._text
        event.oldValue = self._old_text
        self:dispatchEvent(event) 
    end
end

local function _do_on_changing(self, event)
    if (ly.isEventListener(self)) then
        event.target = self
        event.name = ON_CHANGING
        event.value = self._text
        event.oldValue = event.oldValue or self._old_text
        --print( event.newCharacters )
        --print( event.oldText )
        --print( event.startPosition )
        --print( event.text )
        self:dispatchEvent(event) 
    end
end

local function _do_end_changing(self, event)
    if (ly.isEventListener(self)) then
        event.target = self
        event.name = ON_END_CHANGE
        event.value = self._text
        event.oldValue = self._old_text
        self:dispatchEvent(event) 
    end
end

--------------------------------------------------------------------------------
-- private
--------------------------------------------------------------------------------

local function _secure(text, isSecure, placeholder)
    local result = text
    if(isSecure) then
        result = ly.str.fillRight('', #tostring(text), BULLET)
    else
        result = text
    end
    
    result = ly.toString(result, placeholder)
    if(ly.isEmpty(result)) then
        result = ly.str.fillRight('', 50, ' ') -- many spaces 
    end
    
    return result
end

local function _toggle_edit(self, callback)
    local sceneGroup = self.view
    if(ly.isDisplayObject(sceneGroup)) then
        local options = self._options
        if(ly.isNotNull(options)) then
            local isSecure = options.isSecure
            if(ly.isDisplayObject(sceneGroup._textField, sceneGroup._textLabel)) then
                sceneGroup._textField.isVisible = not sceneGroup._textField.isVisible
                if(not IS_ANDROID) then
                    sceneGroup._textLabel.isVisible = not sceneGroup._textLabel.isVisible
                end
                
                --focus
                native.setKeyboardFocus( nil )
                ly.delay(function() 
                    if(ly.isDisplayObject(sceneGroup)) then
                        if(ly.isDisplayObject(sceneGroup._textField, sceneGroup._textLabel))then
                            if(sceneGroup._textField.isVisible)then
                                native.setKeyboardFocus( sceneGroup._textField )
                            else
                                local value =_secure(sceneGroup._textField.text, isSecure, options.placeholder)
                                sceneGroup._textLabel.text = value
                                if (ly.isEmpty(sceneGroup._textField.text)) then
                                    sceneGroup._textLabel:setFillColor(unpack(COLOR_PLACEHOLDER))
                                else
                                    sceneGroup._textLabel:setFillColor(unpack{COLOR_EDIT})
                                end
                            end
                        end
                    end
                    ly.invoke(callback)
                end, 100)
                
            end
        end
    end
end

local function _handle_text_native_event(self, native_input, event)
    local phase = event.phase
    local options = self._options
    
    if(ly.isNull(options) or 
        not ly.isDisplayObject(self.view) or 
        not ly.isDisplayObject(self.view._textLabel) or 
        not native_input or
        self._changing) then
        return true
    end
    
    local isSecure = options.isSecure
    
    if "began" == phase then
        -- make sure we are in a display group
        -- the trick is our master object is a group, so we need to make sure our 
        -- grandparent isn't the stage
        if display.getCurrentStage() ~= native_input.parent.parent then
            -- make a guess at the keyboard height.  
            local kbHeight = 0.5 * display.contentHeight
            local fieldLoc = 0.25 * display.contentHeight
            -- scroll into view
            if native_input.y > kbHeight then
                native_input.yOrig = native_input.y
                transition.to(native_input.parent.parent, {time=500, y = fieldLoc})
            end
        end 
    elseif "submitted" == phase or "ended" == phase then
        -- Hide keyboard
        if native_input.yOrig ~= native_input.y then -- we have been scrolled
            transition.to(native_input.parent.parent, {time=500, y = native_input.yOrig})
        end
    end
    
    -- If there is a listener defined, execute it
    if (ly.isFunction(options.listener)) then
        options.listener( event )
    end
    
    -- events
    if event.phase == "began" then
        -- user begins editing textField
        self._old_text = native_input.text or ''
        self._text = native_input.text
        _do_begin_changing(self, event)
    elseif event.phase == "editing" then
        self._text = event.text
        self.view._textLabel.text = _secure(self._text, isSecure, options.placeholder)
        _do_on_changing(self, event)
    elseif event.phase == "ended" or event.phase == "submitted" then
        self._changing = true
        _toggle_edit(self, function() 
            if (self._old_text~=self._text) then
                Super.setValue(self, self._text)
            end
            _do_end_changing(self, event)
            self._changing = false
        end)
    end
end

local function _build_edit(self, options)
    local self_ref = self
    local sceneGroup = self.view
    local deviceScale = (display.pixelWidth / display.contentWidth) * 0.5
    local labelPadding = options.labelPadding
    local cornerRadius = options.cornerRadius
    local t_height = options.height - options.strokeWidth * 2 - 4
    if "Android" == system.getInfo("platformName") then
        --
        -- Older Android devices have extra "chrome" that needs to be compensated for.
        --
        t_height = t_height + 10
    end
    
    local t_width = options.width - cornerRadius - labelPadding
    local x = sceneGroup.x + labelPadding 
    local y = sceneGroup._background.y + (sceneGroup._background.height-sceneGroup._text.height)/2
    local value = options.value
    local placeholder = options.placeholder
    local isSecure = options.isSecure
    
    --LABEL EDIT
    local labelParameters = {
        x = labelPadding , y = y, 
        text = _secure(value, isSecure, placeholder),
        --width = t_width,  height = t_height,
        font = native.systemFont, --options.labelFont,
        fontSize = options.labelFontSize, 
        align = "left"
    }
    sceneGroup._textLabel = display.newText(labelParameters)
    sceneGroup._textLabel.anchorX = 0 -- aligned at left
    sceneGroup._textLabel.anchorY = 0
    if(ly.isEmpty(value)) then
        sceneGroup._textLabel:setFillColor(unpack(COLOR_PLACEHOLDER))
    else
        sceneGroup._textLabel:setFillColor(unpack{COLOR_EDIT})
    end
    sceneGroup._textLabel.isVisible = true
    sceneGroup:insert(sceneGroup._textLabel)
    sceneGroup._textLabel:addEventListener('tap', function(event) 
        _toggle_edit(self) 
        return true
    end)
    
    --NATIVE EDIT
    if (IS_ANDROID) then
        sceneGroup._textField = native.newTextField(0, 0, 5, t_height )
    else
        sceneGroup._textField = native.newTextField(0, 0, t_width, t_height )
    end
    sceneGroup._textField.anchorX = 0 -- aligned at left
    sceneGroup._textField.anchorY = 0 
    sceneGroup._textField.x = x 
    sceneGroup._textField.y = y
    sceneGroup._textField.hasBackground = false
    sceneGroup._textField.inputType = options.inputType
    sceneGroup._textField.text = value
    sceneGroup._textField.isSecure = options.isSecure
    sceneGroup._textField.placeholder = placeholder
    sceneGroup._textField.isVisible = false
    --font size
    sceneGroup._textField.isFontSizeScaled = true  -- make the field use the same font units as the text object
    --sceneGroup._textField.font = native.newFont( options.font, options.fontSize * deviceScale )
    --sceneGroup._textField.size = options.fontSize * deviceScale
    sceneGroup._textField.size = sceneGroup._textLabel.size
    --sceneGroup._textField:resizeHeightToFitFont()
    
    --print(options.font, options.fontSize, deviceScale)
    -- Function to listen for textbox events
    function sceneGroup._textField:_inputListener( event )
        _handle_text_native_event(self_ref, self, event)
    end
    sceneGroup._textField.userInput = sceneGroup._textField._inputListener
    sceneGroup._textField:addEventListener( "userInput" )
    
end

local function _init(self)
    local options = self._options
    
    options.offsetX = options.offsetX or 0 --applied to native text to align X
    options.offsetY = options.offsetY or 0 --applied to native text to align Y
    options.left = options.left or 0
    options.top = options.top or 0
    options.x = options.x or 0
    options.y = options.y or 0
    options.width = options.width or (display.contentWidth * 0.75)
    options.height = options.height or 20
    options.listener = options.listener or nil
    options.text = options.value or options.text or '' -- DEPRECATED
    options.value = options.value or options.text or ''
    options.inputType = options.inputType or "default"
    options.isSecure = options.isSecure or false
    options.font = options.font or native.systemFont
    options.fontSize = options.fontSize or options.height * 0.67
    options.fontColor = options.fontColor or { 0.25, 0.25, 0.25 }
    options.placeholder = options.placeholder or nil
    options.label = options.label or ""
    options.labelWidth = options.labelWidth or options.width * 0.10
    options.labelFont = options.labelFont or native.systemFontBold
    options.labelFontSize = options.labelFontSize or options.fontSize
    options.labelFontColor = options.labelFontColor or { 0, 0, 0 }
    -- Vector options
    options.strokeWidth = options.strokeWidth or 2
    options.cornerRadius = options.cornerRadius or options.height * 0.33 or 10
    options.strokeColor = options.strokeColor or {0, 0, 0}
    options.backgroundColor = options.backgroundColor or { 1, 1, 1 }
    options.labelPadding = 0
    -- iOS7
    if widget.isSeven() then
        options.labelPadding = options.labelWidth + (options.cornerRadius + (options.labelWidth * 0.5)) 
    end
    
    local sceneGroup = self.view
    if options.left then
        sceneGroup.x = options.left -- options.left + options.width * 0.5
    elseif options.x then
        sceneGroup.x = options.x
    end
    if options.top then
        sceneGroup.y = options.top + options.height * 0.5
    elseif options.y then
        sceneGroup.y = options.y + options.height * 0.5
    end
    
    local bgWidth = options.width
    --if widget.isSeven() then
    --    bgWidth = bgWidth + opt.labelWidth -- make room in the box for the label
    --end
    
    sceneGroup._background = display.newRoundedRect( 0, 0, bgWidth, options.height, options.cornerRadius )
    sceneGroup._background.anchorX = 0
    sceneGroup._background.anchorY = 0
    sceneGroup._background:setFillColor(unpack(options.backgroundColor))
    sceneGroup._background.strokeWidth = options.strokeWidth
    sceneGroup._background.stroke = options.strokeColor
    sceneGroup:insert(sceneGroup._background)
    
    --print("x", sceneGroup.x, "y", sceneGroup.y)
    --
    -- Support adding a label.
    -- iOS 6 and earlier and Android draw the Label above the field.
    -- iOS 7 draws it in the field.
    
    --label
    local labelParameters = {
        x = 0,
        y = 0, 
        text = options.label,
        width = options.labelWidth,
        height = 0,
        font = options.labelFont,
        fontSize = options.labelFontSize, 
        align = "left"
    }
    sceneGroup._text = display.newText(labelParameters)
    sceneGroup._text.anchorX = 0
    sceneGroup._text.anchorY = 0
    sceneGroup._text:setFillColor(unpack(options.labelFontColor))
    sceneGroup._text.x = options.cornerRadius --sceneGroup._background.x - bgWidth / 2 + options.cornerRadius + options.labelWidth * 0.5
    sceneGroup._text.y = sceneGroup._background.y + (sceneGroup._background.height-sceneGroup._text.height)/2
    if not widget.isSeven() then
        sceneGroup._text.y = sceneGroup._background.y + options.height + 5
    end
    sceneGroup:insert(sceneGroup._text)
    
    -- create the native.newTextField to handle the input
    _build_edit(self, options)
    
    
    local on_sync_fields = function (event)
        if(ly.isDisplayObject(sceneGroup)) then
            if(ly.isDisplayObject(sceneGroup._textField)) then
                if (ly.isNotNull(sceneGroup.x)) then 
                    local x, y = sceneGroup:localToContent( sceneGroup.x, sceneGroup.y )
                    sceneGroup._textField.x = options.labelPadding + options.offsetX
                    sceneGroup._textField.y = y - sceneGroup.y +2 + options.offsetY
                    sceneGroup._textField.alpha = sceneGroup.alpha
                    if not sceneGroup.isVisible then
                        -- move the text field off screen when the display field is hidden
                        sceneGroup._textField.y = sceneGroup._textField.y * -1 
                    end
                end
            end
        end
    end
    Runtime:addEventListener( "enterFrame", on_sync_fields )
    
    
    self.onFree = function()
        Runtime:removeEventListener( "enterFrame", on_sync_fields )
    end
end

local function _free(self)
    local sceneGroup = self.view
    if(ly.isDisplayObject(sceneGroup)) then
        if (ly.isDisplayObject(sceneGroup._textField)) then
            sceneGroup._textField:removeSelf()
            sceneGroup._textField = nil
        end
        if (ly.isDisplayObject(sceneGroup._textLabel)) then
            sceneGroup._textLabel:removeSelf()
            sceneGroup._textLabel = nil
        end
        if (ly.isDisplayObject(sceneGroup._text)) then
            sceneGroup._text:removeSelf()
            sceneGroup._text = nil
        end
    end
end

--------------------------------------------------------------------------------
-- constructor
--------------------------------------------------------------------------------

function ElementClass:initialize(options)
    --calls parent Class's initialize()
    --with "self" as this instance
    if(Super.initialize(self, options)) then
        _init(self, options)
        
        self.height = self.view.height
        self.width = self.view.width
        
        return true
    else
        return false
    end
end

function ElementClass:finalize()
    _free(self)
    if(Super.finalize(self)) then
        if (nil~=self.view) then
            print('WARNING: Probable Memory Leak issue.') 
        end
        return true
    else
        return false
    end
end

--------------------------------------------------------------------------------
-- override abstract methods
--------------------------------------------------------------------------------

function ElementClass:_viewRefresh()
    --print('REFRESH VIEW') 
end

--------------------------------------------------------------------------------
-- public
--------------------------------------------------------------------------------

ElementClass.onBeginChange = ON_BEGIN_CHANGE
ElementClass.onChanging = ON_CHANGING
ElementClass.onChanged = ON_CHANGED
ElementClass.onEndChange = ON_END_CHANGE


function ElementClass:setLabel(value)
    if(self and self.view and self.view._text) then
        self.view._text.text = value
    end
end

function ElementClass:getLabel()
    if(self and self.view and self.view._text) then
        return self.view._text.text
    end
    return ''
end

function ElementClass:setValue(value)
    if(self and self.view and self.view._textField) then
        local options = self._options
        self.view._textField.text = value
        self.view._textLabel.text = ly.toString(value, options.placeholder)
        Super.setValue(self, value)
    end
end

function ElementClass:getValue()
    if(self and self.view and self.view._textField) then
        return self.view._textField.text
    end
    return ''
end

function ElementClass:setKeyboardFocus(callback)
    if(self) then
        if (ly.isDisplayObject(self.view) and ly.isDisplayObject(self.view._textField)) then
            _toggle_edit(self, function()
                --native.setKeyboardFocus( self.view._textField )
                ly.invoke(callback)
            end)
        end
    end
end

--------------------------------------------------------------------------------
-- export
--------------------------------------------------------------------------------

return ElementClass






--ly.ui.inputs.iconswitch.IconSwitch
--------------------------------------------------------------------------------
-- import
--------------------------------------------------------------------------------
local widget = require('widget')

local ly = require("ly.ly")

local Super = require('ly.ui.BaseInput') -- Superclass reference

--------------------------------------------------------------------------------
-- events
--------------------------------------------------------------------------------

local ON_CHANGED = Super.onChanged

--------------------------------------------------------------------------------
-- const
--------------------------------------------------------------------------------

local ICON_ON = 'ly/ui/inputs/iconswitch/btn_star_on.png'
local ICON_OFF = 'ly/ui/inputs/iconswitch/btn_star_off.png'

--------------------------------------------------------------------------------
-- private
--------------------------------------------------------------------------------

local function _set_enabled(self, value)
    if(self and self.view and self.view._switch and ly.isBoolean(value)) then
        self.view._switch.enabled = value
        if(value) then
            -- enabled
            transition.to(self.view._switch, {alpha=1, time=200})
        else
            -- not enabled
            transition.to(self.view._switch, {alpha=0.3, time=200})
        end
    end
end

local function _set_value(self, value, isAnimated, triggerEvent)
    if(not ly.isNull(value)) then
        local switch = self.view._switch
        local isOn = ly.toBoolean(value, false)
        isAnimated=ly.toBoolean(isAnimated, true)
        
        if ( switch.enabled == false ) then
            -- disabled
        else
            local alpha_on = ly.conditional(isOn, 1, 0)
            local alpha_off = ly.conditional(isOn, 0, 1)
            transition.to(switch.img_on, {time=400, alpha=alpha_on})
            transition.to(switch.img_off, {time=400, alpha=alpha_off, 
                onComplete=function() 
                    Super.setValue(self, isOn, triggerEvent)
                end
            })
        end
    end
end

--------------------------------------------------------------------------------
-- handlers
--------------------------------------------------------------------------------

local function _on_tap_switch(self)
    local options = self._options
    if(options.locked) then
        return
    end
    if (ly.isDisplayObject(self.view)) then
        local switch = self.view._switch
        if (ly.isDisplayObject(switch) and switch.enabled) then
            local isOn = self:getValue()
            _set_value(self, not isOn, true)
        end
    end
end

--------------------------------------------------------------------------------
-- init
--------------------------------------------------------------------------------

local function _init_background(self)
    local options = self._options
    
    --POSITIONATE VIEW
    local bgWidth = options.width
    --if widget.isSeven() then
    --    bgWidth = bgWidth + opt.labelWidth -- make room in the box for the label
    --end
    local background = display.newRect( 0, 0, bgWidth, options.height)
    background:setFillColor(unpack(options.backgroundColor))
    background.strokeWidth = options.strokeWidth
    background.stroke = options.strokeColor
    background.anchorX = 0.5
    background.anchorY = 0.5
    
    return background
end

local function _init_switch(self)
    local options = self._options
    local width = options.iconWidth
    local height = options.iconHeight
    
    -- switch
    local switch = display.newGroup()
    
    switch.img_on = display.newImage(options.iconOn)
    ly.graphics.resizeByWidth(switch.img_on, width)
    switch.img_on.anchorX = 0
    switch.img_on.anchorY = 0
    switch.img_on.x = 0
    switch.img_on.y = 0
    switch:insert(switch.img_on)
    
    switch.img_off = display.newImage(options.iconOff)
    ly.graphics.resizeByWidth(switch.img_off, width)
    switch.img_off.anchorX = 0
    switch.img_off.anchorY = 0
    switch.img_off.x = 0
    switch.img_off.y = 0
    switch:insert(switch.img_off)
    
    switch.enabled = true --added custom property
    
    return switch
end

local function _init_text(self)
    local options = self._options
    local switch = self.view._switch
    
    --label
    local labelParameters = {
        x = (switch.width) + switch.x + 5,
        y = switch.height*0.5, 
        text = options.label,
        width = 0,
        height = 0,
        font = options.labelFont,
        fontSize = options.labelFontSize, 
        align = "left"
    }
    local text = display.newText(labelParameters)
    text:setFillColor(unpack(options.labelFontColor))
    text.anchorX = 0
    text.anchorY = 0.5
    
    return text
end

local function _init(self)
    local options = self._options
    --position and size options
    options.x = options.x or 0
    options.y = options.y or 0
    options.width = options.width or display.contentWidth
    options.height = options.height or 48
    --style options
    options.iconOn = options.iconOn or ICON_ON
    options.iconOff = options.iconOff or ICON_OFF
    options.iconWidth = options.iconWidth or 48
    options.iconHeight = options.iconHeight or 48
    options.backgroundColor = options.backgroundColor or nil
    --label options
    options.label = options.label or ""
    options.labelFont = options.labelFont or native.systemFont
    options.labelFontSize = options.labelFontSize or options.height * 0.67
    options.labelFontColor = options.labelFontColor or { 0, 0, 0 }
    --attributes
    options.locked = ly.toBoolean(options.locked)
    --data
    self.id = options.id
    
    local sceneGroup = self.view
    sceneGroup.x = options.x
    sceneGroup.y = options.y
    
    -- background
    if(options.backgroundColor)then
        sceneGroup._background = _init_background(self)
        sceneGroup:insert(sceneGroup._background)
    end
    
    -- switch
    sceneGroup._switch = _init_switch(self)
    sceneGroup:insert(sceneGroup._switch)
    
    -- text
    sceneGroup._text = _init_text(self)
    sceneGroup:insert(sceneGroup._text)
    
    -- value
    local value = self:getValue() or false
    self:setValue(value)
    
    --handlers
    local function on_tap_switch(event)
        _on_tap_switch(self)
        return true
    end
    if(ly.isEventListener(sceneGroup._switch))then
        sceneGroup._switch:addEventListener('tap', on_tap_switch)
    end
    
    self.onFree = function()
        if(ly.isDisplayObject(self.view)) then
            if(ly.isEventListener(sceneGroup._switch))then
                sceneGroup._switch:removeEventListener('tap', on_tap_switch)
            end
        end
    end
end



--------------------------------------------------------------------------------
-- constructor
--------------------------------------------------------------------------------

local ElementClass = Class(Super) --inherits Object Class

function ElementClass:initialize(options)
    --calls parent Class's initialize()
    --with "self" as this instance
    if(Super.initialize(self, options)) then
        _init(self)
        
        self.height = self.view.height
        self.width = self.view.width
        
        return true
    else
        return false
    end
end

function ElementClass:finalize()
    --calls parent Class's initialize()
    --with "self" as this instance
    if(Super.finalize(self)) then
        if (nil~=self.view) then
            print('WARNING: Probable Memory Leak issue.') 
        end
        return true
    else
        return false
    end
end

--------------------------------------------------------------------------------
-- public
--------------------------------------------------------------------------------

function ElementClass:setLabel(value)
    self.view._text.text = value
end

function ElementClass:getLabel()
    return self.view._text.text
end

function ElementClass:setEnabled(value)
    _set_enabled(self, value)
end

function ElementClass:isEnabled()
    if(self and self.view and self.view._switch) then
        return ly.toBoolean(self.view._switch.enabled)
    end
    return false
end

function ElementClass:setValue(value, isAnimated, triggerEvent)
    triggerEvent = ly.toBoolean(triggerEvent, true)
    _set_value(self, value, isAnimated, triggerEvent)
end

--------------------------------------------------------------------------------
-- export
--------------------------------------------------------------------------------

return ElementClass
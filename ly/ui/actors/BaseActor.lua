--------------------------------------------------------------------------------
-- import
--------------------------------------------------------------------------------
-- @return

local ly = require('ly.ly')
local SoundTableClass = require('ly.components.soundtable.SoundTable')

local Super = require('ly.ui.BaseControl') -- Superclass reference
local ElementClass = Class(Super) --inherits Object Class

--------------------------------------------------------------------------------
-- CONST
--------------------------------------------------------------------------------

local EVENT_ANIMATION = 'on_animation'
local EVENT_CLICK = 'on_click'
local EVENT_PLAY = 'on_play'
local EVENT_PAUSE = 'on_pause'

--------------------------------------------------------------------------------
-- private
--------------------------------------------------------------------------------

local function _resetCounters(self)
    local callback = self._loopCallback
    
    self._loopCount = 1
    self._loopDone = 0
    self._loopCallback = nil
    
    -- invoke pending callback
    ly.invoke(callback)
end

local function _countLoop(self)
    --in counter
    self._loopDone = self._loopDone + 1
    
    if(self._loopDone>=self._loopCount) then
        
        if (self:isPlaying()) then
            self:stop()
        else
            _resetCounters(self)
        end
    end
end

local function _handle_tap(event)
    local sprite = event.target
    if(sprite and sprite._owner) then
        -- extend event
        event.actor = sprite._owner
        event.name = EVENT_CLICK
        
        --dispatch event
        sprite._owner:dispatchEvent(event)
    end
    return true
end

local function _handle_sprite(event)
    local phase = event.phase --began, next, ended, bounce, loop
    local sprite = event.target
    
    --print('BASE: '..phase)
    
    if (sprite) then
        local self = sprite._owner
        
        -- extend event
        event.actor = self
        
        -- dipatch native event
        self:dispatchEvent(event)
        
        if ('began'==phase)then
            -- The animation began playing.
            event.name = EVENT_ANIMATION
            event.phase = 'began'
            self:dispatchEvent(event)
        elseif('next'==phase) then
            -- The animation played a subsequent frame that's not one of the above phases.
            
        elseif('ended'==phase) then
            -- The animation finished playing.
            _countLoop(self)
            
            event.name = EVENT_ANIMATION
            event.phase = 'ended'
            self:dispatchEvent(event)
        elseif('bounce'==phase) then
            -- The animation bounced from forward to backward while playing.
            
        elseif('loop'==phase) then
            -- The animation looped from the beginning of the sequence.
            _countLoop(self)
            
            event.name = EVENT_ANIMATION
            event.phase = 'ended'
            self:dispatchEvent(event)
        end
    end
    return true
end

---
-- @param self
-- @param options {sound={baseDir=nil}}
-- @return
local function _build_instance(self, options)
    options = options or {}
    
    local sceneGroup = self.view
    sceneGroup.anchorChildren = true
    sceneGroup.x = display.contentCenterX
    sceneGroup.y = display.contentCenterY
    
    -- options
    options.name = options.name or ''
    options.minX = options.minX or (-1 * ly.screen.width/2)
    options.maxX = options.maxX or ly.screen.width/2
    ly.defaults(self, options, false)
    
    _resetCounters(self)
    
    -- sound table
    self.soundTable = SoundTableClass:new(options.sound) 
end

local function _remove_handlers(self)
    local sceneGroup = self.view
    
    if (sceneGroup._sprite) then
        sceneGroup._sprite:removeEventListener('sprite', _handle_sprite)
        sceneGroup._sprite:removeEventListener('tap', _handle_tap)
    end
end

local function _add_handlers(self)
    local sceneGroup = self.view
    
    if (sceneGroup._sprite) then
        sceneGroup._sprite:addEventListener('sprite', _handle_sprite)
        sceneGroup._sprite:addEventListener('tap', _handle_tap)
    end
end

local function _set_sprite(self, defaultSheet, sequence)
    local sceneGroup = self.view
    
    _remove_handlers(self)
    
    if (sceneGroup._sprite) then
        sceneGroup:remove(sceneGroup._sprite)
        sceneGroup._sprite = nil
    end
    
    sceneGroup._sprite = display.newSprite( defaultSheet, sequence )
    sceneGroup._sprite._owner = self
    sceneGroup:insert(sceneGroup._sprite)
    
    _add_handlers(self)
end

--------------------------------------------------------------------------------
-- constructor
--------------------------------------------------------------------------------

---
-- @param options {sound={baseDir=nil}}
-- @return
function ElementClass:initialize(options)
    --calls parent Class's initialize()
    --with "self" as this instance
    if(Super.initialize(self, options)) then
        _build_instance(self, options)
        
        self.height = self.view.height
        self.width = self.view.width
        return true
    else
        return false
    end
end

function ElementClass:finalize()
    --calls parent Class's initialize()
    --with "self" as this instance
    if(Super.finalize(self)) then
        -- clear internal fields
        --free soundTable
        if (self.soundTable) then
            self.soundTable:free()
            self.soundTable = nil
        end
        return true
    else
        return false
    end
end

--------------------------------------------------------------------------------
-- public
--------------------------------------------------------------------------------

ElementClass.EVENT_ANIMATION = EVENT_ANIMATION
ElementClass.EVENT_CLICK = EVENT_CLICK
ElementClass.EVENT_PLAY = EVENT_PLAY
ElementClass.EVENT_PAUSE = EVENT_PAUSE

function ElementClass:setSprite(defaultSheet, sequence)
    local sceneGroup = self.view
    if (sceneGroup) then
        _set_sprite(self, defaultSheet, sequence) 
    end
end

function ElementClass:isPlaying()
    local sceneGroup = self.view
    if(sceneGroup and sceneGroup._sprite)then
        return sceneGroup._sprite.isPlaying
    end
    return false
end

function ElementClass:stop()
    --reset loop
    _resetCounters(self)
    
    local sceneGroup = self.view
    if(sceneGroup and sceneGroup._sprite)then
        sceneGroup._sprite:pause()
        sceneGroup._sprite:setFrame(1)
        return true
    end
    return false
end

function ElementClass:pause()
    local sceneGroup = self.view
    if(sceneGroup and sceneGroup._sprite)then
        sceneGroup._sprite:pause()
        
        --pause event
        self:dispatchEvent({name=EVENT_PAUSE, sequence=sceneGroup._sprite.sequence, target=self})
        
        return true
    end
    return false
end

function ElementClass:play(sequence, loopCount, loopCallback)
    self:stop()
    local sceneGroup = self.view
    if (sceneGroup and sceneGroup._sprite) then
        if (ly.isNull(loopCount)) then
            loopCount = 1; --just once
        end
        
        self._loopCount = loopCount
        self._loopDone = 0
        self._loopCallback = loopCallback
        
        --play event
        self:dispatchEvent({name=EVENT_PLAY, sequence=sequence, target=self})
        
        sceneGroup._sprite:setSequence(sequence)
        sceneGroup._sprite:play()
    end
end

function ElementClass:setSequence(sequence, frame)
    local sceneGroup = self.view
    if(sceneGroup and sceneGroup._sprite)then
        if (ly.isNull(frame)) then
            frame = 1; --first
        end
        sceneGroup._sprite:pause()
        sceneGroup._sprite:setSequence(sequence)
        sceneGroup._sprite:setFrame(frame)
        return true
    end
    return false
end

--------------------------------------------------------------------------------
-- export
--------------------------------------------------------------------------------

return ElementClass


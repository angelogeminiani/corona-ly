--[[ 

  Because containers use a mask and there is a nested masking limit of 3, 
  care must be taken when inserting other masked objects into containers. 
  Display objects that utilize masks include 
  display.newText, display.newEmbossedText, widget.newScrollView, widget.newTableView, 
  other containers, and any other masked image, masked vector object, 
  or masked display group. 
  For example, a text object (one mask) inside a container (one mask) 
  inside yet another container (one mask) would reach but 
  not exceed the limit of 3 nested masks.
  
]]--
local CLASS_NAME = 'ly.ui.BaseContainer'

--------------------------------------------------------------------------------
-- import
--------------------------------------------------------------------------------

local ly = require("ly.ly")

local Super = require('ly.ui.BaseControl') -- Superclass reference


--------------------------------------------------------------------------------
-- const
--------------------------------------------------------------------------------



--------------------------------------------------------------------------------
-- private
--------------------------------------------------------------------------------


local function _init(self)
    local options = self._options or {}
    
    --positionate container
    self.view.x = options.x
    self.view.y = options.y
    
    
    local super_on_free = self.onFree
    self.onFree = function()
        ly.invoke(super_on_free)
    end
end



--------------------------------------------------------------------------------
-- constructor
--------------------------------------------------------------------------------

local ElementClass = Class(Super) --inherits Object Class

function ElementClass:initialize(options)
    
    options = options or {}
    options.width = options.width or ly.screen.width
    options.height = options.height or ly.screen.height
    options.x = options.x or ly.screen.centerX
    options.y = options.y or ly.screen.centerY
    
    options.view = display.newContainer(options.width, options.height)
    
    if(Super.initialize(self, options)) then
        self._className = CLASS_NAME
        _init(self, options)
        
        self.height = self.view.height
        self.width = self.view.width
        
        return true
    else
        return false
    end
end

function ElementClass:finalize()
    --calls parent Class's initialize()
    --with "self" as this instance
    if (Super.finalize(self)) then
        if (nil~=self.view) then
            print('WARNING: Probable Memory Leak issue.') 
        end
        return true
    else
        return false
    end
    
end

--------------------------------------------------------------------------------
-- public
--------------------------------------------------------------------------------


--------------------------------------------------------------------------------
-- export
--------------------------------------------------------------------------------

return ElementClass
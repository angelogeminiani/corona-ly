local CLASS_NAME = 'ly.ui.screens.web.WebScreen'
--------------------------------------------------------------------------------
-- import
--------------------------------------------------------------------------------
local widget = require('widget')

local ly = require('ly.ly')

local Super = require('ly.ui.BaseControl') -- Superclass reference


--------------------------------------------------------------------------------
-- const
--------------------------------------------------------------------------------

local BACKGROUND =  { 0.55, 0.55, 0.55, 1 }

--------------------------------------------------------------------------------
-- private
--------------------------------------------------------------------------------



local function _toggle(self, visible)
    
end

local function _navigate(self, url, baseDir)
    if (ly.isDisplayObject(self.view)) then
        if(ly.isNotNull(self.view._webview)) then
            baseDir = baseDir or system.ResourceDirectory
            if (ly.path.isHttp(url) or ly.path.isHttps(url)) then
                --remote
                self.view._webview:request(url)
            else
                --local
                self.view._webview:request(url, baseDir)
            end
        end
    end
end


--------------------------------------------------------------------------------
-- init
--------------------------------------------------------------------------------

local function _init_background(self)
    local options = self._options
    local sceneGroup = self.view
    
    sceneGroup._background = display.newRect(options.x, options.y, options.width, options.height)
    sceneGroup._background:setFillColor( unpack(BACKGROUND) )
    sceneGroup:insert(sceneGroup._background)
    
    sceneGroup._background:addEventListener('tap', function() return true end)
    sceneGroup._background:addEventListener('touch', function() return true end)
end

local function _init_spinner(self)
    local options = self._options
    local sceneGroup = self.view
    
    sceneGroup._spinner = widget.newSpinner({ 
        width = 48, 
        height = 48, 
    })
    sceneGroup._spinner.x = options.x
    sceneGroup._spinner.y = options.y
    sceneGroup:insert(sceneGroup._spinner)
    sceneGroup._spinner:start()
end

local function _init_webview(self)
    if (ly.isDisplayObject(self.view)) then
        if(ly.isNull(self.view._webview)) then
            local options = self._options
            self.view._webview = native.newWebView( options.x, options.y, options.width, options.height )
            self.view._webview.hasBackground = false
            self.view:insert(self.view._webview)
        end
    end
end

local function _init(self, options)
    options = options or {}
    
    options.params = options.params or {} --page params
    
    options.width = options.width or ly.screen.width
    options.height = options.height or ly.screen.height
    options.x = options.x or options.width*0.5
    options.y = options.y or options.height*0.5
    
    options.url = options.url or ''
    
    
    _init_background(self)
    _init_spinner(self)
    _init_webview(self)
    
    if (ly.isNotEmpty(options.url)) then
        self:navigate(options.url)
    end
    
    self.onFree = function()
        if (ly.isEventListener(self.view)) then
            
        end
    end
end

local function _free(self)
    if(ly.isDisplayObject(self.view)) then
        
        if(ly.isDisplayObject(self.view._webview)) then
            self.view._webview:removeSelf()
            self.view._webview = nil
        end
        
    end
end

--------------------------------------------------------------------------------
-- constructor
--------------------------------------------------------------------------------
local ElementClass = Class(Super) --inherits Object Class

function ElementClass:initialize(options)
    if(Super.initialize(self, options)) then
        self._className = CLASS_NAME
        _init(self, options)
        
        return true
    else
        return false
    end
end

function ElementClass:finalize()
    _free(self)
    if(Super.finalize(self)) then
        return true
    else
        return false
    end
end

--------------------------------------------------------------------------------
-- public
--------------------------------------------------------------------------------

function ElementClass:navigate(url, baseDir)
    _navigate(self, url, baseDir)
end

--------------------------------------------------------------------------------
-- export
--------------------------------------------------------------------------------

return ElementClass
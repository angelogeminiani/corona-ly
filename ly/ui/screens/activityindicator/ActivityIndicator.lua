--'ly.ui.elements.activityindicator.ActivityIndicator'
--------------------------------------------------------------------------------
-- import
--------------------------------------------------------------------------------
local widget = require('widget')

local ly = require('ly.ly')

local Super = require('ly.ui.BaseControl') -- Superclass reference
local ElementClass = Class(Super) --inherits Object Class

--------------------------------------------------------------------------------
-- const
--------------------------------------------------------------------------------

local BACKGROUND =  { 0.55, 0.55, 0.55, 0.5 }


--------------------------------------------------------------------------------
-- private
--------------------------------------------------------------------------------

local function _create_background(self)
    local options = self._options
    local sceneGroup = self.view._content
    
    sceneGroup._background = display.newRect(0, 0, options.width, options.height)
    sceneGroup._background:setFillColor( unpack(BACKGROUND) )
    sceneGroup._background.anchorX = 0
    sceneGroup._background.anchorY = 0
    sceneGroup:insert(sceneGroup._background)
    
    sceneGroup._background:addEventListener('tap', function() return true end)
    sceneGroup._background:addEventListener('touch', function() return true end)
end

local function _create_spinner(self)
    local options = self._options
    local sceneGroup = self.view._content
    
    sceneGroup._spinner = widget.newSpinner({ 
        width = options.spinner.width, 
        height = options.spinner.height, 
    })
    sceneGroup._spinner.x = options.x
    sceneGroup._spinner.y = options.y
    sceneGroup:insert(sceneGroup._spinner)
    sceneGroup._spinner:start()
end

local function _remove(self, callback)
    if(ly.isDisplayObject(self.view)) then
        if(ly.isDisplayObject(self.view._content)) then
            self.view._content:removeSelf()
            self.view._content = nil
            ly.delay(function() 
                ly.invoke(callback)
            end, 100)
        else
            ly.invoke(callback)
        end
    else
        ly.invoke(callback)
    end
end

local function _create(self, callback)
    if(ly.isDisplayObject(self.view)) then
        _remove(self, function() 
            local stage = display.currentStage
            if(ly.isNotNull(stage)) then
                self.view._content = display.newGroup()
                stage:insert(self.view._content)
            end
            
            --begin create content
            _create_background(self)
            _create_spinner(self)
            
            --timeout
            if(ly.isNotNull(self._options)) then
                local timeout = ly.toNumber(self._options.timeout)
                if (timeout>0) then
                   ly.delay(function() self:toggle(false) end, timeout) 
                end
            end
            
            ly.invoke(callback)
        end)
    else
        ly.invoke(callback)
    end
end

local function _toggle(self, visible, callback)
    if(visible) then
        _create(self, callback)
    else
        _remove(self, callback)
    end
end

--------------------------------------------------------------------------------
-- init
--------------------------------------------------------------------------------

local function _init(self, options)
    options = options or {}
    
    options.width = options.width or ly.screen.width
    options.height = options.height or ly.screen.height
    options.x = options.x or options.width*0.5
    options.y = options.y or options.height*0.5
    
    options.spinner = options.spinner or {}
    options.spinner.width = options.spinner.width or 64
    options.spinner.height = options.spinner.height or options.spinner.width
    
    self.view.x = options.x
    self.view.y = options.y
    
    --register self as singleton
    if (ly.isNull(ElementClass.sigleton)) then
        ElementClass.singleton = self
    end
    
    self.onFree = function()
        if (ly.isEventListener(self.view)) then
            
        end
    end
end

local function _free(self)
    if(ly.isDisplayObject(self.view)) then
        
        if(ly.isDisplayObject(self.view._content)) then
            self.view._content:removeSelf()
            self.view._content = nil
        end
        
    end
end

--------------------------------------------------------------------------------
-- constructor
--------------------------------------------------------------------------------

function ElementClass:initialize(options)
    if(Super.initialize(self, options)) then
        self._className = 'ly.ui.elements.activityindicator.ActivityIndicator'
        _init(self, options)
        
        return true
    else
        return false
    end
end

function ElementClass:finalize()
    _free(self)
    if(Super.finalize(self)) then
        return true
    else
        return false
    end
end

--------------------------------------------------------------------------------
-- public
--------------------------------------------------------------------------------

function ElementClass:toggle(visible, callback)
    _toggle(self, visible, callback)
end

--------------------------------------------------------------------------------
-- STATIC
--------------------------------------------------------------------------------

--SINGLETON INSTANCE
ElementClass.singleton = nil

ElementClass.options = {}

local function _get_instance()
    local instance = ElementClass.singleton
    if(ly.isNull(instance)) then
        --ly.logger.debug('Created instance of Activity Indicator')
        instance = ElementClass:new(ElementClass.options)
    else
        --ly.logger.debug('Found instance of Activity Indicator')
    end
    return instance
end

function ElementClass.setActivityIndicator(visible, callback)
    _get_instance():toggle(visible, callback)
end

function ElementClass.setOptions(options)
    options = options or {}
    local existing_options = _get_instance()._options or {}
    --override
    existing_options.timeout = options.timeout
end

--------------------------------------------------------------------------------
-- STATIC INIT
--------------------------------------------------------------------------------

--store native
ElementClass.setNativeActivityIndicator = native.setActivityIndicator

--replace native
native.setActivityIndicator = ElementClass.setActivityIndicator

--------------------------------------------------------------------------------
-- export
--------------------------------------------------------------------------------

return ElementClass
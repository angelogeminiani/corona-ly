--[[

    Button without a background

]]--
local CLASS_NAME = 'ly.ui.elements.button.TextButton'
--------------------------------------------------------------------------------
-- import
--------------------------------------------------------------------------------
local ly = require('ly.ly')

local Super = require('ly.ui.BaseControl') -- Superclass reference
local ElementClass = Class(Super) --inherits Object Class

local NotificationClass = require('ly.ui.elements.notification.Notification')


--------------------------------------------------------------------------------
-- events
--------------------------------------------------------------------------------

--inbound
local IN_EVENT_TOUCH = 'touch'
local IN_EVENT_TAP = 'tap'

--outbound
local OUT_EVENT_PRESSED = 'pressed'

--------------------------------------------------------------------------------
-- const
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
-- animation
--------------------------------------------------------------------------------

local function _animate_press(self, event, callback)
    if(ly.isDisplayObject(self.view)) then
        transition.to( self.view, {
            time=100, alpha=0.5, transition=easing.inOutSine, 
            onComplete=function() 
                ly.invoke(callback)
            end
        } )
    else
        ly.invoke(callback)
    end
end

local function _animate_release(self, event, callback)
    if(ly.isDisplayObject(self.view)) then
        transition.to(self.view, {time=300, alpha=1, transition=easing.inOutSine, 
            onComplete = function() 
                ly.invoke(callback)
            end
        }) 
    else
        ly.invoke(callback)
    end
end

--------------------------------------------------------------------------------
-- private
--------------------------------------------------------------------------------

local function _set_notification(self, value)
    if(ly.isDisplayObject(self.view)) then
        local notification = self.view._notification
        if(ly.isDisplayObject(notification)) then
            notification:setText(value)
        end
    end
end

local function _set_enabled(self, value)
    if(self and self.view and self.view._icon and ly.isBoolean(value)) then
        self.view._icon.enabled = value
        if(value) then
            -- enabled
            transition.to(self.view._icon, {alpha=1, time=200})
        else
            -- not enabled
            transition.to(self.view._icon, {alpha=0.3, time=200})
        end
    end
end

local function _do_pressed(self)
    if(ly.isDisplayObject(self.view)) then
        self:dispatchEvent({
            name = OUT_EVENT_PRESSED,
            target = self,
        })
    end
end

--------------------------------------------------------------------------------
-- init
--------------------------------------------------------------------------------

local function _init_caption(self)
    local options = self._options
    local sceneGroup = self.view
    
    local has_icon = ly.isNotEmpty(options.iconFile)
    local align = options.align
    local text = options.text
    local width = options.textWidth
    local font = options.font
    local fontSize = options.fontSize
    local fontColor = options.fontColor
    local anchorX = 0
    local x = ly.conditional(has_icon, options.height + 5, 0)
    
    local labelParameters = {
        x = x,
        y = 0, 
        text = text,
        font = font,
        fontSize = fontSize, 
        align = align
    }
    
    --full space label
    labelParameters.width = width
    labelParameters.height = 0
    
    sceneGroup._caption = display.newText(labelParameters)
    sceneGroup._caption:setFillColor(unpack(fontColor))
    sceneGroup._caption.isVisible = (text:len()>0)
    sceneGroup:insert(sceneGroup._caption)
    sceneGroup._caption.anchorX = anchorX
    
end

local function _create_icon(self)
    local options = self._options
    local sceneGroup = self.view
    
    local filename = options.iconFile
    local baseDir = options.iconBaseDir
    
    local bounds = {
        width = options.iconWidth,
        height = options.iconWidth,
        x = 0,
        y = 0
    }
    
    sceneGroup._icon = display.newImage(filename, baseDir)
    if (ly.isDisplayObject(sceneGroup._icon)) then
        sceneGroup._icon.enabled = true
        ly.graphics.resizeByWidth(sceneGroup._icon, bounds.width)
        if(bounds.width~=bounds.height) then
            sceneGroup._icon.height = bounds.height
        end
        sceneGroup._icon.anchorX = 0
        sceneGroup._icon.x = bounds.x
        sceneGroup._icon.y = bounds.y
        sceneGroup:insert(sceneGroup._icon)
    end
end

local function _init_icon(self)
    local options = self._options
    local sceneGroup = self.view
    
    local filename = options.iconFile
    local baseDir = options.iconBaseDir
    
    if (ly.isNotEmpty(filename)) then
        if (ly.path.isHttp(filename)) then
            -- download
            ly.net.download(filename, function(event) 
                if ( event.phase == "ended" ) then
                    local response = event.response
                    if(ly.isObject(response)) then
                        --replace options
                        options.iconFile = response.filename
                        options.iconBaseDir = response.baseDirectory
                        _create_icon(self)
                    else
                        --NOT FOUND
                    end
                end
            end, {baseDir=baseDir})
        else
            -- get from local
            _create_icon(self)
        end
    end
end

local function _init_notification(self)
    local options = self._options
    
    local btn_x, btn_y = 0, 0
    local btn_w, btn_h = options.iconWidth, options.iconWidth
    
    local w = 12
    local x = btn_x + (btn_w*0.5) - w
    local y = btn_y - (btn_h*0.5) 
    
    self.view._notification = NotificationClass:new({
        text = '0',
        x = x, y= y
    })
    self.view:insert(self.view._notification.view)
end

local function _init(self, options)
    options = options or {}
    
    options.id = options.id
    options.left = options.left or 0
    options.top = options.top or 0
    options.x = options.x or 0
    options.y = options.y or 0
    options.width = options.width or 48
    options.height = options.height or options.width
    
    --icon options
    options.iconFile = options.iconFile or ''
    options.iconBaseDir = options.iconBaseDir or system.ResourceDirectory
    options.iconWidth = options.iconWidth or options.height
    
    --caption
    options.text = options.text or 'undefined'
    options.textWidth = options.textWidth or options.width-10
    options.align = options.align or "left"
    options.font = options.font or native.systemFont
    options.fontSize = options.fontSize or 14
    options.fontColor = options.fontColor or { 0, 0, 0 }
    
    local sceneGroup = self.view
    --sceneGroup.anchorChildren = true
    if options.left then
        sceneGroup.x = options.left + options.width * 0.5
    elseif options.x then
        sceneGroup.x = options.x
    end
    if options.top then
        sceneGroup.y = options.top + options.height * 0.5
    elseif options.y then
        sceneGroup.y = options.y
    end
    
    -- set height and width
    self.height = options.height
    self.width = options.width
    
    --icon (async)
    if(ly.isNotEmpty(options.iconFile)) then
        _init_icon(self)
    end
    
    --caption
    _init_caption(self)

    _init_notification(self)
    
    --handlers
    local function on_touch(event)
        if (self:isEnabled()) then
            --animate the button
            local phase = event.phase
            if (phase=='began') then
                _animate_press(self, event)
            elseif(phase=='ended' or phase=='moved') then
                _animate_release(self, event)
            end
        end
        --_animate(self, event) 
        return true
    end
    local function on_tap(event)
        _do_pressed(self)
        return true
    end
    self.view:addEventListener(IN_EVENT_TOUCH, on_touch)
    self.view:addEventListener(IN_EVENT_TAP, on_tap)
    
    self.onFree = function()
        if (ly.isEventListener(self.view)) then
            self.view:removeEventListener(IN_EVENT_TOUCH, on_touch)
            self.view:removeEventListener(IN_EVENT_TAP, on_tap)
        end
    end
end

local function _free(self)
    if(ly.isDisplayObject(self.view)) then
        --notification
        if (ly.isDisplayObject(self.view._notification)) then
            self.view._notification:removeSelf()
            self.view._notification=nil
        end
    end
end

--------------------------------------------------------------------------------
-- constructor
--------------------------------------------------------------------------------

function ElementClass:initialize(options)
    if(Super.initialize(self, options)) then
        self._className = CLASS_NAME
        _init(self, options)
        
        --self.height = self.view.height
        --self.width = self.view.width
        
        return true
    else
        return false
    end
end

function ElementClass:finalize()
    _free(self)
    if(Super.finalize(self)) then
        if (nil~=self.view) then
            print('WARNING: Probable Memory Leak issue in Button.lua') 
        end
        return true
    else
        return false
    end
end

--------------------------------------------------------------------------------
-- public
--------------------------------------------------------------------------------

ElementClass.onPressed = OUT_EVENT_PRESSED

function ElementClass:setLabel(value)
    self.view._caption.text = value
end

function ElementClass:getLabel()
    return self.view._caption.text
end

function ElementClass:setNotification(value)
    _set_notification(self, value)
end

function ElementClass:setEnabled(value)
    _set_enabled(self, value)
end

function ElementClass:isEnabled()
    if(self and self.view and self.view._icon) then
        return ly.toBoolean(self.view._icon.enabled)
    end
    return false
end

--------------------------------------------------------------------------------
-- export
--------------------------------------------------------------------------------

return ElementClass
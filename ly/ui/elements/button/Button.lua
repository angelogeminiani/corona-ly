local CLASS_NAME = 'ly.ui.elements.button.Button'
--------------------------------------------------------------------------------
-- import
--------------------------------------------------------------------------------
local ly = require('ly.ly')

local Super = require('ly.ui.BaseControl') -- Superclass reference
local ElementClass = Class(Super) --inherits Object Class

local NotificationClass = require('ly.ui.elements.notification.Notification')


--------------------------------------------------------------------------------
-- events
--------------------------------------------------------------------------------

--inbound
local IN_EVENT_TOUCH = 'touch'
local IN_EVENT_TAP = 'tap'

--outbound
local OUT_EVENT_PRESSED = 'pressed'

--------------------------------------------------------------------------------
-- animation
--------------------------------------------------------------------------------

local function _animate_press(self, event, callback)
    if(ly.isDisplayObject(self.view)) then
        transition.to( self.view, {
            time=100, alpha=0.5, transition=easing.inOutSine, 
            onComplete=function() 
                ly.invoke(callback)
            end
        } )
    else
        ly.invoke(callback)
    end
end

local function _animate_release(self, event, callback)
    if(ly.isDisplayObject(self.view)) then
        transition.to(self.view, {time=300, alpha=1, transition=easing.inOutSine, 
            onComplete = function() 
                ly.invoke(callback)
            end
        }) 
    else
        ly.invoke(callback)
    end
end

local function _animate(self, event, callback)
    _animate_press(self, event, function() 
        _animate_release(self, event, function() 
            ly.invoke(callback)
        end)
    end)
end

--------------------------------------------------------------------------------
-- private
--------------------------------------------------------------------------------

local function _set_notification(self, value)
    if(ly.isDisplayObject(self.view)) then
        local notification = self.view._notification
        if(ly.isDisplayObject(notification)) then
            notification:setText(value)
        end
    end
end

local function _set_backgroundcolor(self, color)
    if(ly.isDisplayObject(self.view)) then
        local background = self.view._background
        if(ly.isDisplayObject(background)) then
            if(ly.isObject(color)) then
                background:setFillColor(color)
            else
                background:setFillColor(unpack(color)) 
            end
        end
    end
end

local function _do_pressed(self)
    if(ly.isDisplayObject(self.view)) then
        local options = self._options
        self:dispatchEvent({
            name = OUT_EVENT_PRESSED,
            target = self,
            data = options.event_args
        })
    end
end

--------------------------------------------------------------------------------
-- init
--------------------------------------------------------------------------------

local function _init_background(self)
    local options = self._options
    
    local width = options.width
    local height = options.height
    local cornerRadius = options.cornerRadius
    
    --background
    if(ly.isZero(cornerRadius)) then
        self.view._background = display.newRect( 0, 0, width, height ) 
    else
        self.view._background = display.newRoundedRect( 0, 0, width, height, cornerRadius )
    end
    self.view._background:setFillColor(unpack(options.backgroundColor))
    self.view._background.strokeWidth = options.strokeWidth
    self.view._background.stroke = options.strokeColor
    
    self.view._background.anchorX = 0
    self.view._background.anchorY = 0
    
    self.view:insert(self.view._background)
end

local function _init_caption(self)
    local options = self._options
    
    local align = options.align
    local text = options.text
    local width = options.textWidth
    local font = options.font
    local fontSize = options.fontSize
    local iconWidth = options.iconWidth
    local anchorX = 0.5
    
    local labelParameters = {
        x = 0,
        y = 0, 
        text = text,
        font = font,
        fontSize = fontSize, 
        align = align,
        width = 0, height = 0
    }
    
    if(iconWidth==0) then
        --full space label
        labelParameters.width = width
        anchorX = 0.5
    else
        --icon + label
        labelParameters.align = 'left'
        anchorX = 0
    end
    
    local caption = display.newText(labelParameters)
    caption:setFillColor(unpack(options.fontColor))
    caption.isVisible = (text:len()>0)
    caption.anchorX = anchorX
    --positionate
    if (iconWidth > 0) then
        if (align=='center') then
            caption.x =  caption.width*0.5 + 2 + iconWidth*0.5 --(-1* (self.view._caption.width/2)) + 2 + (iconWidth*0.5)
        end
    else
        caption.x = width*0.5
    end
    caption.y = options.height*0.5 -- caption.height*0.5
    
    self.view:insert(caption)
    self.view._caption = caption
end

local function _create_icon(self)
    local options = self._options
    
    local filename = options.iconFile
    local baseDir = options.iconBaseDir
    local width = options.iconWidth
    
    local icon = display.newImage(filename, baseDir)
    if (ly.isDisplayObject(icon)) then
        ly.graphics.scaleByWidth(icon, width)
        self.view:insert(icon)
        
        --positionate
        local x = 0
        local caption = self.view._caption
        if (ly.isDisplayObject(caption) and caption.isVisible) then
            x = caption.x - (2 + width*0.5)
            
        end
        icon.x = x
        icon.y = options.height*0.5
        
        self.view._icon = icon
    end
end

local function _init_icon(self)
    local options = self._options
    local sceneGroup = self.view
    
    local filename = options.iconFile
    local baseDir = options.iconBaseDir
    
    if (ly.isNotEmpty(filename)) then
        if (ly.path.isHttp(filename)) then
            -- download
            ly.net.download(filename, function(event) 
                if ( event.phase == "ended" ) then
                    local response = event.response
                    if(ly.isObject(response)) then
                        --replace options
                        options.iconFile = response.filename
                        options.iconBaseDir = response.baseDirectory
                        _create_icon(self)
                    else
                        --NOT FOUND
                    end
                end
            end, {baseDir=baseDir})
        else
            -- get from local
            _create_icon(self)
        end
    end
end

local function _init_notification(self)
    local options = self._options
    
    local btn_w, btn_h = options.width, options.height
    
    local w = 12
    local x = btn_w --btn_x + (btn_w*0.5) - w
    local y = w --btn_y - (btn_h*0.5) 
    
    self.view._notification = NotificationClass:new({
        text = '0',
        x = x, y= y
    })
    self.view:insert(self.view._notification.view)
end

local function _init(self, options)
    options = options or {}
    
    options.id = options.id
    options.left = options.left or 0
    options.top = options.top or 0
    options.x = options.x or 0
    options.y = options.y or 0
    options.width = options.width or (display.contentWidth * 0.75)
    options.height = options.height or 20
    
    -- Vector options
    options.cornerRadius = options.cornerRadius or options.height * 0.33 or 10
    options.backgroundColor = options.backgroundColor or { 1, 1, 1 }
    options.strokeColor = options.strokeColor or options.backgroundColor
    options.strokeWidth = options.strokeWidth or 2
    
    --icon options
    options.iconFile = options.iconFile or ''
    options.iconBaseDir = options.iconBaseDir or system.ResourceDirectory
    if(ly.isNotEmpty(options.iconFile)) then
        if(options.iconHeight)then
            options.iconWidth = options.iconWidth or options.iconHeight
        else
            --autosize
            options.iconHeight = options.height - (options.strokeWidth + 5)
            options.iconWidth = options.iconHeight
        end
        
    else
        options.iconHeight = 0
        options.iconWidth = 0
    end
    
    --caption
    options.text = options.text or options.caption or ""
    options.textWidth = options.textWidth or options.captionWidth or ly.conditional(options.iconWidth==0, options.width-10, options.width-(options.iconWidth + 10))
    options.align = options.align or "center"
    options.font = options.font or native.systemFontBold
    options.fontSize = options.fontSize or options.fontSize
    options.fontColor = options.fontColor or { 0, 0, 0 }
    
    local deviceScale = (display.pixelWidth / display.contentWidth) * 0.5
    local bgWidth = options.width
    --if widget.isSeven() then
    --    bgWidth = bgWidth + opt.labelWidth -- make room in the box for the label
    --end
    
    -- positionate view
    self.view.anchorChildren = true
    if (options.left>0) then
        self.view.x = options.left --+ options.width * 0.5
    elseif (options.x>0) then
        self.view.x = options.x -- + options.width * 0.5
    end
    if (options.top>0) then
        self.view.y = options.top --+ options.height * 0.5
    elseif (options.x>0) then
        self.view.y = options.y --+ options.height * 0.5
    end
    
    --background
    _init_background(self)
    
    -- set height and width
    self.height = self.view._background.height
    self.width = self.view._background.width
    
    --caption
    _init_caption(self)
    
    --icon (async)
    _init_icon(self)
    
    _init_notification(self)
    
    --handlers
    local function on_touch(event)
        --animate the button
        local phase = event.phase
        if (phase=='began') then
            _animate_press(self, event)
        elseif(phase=='ended' or phase=='moved') then
            _animate_release(self, event)
        end
        --_animate(self, event) 
        return true
    end
    local function on_tap(event)
        _do_pressed(self)
        return true
    end
    self.view:addEventListener(IN_EVENT_TOUCH, on_touch)
    self.view:addEventListener(IN_EVENT_TAP, on_tap)
    
    self.onFree = function()
        if (ly.isEventListener(self.view)) then
            self.view:removeEventListener(IN_EVENT_TOUCH, on_touch)
            self.view:removeEventListener(IN_EVENT_TAP, on_tap)
        end
    end
end

local function _free(self)
    if(ly.isDisplayObject(self.view)) then
        --notification
        if (ly.isDisplayObject(self.view._notification)) then
            self.view._notification:removeSelf()
            self.view._notification=nil
        end
    end
end

--------------------------------------------------------------------------------
-- constructor
--------------------------------------------------------------------------------

function ElementClass:initialize(options)
    if(Super.initialize(self, options)) then
        self._className = CLASS_NAME
        _init(self, options)
        
        --self.height = self.view.height
        --self.width = self.view.width
        
        return true
    else
        return false
    end
end

function ElementClass:finalize()
    _free(self)
    if(Super.finalize(self)) then
        if (nil~=self.view) then
            print('WARNING: Probable Memory Leak issue in Button.lua') 
        end
        return true
    else
        return false
    end
end

--------------------------------------------------------------------------------
-- public
--------------------------------------------------------------------------------

ElementClass.onPressed = OUT_EVENT_PRESSED

function ElementClass:setLabel(value)
    self.view._caption.text = value
end

function ElementClass:getLabel()
    return self.view._caption.text
end

function ElementClass:setNotification(value)
    _set_notification(self, value)
end

function ElementClass:setBackgroundColor(color)
    if(ly.isNotNull(color)) then
        _set_backgroundcolor(self, color)
    end
end

--------------------------------------------------------------------------------
-- export
--------------------------------------------------------------------------------

return ElementClass
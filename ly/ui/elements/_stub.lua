
--------------------------------------------------------------------------------
-- import
--------------------------------------------------------------------------------

local ly = require("ly.ly")

local Super = require('ly.ui.BaseControl') -- Superclass reference
--------------------------------------------------------------------------------
-- fields
--------------------------------------------------------------------------------

-- declare here local fields

--------------------------------------------------------------------------------
-- private
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
-- init
--------------------------------------------------------------------------------

local function _init(self)
    local options = self._options or {}
    --data binding
    options.id = options.id or '' --may be ID of an external model for data binding
    options.model = options.model or {} --data model to bind
    options.value = ly.toBoolean(options.value, ly.getValue(options.model, options.id, false))
    --other options
    
    
    
    local sceneGroup = self.view
    
end

local function _free(self)
    
end

--------------------------------------------------------------------------------
-- constructor
--------------------------------------------------------------------------------

local ElementClass = Class(Super) --inherits Object Class

function ElementClass:initialize(options)
    --calls parent Class's initialize()
    --with "self" as this instance
    if(Super.initialize(self, options)) then
        _init(self, options)
        
        self.height = self.view.height
        self.width = self.view.width
        
        return true
    else
        return false
    end
end

function ElementClass:finalize()
    _free(self)
    if (Super.finalize(self)) then
        if (nil~=self.view) then
            print('WARNING: Probable Memory Leak issue.') 
        end
        return true
    else
        return false
    end
    
end

--------------------------------------------------------------------------------
-- public
--------------------------------------------------------------------------------



--------------------------------------------------------------------------------
-- export
--------------------------------------------------------------------------------

return ElementClass
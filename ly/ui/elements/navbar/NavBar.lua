
--------------------------------------------------------------------------------
-- import
--------------------------------------------------------------------------------

local widget = require("widget")

local ly = require('ly.ly')

local Super = require('ly.ui.BaseControl') -- Superclass reference
local ElementClass = Class(Super) --inherits Object Class


--------------------------------------------------------------------------------
-- private
--------------------------------------------------------------------------------

local function _build_instance(self, options)
    -- options --
    options = options or {}
    
    options.left = options.left or nil
    options.top = options.top or nil
    options.width = options.width or display.contentWidth
    options.height = options.height or 50
    if options.includeStatusBar == nil then
        options.includeStatusBar = true -- assume status bars for business apps
    else
        options.includeStatusBar = options.includeStatusBar
    end
    
    local statusBarPad = 0
    if options.includeStatusBar then
        statusBarPad = display.topStatusBarContentHeight
    end
    
    options.x = options.x or display.contentCenterX
    options.y = options.y or (options.height + statusBarPad) * 0.5
    options.id = options.id
    options.isTransluscent = options.isTransluscent or true
    options.background = options.background
    options.backgroundColor = options.backgroundColor
    options.title = options.title or ""
    options.titleColor = options.titleColor or { 0, 0, 0 }
    options.font = options.font or native.systemFontBold
    options.fontSize = options.fontSize or 18
    options.leftButton = options.leftButton or nil
    options.rightButton = options.rightButton or nil
    
    if options.left then
    	options.x = options.left + options.width * 0.5
    end
    if options.top then
    	options.y = options.top + (options.height + statusBarPad) * 0.5
    end
    
    local sceneGroup = self.view
    
    --background
    sceneGroup._background = display.newRect(sceneGroup, options.x, options.y, options.width, options.height + statusBarPad )
    if options.background then
        sceneGroup._background.fill = { type = "image", filename=options.background}
    elseif options.backgroundColor then
        sceneGroup._background.fill = options.backgroundColor
    else
        if widget.isSeven() then
            sceneGroup._background.fill = {1,1,1} 
        else
            sceneGroup._background.fill = { type = "gradient", color1={0.5, 0.5, 0.5}, color2={0, 0, 0}}
        end
    end
    
    sceneGroup._title = display.newText(options.title, sceneGroup._background.x, sceneGroup._background.y + statusBarPad * 0.5, options.font, options.fontSize)
    sceneGroup._title:setFillColor(unpack(options.titleColor))
    sceneGroup:insert(sceneGroup._title)
    
    -- buttons --
    local leftButton
    if options.leftButton then
        options.leftButton.id = options.leftButton.id or "left"
        if options.leftButton.defaultFile then -- construct an image button
            leftButton = widget.newButton({
                id = options.leftButton.id,
                label = options.leftButton.label,
                labelColor = options.leftButton.labelColor or { default={ 1, 1, 1 }, over={ 0, 0, 0, 0.5 } },
                labelAlign = "center",
                font = options.leftButton.font or options.font,
                fontSize = options.fontSize,
                width = options.leftButton.width,
                height = options.leftButton.height,
                baseDir = options.leftButton.baseDir,
                defaultFile = options.leftButton.defaultFile,
                overFile = options.leftButton.overFile,
                onEvent = options.leftButton.onEvent,
            })
        else -- construct a text button
            leftButton = widget.newButton({
                id = options.leftButton.id,
                label = options.leftButton.label,
                font = options.leftButton.font or options.font,
                fontSize = options.fontSize,
                labelColor = options.leftButton.labelColor or { default={ 1, 1, 1 }, over={ 0, 0, 0, 0.5 } },
                labelAlign = "left",
                onEvent = options.leftButton.onEvent
            })
        end
        leftButton.x = 15 + leftButton.width * 0.5
        leftButton.y = sceneGroup._title.y
        sceneGroup:insert(leftButton)
    end
    
    local rightButton
    if options.rightButton then
        options.rightButton.id = options.rightButton.id or "right"
        if options.rightButton.defaultFile then -- construct an image button
            rightButton = widget.newButton({
                id = options.rightButton.id,
                width = options.rightButton.width,
                height = options.rightButton.height,
                baseDir = options.rightButton.baseDir,
                defaultFile = options.rightButton.defaultFile,
                overFile = options.rightButton.overFile,
                onEvent = options.rightButton.onEvent,
            })
        else -- construct a text button
            rightButton = widget.newButton({
                id = options.rightButton.id,
                label = options.rightButton.label or "Default",
                onEvent = options.rightButton.onEvent,
                font = options.leftButton.font or options.font,
                fontSize = options.fontSize,
                labelColor = options.rightButton.labelColor or { default={ 1, 1, 1 }, over={ 0, 0, 0, 0.5 } },
                labelAlign = "right",
            })
        end
        rightButton.x = display.contentWidth - (15 + rightButton.width * 0.5)
        rightButton.y = sceneGroup._title.y
        sceneGroup:insert(rightButton)
    end
    
end

--------------------------------------------------------------------------------
-- constructor
--------------------------------------------------------------------------------

function ElementClass:initialize(options)
    self._className = 'NavBar'
    if(Super.initialize(self, options)) then
        _build_instance(self, options)
        
        self.height = self.view.height
        self.width = self.view.width
    end
end

function ElementClass:finalize()
    --destroy children
    if(self and ly.isDisplayObject(self.view)) then
        if (ly.isDisplayObject(self.view._title)) then
            self.view._title:removeSelf()
            self.view._title = nil
        end
    end
    
    if(Super.finalize(self)) then
        if (nil~=self.view) then
            print('WARNING: Probable Memory Leak issue.') 
        end
    end
end

--------------------------------------------------------------------------------
-- public
--------------------------------------------------------------------------------

function ElementClass:setLabel(text)
    self.view._title.text = text
end

function ElementClass:getLabel()
    return self.view._title.text
end

--------------------------------------------------------------------------------
-- export
--------------------------------------------------------------------------------

return ElementClass




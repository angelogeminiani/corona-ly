--ly.ui.elements.pageview.PageView
--------------------------------------------------------------------------------
-- import
--------------------------------------------------------------------------------

local ly = require("ly.ly")

local Super = require('ly.ui.BaseControl') -- Superclass reference
local ElementClass = Class(Super) --inherits Object Class

--------------------------------------------------------------------------------
-- events
--------------------------------------------------------------------------------

local ON_PAGE_RENDER = 'pageRender'
local ON_PAGE_TAP = 'pageTap'
local ON_PAGE_SWIPE = 'pageSwipe'
local ON_PAGE_FIRST = 'pageFirst'
local ON_PAGE_LAST = 'pageLast'

--------------------------------------------------------------------------------
-- const
--------------------------------------------------------------------------------

local SWIPE_THRESHOLD = 20

--------------------------------------------------------------------------------
-- trigger
--------------------------------------------------------------------------------

local function _do_on_render(self, item)
    self:dispatchEvent({
        name = ON_PAGE_RENDER,
        target = self,
        item = item,
        index = self.currentIndex,
        --view = self.view.content
    })
end

local function _do_on_first(self, item)
    self:dispatchEvent({
        name = ON_PAGE_FIRST,
        target = self,
        item = item,
        index = self.currentIndex
    })
end

local function _do_on_last(self, item)
    self:dispatchEvent({
        name = ON_PAGE_LAST,
        target = self,
        item = item,
        index = self.currentIndex
    })
end

local function _do_on_touch(self)
    local item = self:get(self.currentIndex)
    self:dispatchEvent({
        name = ON_PAGE_TAP,
        target = self,
        item = item,
        index = self.currentIndex
    })
end

local function _do_on_swipe(self, direction)
    local item = self:get(self.currentIndex)
    self:dispatchEvent({
        name = ON_PAGE_SWIPE,
        target = self,
        item = item,
        index = self.currentIndex,
        direction = direction
    })
end

local function _handle_on_touch(self, event)
    local phase = event.phase
    local threshold = self._options.swipeThreshold
    if(phase=='began') then
        
    elseif(phase=='ended') then
        local x_dir = event.x - event.xStart
        local y_dir = event.y - event.yStart
        if(x_dir~=0 and math.abs(x_dir)>threshold) then
            if(x_dir>0) then
                _do_on_swipe(self, 'right')
            else
                _do_on_swipe(self, 'left')
            end
        end
        if(y_dir~=0 and math.abs(y_dir)>threshold) then
            if(y_dir>0) then
                _do_on_swipe(self, 'down')
            else
                _do_on_swipe(self, 'up')
            end
        end
    end
end

--------------------------------------------------------------------------------
-- items
--------------------------------------------------------------------------------

local function _count(self)
    if(self.items) then
        return #self.items
    end
    return 0
end

local function _clear(self)
    if (ly.isDisplayObject(self.view)) then
        --clear items
        if(_count(self)>0) then
            self.items = {}
        end
        self.currentIndex = 0
    end
end

local function _is_valid_index(self, index)
    local count = _count(self)
    return (index>0) and (count>index or count ==index)
end

local function _get(self, index)
    if (_is_valid_index(self, index)) then
        return self.items[index]
    end
    return nil
end

--------------------------------------------------------------------------------
-- render
--------------------------------------------------------------------------------

local function _clear_view(self, handle)
    
    local function on_tap(event)
        _do_on_touch(self)
        return true
    end
    
    local function on_touch(event)
        _handle_on_touch(self, event)
        return true
    end
    
    local view = self.view
    if(ly.isDisplayObject(view)) then
        if(ly.isDisplayObject(view.content)) then
            if (ly.isEventListener(view.content)) then
                view.content:removeEventListener('tap', on_tap)
                view.content:removeEventListener('touch', on_touch)
            end
            view.content:removeSelf()
            view.content = nil
        end
        view.content = display:newGroup()
        view:insert(view.content)
        if (ly.isEventListener(view.content)) then
            view.content:addEventListener('tap', on_tap)
            view.content:addEventListener('touch', on_touch)
        end
    end
end

local function _render(self, index)
    local item =_get(self, index)
    if(ly.isNotNull(item)) then
        
        self.currentIndex = index
        
        --clear current view and create new one with listeners
        _clear_view(self, true)
        
        --render event
        _do_on_render(self, item)
        
        if (index==1) then
            _do_on_first(self, item)
        elseif(index == _count(self)) then
            _do_on_last(self, item)
        end
    else
        --no more items
        _clear_view(self, true)
        
        --render empty object
        _do_on_render(self, nil)
        
        -- set index to max + 1 or zero
        self.currentIndex = ly.conditional(index<=0, 0, _count(self)+1)
    end
    
    return item
end

local function _next(self)
    return _render(self, self.currentIndex  + 1)
end

local function _prev(self)
    return _render(self, self.currentIndex  - 1)
end

--------------------------------------------------------------------------------
-- display
--------------------------------------------------------------------------------

local function _insert(self, displayObject)
    if(ly.isDisplayObject(self.view)) then
        local content = self.view.content
        if(ly.isDisplayObject(content)) then
            if(ly.isLyObject(displayObject)) then
                content:insert(displayObject.view)
                self.currentPage = displayObject
            elseif(ly.isDisplayObject(displayObject)) then
                content:insert(displayObject)
                self.currentPage = displayObject
            end
        end
    end
end

--------------------------------------------------------------------------------
-- init
--------------------------------------------------------------------------------

local function _init(self)
    local options = self._options or {}
    options.swipeThreshold = options.swipeThreshold or SWIPE_THRESHOLD
    
    self.currentIndex = 0
    self.currentPage = nil
    
    self.view.anchorChildren = false
    
    self.bounds = {
        x = options.x or 0,
        y = options.y or ly.screen.statusBar.height,
        width = options.width or ly.screen.width,
        height = options.height or ly.screen.height-ly.screen.statusBar.height
    }
    self.view.x = self.bounds.x
    self.view.y = self.bounds.y
    self.view.height = self.bounds.height
    self.view.width = self.bounds.width
end

local function _free(self)
    _clear(self)
    _clear_view(self, false)
end

--------------------------------------------------------------------------------
-- constructor
--------------------------------------------------------------------------------

local ElementClass = Class(Super) --inherits Object Class

function ElementClass:initialize(options)
    if(Super.initialize(self, options)) then
        
        _init(self)
        
        return true
    else
        return false
    end
end

function ElementClass:finalize()
    _free(self)
    if (Super.finalize(self)) then
        if (nil~=self.view) then
            print('WARNING: Probable Memory Leak issue.') 
        end
        return true
    else
        return false
    end
    
end

--------------------------------------------------------------------------------
-- public
--------------------------------------------------------------------------------

ElementClass.onRender = ON_PAGE_RENDER
ElementClass.onTap = ON_PAGE_TAP
ElementClass.onSwipe = ON_PAGE_SWIPE
ElementClass.onFirst = ON_PAGE_FIRST
ElementClass.onLast = ON_PAGE_LAST

function ElementClass:insert(displayObject)
    _insert(self, displayObject)
end

function ElementClass:count()
    return _count(self)
end

function ElementClass:addItem(model)
    if (ly.isNull(self.items)) then
        self.items = {}
    end
    table.insert(self.items, model)
end

function ElementClass:addItems(list)
    ly.forEach(list, function(value, key, index) 
        self:addItem(value) 
    end) 
    
end

function ElementClass:sort(compare)
    if (ly.isNotNull(self.items)) then
        if(ly.isFunction(compare))then
            table.sort(self.items, compare) 
        else    
            table.sort(self.items) 
        end
    end
end

function ElementClass:clear()
    _clear(self)
end

function ElementClass:render(index)
    index = index or ly.conditional(self.currentIndex==0, 1, self.currentIndex)
    return _render(self, index)
end

function ElementClass:get(index)
    return _get(self, index)
end

function ElementClass:next()
    _next(self)
end

function ElementClass:previous()
    _prev(self)
end

--------------------------------------------------------------------------------
-- export
--------------------------------------------------------------------------------

return ElementClass
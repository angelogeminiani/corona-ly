
local CLASS_NAME = 'ly.ui.elements.tabview.TabBarButton'

--------------------------------------------------------------------------------
-- import
--------------------------------------------------------------------------------

local ly = require("ly.ly")

local Super = require('ly.ui.BaseControl') -- Superclass reference



--------------------------------------------------------------------------------
-- events
--------------------------------------------------------------------------------

--inbounds
local EVENT_TOUCH = 'touch'

--outbound
local EVENT_ON_CLICK = 'click' 

--------------------------------------------------------------------------------
-- const
--------------------------------------------------------------------------------

local UNDERLINE_POS_TOP = 'top'
local UNDERLINE_POS_BOTTOM = 'bottom'

local UNDERLINE_COLOR = {.9,.9,1}
local UNDERLINE_HEIGHT = 4

local TEXT_COLOR = {.5,.5,.5}
local TEXT_COLOR_PRESS = {1,1,1}

--------------------------------------------------------------------------------
-- private
--------------------------------------------------------------------------------

local function _toggle_background(self, value)
    if (ly.isDisplayObject(self.view)) then
        if (ly.isDisplayObject(self.view._press_background)) then
            local options = self._options
            if (ly.isNotNull(options)) then
                local back = self.view._press_background
                local color_back, color_text
                if (value) then
                    color_back = options.backgroundColorPress
                    color_text = options.textColorPress
                else
                    color_back = options.backgroundColor
                    color_text = options.textColor
                end
                back:setFillColor(unpack(color_back))
                if (ly.isDisplayObject(self.view._text)) then
                    self.view._text:setFillColor(unpack(color_text))
                end
            end
        end
    end
end

--------------------------------------------------------------------------------
-- trigger
--------------------------------------------------------------------------------

local function _do_on_click(self)
    if (ly.isDisplayObject(self.view)) then
        local options = self._options
        if (ly.isNotNull(options)) then
            ly.invoke(options.onClick, {
                target = self,
                name = EVENT_ON_CLICK,
                data = options.data,
                parent = options.parent,
            })
        end
    end
end

--------------------------------------------------------------------------------
-- handlers
--------------------------------------------------------------------------------

local function _handle_on_touch(self, event)
    local phase = event.phase
    
    if(phase=='began') then
        _toggle_background(self, true)
    else
        _toggle_background(self, false)
        _do_on_click(self)
    end
end

--------------------------------------------------------------------------------
-- display
--------------------------------------------------------------------------------

local function _set_selected(self, value)
    if (ly.isDisplayObject(self.view)) then
        if (ly.isDisplayObject(self.view._underline)) then
            self.view._underline.isVisible = value
        end
    end
end

--------------------------------------------------------------------------------
-- init
--------------------------------------------------------------------------------

local function _init_press_background(self)
    local options = self._options
    
    self.view._press_background = display.newRect(0, 0, options.width, options.height)
    self.view._press_background:setFillColor( unpack(options.backgroundColor) )
    self.view._press_background.anchorX = 0
    self.view._press_background.anchorY = 0
    self.view:insert(self.view._press_background)
    
    --hide
    --self.view._press_background.alpha = 0
end

local function _init_underline(self)
    local options = self._options or {}
    
    local pos = options.underlinePos
    local width = options.width
    local height = options.underlineHeight
    local color = options.underlineColor
    local x = 0
    local y = ly.conditional(UNDERLINE_POS_BOTTOM==pos, options.height, 0)
    local anchorY = ly.conditional(UNDERLINE_POS_BOTTOM==pos, 1, 0)
    
    self.view._underline = display.newRect(x,y, width, height)
    self.view._underline.anchorX = 0
    self.view._underline.anchorY = anchorY
    self.view._underline:setFillColor( unpack(color) )
    self.view._underline:setStrokeColor( unpack(color) )
    self.view._underline.strokeWidth = 0
    self.view:insert(self.view._underline)
end

local function _init_label(self)
    local options = self._options or {}
    
    local data = options.data
    if (ly.isNotEmpty(data)) then
        local label = data.label
        local pos = options.underlinePos
        local width = options.width
        local color = options.textColor
        local x = options.width*0.5
        local y = options.height*0.5
        local y = ly.conditional(UNDERLINE_POS_BOTTOM==pos, y - options.underlineHeight*0.5, y + options.underlineHeight*0.5)
        
        self.view._text = display.newText({
            width = width, x=x, y=y,
            text = label,
            align='center'
        })
        self.view._text:setFillColor( unpack(color) )
        self.view:insert(self.view._text)
    end
end

local function _init(self)
    local options = self._options or {}
    
    --component
    options.width = options.width 
    options.height = options.height 
    options.x = options.x or options.width*0.5
    options.y = options.y or options.height*0.5
    
    options.underlineColor =  options.underlineColor or UNDERLINE_COLOR
    options.underlinePos = options.underlinePos or UNDERLINE_POS_BOTTOM
    options.underlineHeight = options.underlineHeight or UNDERLINE_HEIGHT
    options.textColor = options.textColor or TEXT_COLOR
    options.textColorPress = options.textColorPress or TEXT_COLOR_PRESS
    
    options.backgroundColor = options.backgroundColor
    options.backgroundColorPress = options.backgroundColorPress
    
    options.parent = options.parent -- tabbar reference
    options.data = options.data or {} --never null data
    options.onClick = options.onClick --callback handler
    
    --move view
    self.view.anchorChildren = true
    self.view.x = options.x
    self.view.y = options.y
    
    
    self.height = options.height
    self.width = options.width
    
    if (ly.isNotEmpty(options.data)) then
        _init_press_background(self)
        _init_underline(self)
        _init_label(self)
        
        --show underline
        self:setSelected(options.data.selected)
    end
    
    --handlers
    local function on_touch(event)
        _handle_on_touch(self, event)
        return true
    end
    
    if (ly.isEventListener(self.view._press_background)) then
        self.view._press_background:addEventListener(EVENT_TOUCH, on_touch)
    end
    
    self.onFree = function()
        if (ly.isDisplayObject(self.view)) then
            
            if (ly.isEventListener(self.view._press_background)) then
                self.view._press_background:removeEventListener(EVENT_TOUCH, on_touch)
            end
            
        end
    end
end

local function _free(self)
    if(ly.isDisplayObject(self.view)) then
        
        if (ly.isDisplayObject(self.view._press_background)) then
            self.view._press_background:removeSelf()
            self.view._press_background = nil
        end
        if (ly.isDisplayObject(self.view._underline)) then
            self.view._underline:removeSelf()
            self.view._underline = nil
        end
        if (ly.isDisplayObject(self.view._text)) then
            self.view._text:removeSelf()
            self.view._text = nil
        end
    end
end

--------------------------------------------------------------------------------
-- constructor
--------------------------------------------------------------------------------

local ElementClass = Class(Super) --inherits Object Class

function ElementClass:initialize(options)
    if(Super.initialize(self, options)) then
        self._className = CLASS_NAME
        
        _init(self)
        
        return true
    else
        return false
    end
end

function ElementClass:finalize()
    _free(self)
    if (Super.finalize(self)) then
        if (nil~=self.view) then
            print('WARNING: Probable Memory Leak issue.') 
        end
        return true
    else
        return false
    end
    
end

--------------------------------------------------------------------------------
-- public
--------------------------------------------------------------------------------

ElementClass.UNDERLINE_POS_TOP = UNDERLINE_POS_TOP
ElementClass.UNDERLINE_POS_BOTTOM = UNDERLINE_POS_BOTTOM
ElementClass.TEXT_COLOR = TEXT_COLOR
ElementClass.TEXT_COLOR_PRESS = TEXT_COLOR_PRESS
ElementClass.UNDERLINE_COLOR = UNDERLINE_COLOR
ElementClass.UNDERLINE_HEIGHT = UNDERLINE_HEIGHT

function ElementClass:setSelected(value)
    _set_selected(self, value)
end

--------------------------------------------------------------------------------
-- export
--------------------------------------------------------------------------------

return ElementClass
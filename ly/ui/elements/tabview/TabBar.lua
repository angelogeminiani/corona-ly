--ly.ui.elements.tabview.TabBar
local CLASS_NAME = 'ly.ui.elements.tabview.TabBar'

--------------------------------------------------------------------------------
-- import
--------------------------------------------------------------------------------

local ly = require("ly.ly")

local Super = require('ly.ui.BaseControl') -- Superclass reference

local ButtonClass = require('ly.ui.elements.tabview.TabBarButton')

--------------------------------------------------------------------------------
-- events
--------------------------------------------------------------------------------

--outbound
local EVENT_ON_CHANGE = 'change' --changed tab

--------------------------------------------------------------------------------
-- const
--------------------------------------------------------------------------------

local SWIPE_THRESHOLD = 20
local TAB_BAR_HEIGHT = 54
local COLOR_BACKGROUND =  {1,1,1}
local COLOR_PRESS_BACKGROUND =  {.9,.9,.9}

--button
local BUTTON_MARGIN = 3
local SEPARATOR_COLOR = {.9,.9,.9}
local SEPARATOR_WIDTH = 1
local UNDERLINE_POS_BOTTOM = ButtonClass.UNDERLINE_POS_BOTTOM
local UNDERLINE_COLOR =  ButtonClass.UNDERLINE_COLOR
local UNDERLINE_HEIGHT =  ButtonClass.UNDERLINE_HEIGHT
local TEXT_COLOR = ButtonClass.TEXT_COLOR
local TEXT_COLOR_PRESS = ButtonClass.TEXT_COLOR_PRESS

local TAB_DATA = {
    _id=0,
    label = 'undefined',
    selected=false,
    view = nil
}
--------------------------------------------------------------------------------
-- trigger
--------------------------------------------------------------------------------

local function _do_change_tab(self, data)
    local index = data['_id']
    
    if (index>0) then
        -- trigger event
        self:dispatchEvent({
            target = self,
            name = EVENT_ON_CHANGE,
            data = data,
            index = index
        })
    end
end

local function _select_button(self, index)
    local options = self._options
    if (ly.isNotNull(options) and ly.isDisplayObject(self.view)) then
        ly.forEach(self._tabs, function(tabData) 
            if (ly.isNotNull(tabData)) then
                local button = tabData['button']
                if (ly.isDisplayObject(button)) then
                    local selected = index==tabData['_id']
                    button:setSelected(selected)
                    if (selected) then
                        _do_change_tab(self, tabData)
                    end
                end
            end
        end)
    end
end

--------------------------------------------------------------------------------
-- handlers
--------------------------------------------------------------------------------

local function _on_button_click(event)
    local self = event.parent
    if (ly.isLyObject(self)) then
        if(ly.isDisplayObject(self.view))then
            local data = event.data
            if (ly.isNotNull(data)) then
                local index = data['_id']
                if (ly.isNotEmpty(index)) then
                    -- toggle selection
                    _select_button(self, index)
                end
            end
        end
    end
end

--------------------------------------------------------------------------------
-- items
--------------------------------------------------------------------------------

local function _count(self)
    if(self._tabs) then
        return #self._tabs
    end
    return 0
end

local function _clear(self)
    if (ly.isDisplayObject(self.view)) then
        --clear items
        if(_count(self)>0) then
            self._tabs = {}
        end
        self.currentIndex = 0 --no active item
    end
end

local function _is_valid_index(self, index)
    local count = _count(self)
    return (index>0) and (count>index or count ==index)
end

local function _get(self, index)
    if (_is_valid_index(self, index)) then
        return self._tabs[index]
    end
    return nil
end

local function _add(self, tabData)
    if (ly.isObject(tabData)) then
        local index = tabData['_id'] or #self._tabs + 1
        tabData['_id'] = tabData['_id'] or index
        self._tabs[tabData['_id']] = tabData
        return tabData['_id']
    end
    return 0
end

--------------------------------------------------------------------------------
-- display
--------------------------------------------------------------------------------

local function _add_button(self, tabData)
    local options = self._options
    if (ly.isNotNull(options) and ly.isDisplayObject(self.view) and ly.isObject(tabData)) then
        local margin =  options.buttonMargin
        local count = options.count
        local width = (options.width-(margin*(count-1)))/count
        local height = options.height
        
        --creates index
        local index = _add(self, tabData)
        if (index>0) then
            
            --creates button
            local params = {}
            params.parent = self --pass self reference as parent to use in event handler
            params.onClick = _on_button_click
            params.data = tabData
            params.width = width
            params.height = height
            params.x = width*0.5 + width*(index-1) + margin*(index-1)
            params.y = height*0.5
            params.buttonMargin = options.buttonMargin 
            params.underlineColor =  options.underlineColor 
            params.underlinePos = options.underlinePos 
            params.underlineHeight = options.underlineHeight 
            params.textColor = options.textColor
            params.textColorPress = options.textColorPress
            params.backgroundColor = options.backgroundColor
            params.backgroundColorPress = options.backgroundColorPress
            local button = ButtonClass:new(params)
            tabData['button'] = button
            
            --create separator
            local sep
            local separatorWidth = ly.toNumber(options.separatorWidth)
            if (separatorWidth>0 and index<count) then
                local sep_x = width*index - margin*0.5*index
                local sep_y = 5
                sep = display.newLine(sep_x, sep_y, sep_x, sep_y + height - 10)
                sep:setStrokeColor( unpack(options.separatorColor) )
                tabData['separator'] = sep
                
                --insert sep
                self.view:insert(sep)
            end
            
            -- insert view
            self.view:insert(button.view)
            
            return button
        end
    end
    return nil
end

local function _clear_buttons(self)
    local options = self._options
    if (ly.isNotNull(options) and ly.isDisplayObject(self.view)) then
        ly.forEach(self._tabs, function(tabData) 
            if (ly.isNotNull(tabData)) then
                if (ly.isDisplayObject(tabData['button'])) then
                    tabData['button']:removeSelf()
                    tabData['button'] = nil
                end
                if (ly.isDisplayObject(tabData['separator'])) then
                    tabData['separator']:removeSelf()
                    tabData['separator'] = nil
                end
            end
        end)
    end
end

--------------------------------------------------------------------------------
-- init
--------------------------------------------------------------------------------

local function _init_background(self)
    local options = self._options
    
    self.view._background = display.newRect(0, 0, options.width, options.height)
    self.view._background:setFillColor( unpack(options.backgroundColor) )
    self.view._background.anchorX = 0
    self.view._background.anchorY = 0
    self.view:insert(self.view._background)
end

local function _init(self)
    local options = self._options or {}
    
    --component
    options.width = options.width or display.contentWidth
    options.height = options.height or TAB_BAR_HEIGHT
    options.x = options.x or options.width*0.5
    options.y = options.y or options.height*0.5
    
    options.backgroundColor = options.backgroundColor or COLOR_BACKGROUND
    options.backgroundColorPress = options.backgroundColorPress or COLOR_PRESS_BACKGROUND
    
    --buttons
    options.textColor = options.textColor or TEXT_COLOR
    options.textColorPress = options.textColorPress or TEXT_COLOR_PRESS
    options.separatorColor = options.separatorColor or SEPARATOR_COLOR
    options.separatorWidth = options.separatorWidth or SEPARATOR_WIDTH
    options.buttonMargin = options.buttonMargin or BUTTON_MARGIN
    options.underlineColor =  options.underlineColor or UNDERLINE_COLOR
    options.underlinePos = options.underlinePos or UNDERLINE_POS_BOTTOM
    options.underlineHeight = options.underlineHeight or UNDERLINE_HEIGHT
    
    -- extend
    options.count = 0
    
    --move view
    self.view.anchorChildren = true
    self.view.x = options.x
    self.view.y = options.y
    
    --array of button data
    self._tabs = {}
    
    
    self.height = options.height
    self.width = options.width
    
    _init_background(self)
    
    self.onFree = function()
        if (ly.isDisplayObject(self.view)) then
            
        end
    end
end

local function _free(self)
    _clear_buttons(self)
    _clear(self)
    if(ly.isDisplayObject(self.view)) then
        
        if (ly.isDisplayObject(self.view._background)) then
            self.view._background:removeSelf()
            self.view._background = nil
        end
        
    end
end

--------------------------------------------------------------------------------
-- constructor
--------------------------------------------------------------------------------

local ElementClass = Class(Super) --inherits Object Class

function ElementClass:initialize(options)
    if(Super.initialize(self, options)) then
        self._className = CLASS_NAME
        
        _init(self)
        
        return true
    else
        return false
    end
end

function ElementClass:finalize()
    _free(self)
    if (Super.finalize(self)) then
        if (nil~=self.view) then
            print('WARNING: Probable Memory Leak issue.') 
        end
        return true
    else
        return false
    end
    
end

--------------------------------------------------------------------------------
-- public
--------------------------------------------------------------------------------

ElementClass.onChange = EVENT_ON_CHANGE


function ElementClass:setButtons(tabDataArray)
    if (ly.isArray(tabDataArray)) then
        local options = self._options
        local selected_index = 0
        if (ly.isNotNull(options)) then
            options.count = #tabDataArray
            ly.forEach(tabDataArray, function(tabData) 
                _add_button(self, tabData)
            end)
            -- separators to front, select button
            ly.forEach(tabDataArray, function(tabData) 
                -- separator to front
                if (ly.isDisplayObject(tabData['separator'])) then
                    tabData['separator']:toFront()
                end
                --button selected
                if (tabData['selected']) then
                    selected_index = tabData['_id']
                end
            end)
            -- select current tab and trigger change event
            if (selected_index>0) then
                self:setSelected(selected_index)
            end
        end
    end
end

function ElementClass:setSelected(index)
    _select_button(self, index)
end

--------------------------------------------------------------------------------
-- export
--------------------------------------------------------------------------------

return ElementClass
local CLASS_NAME = 'ly.ui.elements.tabview.TabView'
--------------------------------------------------------------------------------
-- import
--------------------------------------------------------------------------------

local ly = require("ly.ly")

local Super = require('ly.ui.BaseControl') -- Superclass reference

local TabBarClass = require ('ly.ui.elements.tabview.TabBar')


--------------------------------------------------------------------------------
-- events
--------------------------------------------------------------------------------

--inbound
local IN_EVENT_CHANGE = TabBarClass.onChange

--outbound
local OUT_EVENT_CHANGE = 'change' --changed tab

--------------------------------------------------------------------------------
-- const
--------------------------------------------------------------------------------

local TAB_BAR_HEIGHT = 48
local BACKGROUND =  {0,.9,.9}

local TAB_DATA = {
    _id=0,
    label = 'undefined',
    selected=false,
    view = nil
}
--------------------------------------------------------------------------------
-- private
--------------------------------------------------------------------------------

local function _on_change_tab(self, event)
    if (ly.isDisplayObject(self.view)) then
        local options = self._options
        if (ly.isNotNull(options) and ly.isNotNull(event.data) and ly.isArray(self._tabs)) then
            local index = event.data['_id']
            
            --set current index
            self.currentIndex = index
            
            --select current tab
            ly.forEach(self._tabs, function(data) 
                local is_current = data['_id']==index
                local view = data['view']
                if (ly.isDisplayObject(view)) then
                    view.isVisible = is_current
                end
            end)
            
            --trigger event
            self:dispatchEvent({
                target = self,
                name = OUT_EVENT_CHANGE,
                data = event.data
            })
        end
    end
end

--------------------------------------------------------------------------------
-- items
--------------------------------------------------------------------------------

local function _count(self)
    if(self._tabs) then
        return #self._tabs
    end
    return 0
end

local function _clear(self)
    if (ly.isDisplayObject(self.view)) then
        --clear items
        if(_count(self)>0) then
            self._tabs = {}
        end
        self.currentIndex = 0 --no active item
    end
end

local function _is_valid_index(self, index)
    local count = _count(self)
    return (index>0) and (count>index or count ==index)
end

local function _get(self, index)
    if (_is_valid_index(self, index)) then
        return self._tabs[index]
    end
    return nil
end

local function _add(self, tabData)
    if (ly.isObject(tabData)) then
        local index = #self._tabs + 1
        tabData['_id'] = index
        self._tabs[index] = tabData
        return index
    end
    return 0
end

--------------------------------------------------------------------------------
-- display
--------------------------------------------------------------------------------

local function _add_view(self, tabData)
    local options = self._options
    if (ly.isNotNull(options) and ly.isDisplayObject(self.view) and ly.isObject(tabData)) then
        --creates index
        local index = tabData['_id']
        if (index>0) then
            --get view
            local view
            if (ly.isDisplayObject(tabData['view'])) then
                view = tabData['view']
            else
                view = display.newGroup()
                tabData['view'] = view
            end
            
            --view.anchorChildren = true
            view.x = options.contentX
            view.y = options.contentY
            view.isVisible = false --hidden
            
            -- insert view
            self.view:insert(view)
            
            return view
        end
    end
    return nil
end

local function _set_buttons(self, tabDataArray)
    --create indexes and view
    ly.forEach(tabDataArray, function(tabData) 
        _add(self, tabData)
        _add_view(self, tabData)
    end)
    
    --tab bar
    if (ly.isDisplayObject(self.view._tab_bar)) then
        self.view._tab_bar:setButtons(tabDataArray)
    end
    
    return tabDataArray
end

local function _clear_views(self)
    local options = self._options
    if (ly.isNotNull(options) and ly.isDisplayObject(self.view)) then
        ly.forEach(self._tabs, function(tabData) 
            if (ly.isNotNull(tabData)) then
                if (ly.isDisplayObject(tabData['view'])) then
                    tabData['view']:removeSelf()
                    tabData['view'] = nil
                end
            end
        end)
    end
end

--------------------------------------------------------------------------------
-- init
--------------------------------------------------------------------------------

local function _init_background(self)
    local options = self._options
    
    self.view._background = display.newRect(0, 0, options.width, options.height)
    self.view._background:setFillColor( unpack(options.backgroundColor) )
    self.view._background.anchorX = 0
    self.view._background.anchorY = 0
    self.view:insert(self.view._background)
end

local function _init_tab_bar(self)
    local options = self._options or {}
    
    local params = {
        height = options.tabBarHeight,
        width = options.width,
        --x = options.x,
        y = options.tabBarHeight*0.5,
        --buttons
        buttonMargin = options.buttonMargin,
        underlineColor =  options.underlineColor,
        underlinePos = options.underlinePos,
        underlineHeight = options.underlineHeight,
        separatorColor = options.separatorColor,
        separatorWidth = options.separatorWidth,
    }
    
    self.view._tab_bar = TabBarClass:new(params)
    self.view:insert(self.view._tab_bar.view)
end

local function _init(self)
    local options = self._options or {}
    
    --component
    options.width = options.width or display.contentWidth
    options.height = options.height or display.contentHeight
    options.x = options.x or options.width*0.5
    options.y = options.y or options.height*0.5
    options.backgroundColor =  options.backgroundColor or BACKGROUND
    
    --tabbar
    options.tabBarHeight = options.tabBarHeight or TAB_BAR_HEIGHT
    
    --buttons
    options.separatorColor = options.separatorColor
    options.separatorWidth = options.separatorWidth
    options.buttonMargin = options.buttonMargin 
    options.underlineColor =  options.underlineColor
    options.underlinePos = options.underlinePos 
    options.underlineHeight = options.underlineHeight 
    
    --extend
    options.count = 0
    options.contentWidth = options.width
    options.contentHeight = options.height - options.tabBarHeight
    options.contentX = 0
    options.contentY = options.tabBarHeight
    
    --move view
    self.view.anchorChildren = true
    self.view.x = options.x
    self.view.y = options.y
    
    self._tabs = {}
    self.currentIndex = 0
    
    self.height = options.height
    self.width = options.width
    self.tabBarHeight = options.tabBarHeight
    self.contentWidth = options.contentWidth
    self.contentHeight = options.contentHeight
    
    _init_background(self)
    _init_tab_bar(self)
    
    --handlers
    local function on_change_tab(event)
        _on_change_tab(self, event)
        return true
    end
    
    if (ly.isEventListener(self.view._tab_bar)) then
        self.view._tab_bar:addEventListener(IN_EVENT_CHANGE, on_change_tab)
    end
    
    --free
    self.onFree = function()
        if (ly.isDisplayObject(self.view)) then
            
            if (ly.isEventListener(self.view._tab_bar)) then
                self.view._tab_bar:removeEventListener(IN_EVENT_CHANGE, on_change_tab)
            end
        end
    end
end

local function _free(self)
    _clear_views(self)
    _clear(self)
    if(ly.isDisplayObject(self.view)) then
        
        if (ly.isDisplayObject(self.view._background)) then
            self.view._background:removeSelf()
            self.view._background = nil
        end
        
        if (ly.isDisplayObject(self.view._tab_bar)) then
            self.view._tab_bar:removeSelf()
            self.view._tab_bar = nil
        end
    end
end

--------------------------------------------------------------------------------
-- constructor
--------------------------------------------------------------------------------

local ElementClass = Class(Super) --inherits Object Class

function ElementClass:initialize(options)
    if(Super.initialize(self, options)) then
        self._className = CLASS_NAME
        
        _init(self)
        
        return true
    else
        return false
    end
end

function ElementClass:finalize()
    _free(self)
    if (Super.finalize(self)) then
        if (nil~=self.view) then
            print('WARNING: Probable Memory Leak issue.') 
        end
        return true
    else
        return false
    end
    
end

--------------------------------------------------------------------------------
-- public
--------------------------------------------------------------------------------

ElementClass.onChange = OUT_EVENT_CHANGE

function ElementClass:getTabData(index)
    if (ly.isNotNull(index)) then
        return _get(self, index)
    end
end

function ElementClass:setButtons(tabDataArray)
    if (ly.isArray(tabDataArray)) then
        local options = self._options
        if (ly.isNotNull(options)) then
            options.count = #tabDataArray
            _set_buttons(self, tabDataArray)
        end
    end
    return tabDataArray
end

function ElementClass:getView(index)
    local tabData = self:getTabData(index)
    if (ly.isNotNull(tabData)) then
        return tabData['view']
    end
    return nil
end

function ElementClass:getViewBounds()
    local options = self._options
    local result = {
        width = options.contentWidth,
        height = options.contentHeight,
        x = options.contentX,
        y = options.contentY,
    }
    return result
end

function ElementClass:insert(index, displayObject)
    local tabData = self:getTabData(index)
    if (ly.isNotNull(tabData)) then
        local view = tabData['view']
        if (ly.isDisplayObject(view) and ly.isDisplayObject(displayObject)) then
            if(ly.isLyObject(displayObject)) then
                displayObject = displayObject.view
            end
            displayObject.anchorX = 0
            displayObject.anchorY = 0
            view:insert(displayObject)
        end
    end
    return nil
end

--------------------------------------------------------------------------------
-- export
--------------------------------------------------------------------------------

return ElementClass
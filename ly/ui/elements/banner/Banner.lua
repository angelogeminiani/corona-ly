--ly.ui.elements.banner.Banner
--------------------------------------------------------------------------------
-- import
--------------------------------------------------------------------------------
local widget = require("widget")

local ly = require('ly.ly')
local app = require("ly.lyApp")

local Super = require('ly.ui.BaseControl') -- Superclass reference

local ImageLoader = require('ly.components.imageloader.ImageLoader')

--------------------------------------------------------------------------------
-- events
--------------------------------------------------------------------------------

--inbound
local EVENT_ON_TAP = 'tap'
local EVENT_ON_TOUCH = 'touch'

--outbound
local EVENT_CLICK = 'click'

--------------------------------------------------------------------------------
-- constants
--------------------------------------------------------------------------------

local BLANK_IMAGE = 'ly/ui/elements/banner/banner_frame.png'

local DEF_WIDTH = 320
local DEF_HEIGHT = 100
local MODULE_RATIO = 1 / (DEF_WIDTH/DEF_HEIGHT) --0.3125

--------------------------------------------------------------------------------
-- trigget
--------------------------------------------------------------------------------

local function _do_on_click(self)
    local options = self._options
    if(ly.isEventListener(self)) then
        self:dispatchEvent({
            name = EVENT_CLICK,
            target=self,
            data = {
                id = options.id,
                url = options.url,
                name = options.name,
                description = options.description
            }
        })
    end
end


--------------------------------------------------------------------------------
-- private
--------------------------------------------------------------------------------

local function _toggle_loading(self, value)
    local sceneGroup = self.view
    if(ly.isDisplayObject(sceneGroup)) then
        local spinner = sceneGroup._spinner
        if (ly.isDisplayObject(spinner)) then
            spinner.isVisible = value
            spinner:toFront()
            if (value) then
                spinner:start()
                if(ly.isDisplayObject(sceneGroup._block))then
                    sceneGroup._block.alpha = 0.2
                end
            else
                spinner:stop()
                if(ly.isDisplayObject(sceneGroup._block))then
                    transition.to( sceneGroup._block, { alpha=1.0, time=1500 } )
                end
            end
        end
    end
end

--------------------------------------------------------------------------------
-- init
--------------------------------------------------------------------------------

local function _init_background(self)
    local options = self._options
    
    self.view._background = display.newRect(0, 0, options.width, options.height)
    self.view._background:setFillColor( .5,.5,.5 )
    self.view._background.anchorX = 0
    self.view._background.anchorY = 0
    self.view._background.x = options.x
    self.view._background.y = options.y
    self.view:insert(self.view._background)
end

local function _init_spinner(self)
    local options = self._options
    local sceneGroup = self.view
    
    -- spinner
    sceneGroup._spinner = widget.newSpinner({ 
        width = 32, 
        height = 32, 
    })
    sceneGroup._spinner.x = options.width*0.5
    sceneGroup._spinner.y = options.height*0.5
    sceneGroup._spinner.isVisible = false
    sceneGroup:insert(sceneGroup._spinner)
end

local function _init_image(self, callback)
    local options = self._options
    local filename = options.filename
    
    --load image
    _toggle_loading(self, true)
    
    ImageLoader.singleton:loadImage(filename, function(err, response) 
        if (ly.isDisplayObject(self.view) and not err) then
            
            _toggle_loading(self, false)
            
            local parent = self.view._block
            if(ly.isDisplayObject(parent)) then
                local bounds = ly.graphics.getBounds(response.filename, response.baseDirectory)
                if(ly.isNotNull(bounds)) then
                    --adjust bounds
                    if (bounds.height~=options.height) then
                        local ratio = bounds.height/options.height
                        bounds.height = bounds.height/ratio
                        bounds.width = bounds.width/ratio
                    end
                    --{width=40, height=40, numFrames=20, sheetContentWidth = 200,  sheetContentHeight = 160 }
                    local sheet_options = {
                        width = options.width, --frame width
                        height = options.height, --frame height
                        sheetContentWidth = bounds.width,
                        sheetContentHeight = bounds.height
                    }
                    sheet_options.numFrames = math.ceil(bounds.width / sheet_options.width)
                    local sheet= graphics.newImageSheet(response.filename, response.baseDirectory, sheet_options)
                    if (ly.isNotNull(sheet)) then
                        local sequence = { name="banner", sheet=sheet, start=1, count=sheet_options.numFrames, time=sheet_options.numFrames*options.frame_time, loopCount=0, loopDirection = "forward" }
                        local sprite = display.newSprite( sheet, sequence )
                        sprite.x = options.width*0.5
                        sprite.y = options.height*0.5
                        
                        sprite._numFrames = sheet_options.numFrames 
                        
                        parent:insert(sprite)
                        self.view._sprite = sprite
                    end
                end 
            end
        end
        ly.invoke(callback)
    end)
end

local function _init(self)
    local view = self.view
    local options = self._options
    
    options.id = options.id or ly.guid.generate()
    options.name = options.name or ''
    options.description = options.description or ''
    options.filename = options.filename or BLANK_IMAGE
    options.url = options.url
    options.frame_time = ly.toNumber(options.frame_time, 3000)
    options.width = options.width or ly.screen.width
    options.height = options.height or options.width * MODULE_RATIO
    options.x = options.x or 0
    options.y = options.y or 0
    
    self.width = options.width
    self.height = options.height
    
    view.anchorChildren=true
    view.x = options.x
    view.y = options.y
    
    view._block = display.newGroup() -- main container with fadein effect
    view:insert(view._block)
    
    --background
    _init_background(self)
    
    --spinner
    _init_spinner(self)
    
    --async picture
    _init_image(self, function() 
        if(ly.isDisplayObject(self.view)) then
            
            if(ly.isDisplayObject(self.view._block)) then
                view._block:toFront()
            end
            
            if (ly.isDisplayObject(self.view._sprite)) then
                local frames = ly.toNumber(self.view._sprite._numFrames)
                if(frames>0) then
                    self:play()
                end
            end
            
        end
    end)
    
    --handlers
    local function on_tap(event)
        _do_on_click(self)
        return true
    end
    local function on_touch(event)
        return true
    end
    
    if(ly.isEventListener(self.view)) then
        self.view:addEventListener(EVENT_ON_TAP, on_tap)
        self.view:addEventListener(EVENT_ON_TOUCH, on_touch)
    end
    
    --hook on free
    self.onFree = function()
        if(ly.isEventListener(self.view)) then
            self.view:removeEventListener(EVENT_ON_TAP, on_tap)
            self.view:removeEventListener(EVENT_ON_TOUCH, on_touch)
        end
    end
end

local function _free(self)
    if(ly.isDisplayObject(self.view)) then
        
        if (ly.isDisplayObject(self.view._background)) then
            self.view._background:removeSelf()
            self.view._background = nil
        end
        if (ly.isDisplayObject(self.view._sprite)) then
            self.view._sprite:pause()
            self.view._sprite:removeSelf()
            self.view._sprite = nil
        end
        if (ly.isDisplayObject(self.view._block)) then
            self.view._block:removeSelf()
            self.view._block = nil
        end
        if (ly.isDisplayObject(self.view._spinner)) then
            self.view._spinner:removeSelf()
            self.view._spinner = nil
        end
    end
end

--------------------------------------------------------------------------------
-- constructor
--------------------------------------------------------------------------------

local BaseClass = Class(Super) --inherits Object Class

--INSTANCE FUNCTIONS
function BaseClass:initialize(options)
    options = options or {}
    
    if(Super.initialize(self, options)) then
        self._className = 'ly.ui.elements.banner.Banner'
        _init(self)
        return true
    else
        return false
    end
end

function BaseClass:finalize()
    _free(self)
    
    if(Super.finalize(self)) then
        return true
    else
        return false
    end
end

--------------------------------------------------------------------------------
-- public
--------------------------------------------------------------------------------

BaseClass.onClick = EVENT_CLICK

function BaseClass:play()
    if(ly.isDisplayObject(self.view)) then
        if(ly.isDisplayObject(self.view._sprite)) then
            self.view._sprite:play()
        end
    end
end

function BaseClass:stop()
    if(ly.isDisplayObject(self.view)) then
        if(ly.isDisplayObject(self.view._sprite)) then
            self.view._sprite:stop()
        end
    end
end

--------------------------------------------------------------------------------
-- export
--------------------------------------------------------------------------------

return BaseClass



-- ly.ui.elements.notification.Notification
--------------------------------------------------------------------------------
-- import
--------------------------------------------------------------------------------

local ly = require("ly.ly")

local Super = require('ly.ui.BaseControl') -- Superclass reference
local ElementClass = Class(Super) --inherits Object Class

--------------------------------------------------------------------------------
-- fields
--------------------------------------------------------------------------------

-- declare here local fields

--------------------------------------------------------------------------------
-- private
--------------------------------------------------------------------------------

local function _hide(self, view)
    view.alpha = 0
end

local function _show(self, view)
    transition.to(view, {alpha=1, time=600})
end

local function _set_notification(self, value)
    if(ly.isDisplayObject(self.view)) then
        self.view:toFront()
        local notification = self.view._notification
        local group = self.view._group_notification
        if(ly.isDisplayObject(notification) and ly.isDisplayObject(group)) then
            local show = false
            if(ly.isNumber(value)) then
                notification.text = tostring(value)
                show = ly.conditional(value>0, true, false)
            elseif(ly.isString(value)) then
                notification.text = value
                show = ly.conditional(ly.isNotZero(value), true, false)
            else
                --not supported
                show = false
            end
            if (show) then
                -- transition.to(group, {alpha=1, time=600})
                _show(self, group)
            else
                -- transition.to(group, {alpha=0, time=100})
                _hide(self, group)
            end
        end
    end
end

--------------------------------------------------------------------------------
-- init
--------------------------------------------------------------------------------

local function _init_notification(self)
    local options = self._options
    
    self.view._group_notification = display.newGroup()
    self.view:insert(self.view._group_notification)
    
    --new parent
    local sceneGroup = self.view._group_notification
    
    local w = options.width
    local x = options.x
    local y = options.y
    local text = options.text or ''
    
    local circle = display.newCircle(x, y, w)
    circle.strokeWidth = 1
    circle:setStrokeColor( 1, 1, 1, .6 )
    circle:setFillColor(1,0,0)
    sceneGroup:insert(circle)
    
    self.view._notification = display.newText({
        text = text,
        x = x, y= y,
        fontSize = 12,
        font = native.systemFontBold
    })
    sceneGroup:insert(self.view._notification)
    
    --hide notification
    self:setText(text)
end

local function _init(self)
    local options = self._options or {}
    
    --data binding
    options.x = options.x or 0
    options.y = options.y or 0
    options.width = options.width or 12
    options.height = options.height or 12
    options.text = options.text or ''
    
    
    _init_notification(self)
    
end

local function _free(self)
    if(ly.isDisplayObject(self.view)) then
        if(ly.isDisplayObject(self.view._group_notification)) then
            self.view._group_notification:removeSelf()
            self.view._group_notification = nil
        end
    end
end

--------------------------------------------------------------------------------
-- constructor
--------------------------------------------------------------------------------

local ElementClass = Class(Super) --inherits Object Class

function ElementClass:initialize(options)
    --calls parent Class's initialize()
    --with "self" as this instance
    if(Super.initialize(self, options)) then
        _init(self, options)
        
        self.height = self.view.height
        self.width = self.view.width
        
        return true
    else
        return false
    end
end

function ElementClass:finalize()
    _free(self)
    if (Super.finalize(self)) then
        if (nil~=self.view) then
            print('WARNING: Probable Memory Leak issue.') 
        end
        return true
    else
        return false
    end
    
end

--------------------------------------------------------------------------------
-- public
--------------------------------------------------------------------------------

function ElementClass:setText(value)
    _set_notification(self, value)
end

--------------------------------------------------------------------------------
-- export
--------------------------------------------------------------------------------

return ElementClass
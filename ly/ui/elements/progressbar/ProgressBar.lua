
--------------------------------------------------------------------------------
-- import
--------------------------------------------------------------------------------

local widget = require('widget')

local ly = require('ly.ly')

local Super = require('ly.ui.BaseControl') -- Superclass reference
local ElementClass = Class(Super) --inherits Object Class

--------------------------------------------------------------------------------
-- private
--------------------------------------------------------------------------------

local function _set_progress(self, value)
    if (self.view and self.view._progress) then
        local text = (tostring(100 * value)..'%')
        self.view._progress:setProgress(value)
        self.view._text.text = text
    end
end

local function _build_instance(self, options)
    options = options or {}
    
    options.left = options.left or 0
    options.top = options.top or 0
    options.x = options.x or 0
    options.y = options.y or 0
    options.width = options.width or (display.contentWidth * 1)
    options.height = options.height or 20
    
    options.progress = options.progress or 0 -- from 0 to 1
    options.text = options.text or (tostring(100 * options.progress)..'%')
    options.textWidth = options.textWidth or options.width-10
    options.textAlign = options.textAlign or 'center'
    options.font = options.font or native.systemFontBold
    options.fontSize = options.fontSize or 12
    options.fontColor = options.fontColor or { 0, 0, 0 }
    
    --view
    local sceneGroup = self.view
    if options.left then
        sceneGroup.x = options.left + options.width * 0.5
    elseif options.left then
        sceneGroup.x = options.x
    end
    if options.top then
        sceneGroup.y = options.top + options.height * 0.5
    elseif options.top then
        sceneGroup.y = options.y
    end
    
    --progress
    sceneGroup._progress = widget.newProgressView {
        x = (ly.screen.width - options.width)*0.5, --center
        y = 0,
        width = options.width,
        height = options.height,
        isAnimated = true
    }
    sceneGroup._progress:setProgress(options.progress)
    sceneGroup:insert(sceneGroup._progress)
    
    --caption
    local labelParameters = {
        x = (ly.screen.width - options.width)*0.5,
        y = 10, 
        text = options.text,
        --width = ly.screen.width,
        align = options.textAlign,
        font = options.font,
        fontSize = options.fontSize
    }
    sceneGroup._text = display.newText( labelParameters )
    sceneGroup._text:setFillColor( unpack(options.fontColor) )
    sceneGroup:insert(sceneGroup._text)
    
    local super_on_free = self.onFree
    self.onFree = function()
        ly.invoke(super_on_free)
    end
end

local function _free(self)
    local sceneGroup = self.view
    
    if(ly.isDisplayObject(sceneGroup)) then
        if(ly.isDisplayObject(sceneGroup._progress))then
            sceneGroup._progress:removeSelf()
            sceneGroup._progress = nil
        end
        if(ly.isDisplayObject(sceneGroup._text))then
            sceneGroup._text:removeSelf()
            sceneGroup._text = nil
        end
    end
end

--------------------------------------------------------------------------------
-- constructor
--------------------------------------------------------------------------------

function ElementClass:initialize(options)
    self._className = 'ProgressBar'
    if(Super.initialize(self, options)) then
        
        _build_instance(self, options)
        
        self.height = self.view.height
        self.width = self.view.width
    end
end

function ElementClass:finalize()
    _free(self)
    if(Super.finalize(self)) then
        if (nil~=self.view) then
            print('WARNING: Probable Memory Leak issue.') 
        end
    end
end

--------------------------------------------------------------------------------
-- public
--------------------------------------------------------------------------------

function ElementClass:setText(value)
    if(self.view and self.view._text) then
        self.view._text.text = value 
    end
end

function ElementClass:getText()
    if(self.view and self.view._text) then
        return self.view._text.text
    else
        return ''
    end
end

function ElementClass:setProgress(value)
    _set_progress(self, value)
end

function ElementClass:getText()
    if(self.view and self.view._text) then
        return self.view._progress:getProgress()
    else
        return 0
    end
end
--------------------------------------------------------------------------------
-- export
--------------------------------------------------------------------------------

return ElementClass


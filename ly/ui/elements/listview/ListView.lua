--------------------------------------------------------------------------------
-- import
--------------------------------------------------------------------------------

local widget = require("widget")

local ly = require("ly.ly")
local Super = require('ly.ui.BaseControl') -- Superclass reference


--------------------------------------------------------------------------------
-- events
--------------------------------------------------------------------------------

local ON_ROW_RENDER = 'rowRender'
local ON_ROW_TOUCH = 'rowTouch'
local ON_NEED_RELOAD = 'needReload'
local ON_TOP_LIMIT_REACHED = 'topLimitReached'
local ON_BOTTOM_LIMIT_REACHED = 'bottomLimitReached'

--------------------------------------------------------------------------------
-- private
--------------------------------------------------------------------------------

local function _spinnerToggle(sceneGroup, name, enabled)
    if(sceneGroup and sceneGroup._spinner_top) then
        local spinner = sceneGroup._spinner_top
        if (name=='bottom') then
            spinner = sceneGroup._spinner_bottom
        end
        if (spinner) then
            if (enabled) then
                spinner.isVisible = true
                spinner:start()
            else
                spinner:stop()
                spinner.isVisible = false 
            end
        end
    end
end

local function _tableViewListener(self, event)
    if (not self._finalized and ly.isDisplayObject(self.view) and ly.isDisplayObject(self.view._list)) then
        local sceneGroup = self.view
        local options = self._options
        local is_reloadable = options.reloadable
        
        local contentPosition = 0
        if (event.target.parent) then
            if (event.target.parent.parent and event.target.parent.parent.getContentPosition) then
                contentPosition = event.target.parent.parent:getContentPosition()
            elseif(event.target.parent.getContentPosition) then
                contentPosition = event.target.parent:getContentPosition() 
            end
        end
        
        if event.phase == "began" then
            self._springStart = contentPosition
            --print("springStart", springStart)
            self._needToReload = false
            if(is_reloadable) then
                _spinnerToggle(sceneGroup, 'top', true)
            end
        elseif (event.phase == "moved") then
            if contentPosition>0 and contentPosition > self._springStart + options.topRealoadHeight then
                self._needToReload = true and is_reloadable
                --print("needToReload", needToReload, myList:getContentPosition( ), springStart + 60)
            end
        elseif (event.phase == nil and event.limitReached == true) then
            -- DOWN
            if(event.direction == "down") then
                --need reload?
                if(self._needToReload) then
                    --print("reloading Table!")
                    self._needToReload = false
                    _spinnerToggle(sceneGroup, 'top', false)
                    
                    self:dispatchEvent({name=ON_NEED_RELOAD})
                end
                self:dispatchEvent({name=ON_TOP_LIMIT_REACHED})
                -- UP
            elseif(event.direction == "up") then
                self:dispatchEvent({name=ON_BOTTOM_LIMIT_REACHED})  
            end
        end 
    end
    
    return true
end

--------------------------------------------------------------------------------
-- init
--------------------------------------------------------------------------------

local function _init_list(self)
    local options = self._options
    
    local tWidth = options.width
    local tHeight = 380
    local maskFile = "ly/assets/listview/mask-320x380.png"
    if ly.screen.isTablet then
        tWidth = 360
        maskFile = "ly/assets/listview/mask-360x380.png"
    end
    if ly.screen.isTall then
        tHeight = 448
        maskFile = "ly/assets/listview/mask-320x448.png"
    end
    
    local list = widget.newTableView{ 
        top = options.top, 
        width = options.width, 
        height = options.height, 
        maskFile = maskFile,
        hideBackground = true,
        listener = function(event) 
            _tableViewListener(self, event)
            return true
        end,
        onRowRender = function(event) 
            self:dispatchEvent(event)
            return true
        end,
        onRowTouch =  function(event) 
            self:dispatchEvent(ly.merge(event, {name=ON_ROW_TOUCH}))
            return true
        end 
    }
    -- center list
    list.x = options.x
    
    list:scrollToY( {y=0} )
    
    return list
end

local function _init_background(self)
    local options = self._options
    
    if(ly.isNotEmpty(options.backgroundColor))then
        self.view._background = display.newRect(options.x, options.top, options.width, options.height)
        self.view._background.anchorY = 0
        if(ly.isNotEmpty(options.backgroundColor.type)) then
            --gradient
            self.view._background:setFillColor(options.backgroundColor)
        else
            self.view._background:setFillColor(unpack(options.backgroundColor))
        end
        self.view._background.strokeWidth = options.strokeWidth
        self.view._background.stroke = options.strokeColor
        self.view:insert(self.view._background)
    end
end

local function _init_spinner(self)
    local options = self._options
    
    self.view._spinner_top = widget.newSpinner({ 
        width = 32, 
        height = 32, 
    })
    self.view._spinner_top.x = options.x
    self.view._spinner_top.y = options.top + 20
    self.view._spinner_top.isVisible = false
    self.view:insert(self.view._spinner_top)
    
    -- spinner BOTTOM
    self.view._spinner_bottom = widget.newSpinner({ 
        width = 32, 
        height = 32, 
    })
    self.view._spinner_bottom.x = options.x
    self.view._spinner_bottom.y = options.height + options.top - 20
    self.view._spinner_bottom.isVisible = false
    self.view:insert(self.view._spinner_bottom)
end

local function _init(self, options)
    options = options or {}
    
    options.id = options.id
    options.rowHeight = options.rowHeight or 60
    options.topRealoadHeight = options.topRealoadHeight or 100
    
    options.x = ly.conditional(ly.isNull(options.x), display.contentWidth / 2, options.x)
    options.top = options.top or 0
    options.width = options.width or (display.contentWidth * 0.75)
    options.height = options.height or display.contentHeight
    
    -- Vector options
    options.backgroundColor = options.backgroundColor or { 0.8, 0.8, 0.8 }
    options.strokeColor = options.strokeColor or options.backgroundColor
    options.strokeWidth = options.strokeWidth or 2
    
    options.reloadable = ly.toBoolean(options.reloadable, true) -- allow reload
    
    local sceneGroup = self.view
    
    --background
    _init_background(self)
    
    --spinner TOP and BOTTOM
    _init_spinner(self)
    
    --list
    sceneGroup._list = _init_list(self)
    sceneGroup:insert(sceneGroup._list)
    --self.view._list:scrollToY( {y=0} )
    
    
end

--------------------------------------------------------------------------------
-- list methods
--------------------------------------------------------------------------------

local function _deleteAllRows(self)
    local sceneGroup = self.view
    if (sceneGroup and sceneGroup._list) then
        sceneGroup._list:deleteAllRows()  
    end
end

local function _clear(self)
    local sceneGroup = self.view
    if (sceneGroup) then
        --clear items
        if(self.items and #self.items>0) then
            self.items = {}
        end
        --remove list
        if (sceneGroup._list) then
            sceneGroup._list:deleteAllRows()  
            sceneGroup._list:removeSelf()
            sceneGroup._list = nil
        end
        --new list
        sceneGroup._list = _init_list(self)
        sceneGroup:insert(sceneGroup._list)
    end
end

local function _render(self, item)
    local options = self._options
    
    local rowClass = options.rowClass
    local rowClassOptions = options.rowClassOptions or  {}
    if(ly.isNotNull(rowClass) and ly.isFunction(rowClass.new))then
        rowClassOptions = ly.merge(rowClassOptions, {item=item})
        --create only for height
        local rowView = rowClass:new(rowClassOptions)
        --update options
        options.rowClassOptions = rowClassOptions
        options.rowHeight = rowView.height
        --destroy
        rowView:removeSelf()
        rowView = nil
    end
    
    local params = ly.merge(options)
    params['item'] = item
    
    local list_view = self.view._list
    if(ly.isDisplayObject(list_view)) then
        local rowHeight = options.rowHeight or 60
        list_view:insertRow{
            rowHeight = rowHeight,
            rowColor = options.rowColor or { 1, 1, 1 },
            lineColor = options.lineColor or { 0.90, 0.90, 0.90 },
            isCategory = false,
            params = params --custom parameters for onRowRender
        }
    end
end

local function _scrollToIndex(self, index)
    if (ly.isNotNull(self.items)) then
        local list_view = self.view._list
        if(ly.isDisplayObject(list_view)) then
            list_view:scrollToIndex(index)
        end
    end  
end

local function _scrollToY(self, options)
    if (ly.isNotNull(self.items)) then
        local list_view = self.view._list
        if(ly.isDisplayObject(list_view)) then
            if (ly.isNumber(options)) then
                options = {y=options}
            end
            list_view:scrollToY(options)
        end
    end  
end

--------------------------------------------------------------------------------
-- constructor
--------------------------------------------------------------------------------

local ElementClass = Class(Super) --inherits Object Class

function ElementClass:initialize(options)
    --calls parent Class's initialize()
    --with "self" as this instance
    if(Super.initialize(self, options)) then
        self._className = 'ly.ui.elements.listview.ListView'
        -- add instance for row.params
        options = options or {}
        options._instance = self
        
        _init(self, options)
        
        self.items = {} -- collection of models
        
        self._springStart = 0;
        self._needToReload = false
        
        self.height = self.view.height
        self.width = self.view.width
        
        return true
    else
        return false
    end
end

function ElementClass:finalize()
    
    -- clean list
    if(ly.isDisplayObject(self.view) and ly.isDisplayObject(self.view._list)) then
        _deleteAllRows(self)
        self.view._list:removeSelf()
        self.view._list = nil
    end
    
    --calls parent Class's initialize()
    --with "self" as this instance
    if(Super.finalize(self)) then
        
        if(self.items) then
            self.items = nil
        end
        
        return true
    else
        return false
    end
end

--------------------------------------------------------------------------------
-- public
--------------------------------------------------------------------------------

ElementClass.EventRowRender = ON_ROW_RENDER
ElementClass.EventRowTouch = ON_ROW_TOUCH
ElementClass.EventNeedReload = ON_NEED_RELOAD
ElementClass.EventTopLimitReached = ON_TOP_LIMIT_REACHED
ElementClass.EventBottomLimitReached = ON_BOTTOM_LIMIT_REACHED
ElementClass.onRowRender = ON_ROW_RENDER
ElementClass.onRowTouch = ON_ROW_TOUCH
ElementClass.onNeedReload = ON_NEED_RELOAD
ElementClass.onTopLimitReached = ON_TOP_LIMIT_REACHED
ElementClass.onBottomLimitReached = ON_BOTTOM_LIMIT_REACHED

function ElementClass:toggleBottomSpinner(enabled)
    if(ly.isNotNull(self.view))then
        _spinnerToggle(self.view, 'bottom', enabled)
    end
end

function ElementClass:addItem(model)
    if (ly.isNull(self.items)) then
        self.items = {}
    end
    table.insert(self.items, model)
end

function ElementClass:addItems(list)
    ly.forEach(list, function(value, key, index) 
        self:addItem(value) 
    end) 
    
end

function ElementClass:sort(compare)
    if (ly.isNotNull(self.items)) then
        if(ly.isFunction(compare))then
            table.sort(self.items, compare) 
        else    
            table.sort(self.items) 
        end
    end
end

function ElementClass:clear()
    _clear(self)
end

function ElementClass:render()
    _deleteAllRows(self)
    if (ly.isNotNull(self.items)) then
        local list_view = self.view._list
        if(ly.isDisplayObject(list_view)) then
            for i = 1, #self.items do
                --add item to params
                _render(self, self.items[i])
            end  
        end
    end  
end

function ElementClass:scrollToIndex(index)
    _scrollToIndex(self, index)
end

function ElementClass:scrollToY(options)
    _scrollToY(self, options)
end

function ElementClass:scrollToTop()
    _scrollToY(self, {y=0})
end

function ElementClass:scrollToBottom()
    if (ly.isNotNull(self.items)) then
        local count = #self.items
        self:scrollToIndex(count)
    end  
end

--------------------------------------------------------------------------------
-- export
--------------------------------------------------------------------------------

return ElementClass


local CLASS_NAME = 'ly.ui.elements.icon.RoundIcon'
--------------------------------------------------------------------------------
-- import
--------------------------------------------------------------------------------
local widget = require("widget")

local ly = require("ly.ly")

local Super = require('ly.ui.BaseControl') -- Superclass reference

local ImageLoader = require('ly.components.imageloader.ImageLoader')

--------------------------------------------------------------------------------
-- constants
--------------------------------------------------------------------------------

local ICON_EMPTY = 'assets/images/avatars/empty.png'

local STROKE_WIDTH = 4
local STROKE_COLOR = ly.color.packed.fromHex('#d9d9d9')

--------------------------------------------------------------------------------
-- private
--------------------------------------------------------------------------------

local function _toggle_loading(self, value)
    local sceneGroup = self.view
    if (ly.isDisplayObject(sceneGroup) and (ly.isDisplayObject(sceneGroup._spinner))) then
        sceneGroup._spinner.isVisible = value
        sceneGroup._spinner:toFront()
        if (value) then
            sceneGroup._spinner:start()
            if(ly.isDisplayObject(sceneGroup._av_image))then
                sceneGroup._av_image.alpha = 0.2
            end
        else
            sceneGroup._spinner:stop()
            if(ly.isDisplayObject(sceneGroup._av_image))then
                transition.to( sceneGroup._av_image, { alpha=1.0, time=1500 } )
            end
        end
        return true
    else
        return false
    end
end

local function _create_display_image(self, filename, baseDir)
    local options = self._options
    
    filename = filename or options.filename
    baseDir = baseDir or system.ResourcesDirectory
    
    --remove existing
    if (ly.isDisplayObject(self.view._av_image)) then
        self.view._av_image:removeSelf()
        self.view._av_image = nil
    end
    
    -- local
    self.view._av_image = display.newCircle( 0, 0, options.width-5)
    self.view._av_image:setFillColor( 1, 1, 1, 166/255 )
    if(ly.toNumber(options.strokeWodth, 0)>0) then
        self.view._av_image.strokeWidth = options.strokeWidth
        self.view._av_image:setStrokeColor( unpack(options.strokeColor) )
    end
    self.view._av_image.fill = {
        type = "image",
        filename = filename,
        baseDir = baseDir
    }
    self.view:insert(self.view._av_image)
end

local function _set_image(self, path, baseDir)
    if (ly.isDisplayObject(self.view) and ly.isNotEmpty(path)) then
        if (ly.path.isHttp(path)) then
            _toggle_loading(self, true)
            -- remote
            ImageLoader.singleton:loadImage(path, function(err, response) 
                if(_toggle_loading(self, false)) then
                    if(err) then
                        ly.logger.debug('[RoundIcon.lua] _set_image: '..ly.json.encode(err)) 
                    else
                        local filename = response.filename
                        local baseDir = response.baseDirectory
                        _create_display_image(self, filename, baseDir)
                    end
                else
                    -- wrong context
                    ly.logger.debug('[RoundIcon.lua] _set_image: self.view is nil.')
                end
                
            end)
        else
            -- local
            baseDir = baseDir or system.ResourcesDirectory
            _create_display_image(self, path, baseDir)
            
            _toggle_loading(self, false)
        end
    end
end

local function _init(self, options)
    options = options or {}
    options.filename = options.filename or ICON_EMPTY
    options.width = options.width or 100
    options.x = options.x or 0
    options.y = options.y or 0
    options.strokeWidth = options.strokeWidth or STROKE_WIDTH
    options.strokeColor = options.strokeColor or STROKE_COLOR
    
    self.view.anchorChildren = true
    self.view.x = options.x
    self.view.y = options.y
    
    --avatar back
    self.view._av_back = display.newCircle( 0, 0, options.width*0.5)
    self.view._av_back:setFillColor( 1, 1, 1, 0.3 )
    -- attach to view
    self.view:insert(self.view._av_back)
    
    --avatar image
    _create_display_image(self)
    
    -- spinner
    --[[]]--
    self.view._spinner = widget.newSpinner({ 
        width = 32, 
        height = 32, 
    })
    self.view._spinner.x = self.view._av_back.x
    self.view._spinner.y = self.view._av_back.y
    self.view._spinner.isVisible = false
    self.view:insert(self.view._spinner)
    
end

local function _free(self)
    local sceneGroup = self.view
    if (ly.isDisplayObject(sceneGroup)) then
        if (ly.isDisplayObject(sceneGroup._av_image)) then
            sceneGroup._av_image:removeSelf()
            sceneGroup._av_image = nil
        end
        if (ly.isDisplayObject(sceneGroup._av_back)) then
            sceneGroup._av_back:removeSelf()
            sceneGroup._av_back = nil
        end
        if (ly.isDisplayObject(sceneGroup._spinner)) then
            sceneGroup._spinner:removeSelf()
            sceneGroup._spinner = nil
        end
    end
end

--------------------------------------------------------------------------------
-- constructor
--------------------------------------------------------------------------------
local ElementClass = Class(Super) --inherits Object Class

function ElementClass:initialize(options)
    if(Super.initialize(self, options)) then
        self._className = CLASS_NAME
        _init(self, options)
        
        self.height = options.width + (options.width*1.1)
        self.width = self.height
        return true
    else
        return false
    end
end

function ElementClass:finalize()
    _free(self)
    
    if(Super.finalize(self)) then
        if (nil~=self.view) then
            print('WARNING: Probable Memory Leak issue.') 
        end
        return true
    else
        return false
    end
end

--------------------------------------------------------------------------------
-- public
--------------------------------------------------------------------------------
function ElementClass:loading(loading)
    _toggle_loading(self, loading)  
end

function ElementClass:setImage(path, baseDir)
    _set_image(self, path, baseDir)
end


--------------------------------------------------------------------------------
-- export
--------------------------------------------------------------------------------

return ElementClass
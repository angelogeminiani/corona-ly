--ly.ui.elements.panel.BarPanel
--------------------------------------------------------------------------------
-- import
--------------------------------------------------------------------------------

local ly = require('ly.ly')
local app = require("ly.lyApp")

local Super = require('ly.ui.elements.panel.Panel') -- Superclass reference

local NotificationClass = require('ly.ui.elements.notification.Notification')

--------------------------------------------------------------------------------
-- events
--------------------------------------------------------------------------------

local EVENT_ON_SHOW = 'show'
local EVENT_ON_HIDE = 'hide'

--------------------------------------------------------------------------------
-- constants
--------------------------------------------------------------------------------

local DEFAULT_BAR_IMAGE = 'ly/ui/elements/panel/bar_panel_search.png'

local STOKE_COLOR = {0.33,0.33,0.33}
local STROKE_WIDTH = 4
local BACK_COLOR = {0.33,0.33,0.33, 0.8}

--------------------------------------------------------------------------------
-- show/hide
--------------------------------------------------------------------------------
local function _show(self, callback)
    if(not self._ani_showing and not self._ani_hiding) then
        self._ani_showing = true
        --print('showing')
        
        Super.show(self)
        local in_transition = ly.defaults({}, self.transitionIn)
        in_transition.tag = 'show'
        in_transition.onComplete = function(event) 
            --trigger search event
            self:dispatchEvent({
                name = EVENT_ON_SHOW,
                phase='did',
                target = self,
                options = self.searchOptions
            })
            --invoke callback
            ly.invoke(callback, self)
            
            self._ani_showing = false
            --print('showed')
        end
        self:dispatchEvent({
            name = EVENT_ON_SHOW,
            phase='will',
            target = self,
            options = self.searchOptions
        })
        
        --animate topbar
        if(ly.isDisplayObject(self.view._topbar)) then
            in_transition.y = in_transition.y +1--1 pixel correction
            transition.to( self.view._topbar, in_transition )
        end
    else
        ly.invoke(callback, self)
        --print('already showing')
    end
end

local function _hide(self, callback)
    if(not self._ani_hiding and not self._ani_showing) then
        self._ani_hiding = true
        
        Super.hide(self)
        local out_transition = ly.defaults({}, self.transitionOut)
        out_transition.tag = 'hide'
        out_transition.onComplete = function(event) 
            self._ani_hiding = false
            
            --trigger search event
            self:dispatchEvent({
                name = EVENT_ON_HIDE,
                phase='did',
                target = self,
                options = self.searchOptions
            })
            --invoke callback
            ly.invoke(callback, self)
        end
        self:dispatchEvent({
            name = EVENT_ON_HIDE,
            phase='will',
            target = self,
            options = self.searchOptions
        })
        
        --animate topbar
        if(ly.isDisplayObject(self.view._topbar)) then
            out_transition.y = out_transition.y +1--1 pixel correction
            transition.to( self.view._topbar, out_transition )
        end
    else
        ly.invoke(callback, self)
    end
end

local function _toggle(self, callback)
    if (self.container.showing) then
        _hide(self, function() 
            ly.invoke(callback)
        end)
    else
        _show(self, function() 
            ly.invoke(callback)
        end)
    end
end

local function _handle_topbar_touch(self, event)
    local phase = event.phase
    if (phase=='began') then
        --starttouch
    elseif(phase=='ended' or phase=='moved') then
        _toggle(self)
    end
end

--------------------------------------------------------------------------------
-- private
--------------------------------------------------------------------------------

local function _set_notification(self, value)
    if(ly.isDisplayObject(self.view)) then
        local notification = self.view._notification
        if(ly.isDisplayObject(notification)) then
            notification:setText(value)
        end
    end
end

--------------------------------------------------------------------------------
-- init
--------------------------------------------------------------------------------

local function _init_topbar(self)
    local options = self._options
    
    local group = display.newContainer( options.width, options.height )
    group.anchorChildren = true
    self.view:insert(group)
    
    local parent = self.view
    
    local topbar = display.newImage(options.barImage)
    if(topbar) then
        local ratio = ly.graphics.scaleByWidth(topbar, ly.screen.width)
        
        topbar.anchorX = 0.5
        topbar.anchorY = 0
         --[[topbar.x = ly.screen.centerX
        topbar.y = display.actualContentHeight
        parent:insert(topbar)
        ]]--
        local container = display.newContainer( topbar.width, topbar.height )
        container.anchorX = 0.5
        container.anchorY = 1.0
        container.x = ly.screen.centerX
        container.y = display.actualContentHeight + 1 --1 pixel correction
        parent:insert(container)
        
        container:insert(topbar)
        
        return container
    else
        print('IMAGE NOT FOUND: '..options.barImage)
        return display.newGroup()
    end
end

local function _init_notification(self)
    local options = self._options
    
    local topbar = self.view._topbar
    if(ly.isDisplayObject(topbar)) then
        local w, h = topbar.width, topbar.height
        
        local x = options.notificationX
        local y = options.notificationY
        
        self.view._notification = NotificationClass:new({
            text = '0',
            x = x, y= y
        })
        topbar:insert(self.view._notification.view)
        self.view._notification.view:toFront()
    end
end

local function _init(self)
    local options = self._options
    local view = self.view
    local container = self.container
    
    options.notificationX = options.notificationX or 15
    options.notificationY = options.notificationY or 5
    
    self._ani_showing = false
    self._ani_hiding = false
    
    -- bar at the top of panel
    view._topbar = _init_topbar(self)
    
    _init_notification(self)
    
    --handler
    local function on_touch_bar(event)
        _handle_topbar_touch(self, event)
        return true
    end
    local function on_tap_bar(event)
        return true
    end
    local function on_touch_view(event)
        return true
    end
    view._topbar:addEventListener('touch', on_touch_bar)
    view._topbar:addEventListener('tap', on_tap_bar)
    view:addEventListener('touch', on_touch_view)
    
    --remove handler
    local super_on_free = self.onFree
    self.onFree = function() 
        ly.invoke(super_on_free)
        if(ly.isDisplayObject(self.view)) then
            if(ly.isEventListener(self.view_topbar)) then
                self.view._topbar:removeEventListener('tap', on_tap_bar)
                self.view._topbar:removeEventListener('touch', on_touch_bar)
                self.view:removeEventListener('touch', on_touch_view)
            end
        end
    end
    
end

--------------------------------------------------------------------------------
-- constructor
--------------------------------------------------------------------------------

local ElementClass = Class(Super) --inherits Object Class

--INSTANCE FUNCTIONS
function ElementClass:initialize(options)
    options = options or {}
    
    options.backgroundColor = options.backgroundColor or BACK_COLOR
    options.strokeColor = options.strokeColor or STOKE_COLOR
    options.strokeWidth = options.strokeWidth or STROKE_WIDTH
    options.barImage = options.barImage or DEFAULT_BAR_IMAGE
    
    --calls parent Class's initialize()
    --with "self" as this instance
    if(Super.initialize(self, options)) then
        _init(self)
        return true
    else
        return false
    end
end

function ElementClass:finalize()
    if(ly.isDisplayObject(self.view)) then
        if(ly.isDisplayObject(self.view._topbar)) then
            self.view._topbar:removeSelf()
            self.view._topbar = nil
        end
    end
    --calls parent Class's initialize()
    --with "self" as this instance
    if(Super.finalize(self)) then
        
        return true
    else
        return false
    end
end

--------------------------------------------------------------------------------
-- public
--------------------------------------------------------------------------------

ElementClass.onShow = EVENT_ON_SHOW
ElementClass.onHide = EVENT_ON_HIDE

function ElementClass:show(callback)
    _show(self, callback)
end

function ElementClass:hide(callback)
    _hide(self, callback)
end

function ElementClass:toggle(callback)
    _toggle(self, callback)
end


function ElementClass:setNotification(value)
    _set_notification(self, value)
end

--------------------------------------------------------------------------------
-- export
--------------------------------------------------------------------------------

return ElementClass


--[[
Popup sliding panel.

]]--

--------------------------------------------------------------------------------
-- import
--------------------------------------------------------------------------------

local ly = require("ly.ly")

local Super = require('ly.ui.BaseControl') -- Superclass reference
local ElementClass = Class(Super) --inherits Object Class

--------------------------------------------------------------------------------
-- EVENTS
--------------------------------------------------------------------------------

local ON_COMPLETE = 'complete'

--------------------------------------------------------------------------------
-- private
--------------------------------------------------------------------------------

local function _calc_def_size(self)
    local options = self._options
    local default_width, default_height
    if ( options.location == "top" or options.location == "bottom" ) then
        default_width = display.contentWidth
        default_height = display.contentHeight * 0.33
    else
        default_width = display.contentWidth * 0.33
        default_height = display.contentHeight
    end
    
    return default_width, default_height
end

local function _calc_transition_options_in(options)
    local transition_options = {
        time = options.speed,
        transition = options.inEasing
    }
    if ( options.location == "top" ) then
        transition_options.y = display.screenOriginY + options.height
    elseif ( options.location == "bottom" ) then
        transition_options.y = display.actualContentHeight - options.height
    elseif ( options.location == "left" ) then
        transition_options.x = display.screenOriginX + options.width
    else
        transition_options.x = display.actualContentWidth - options.width
    end
    return transition_options
end

local function _calc_transition_options_out(options)
    local transition_options = {
        time = options.speed,
        transition = options.outEasing
    }
    if ( options.location == "top" ) then
        transition_options.y = display.screenOriginY
    elseif ( options.location == "bottom" ) then
        transition_options.y = display.actualContentHeight
    elseif ( options.location == "left" ) then
        transition_options.x = display.screenOriginX
    else
        transition_options.x = display.actualContentWidth
    end
    return transition_options
end

--------------------------------------------------------------------------------
-- init
--------------------------------------------------------------------------------

local function _init_container(self)
    local options = self._options
    local sceneGroup = self.view
    
    sceneGroup._container = display.newContainer( options.width, options.height )
    if ( options.location == "left" ) then
        sceneGroup._container.anchorX = 1.0
        sceneGroup._container.x = display.screenOriginX
        sceneGroup._container.anchorY = 0.5
        sceneGroup._container.y = display.contentCenterY
    elseif ( options.location == "right" ) then
        sceneGroup._container.anchorX = 0.0
        sceneGroup._container.x = display.actualContentWidth
        sceneGroup._container.anchorY = 0.5
        sceneGroup._container.y = display.contentCenterY
    elseif ( options.location == "top" ) then
        sceneGroup._container.anchorX = 0.5
        sceneGroup._container.x = display.contentCenterX
        sceneGroup._container.anchorY = 1.0
        sceneGroup._container.y = display.screenOriginY
    else
        --bottom
        sceneGroup._container.anchorX = 0.5
        sceneGroup._container.x = display.contentCenterX
        sceneGroup._container.anchorY = 0.0
        sceneGroup._container.y = display.actualContentHeight
    end
    self.container = sceneGroup._container
    
    local background = display.newRect( 0, 0, options.width, options.height)
    background:setFillColor(unpack(options.backgroundColor))
    background.strokeWidth = options.strokeWidth
    background.stroke = options.strokeColor
    sceneGroup._container:insert(background)
    
    sceneGroup:insert(sceneGroup._container)
end

local function _init(self)
    local self_instance = self
    if (nil==display.contentHeight) then
        return
    end
    
    local default_width, default_height = _calc_def_size(self)
    local options = self._options
    
    options.location = options.location or "top"
    options.width = options.width or default_width
    options.height = options.height or default_height
    options.speed = options.speed or 500
    options.inEasing = options.inEasing or easing.linear
    options.outEasing = options.outEasing or easing.linear
    
    -- Vector options
    options.cornerRadius = options.cornerRadius or 0
    options.strokeWidth = options.strokeWidth or 2
    options.strokeColor = options.strokeColor or {1, 1, 1}
    options.backgroundColor = options.backgroundColor or { 1, 1, 1 }
    
    --add animation to instance
    self.transitionIn = _calc_transition_options_in(options)
    self.transitionOut = _calc_transition_options_out(options)
    
    local sceneGroup = self.view
    
    _init_container(self)
    
    sceneGroup.height = sceneGroup._container.height
    sceneGroup.width = sceneGroup._container.width
    
    function sceneGroup._container:show(callback)
        if (nil==display.contentHeight) then
            return
        end
        
        self.showing = true
        
        local transition_options = ly.defaults({}, _calc_transition_options_in(options))
        
        --callback and event
        transition_options.onComplete = function(event) 
            event.name = ON_COMPLETE
            event.target = self_instance
            self_instance:dispatchEvent(event)
            ly.invoke(callback)
        end
        self.completeState = "shown"
        
        
        transition.to( self, transition_options )
    end
    
    function sceneGroup._container:hide(callback)
        if (nil==display.contentHeight) then
            return
        end
        
        self.showing = false
        
        local transition_options = ly.defaults({}, _calc_transition_options_out(options))
        
        --callback and event
        transition_options.onComplete = function(event) 
            event.name = ON_COMPLETE
            event.target = self_instance
            self_instance:dispatchEvent(event)
            ly.invoke(callback)
        end
        self.completeState = "hidden"
        
        transition.to( self, transition_options )
    end
    
    function sceneGroup._container:toggle(callback)
        if (self.showing) then
            self:hide(callback)
        else
            self:show(callback)
        end
    end
    
    -- handlers
    local function on_tap_touch(event)
        return true
    end
    if(ly.isEventListener(sceneGroup._container)) then
        sceneGroup._container:addEventListener('tap', on_tap_touch)
        sceneGroup._container:addEventListener('touch', on_tap_touch)
    end
    
    local super_on_free = self.onFree
    self.onFree = function()
        ly.invoke(super_on_free)
        if(ly.isDisplayObject(self.view)) then
            if(ly.isEventListener(self.view._container)) then
                self.view._container:removeEventListener('tap', on_tap_touch)
                self.view._container:removeEventListener('touch', on_tap_touch)
            end
        end
    end
end

--------------------------------------------------------------------------------
-- constructor
--------------------------------------------------------------------------------

function ElementClass:initialize(options)
    --calls parent Class's initialize()
    --with "self" as this instance
    if(Super.initialize(self, options)) then
        _init(self, options)
        
        self.height = self.view.height
        self.width = self.view.width
        return true
    else
        return false
    end
end

function ElementClass:finalize()
    --calls parent Class's initialize()
    --with "self" as this instance
    if(Super.finalize(self)) then
        
        return true
    else
        return false
    end
end

--------------------------------------------------------------------------------
-- public
--------------------------------------------------------------------------------

ElementClass.ON_COMPLETE = ON_COMPLETE
ElementClass.onComplete = ON_COMPLETE

ElementClass.container = nil --assigned when container is build

function ElementClass:insert(displayObject)
    if(self.view and self.view._container) then
        self.view._container:insert(displayObject) 
    end
end

function ElementClass:show(callback)
    self.view._container:show(callback)
end

function ElementClass:hide(callback)
    return self.view._container:hide(callback)
end

function ElementClass:toggle(callback)
    return self.view._container:toggle(callback)
end
--------------------------------------------------------------------------------
-- export
--------------------------------------------------------------------------------

return ElementClass
--ly.ui.elements.panel.BarPanelPicture
--------------------------------------------------------------------------------
-- import
--------------------------------------------------------------------------------

local ly = require('ly.ly')
local app = require('ly.lyApp')

local Super = require('ly.ui.elements.panel.BarPanel') -- Superclass reference

local ButtonClass = require('ly.ui.elements.button.Button')
local PicturePickerClass = require('ly.components.picturepicker.PicturePicker')

--------------------------------------------------------------------------------
-- events
--------------------------------------------------------------------------------

local EVENT_ON_PICTURE = 'gotpicture'

--------------------------------------------------------------------------------
-- constants
--------------------------------------------------------------------------------

local IMG_TOPBAR = 'ly/ui/elements/panel/bar_panel_photo.png'

local STOKE_COLOR = app.theme.backgroundColor or ly.color.packed.fromRGB(37, 197, 255)
local STROKE_WIDTH = 6
local BACK_COLOR = {0.33,0.33,0.33, 0.8}

local I18N_TAKE_PICTURE = app.i18n:get('TAKE_PICTURE') or 'Take Picture'
local I18N_SELECT_PICTURE = app.i18n:get('SELECT_PICTURE') or 'Select Picture'
local FONT = native.systemFontBold
local FONT_SIZE = 15
local FONT_COLOR = {1,1,1}
local BUTTON_COLOR = app.theme.backgroundColor or STOKE_COLOR

--------------------------------------------------------------------------------
-- trigger
--------------------------------------------------------------------------------

local function _do_on_picture(self, event)
    self:dispatchEvent(event)
end

--------------------------------------------------------------------------------
-- private
--------------------------------------------------------------------------------

local function _handle_on_take(self, event)
    if (self._picker) then
        self._picker:takePicture()
    end
end

local function _handle_on_select(self, event)
    if (self._picker) then
        self._picker:selectPicture()
    end
end

local function _handle_on_hide(self, event)
    if (event.phase=='did') then
        
    end
end

--------------------------------------------------------------------------------
-- init
--------------------------------------------------------------------------------

local function _init_buttons(self)
    local options = self._options
    
    local parent = self.container
    local view = self.view
    
    view._btn_take = ButtonClass:new({
        width = display.contentWidth-30,
        height = 60,
        cornerRadius = 6,
        backgroundColor = options.buttonColor,
        strokeWidth = 1,
        strokeColor = {1, 1, 1, 0.6},
        caption = options.captionTake,
        fontSize = options.fontSize,
        font = options.font,
        fontColor = options.fontColor
    })
    view._btn_take:setPos( 0, -60)
    parent:insert(view._btn_take.view)
    
    view._btn_select = ButtonClass:new({
        width = display.contentWidth-30,
        height = 60,
        cornerRadius = 6,
        backgroundColor = options.buttonColor,
        strokeWidth = 1,
        strokeColor = {1, 1, 1, 0.6},
        caption = options.captionSelect,
        fontSize = options.fontSize,
        font = options.font,
        fontColor = options.fontColor
    })
    view._btn_select:setPos( 0, 30)
    parent:insert(view._btn_select.view)
end

local function _init(self)
    local options = self._options
    
    local view = self.view
    local container = self.container
    
    self._picker = PicturePickerClass:new({})
    
    -- inputs
    _init_buttons(self)
    
    --handler
    local function on_hide(event)
        _handle_on_hide(self, event)
        return true
    end
    local function on_take(event)
        Super.hide(self, function() 
            _handle_on_take(self, event)
        end)
        return true
    end
    local function on_select(event)
        Super.hide(self, function() 
            _handle_on_select(self, event)
        end)
        return true
    end
    local function on_picture(event)
        _do_on_picture(self, event)
        return true
    end
    self:addEventListener('hide', on_hide)
    self.view._btn_select:addEventListener('tap', on_select)
    self.view._btn_take:addEventListener('tap', on_take)
    self._picker:addEventListener(EVENT_ON_PICTURE, on_picture)
    
    --remove handler
    local super_on_free = self.onFree
    self.onFree = function()
        ly.invoke(super_on_free)
        if (self and self.view) then
            if(self.view) then
                self:removeEventListener('hide', on_hide)
            end
            if(self.view._btn_select) then
                self.view._btn_select:removeEventListener('tap', on_select)
            end
            if(self.view._btn_take) then
                self.view._btn_take:removeEventListener('tap', on_take)
            end
            if(ly.isEventListener(self._picker)) then
                self._picker:removeEventListener(EVENT_ON_PICTURE, on_picture)
            end
        end
    end
end

--------------------------------------------------------------------------------
-- constructor
--------------------------------------------------------------------------------

local BaseClass = Class(Super) --inherits Object Class

--INSTANCE FUNCTIONS
function BaseClass:initialize(options)
    
    --super properties
    options = options or {}
    options.backgroundColor = BACK_COLOR
    options.strokeColor = STOKE_COLOR
    options.strokeWidth = STROKE_WIDTH
    options.barImage = options.barImage or IMG_TOPBAR
    
    --extended properties
    options.captionSelect = options.captionSelect or I18N_SELECT_PICTURE
    options.captionTake = options.captionTake or I18N_TAKE_PICTURE
    options.font = options.font or FONT
    options.fontSize = options.fontSize or FONT_SIZE
    options.fontColor = options.fontColor or FONT_COLOR
    options.buttonColor = options.buttonColor or BUTTON_COLOR
    
    if(Super.initialize(self, options)) then
        _init(self)
        return true
    else
        return false
    end
end

function BaseClass:finalize()
    if(self.view) then
        if(self.view._switch) then
            self.view._switch:free()
            self.view._switch = nil
        end
        if(self.view._btn_take) then
            self.view._btn_take:free()
            self.view._btn_take = nil
        end
        if(self.view._btn_select) then
            self.view._btn_select:free()
            self.view._btn_select = nil
        end
    end
    --calls parent Class's initialize()
    --with "self" as this instance
    if(Super.finalize(self)) then
        
        return true
    else
        return false
    end
end

--------------------------------------------------------------------------------
-- public
--------------------------------------------------------------------------------

BaseClass.onPicture = EVENT_ON_PICTURE

--------------------------------------------------------------------------------
-- export
--------------------------------------------------------------------------------

return BaseClass


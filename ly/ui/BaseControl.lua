local CLASS_NAME = 'ly.ui.BaseControl'

--------------------------------------------------------------------------------
-- import
--------------------------------------------------------------------------------
local ly = require('ly.ly')

local Super = require('ly.lang.Object') -- Superclass reference

--------------------------------------------------------------------------------
-- const
--------------------------------------------------------------------------------



--------------------------------------------------------------------------------
-- private
--------------------------------------------------------------------------------

local function _handle_event(self, event)
    if (event.name=='finalize') then
        self:free()
        return true
    elseif(event.name=='resize') then
        --Super._viewRefresh(self)
    end
end

local function _init(self, options)
    self._id = ly.guid.generate()
    --print('conponent id: ' .. self._id) 
    
    -- store options
    self._options = options or {}
    self.visible = true
    
    -- handlers
    local on_finalize = function(event) 
        local target = event.target
        if (event.name=='finalize') then
            if(self._finalized==false and ly.isDisplayObject(self.view) ) then
                -- dont use: crash for infinite loop
                Super.free(self)
            end
        end
    end
    
    local on_resize = function(event) 
        if(event.name=='resize') then
            self:_viewRefresh()
        end
    end
    
    self.view = options.view or display.newGroup()
    --self.view.anchorChildren = true
    self.view.element = self
    self.view:addEventListener('finalize', on_finalize)
    self.view:addEventListener('resize', on_resize)
    
    local super_on_free = self.onFree
    self.onFree = function()
        ly.invoke(super_on_free)
        if(ly.isDisplayObject(self.view) ) then
            self.view:removeEventListener('finalize', on_finalize)
            self.view:removeEventListener('resize', on_resize)
        end
    end
end


--------------------------------------------------------------------------------
-- constructor
--------------------------------------------------------------------------------

local BaseClass = Class(Super) --inherits Object Class

function BaseClass:initialize(options)
    --calls parent Class's initialize()
    --with "self" as this instance
    if (Super.initialize(self, options)) then
        self._className = CLASS_NAME
        _init(self, options)
        
        self.height = self.view.height
        self.width = self.view.width
        
        return true
    else
        return false
    end
end

function BaseClass:finalize()
    --calls parent Class's finalize()
    --with "self" as this instance
    if(Super.finalize(self)) then
        -- clear internal fields
        self._options = nil
        if (self.view) then
            self.view.element = nil
        end
        self.view = nil 
        
        return true
    else
        return false
    end
end

--------------------------------------------------------------------------------
-- override
--------------------------------------------------------------------------------

function BaseClass:toString()
    return Super.toString(self)
end

--------------------------------------------------------------------------------
-- abstract
--------------------------------------------------------------------------------

function BaseClass:_viewRefresh()
    --[[
    Implement this method to intercept when internal view changes size 
    or position and may need a reposition of native internal components
    --]]
end

--------------------------------------------------------------------------------
-- public
--------------------------------------------------------------------------------

BaseClass.view = nil --created on initialization

function BaseClass:getId()
    return self._id
end

function BaseClass:addEventListener(eventName, listener)
    if(ly.isDisplayObject(self.view)) then
        if(ly.isNotEmpty(eventName))then
            if (ly.isFunction(listener)) then
                self.view:addEventListener(eventName, listener)
            else
                print('['..self._className..'] '..'LISTENER cannot be nil')
            end
        else
            print('['..self._className..'] '..'WARNING: called "addEventListener" with empty eventName!')
        end 
    end
end

function BaseClass:removeEventListener( eventName, listener )
    if(ly.isEventListener(self.view)) then
        if (ly.isFunction(listener)) then
            self.view:removeEventListener(eventName, listener)
        else
            print('['..self._className..'] '..'LISTENER cannot be nil')
        end
    end
end

function BaseClass:dispatchEvent( event )
    if (self.view) then
        self.view:dispatchEvent( event )
    end
end

function BaseClass:getView()
    return self.view 
end

function BaseClass:setX(value)
    self.view.x = value 
    self:_viewRefresh()
end

function BaseClass:getX()
    return self.view.x
end

function BaseClass:setY(value)
    self.view.y = value
    self:_viewRefresh()
end

function BaseClass:getY()
    return self.view.y
end

function BaseClass:setPos(x, y)
    self.view.x = x 
    self.view.y = y 
    self:_viewRefresh()
end

function BaseClass:toFront()
    if (ly.isDisplayObject(self.view)) then
        self.view:toFront()
    end
end

function BaseClass:toBack()
    if (ly.isDisplayObject(self.view)) then
        self.view:toBack()
    end
end

function BaseClass:insert(displayObject)
    if (ly.isDisplayObject(self.view)) then
        if (ly.isDisplayObject(displayObject)) then
            self.view:insert(displayObject)
        elseif(ly.isLyObject(displayObject)) then
            self.view:insert(displayObject.view)
        end
    end
end

function BaseClass:show(animate)
    if (ly.isDisplayObject(self.view)) then
        self:toggleVisible(true, ly.toBoolean(animate, true))
    end
end

function BaseClass:hide(animate)
    if (ly.isDisplayObject(self.view)) then
        self:toggleVisible(false, ly.toBoolean(animate, true))
    end
end

function BaseClass:toggleVisible(value, animate)
    animate = ly.toBoolean(animate, true)
    if (ly.isDisplayObject(self.view)) then
        self.visible = value
        local alpha = ly.conditional(value, 1, 0)
        if (animate) then
            transition.to(self.view, {time=500, alpha=alpha})
        else
            self.view.alpha = alpha
        end
        
    end
end

--------------------------------------------------------------------------------
-- export
-------------------------------------------------------------------------------- 

return BaseClass
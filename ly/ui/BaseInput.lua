local CLASS_NAME = 'ly.ui.BaseInput'

--------------------------------------------------------------------------------
-- import
--------------------------------------------------------------------------------

local ly = require("ly.ly")

local Super = require('ly.ui.BaseControl') -- Superclass reference

--------------------------------------------------------------------------------
-- events
--------------------------------------------------------------------------------

local ON_CHANGED = 'changed'

--------------------------------------------------------------------------------
-- private
--------------------------------------------------------------------------------

local function _do_changed(self, value, old_value, triggerEvent)
    if(old_value~=value)then
        if(triggerEvent) then
            self:dispatchEvent({
                name=ON_CHANGED,
                target=self,
                value = value,
                old_value = old_value
            })
        end
        return true
    else
        return false --no change
    end
end

local function _get_value(self)
    local options = self._options or {}
    local def_val = ly.toValue(options.value, false)
    if(options.model and options.id) then
        return ly.getValue(options.model, options.id, def_val)
    else
        return def_val
    end
end

local function _set_value(self, value, triggerEvent)
    local options = self._options or {}
    local old_value = _get_value(self)
    --event
    if(_do_changed(self, value, old_value, triggerEvent)) then
        ly.setValue(options.model, options.id, value, true)
    end
end

local function _init(self)
    local options = self._options or {}
    
    --data binding
    options.id = options.id or '' --may be ID of an external model for data binding
    options.model = options.model or {} --data model to bind
    options.value = ly.toValue(options.value, _get_value(self))
    
    --update model if value is different
    _set_value(self, options.value, false)
    
    local super_on_free = self.onFree
    self.onFree = function()
        ly.invoke(super_on_free)
    end
end



--------------------------------------------------------------------------------
-- constructor
--------------------------------------------------------------------------------

local ElementClass = Class(Super) --inherits Object Class

function ElementClass:initialize(options)
    if(Super.initialize(self, options)) then
        self._className = CLASS_NAME
        _init(self, options)
        
        self.height = self.view.height
        self.width = self.view.width
        
        return true
    else
        return false
    end
end

function ElementClass:finalize()
    --calls parent Class's initialize()
    --with "self" as this instance
    if (Super.finalize(self)) then
        if (nil~=self.view) then
            print('WARNING: Probable Memory Leak issue.') 
        end
        return true
    else
        return false
    end
    
end

--------------------------------------------------------------------------------
-- public
--------------------------------------------------------------------------------

ElementClass.onChanged = ON_CHANGED

function ElementClass:setValue(value, triggerEvent)
    triggerEvent = ly.toBoolean(triggerEvent, true)
    _set_value(self, value, triggerEvent)
end

---Returns model value.
-- @return Model value

function ElementClass:getValue()
    return _get_value(self)
end

--------------------------------------------------------------------------------
-- export
--------------------------------------------------------------------------------

return ElementClass
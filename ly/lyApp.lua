
--------------------------------------------------------------------------------
-- import
--------------------------------------------------------------------------------

local composer = require('composer')
local widget = require('widget')

local ly = require('ly.ly')

local HashList = require('ly.classes.collections.HashList')

local AppRuntime = require('ly.components.appruntime.AppRuntime')

--------------------------------------------------------------------------------
-- static constructor
--------------------------------------------------------------------------------

local M = {
    _initialized = false,
    _scenes = {},
    
    -- default theme property
    theme={}, 
    
    -- default settings property
    settings={
        enable_audio = true, -- audio ON
    }, 
    
    -- CONST
    const={},
    
    --LOCATION
    location = {
        latitude = 0,
        longitude = 0,
        altitude = 0,
        accuracy = 0,
        speed = 0,
        direction = 0,
        time = 0,
    },
}

--------------------------------------------------------------------------------
-- static override
--------------------------------------------------------------------------------

--
-- turn on debugging
--
ly.logger.enablePrint(true)
ly.logger.setLevel(ly.logger.LEVEL_DEBUG)

--
-- this little GLOBAL snippet will make a copy of the print function
-- and now will only print if debugMode is true
-- quick way to clean up your logging for production
--



--------------------------------------------------------------------------------
-- fields
--------------------------------------------------------------------------------

local _timer_id = {}
local _splashGroup = display.newGroup()
local _scene_history

--------------------------------------------------------------------------------
-- private
--------------------------------------------------------------------------------

local function check_ready(event)
    -- wait gui is initialized
    --[[
    if (M.gui._initialized==false) then
        return
    end
    ]]--
    
    -- remove timer
    timer.cancel(event.source); _timer_id=nil
    
    -- initialized flag
    M._initialized = true
    
    -- callback method
    local callback = event.source.params.callback
    if callback then
        callback()
    end
    
    print('Application Main controller initialized')
end

--------------------------------------------------------------------------------
-- private SPLASH
--------------------------------------------------------------------------------

local function _splashOpen(options)
    if (nil~=_splashGroup) then
        display.remove(_splashGroup)
        _splashGroup = nil
    end
    
    local opts = options or {}
    opts.image = opts.image or "Splash.png"
    opts.content = opts.content or nil -- optional display content
    
    _splashGroup = display.newGroup()
    
    if (nil~=opts.content) then
        -- uses this custom splash (may be animated)
        _splashGroup:insert(opts.content)
    else
        -- creates splash
        local background = display.newRect(0,0, display.contentWidth, display.contentHeight)
        background:setFillColor( 1, 1, 1 )
        background.x = display.contentCenterX
        background.y = display.contentCenterY
        
        _splashGroup:insert(background)
        
        if (opts.image) then
            local logo = display.newImageRect(opts.image, display.contentWidth, display.contentHeight)
            logo.x = display.contentCenterX
            logo.y = display.contentCenterY
            
            _splashGroup:insert(logo)
        end 
    end
    
    
end

local function _splashClose()
    transition.to(_splashGroup, {
        time=250, 
        alpha=0, -- fade out
        onComplete=function ()
            display.remove(_splashGroup)
            _splashGroup = nil
        end
    })
end

--------------------------------------------------------------------------------
-- THEME
--------------------------------------------------------------------------------

local function _theme_set_wallpaper(view, options)
    options = options or {}
    options.baseDir = options.baseDir or system.ResourceDirectory
    options.filename = options.filename or M.theme.wallpaper
    options.width = options.width or display.contentWidth
    options.height = options.height or display.contentHeight
    options.classname = options.classname or M.theme.wallpaperClass -- object wallpaper
    
    if(ly.isDisplayObject(view)) then
        if(ly.isNotEmpty(options.classname)) then
            -- OBJECT WALLPAPER
            local BackgroundClass = require(options.classname)
            if(ly.isNotNull(BackgroundClass))then
                local background = BackgroundClass:new(options)
                view:insert(background.view)
                return background
            else
                return nil
            end
        elseif(ly.isNotEmpty(options.filename)) then
            -- IMAGE WALLPAPER
            local background = display.newImage(options.filename, options.baseDir)
            if(ly.isNotNull(background)) then
                background.width = options.width
                background.height = options.height
                background.x = options.width / 2
                background.y = options.height / 2
                view:insert(background)
                return background
            else
                return nil
            end
        else
            return nil
        end
    else
        return nil
    end
end

--------------------------------------------------------------------------------
-- SCENE
--------------------------------------------------------------------------------

local DEFAULT_EFFECT = 'crossFade'
local DEFAULT_TIME = 250
local DEFAULT_PARAMS = {}

local PARAMS = 'params'
local DIRECTION = 'direction'
local DIRECTION_IN_PREFIX = 'in_'
local DIRECTION_OUT_PREFIX = 'out_'

local IN_EFFECT = DIRECTION_IN_PREFIX..'effect'
local IN_TIME = DIRECTION_IN_PREFIX..'time'
local OUT_EFFECT = DIRECTION_OUT_PREFIX..'effect'
local OUT_TIME = DIRECTION_OUT_PREFIX..'time'


local function _getOptions(key, direction, options)
    local result = {}
    local sceneOptions = ly.clone(M._scenes[key])
    
    result.name = sceneOptions.name
    result.direction = direction
    if (direction=='in') then
        --IN
        result.time = options.time or sceneOptions[IN_TIME] or DEFAULT_TIME
        result.effect = options.effect or sceneOptions[IN_EFFECT] or DEFAULT_EFFECT
        result.params = ly.merge(options.params, sceneOptions[PARAMS], true) --passed in event
        
        --referer (params refer can be overwritten from user input params)
        result.referer = options.referer or key
        result.params.referer = result.params.referer or result.referer
    else
        --OUT
        result.time = options.time or sceneOptions[OUT_TIME] or DEFAULT_TIME
        result.effect = options.effect or sceneOptions[OUT_EFFECT] or DEFAULT_EFFECT
        result.params = ly.merge(options.params, sceneOptions[PARAMS], true) --passed in event
        
        --referer (params refer can be overwritten from user input params)
        result.referer = key
        result.params.referer = result.params.referer or result.referer
    end 
    return result
end

local function _scene_set_home(key, override)
    if(M and M.scene) then
        if (ly.isEmpty(M.scene.home) or override) then
            M.scene.home = key
        end
    end
end

local function _scene_set_current(key)
    if(M and M.scene) then
        M.scene.current = key
    end
end

local function _scene_register(key, optionsOrName)
    local options = {}
    if (ly.isString(optionsOrName)) then
        options['name'] = optionsOrName  
        options[PARAMS] = DEFAULT_PARAMS
        options[IN_EFFECT] = DEFAULT_EFFECT
        options[IN_TIME] = DEFAULT_TIME
        options[OUT_EFFECT] = DEFAULT_EFFECT
        options[OUT_TIME] = DEFAULT_TIME
    else
        options['name'] = optionsOrName['name']
        options[PARAMS] = optionsOrName[PARAMS] or DEFAULT_PARAMS
        options[IN_EFFECT] = optionsOrName[IN_EFFECT] or DEFAULT_EFFECT
        options[IN_TIME] = optionsOrName[IN_TIME] or DEFAULT_TIME
        options[OUT_EFFECT] = optionsOrName[OUT_EFFECT] or DEFAULT_EFFECT
        options[OUT_TIME] = optionsOrName[OUT_TIME] or DEFAULT_TIME
    end
    
    -- add 
    M._scenes[key] = options
    
    -- add key to retrieve by short name
    local name_tokens = ly.str.split(options['name'], '.')
    local short_name = name_tokens[#name_tokens]
    M.scene.keys[short_name] = key
    
    --set home scene
    _scene_set_home(key, false)
end

local function _scene_unregister(key)
    M._scenes[key]=nil  
end

local function _scene_overlay_hide(recycleOnly, effect, time, callback)
    if(ly.isNull(recycleOnly) and ly.isNotNull(effect)) then
        composer.hideOverlay()
    else
        composer.hideOverlay(recycleOnly, effect, time)
    end
    
    if(ly.isFunction(callback)) then
        --is there something to hide?
        if(ly.isNull(M.scene.last_overlay)) then
            ly.invoke(callback)
        else
            -- set time
            time = ly.toNumber(time, 500)
            time = time + 100 -- little more time
            ly.delay(function() 
                M.scene.last_overlay = nil
                ly.invoke(callback)
            end, time)
        end
    else
        ly.delay(function() 
            M.scene.last_overlay = nil
        end, 100)
    end
end

local function _scene_overlay_show(sceneName, options)
    M.scene.last_overlay = sceneName
    composer.showOverlay(sceneName, options)
end

local function _scene_overlay(key, options)
    if(ly.isNotEmpty(key)) then
        options = options or {}
        
        local direction = options[DIRECTION] or 'in'
        local opts = _getOptions(key, direction, options)
        local sceneName = opts['name']
        
        if (nil ~= sceneName) then
            
            if(direction=='in') then
                local out_opts = _getOptions(key, 'out', {})
                --SHOW
                --hide previous
                local effect = 'slideRight'
                local time = 500
                if(ly.isNotNull(out_opts)) then
                    effect = out_opts.effect or effect
                    time = out_opts.time or time
                end
                _scene_overlay_hide(false, effect, time, function() 
                    --show new overlay
                    opts.isModal = true -- force modal
                    
                    _scene_overlay_show(sceneName, opts)
                end)
            else
                -- HIDE
                local effect = 'slideRight'
                local time = 500
                if(ly.isNotNull(opts)) then
                    effect = opts.effect or effect
                    time = opts.time or time
                end
                _scene_overlay_hide(false, effect, time) 
            end
            
            -- check for callback
            if (opts.params and opts.params.onReady) then
                ly.invoke(opts.params.onReady)
            end
            
            return true
        else
            if (direction=='out') then
                _scene_overlay_hide()
                return true 
            else
                print("Scene '"..key.."' not registerd. Please, call 'registerScene('key','packageName') to register a scene.'")
                return false 
            end
            
        end
    else
        _scene_overlay_hide()
        return true 
    end
end

local function _scene_overlayOn(key, options)
    options = options or {}
    options[DIRECTION] = 'in'
    _scene_overlay(key, options)
end

local function _scene_overlayOff(key, options)
    options = options or {}
    options[DIRECTION] = 'out'
    _scene_overlay(key, options)
end

local function _scene_goto(key, options)
    options = options or {}
    
    --remove hidden pages
    composer.removeHidden()
    
    local direction = options[DIRECTION] or 'in'
    local opts = _getOptions(key, direction, options)
    local sceneName = opts['name']
    if (nil ~= sceneName) then
        
        -- add to scene history
        if(_scene_history)then
            _scene_history:tryAdd(opts, key) 
        end
        -- transition to new scene
        composer.gotoScene(sceneName, opts)
        
        _scene_set_current(key)
        
        -- check for callback
        if (opts.params and opts.params.onReady) then
            ly.invoke(opts.params.onReady)
        end
        
        return true
    else
        ly.logger.warn("Scene '"..key.."' not registerd. Please, call 'registerScene('key','packageName') to register a scene.'")
        return false 
    end
end

local function _scene_back(opts)
    if(_scene_history)then
        opts = opts or {}
        --remove current
        local curr_opts, curr_key, curr_index = _scene_history:pop() 
        --remove previous
        local options, key, index = _scene_history:pop() 
        if (key) then
            
            -- get options from registerd scenes
            options = _getOptions(curr_key, 'out', opts)
            
            _scene_goto(key, options)
        elseif(ly.isNotEmpty(M.scene.home)) then
            options = _getOptions(M.scene.home, 'in', opts)
            _scene_goto(M.scene.home, options)
        end
    else
        print('WARNING: history not enabled')
    end
end

local function _scene_gotoAsync(key, opts)
    local options = opts or {}
    ly.delay(_scene_goto, 100, key, options)
end

local function _scene_backAsync(opts)
    ly.delay(_scene_back, 100, opts)
end

local function _scene_clearHistory()
    if(_scene_history)then
        _scene_history:clear() 
    end 
end
--------------------------------------------------------------------------------
-- private THEMES
--------------------------------------------------------------------------------

local function _initTheme(themeOptions)
    M.theme = M.theme or {}
    ly.merge(themeOptions, M.theme, true)
    
    local platform = system.getInfo("platformName")
    
    if (M.theme.native and platform == "Android") then
        M.theme.name = M.theme.name or "widget_theme_android"
        M.theme.font = M.theme.font or "Droid Sans"
        M.theme.fontBold = M.theme.fontBold or "Droid Sans Bold"
        M.theme.fontItalic = M.theme.fontItalic or "Droid Sans"
        M.theme.fontBoldItalic = M.theme.fontBoldItalic or "Droid Sans Bold"
    else
        M.theme.name = M.theme.name or "widget_theme_ios7"
        local coronaBuild = system.getInfo("build")
        if tonumber(coronaBuild:sub(6,12)) < 1206 then
            M.theme.name = M.theme.name or "widget_theme_ios"
        end
        M.theme.font = M.theme.font or "HelveticaNeue-Light"
        M.theme.fontBold = M.theme.fontBold or "HelveticaNeue"
        M.theme.fontItalic = M.theme.fontItalic or "HelveticaNeue-LightItalic"
        M.theme.fontBoldItalic = M.theme.fontBoldItalic or "Helvetica-BoldItalic"
    end
    
    -- set default widget theme
    widget.setTheme(M.theme.name)
end

local function _initSceneHistory()
    _scene_history = HashList:new()
end

--------------------------------------------------------------------------------
-- private LOCATION
--------------------------------------------------------------------------------

local function _handle_location(event)
    if (M and M.location) then
        M.location.latitude = event.latitude
        M.location.longitude = event.longitude
        M.location.altitude = event.altitude
        M.location.accuracy = event.accuracy
        M.location.speed = event.speed
        M.location.direction = event.direction
        M.location.time = event.time
    end
end

local function _init_location(enabled)
    if (enabled) then
        Runtime:addEventListener( "location", _handle_location)
    else
        Runtime:removeEventListener( "location", _handle_location) 
    end
end

--------------------------------------------------------------------------------
-- private SERVICES
--------------------------------------------------------------------------------

local function _service_register(name, serviceController)
    if(M.service) then
        M.service._registered = M.service._registered or {}
        M.service._registered[name] = serviceController
    end
end

local function _service_invoke(name, method, options, callback, callback_error)
    local service = M.service._registered[name]
    if(service) then
        if (ly.isFunction(service[method])) then
            service[method](options, function(err, response) 
                if(err) then
                    --ERROR
                    if(not ly.isFunction(callback_error)) then
                        local last_invoke = {
                            name = name,
                            method = method,
                            options = options,
                            callback = callback
                        }
                        -- call overlay view
                        M.error.overlay({
                            err = err,
                            service = last_invoke
                        })
                    else
                        -- invoke callback error for custom actions
                        ly.invoke(callback_error, err)
                    end
                end
                ly.invoke(callback, err, response)
            end)
        else
            --method not found
            if(not ly.isFunction(callback_error)) then
                ly.invoke(callback, 'Mthod not found: "'..method..'"')
            else
                ly.invoke(callback_error, 'Method not found: "'..method..'"')
            end
        end
    else
        --service not declared
        if(not ly.isFunction(callback_error)) then
            ly.invoke(callback, 'Service not declare: "'..name..'"')
        else
            ly.invoke(callback_error, 'Service not declare: "'..name..'"')
        end
    end
end

--------------------------------------------------------------------------------
-- private ERROR
--------------------------------------------------------------------------------

local function _error_page(name)
    if(M.error) then
        M.error._error_view = name
    end 
end

local function _error_overlay(params, page)
    if(M.error) then
        local error_view = M.error._error_view
        page = page or error_view
        if (page) then
            --invoke overlay
            _scene_overlayOn(page, {
                params = params
            })
        end
    end 
end

--------------------------------------------------------------------------------
-- private INIT
--------------------------------------------------------------------------------

local function _init(func_callback, options)
    if M._initialized then
        return
    end
    
    options=options or {}
    
    -- theme
    _initTheme(options.theme)
    
    --location
    _init_location(not not options.location)
    
    -- scene history manager
    if (options.scene_history) then
        _initSceneHistory() 
    end
    
    -- async callback when ready
    _timer_id = timer.performWithDelay(1000, check_ready, 0)
    _timer_id.params = {callback=func_callback}
end

--------------------------------------------------------------------------------
-- public
--------------------------------------------------------------------------------

-- bootstrap
M.init = _init

-- splash
M.splashOpen = _splashOpen
M.splashClose = _splashClose

-- THEME
M.theme = {}
M.theme.wallpaper = '' --image to use as default wallpaper
M.theme.setWallpaper = _theme_set_wallpaper

-- SCENE
M.scene = {}
M.scene.keys = {} --contain keys mapped by name. i.e. names['page-01']='01.00'
M.scene.home = nil
M.scene.current = ''
M.scene.last_overlay = nil
M.scene.register = _scene_register
M.scene.unregister = _scene_unregister
M.scene.overlayOn = _scene_overlayOn
M.scene.overlayOff = _scene_overlayOff
M.scene.goto = _scene_goto
M.scene.gotoAsync = _scene_gotoAsync
M.scene.back = _scene_back
M.scene.backAsync = _scene_backAsync
M.scene.clearHistory = _scene_clearHistory

-- SERVICE
-- all service's methods must accept 2 parameters: 
-- options (table), callback (function)
M.service = {}
M.service.register = _service_register
M.service.invoke = _service_invoke

-- ERROR
-- 
M.error = {}
M.error.page = _error_page
M.error.overlay = _error_overlay

-- APP RUNTIME
-- 
M.runtime = AppRuntime:new({}) -- use app.runtime:setOptions(options) to set custom options

-- GLOBAL
-- usage: app.global('sample') = 'hello sample data in global repo'
M.global = function(id) 
    id = id or 'global'
    M._global = M._global or {}
    M._global[id] = M._global[id] or {}
    return M._global[id]
end

--------------------------------------------------------------------------------
-- export
--------------------------------------------------------------------------------

return M



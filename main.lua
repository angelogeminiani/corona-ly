
--------------------------------------------------------------------------------
-- import
--------------------------------------------------------------------------------
local ly = require('ly.ly')
local ObjectId = require('ly.lang.ObjectId')

local LocalStorage = require('ly.classes.storage.LocalStorage')
local I18nManager = require('ly.classes.i18n.I18nManager')
local HashList = require('ly.classes.collections.HashList')

local app = require('ly.lyApp')

--------------------------------------------------------------------------------
-- methods
--------------------------------------------------------------------------------

local function _test_HashList()
    local list = HashList:new()
    
    print('List Count:', list.count)
    
    --add
    list:add({name='hello'})
    list:add({name='world'})
    list:add({name='!!!'}, 'dots')
    list:add({name='four'}, 'FOUR')
    local status, response = list:tryAdd({name='...'}, 'dots')
    assert(not status)
    
    print('List Count:', list.count)
    
    -- get
    local item = list:get(1)
    print('Item At #1', item.name)
    print('Item At #2', list:get(2).name)
    print('Item At #3', list:get(3).name)
    print('Item with key "dots"', list:get('dots').name)
    
    -- remove
    print('Removed', '"'..list:remove(2).name..'"')
    print('List Count:', list.count)
    assert(list.count==3)
    print('Removed', '"'..list:remove('dots').name..'"')
    print('List Count:', list.count)
    assert(list.count==2)
    list:remove(20) --nil
    assert(list.count==2)
    
    --add
    list:add({name='yes, it works!'})
    assert(list.count==3)
    print('List Count:', list.count)
    
    --pop (remove last item in the list)
    print('Removed', '"'..list:pop().name..'"')
    assert(list.count==2)
    
    print(list:toString())
end

local function on_ready()
    
    --test object id
    local id = ObjectId:new()
    print(id:toString())
    id:free()
    id = nil
    
    --test hashlist
    _test_HashList()
    
    -- test dir command
    local files = ly.fs.list(system.pathForFile( "", system.DocumentsDirectory))
    print('Files in '..system.pathForFile( "", system.DocumentsDirectory),ly.countPairs(files))
    
    print('"lang" was: '..app.storage:getString('i18n.lang', ''))
    app.storage:setValue('i18n.lang', app.i18n.lang, true)
    print('"lang" is: '..app.storage:getString('i18n.lang', ''))
    
    if (not ly.sys.isWIN) then
        --error in windows with timestamp!!!
        print('Test TIMESTAMP', ly.date.timestamp())
    end
    
    local iconInfo = {
        width = 40,
        height = 40,
        numFrames = 20,
        sheetContentWidth = 200,
        sheetContentHeight = 160
    }
    app.theme.icons = graphics.newImageSheet("test/images/ios7icons.png", iconInfo)
    
    -- move to main scene
    app.scene.gotoAsync("00", {time=600})
    
    app.splashClose()  
end

-- show splash screen
app.splashOpen()

-- init application
app.scene.register('00', 'test.page-00-menu')
app.scene.register('01', {name='test.page-01-test-edit', in_effect='slideLeft', out_effect='slideRight', params={id=123}})
app.scene.register('02', {name='test.page-02-test-panel', in_effect='slideLeft', out_effect='zoomOutInFade'})
app.scene.register('03', {name='test.page-03-test-listview', in_effect='slideLeft', out_effect='flip'})
app.scene.register('04', {name='test.page-04-test-net', in_effect='slideLeft', out_effect='slideRight'})
app.scene.register('05', {name='test.page-05-test-progress', in_effect='slideDown', out_effect='slideUp'})
app.scene.register('06', {name='test.page-06-test-crop', in_effect='slideDown', out_effect='slideUp'})
app.scene.register('07', {name='test.page-07-test-error', in_effect='slideDown', out_effect='slideUp'})
app.scene.register('08', {name='test.page-08-test-web', in_effect='slideDown', out_effect='slideUp'})
app.scene.register('09', {name='test.page-09-test-tabs', in_effect='slideDown', out_effect='slideUp'})
app.scene.register('10', {name='test.page-10-test-download', in_effect='slideDown', out_effect='slideUp'})

--error page
app.scene.register('99.0', {name='test.99-error.page-error'})
app.scene.register('99.1', {name='test.99-error.page-overlay', in_effect='slideLeft', in_time=500, out_effect='slideRight', out_time=500})

-- register error overlay
app.error.page('99.1')

--init LocalStorage
app.storage = LocalStorage:new({root="/test/", name="file.json"})

--init i18n with lang autodetection (lang='')
app.i18n = I18nManager:new({resources='/test/i18n/', lang=''})
app.i18n:regenerate()-- reload data from lua files

-- run
app.init(on_ready, {scene_history=true, theme={native=false}})

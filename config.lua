local aspectRatio = display.pixelHeight / display.pixelWidth
local device_width = aspectRatio > 1.5 and 320 or math.ceil( 480 / aspectRatio )
local device_height = aspectRatio < 1.5 and 480 or math.ceil( 320 * aspectRatio )
--local device_width = aspectRatio > 1.5 and 800 or math.floor( 1200 / aspectRatio )
--local device_height = aspectRatio < 1.5 and 1200 or math.floor( 800 * aspectRatio )

application = {
    content = {
        width = device_width,
        height = device_height, 
        scale = "letterBox",
        
        fps = 30,
        audioPlayFrequency = 22050,
        
        imageSuffix = {
            ["@2x"] = 1.5,
            ["@tall"] = 1.7,
            ["@4x"] = 3,
            ["@6x"] = 4.5,
        }
    },
    
    --[[
    -- Push notifications

    notification =
    {
        iphone =
        {
            types =
            {
                "badge", "sound", "alert", "newsstand"
            }
        }
    }
    --]]    
}
